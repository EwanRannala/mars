/* Simulation_globals.cpp
 *  Created on: 05/06/2022
 *      Author: Samuel Ewan Rannala
 */

#include "Simulation_globals.hpp"

namespace Sim{

    double Time=0.0;
    unsigned int NumLayers=0;
    unsigned int GrainsperLayer=0;
    std::vector<Grain_t> Grains;

}