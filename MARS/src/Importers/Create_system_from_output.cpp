/*
 * Create_system_from_output.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Create_system_from_output.cpp
 * \brief Function used to read in the required files to generated a complete granular system. */

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <istream>
#include <algorithm>

#include "Importers/Create_system_from_output.hpp"
#include "Simulation_globals.hpp"

/** Functions reads in all required data files and fills the required storage vectors.
 * \param[in] IN_data Struct containing filenames for all required input files.
 * \param[out] Structure Pointer to a structure data type.
 * \param[out] VORO Pointer to Voronoi data type.
 * \param[out] Materials Pointer to Materials data type.
 * \param[out] Grain Pointer to Grain data type.
 * \param[out] Int Pointer to Interaction data type.
 * \param[out] Solver Pointer to Solver class.
 *  */
int Read_in_system(const Data_input_t& IN_data, Structure_t& Structure, Voronoi_t& VORO,
                   Material_t& Materials, std::vector<Grain_t>& Grains, Interaction_t& Int, Solver_t& Solver){

    unsigned int Num_Grains=0;
    unsigned int Vert_num=0;
    unsigned int Neigh_num=0;
    unsigned int Mag_Neigh_num=0;
    unsigned int CL_Num=0;
    unsigned int Num_Layers=0;
    unsigned int W_num=0;
    unsigned int H_num=0;
    unsigned int grain=0;
    unsigned int total_grains=0;
    std::string placeholder_str;
    double placeholder_dbl=0.0;

    Log.logandshow(Logger_t::LogLvl::INFO,"Importing system data");

    // INPUTS
    std::ifstream INPUT_VORO_FILE     (("Input/System/"+IN_data.Voro_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_ST_MAT_FILE   (("Input/System/"+IN_data.St_Mat_file).c_str(),    std::ifstream::in);
    // Structure
    std::ifstream INPUT_POS_FILE      (("Input/System/"+IN_data.Pos_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_VERT_FILE     (("Input/System/"+IN_data.Vert_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_GEO_FILE      (("Input/System/"+IN_data.Geo_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_NEIGH_FILE    (("Input/System/"+IN_data.Neigh_file).c_str(),     std::ifstream::in);
    std::ifstream INPUT_AREA_FILE     (("Input/System/"+IN_data.Area_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_CL_FILE       (("Input/System/"+IN_data.CL_file).c_str(),        std::ifstream::in);
    // Grain parameters
    std::ifstream INPUT_GPV_FILE      (("Input/System/"+IN_data.GpV_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_GPC_FILE      (("Input/System/"+IN_data.GpC_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_GP_FILE       (("Input/System/"+IN_data.Gp_file).c_str(),        std::ifstream::in);
    std::ifstream INPUT_GPEQ_FILE     (("Input/System/"+IN_data.GpEQ_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_CHI_FILE      (("Input/System/"+IN_data.CHI_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_CHI_PERP_FILE (("Input/System/"+IN_data.CHI_PP_file).c_str(),    std::ifstream::in);
    std::ifstream INPUT_CHI_PARA_FILE (("Input/System/"+IN_data.CHI_PA_file).c_str(),    std::ifstream::in);
    std::ifstream INPUT_ALPHA_FILE    (("Input/System/"+IN_data.ALPHA_file).c_str(),     std::ifstream::in);
    // Interaction parameters
    std::ifstream INPUT_INT_MN_FILE   (("Input/System/"+IN_data.IpM_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_INT_EN_FILE   (("Input/System/"+IN_data.IpE_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_INT_W_FILE    (("Input/System/"+IN_data.IpW_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_INT_HEXC_FILE (("Input/System/"+IN_data.IpH_file).c_str(),       std::ifstream::in);

    // Check that all the required files have been opened
    if(!INPUT_VORO_FILE){     Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Voro_file      + " system input files");}
    if(!INPUT_ST_MAT_FILE){   Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.St_Mat_file    + " system input files");}
    if(!INPUT_POS_FILE){      Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Pos_file       + " system input files");}
    if(!INPUT_VERT_FILE){     Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Vert_file      + " system input files");}
    if(!INPUT_GEO_FILE){      Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Geo_file       + " system input files");}
    if(!INPUT_NEIGH_FILE){    Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Neigh_file     + " system input files");}
    if(!INPUT_AREA_FILE){     Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Area_file      + " system input files");}
    if(!INPUT_CL_FILE){       Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.CL_file        + " system input files");}
    if(!INPUT_GPV_FILE){      Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.GpV_file       + " system input files");}
    if(!INPUT_GPC_FILE){      Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.GpC_file       + " system input files");}
    if(!INPUT_GP_FILE){       Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.Gp_file        + " system input files");}
    if(!INPUT_GPEQ_FILE){     Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.GpEQ_file      + " system input files");}
    if(!INPUT_CHI_FILE){      Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.CHI_file       + " system input files");}
    if(!INPUT_CHI_PERP_FILE){ Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.CHI_PP_file    + " system input files");}
    if(!INPUT_CHI_PARA_FILE){ Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.CHI_PA_file    + " system input files");}
    if(!INPUT_ALPHA_FILE){    Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.ALPHA_file     + " system input files");}
    if(!INPUT_INT_MN_FILE){   Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.IpM_file       + " system input files");}
    if(!INPUT_INT_EN_FILE){   Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.IpE_file       + " system input files");}
    if(!INPUT_INT_W_FILE){    Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.IpW_file       + " system input files");}
    if(!INPUT_INT_HEXC_FILE){ Log.log(Logger_t::LogLvl::ERR,"Missing " + IN_data.IpH_file       + " system input files");}

//#####################################################################################//

    INPUT_VORO_FILE >> VORO.Centre_X >> VORO.Centre_Y
                    >> VORO.Vx_MAX >> VORO.Vy_MAX
                    >> VORO.Vx_MIN >> VORO.Vy_MIN
                    >> VORO.Input_grain_width >> VORO.Real_grain_width;
    Structure.Grain_width = VORO.Input_grain_width;

    INPUT_ST_MAT_FILE >> VORO.Num_Grains >> Structure.Num_layers;
    Num_Grains = VORO.Num_Grains;
    Num_Layers = Structure.Num_layers;
    Materials.dz.resize(Num_Layers);
    for(unsigned int LAYER=0;LAYER<Num_Layers;++LAYER){
        INPUT_ST_MAT_FILE >> Materials.dz[LAYER];
    }
    total_grains = Num_Grains*Num_Layers;
    Log.logandshow(Logger_t::LogLvl::INFO,"Tot grains = " + to_string_exact(total_grains));
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#################################IMPORTING STRUCTURE#################################//
    // Positions
    VORO.Pos_X_final.resize(Num_Grains);
    VORO.Pos_Y_final.resize(Num_Grains);
    for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_POS_FILE >> VORO.Pos_X_final[grain_in_layer] >> VORO.Pos_Y_final[grain_in_layer];
    }
    // Vertices
    VORO.Vertex_X_final.resize(Num_Grains);
    VORO.Vertex_Y_final.resize(Num_Grains);
    for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_VERT_FILE >> grain >> Vert_num;
        VORO.Vertex_X_final[grain_in_layer].resize(Vert_num);
        VORO.Vertex_Y_final[grain_in_layer].resize(Vert_num);
        for(unsigned int vert=0;vert<Vert_num;++vert){
            INPUT_VERT_FILE >> VORO.Vertex_X_final[grain_in_layer][vert] >> VORO.Vertex_Y_final[grain_in_layer][vert];
        }
        // Clear line with repeated initial vertex
        INPUT_VERT_FILE >> placeholder_dbl >> placeholder_dbl;
    }
    // Geo centre
    VORO.Geo_grain_centre_X.resize(Num_Grains);
    VORO.Geo_grain_centre_Y.resize(Num_Grains);
    for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_GEO_FILE >> VORO.Geo_grain_centre_X[grain_in_layer] >> VORO.Geo_grain_centre_Y[grain_in_layer];
    }
    // Neighbours
    VORO.Neighbour_final.resize(Num_Grains);
    for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_NEIGH_FILE >> grain >> Neigh_num;
        VORO.Neighbour_final[grain_in_layer].resize(Neigh_num);
        for(unsigned int neigh=0;neigh<Neigh_num;++neigh){
            INPUT_NEIGH_FILE >> VORO.Neighbour_final[grain_in_layer][neigh];
        }
    }
    // Areas
    VORO.Grain_Area.resize(Num_Grains);
    for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_AREA_FILE >> VORO.Grain_Area[grain_in_layer];
    }
    // Contact length
    VORO.Contact_lengths.resize(Num_Grains);
    for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_CL_FILE >> grain >> CL_Num;
        VORO.Contact_lengths[grain_in_layer].resize(CL_Num);
        for(unsigned int CL=0;CL<CL_Num;++CL){
            INPUT_CL_FILE >> VORO.Contact_lengths[grain_in_layer][CL];
        }
    }
    // Create sorted vector of grains as this is required for the inclusion zone
    VORO.Grain_ordered_by_Xpos.resize(VORO.Pos_X_final.size());
    std::vector<std::pair<unsigned int,double>> zipped(VORO.Pos_X_final.size());
    for(size_t i=0;i<VORO.Pos_X_final.size();++i){
        zipped[i] = std::make_pair(i,VORO.Pos_X_final[i]);
    }

    std::sort(std::begin(zipped), std::end(zipped),
            [&](const std::pair<unsigned int,double>& a, const std::pair<unsigned int,double>& b){
                return a.second < b.second;
            }
    );

    for(size_t i=0;i<zipped.size();++i){
        VORO.Grain_ordered_by_Xpos[i] = zipped[i].first;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################################INTERACTIONS#####################################//


    // Magnetostatic neighbours
    INPUT_INT_MN_FILE >> Int.Method;
    if(Int.Method == "segment" || Int.Method == "coarsesegment"){
        Int.Magneto_neigh_list_grains.resize(total_grains);
        Int.Magneto_neigh_list_subseg.resize(total_grains);
        Int.Magneto_neigh_list_seg.resize(total_grains);
        for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){

            INPUT_INT_MN_FILE >> grain;
            INPUT_INT_MN_FILE >> Mag_Neigh_num;
            Int.Magneto_neigh_list_grains[grain_in_system].resize(Mag_Neigh_num);
            INPUT_INT_MN_FILE >> Mag_Neigh_num;
            Int.Magneto_neigh_list_subseg[grain_in_system].resize(Mag_Neigh_num);
            INPUT_INT_MN_FILE >> Mag_Neigh_num;
            Int.Magneto_neigh_list_seg[grain_in_system].resize(Mag_Neigh_num);

            for(unsigned int & Mgrain : Int.Magneto_neigh_list_grains[grain_in_system]){
                INPUT_INT_MN_FILE >> Mgrain;
            }
            for(unsigned int & Msseg : Int.Magneto_neigh_list_subseg[grain_in_system]){
                INPUT_INT_MN_FILE >> Msseg;
            }
            for(unsigned int & Mseg : Int.Magneto_neigh_list_seg[grain_in_system]){
                INPUT_INT_MN_FILE >> Mseg;
            }
        }
        INPUT_INT_MN_FILE >> Mag_Neigh_num;
        Int.segments.resize(Mag_Neigh_num);
        INPUT_INT_MN_FILE >> Mag_Neigh_num;
        Int.subsegments.resize(Mag_Neigh_num);

        for(std::vector<unsigned int> & seg : Int.segments){
            INPUT_INT_MN_FILE >> Mag_Neigh_num;
            seg.resize(Mag_Neigh_num);
            for(unsigned int & sseg : seg){
                INPUT_INT_MN_FILE >> sseg;
            }
        }
        for(std::vector<unsigned int> & sseg : Int.subsegments){
            INPUT_INT_MN_FILE >> Mag_Neigh_num;
            sseg.resize(Mag_Neigh_num);
            for(unsigned int & grain : sseg){
                INPUT_INT_MN_FILE >> grain;
            }
        }
    }
    else if(Int.Method == "truncate" || Int.Method == "exact"){
        Int.Magneto_neigh_list_grains.resize(total_grains);
        Int.Magneto_neigh_list_subseg.resize(0);
        Int.Magneto_neigh_list_seg.resize(0);
        for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
            INPUT_INT_MN_FILE >> grain >> Mag_Neigh_num;
            Int.Magneto_neigh_list_grains[grain_in_system].resize(Mag_Neigh_num);
            for(unsigned int Mneigh=0;Mneigh<Mag_Neigh_num;++Mneigh){
                INPUT_INT_MN_FILE >> Int.Magneto_neigh_list_grains[grain_in_system][Mneigh];
            }
            // Blank line in input
        }
    }



    Int.Exchange_neigh_list.resize(total_grains);
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        INPUT_INT_EN_FILE >> grain >> Neigh_num;
        Int.Exchange_neigh_list[grain_in_system].resize(Neigh_num);
        for(unsigned int neigh=0;neigh<Neigh_num;++neigh){
            INPUT_INT_EN_FILE >> Int.Exchange_neigh_list[grain_in_system][neigh];
        }
    }
    Int.Wxx.resize(total_grains); Int.Wxy.resize(total_grains);
    Int.Wxz.resize(total_grains); Int.Wyy.resize(total_grains);
    Int.Wyz.resize(total_grains); Int.Wzz.resize(total_grains);
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        INPUT_INT_W_FILE >> grain >> W_num;
        Int.Wxx[grain_in_system].resize(W_num); Int.Wxy[grain_in_system].resize(W_num);
        Int.Wxz[grain_in_system].resize(W_num); Int.Wyy[grain_in_system].resize(W_num);
        Int.Wyz[grain_in_system].resize(W_num); Int.Wzz[grain_in_system].resize(W_num);
        for(unsigned int W=0;W<W_num;++W){
            INPUT_INT_W_FILE >> Int.Wxx[grain_in_system][W] >> Int.Wxy[grain_in_system][W]
                             >> Int.Wxz[grain_in_system][W] >> Int.Wyy[grain_in_system][W]
                             >> Int.Wyz[grain_in_system][W] >> Int.Wzz[grain_in_system][W];
        }
    }
    Int.H_exch_str.resize(total_grains);
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        INPUT_INT_HEXC_FILE >> grain >> H_num;
        Int.H_exch_str[grain_in_system].resize(H_num);
        for(unsigned int H=0;H<H_num;++H){
            INPUT_INT_HEXC_FILE >> Int.H_exch_str[grain_in_system][H];
        }
    }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################READ IN GRAIN PARAMETERS###############################//
    Grains.resize(total_grains);

    // Magnetisation and Easy axis
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        INPUT_GPV_FILE >> Grains[grain_in_system].m.x >> Grains[grain_in_system].m.y >> Grains[grain_in_system].m.z
                      >> Grains[grain_in_system].Easy_axis.x >> Grains[grain_in_system].Easy_axis.y >> Grains[grain_in_system].Easy_axis.z
                      >> Grains[grain_in_system].H_appl.x >> Grains[grain_in_system].H_appl.y >> Grains[grain_in_system].H_appl.z;
    }
    // Temp, Vol, K, Tc, Ms
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        INPUT_GP_FILE >> Grains[grain_in_system].Temp >> Grains[grain_in_system].diameter
                      >> Grains[grain_in_system].Vol >> Grains[grain_in_system].K
                      >> Grains[grain_in_system].Tc >> Grains[grain_in_system].Ms;
        // Convert volume from CC to nm^3
        Grains[grain_in_system].Vol *= 1.0e21;
    }
    // Layer Damping
    // Empty damping before importing new values
    Solver.empty_Alpha();
    for(unsigned int LAYER=0;LAYER<Num_Layers;++LAYER){
        double Alpha=0.0;
        INPUT_ALPHA_FILE >> Alpha;
        Solver.set_Alpha(Alpha);
    }
    // mEQ
    for(Grain_t & Grain : Grains){
        Grain.Crit_exp = 0.0;
        Grain.a0_mEQ   = 0.0;
        Grain.a1_mEQ   = 0.0;
        Grain.a2_mEQ   = 0.0;
        Grain.a3_mEQ   = 0.0;
        Grain.a4_mEQ   = 0.0;
        Grain.a5_mEQ   = 0.0;
        Grain.a6_mEQ   = 0.0;
        Grain.a7_mEQ   = 0.0;
        Grain.a8_mEQ   = 0.0;
        Grain.a9_mEQ   = 0.0;
        Grain.a1_2_mEQ = 0.0;
        Grain.b1_mEQ   = 0.0;
        Grain.b2_mEQ   = 0.0;
    }
    for(unsigned int LAYER=0;LAYER<Num_Layers;++LAYER){
        unsigned int Offset = Num_Grains*LAYER;
        int mEQType=0;
        INPUT_GPEQ_FILE >> mEQType; // This currently only works for a single layer (needs to switch files)
        if(mEQType==1){
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                Grains[grain_in_system].mEQ_Type=static_cast<Material_t::mEQTypes>(mEQType);
                INPUT_GPEQ_FILE >> Grains[grain_in_system].Crit_exp;
            }
        }
        // What about the fact it is different files PER layer?
        else if(mEQType==2){
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                Grains[grain_in_system].mEQ_Type=static_cast<Material_t::mEQTypes>(mEQType);
                INPUT_GPEQ_FILE >> Grains[grain_in_system].a0_mEQ >> Grains[grain_in_system].a1_mEQ
                                >> Grains[grain_in_system].a2_mEQ >> Grains[grain_in_system].a3_mEQ
                                >> Grains[grain_in_system].a4_mEQ >> Grains[grain_in_system].a5_mEQ
                                >> Grains[grain_in_system].a6_mEQ >> Grains[grain_in_system].a7_mEQ
                                >> Grains[grain_in_system].a8_mEQ >> Grains[grain_in_system].a9_mEQ
                                >> Grains[grain_in_system].a1_2_mEQ
                                >> Grains[grain_in_system].b1_mEQ >> Grains[grain_in_system].b2_mEQ;
            }
        }
        else{Log.log(Logger_t::LogLvl::ERR,"Unknown mEQ type");}
    } // end of meq
    // CHI: First empty suceptibilities factors vectors for safety, then fill them
    Solver.empty_SusType();
    Solver.empty_ChiScaling();
    Solver.empty_ChiParaFit();
    Solver.empty_ChiPerpFit();
    for(unsigned int LAYER=0;LAYER<Num_Layers;++LAYER){
        unsigned int Offset = Num_Grains*LAYER;
        int Ani_method=0;
        int Sus_type=0;
        double ChiScaling=0.0;
        INPUT_CHI_FILE >> Ani_method;
        std::cout << Ani_method << std::endl;
        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
            unsigned int grain_in_system = Offset+grain_in_layer;
            if(Ani_method==1 || Ani_method==2){
                Grains[grain_in_system].Ani_method=static_cast<Material_t::AniMethods>(Ani_method);
            }
            else{
                Log.log(Logger_t::LogLvl::ERR,"Unknown Aniostropy method");
            }
        }
        INPUT_CHI_FILE >> Sus_type >> ChiScaling;
        Solver.set_SusType(Sus_type);
        Solver.set_ChiScaling(ChiScaling);
          double a0=0.0;
          double a1=0.0;
          double a2=0.0;
          double a3=0.0;
          double a4=0.0;
          double a5=0.0;
          double a6=0.0;
          double a7=0.0;
          double a8=0.0;
          double a9=0.0;
          double a1_2=0.0;
          double b0=0.0;
          double b1=0.0;
          double b2=0.0;
          double b3=0.0;
          double b4=0.0;
          INPUT_CHI_FILE >> a0 >> a1 >> a2 >> a3 >> a4 >> a5 >> a6 >> a7 >> a8 >> a9
                              >> a1_2 >> b0 >> b1 >> b2 >> b3 >> b4;
          Solver.set_ChiParaFit(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a1_2, b0, b1, b2, b3, b4);
        if(Grains[Offset].Ani_method == 2){ //chi
              INPUT_CHI_FILE >> a0 >> a1 >> a2 >> a3 >> a4 >> a5 >> a6 >> a7 >> a8 >> a9
                                  >> a1_2 >> b0 >> b1 >> b2 >> b3 >> b4;
              Solver.set_ChiPerpFit(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a1_2, b0, b1, b2, b3, b4);
        }
        // set Chi_perp =1 if callen
        else if(Grains[Offset].Ani_method == 1){ //callen
              Solver.set_ChiPerpFit(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0);
        }
        else{Log.log(Logger_t::LogLvl::ERR,"Unknown Anisotropy type");}
    } // end of Chi
    Solver.EmptyChiIdv();
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){

        std::vector<double> PERP;
        std::vector<double> PARA;
        double tmp=0.0;
        for(unsigned i=0;i<16;++i){
            INPUT_CHI_PERP_FILE >> tmp;
            PERP.push_back(tmp);
        }
        Solver.set_ChiPerpFitIdv(PERP);
        for(unsigned i=0;i<16;++i){
            INPUT_CHI_PARA_FILE >> tmp;
            PARA.push_back(tmp);
        }
        Solver.set_ChiParaFitIdv(PARA);
    }

    // Callen_powers
    for(unsigned int LAYER=0;LAYER<Num_Layers;++LAYER){
        unsigned int Offset = Num_Grains*LAYER;
        int Callen_power_range=0;
        INPUT_GPC_FILE >> Callen_power_range;

        if(Callen_power_range==1){
            for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                INPUT_GPC_FILE >> Grains[grain_in_system].Callen_power;
                Grains[grain_in_system].Callen_factor_lowT =(1.000);
                Grains[grain_in_system].Callen_factor_midT =(1.000);
                Grains[grain_in_system].Callen_factor_highT=(1.000);
                Grains[grain_in_system].Callen_power_lowT  =Grains[grain_in_system].Callen_power;
                Grains[grain_in_system].Callen_power_midT  =Grains[grain_in_system].Callen_power;
                Grains[grain_in_system].Callen_power_highT =Grains[grain_in_system].Callen_power;
                Grains[grain_in_system].Callen_range_lowT  =(1600.0);
                Grains[grain_in_system].Callen_range_midT  =(2000.0);
                Grains[grain_in_system].Callen_power_range = static_cast<Material_t::CallenMethods>(Callen_power_range);
            }
        }
        else if (Callen_power_range==2){
            for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                INPUT_GPC_FILE  >> Grains[grain_in_system].Callen_factor_lowT  >> Grains[grain_in_system].Callen_power_lowT
                                >> Grains[grain_in_system].Callen_range_lowT   >> Grains[grain_in_system].Callen_factor_midT
                                >> Grains[grain_in_system].Callen_power_midT   >> Grains[grain_in_system].Callen_range_midT
                                >> Grains[grain_in_system].Callen_factor_highT >> Grains[grain_in_system].Callen_power_highT;
                Grains[grain_in_system].Callen_power=0.0;
                Grains[grain_in_system].Callen_power_range = static_cast<Material_t::CallenMethods>(Callen_power_range);
            }
        }
        else{Log.log(Logger_t::LogLvl::ERR,"Unknown Callen power_range");}
    } // end of Anisotropy

    Sim::NumLayers = Structure.Num_layers;
    Sim::GrainsperLayer = VORO.Num_Grains;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    INPUT_VORO_FILE.close();        INPUT_ST_MAT_FILE.close();
    INPUT_ALPHA_FILE.close();       INPUT_POS_FILE.close();
    INPUT_VERT_FILE.close();        INPUT_GEO_FILE.close();
    INPUT_NEIGH_FILE.close();       INPUT_AREA_FILE.close();
    INPUT_CL_FILE.close();          INPUT_GPV_FILE.close();
    INPUT_GPC_FILE.close();         INPUT_GP_FILE.close();
    INPUT_GPEQ_FILE.close();        INPUT_CHI_FILE.close();
    INPUT_INT_MN_FILE.close();      INPUT_INT_EN_FILE.close();
    INPUT_INT_W_FILE.close();       INPUT_INT_HEXC_FILE.close();

    return 0;
}
