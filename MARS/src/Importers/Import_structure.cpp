/*
 * Import_structure.cpp
 *
 *  Created on: 11 Mar 2021
 *      Author: Ewan Rannala
 */

/** \file Import_structure.cpp
 * \brief Function used to read in the required files to generated a granular structure. */

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <istream>

#include "Importers/Import_structure.hpp"

/** Functions reads in all required data files and fills the required storage vectors.
 * \param[in] IN_data Struct containing filenames for all required input files.
 * \param[out] Structure Pointer to a structure data type.
 * \param[out] VORO Pointer to Voronoi data type.
 *  */
int Read_in_structure(const Data_input_t IN_data, Structure_t& Structure, Voronoi_t& VORO){

    int Num_Grains, Vert_num, Neigh_num,
        Mag_Neigh_num, CL_Num, Num_Layers;
    int grain, total_grains;
    std::string placeholder_str;
    double placeholder_dbl;

    // INPUTS
    std::ifstream INPUT_VORO_FILE     (("Input/System/"+IN_data.Voro_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_ST_MAT_FILE   (("Input/System/"+IN_data.St_Mat_file).c_str(),    std::ifstream::in);
    // Structure
    std::ifstream INPUT_POS_FILE      (("Input/System/"+IN_data.Pos_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_VERT_FILE     (("Input/System/"+IN_data.Vert_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_GEO_FILE      (("Input/System/"+IN_data.Geo_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_NEIGH_FILE    (("Input/System/"+IN_data.Neigh_file).c_str(),     std::ifstream::in);
    std::ifstream INPUT_MAGNEIGH_FILE (("Input/System/"+IN_data.Mag_neigh_file).c_str(), std::ifstream::in);
    std::ifstream INPUT_AREA_FILE     (("Input/System/"+IN_data.Area_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_CL_FILE       (("Input/System/"+IN_data.CL_file).c_str(),        std::ifstream::in);
    // Grain parameters
    std::ifstream INPUT_GPV_FILE      (("Input/System/"+IN_data.GpV_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_GPC_FILE      (("Input/System/"+IN_data.GpC_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_GP_FILE       (("Input/System/"+IN_data.Gp_file).c_str(),        std::ifstream::in);
    std::ifstream INPUT_GPEQ_FILE     (("Input/System/"+IN_data.GpEQ_file).c_str(),      std::ifstream::in);
    std::ifstream INPUT_CHI_FILE      (("Input/System/"+IN_data.CHI_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_ALPHA_FILE    (("Input/System/"+IN_data.ALPHA_file).c_str(),     std::ifstream::in);
    // Interaction parameters
    std::ifstream INPUT_INT_MN_FILE   (("Input/System/"+IN_data.IpM_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_INT_EN_FILE   (("Input/System/"+IN_data.IpE_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_INT_W_FILE    (("Input/System/"+IN_data.IpW_file).c_str(),       std::ifstream::in);
    std::ifstream INPUT_INT_HEXC_FILE (("Input/System/"+IN_data.IpH_file).c_str(),       std::ifstream::in);


    // Check that all the required files have been opened
    if(!INPUT_VORO_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.Voro_file      + " system input files\n");}
    if(!INPUT_ST_MAT_FILE){  throw std::runtime_error("ERROR: Missing " + IN_data.St_Mat_file    + " system input files\n");}
    if(!INPUT_POS_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.Pos_file       + " system input files\n");}
    if(!INPUT_VERT_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.Vert_file      + " system input files\n");}
    if(!INPUT_GEO_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.Geo_file       + " system input files\n");}
    if(!INPUT_NEIGH_FILE){   throw std::runtime_error("ERROR: Missing " + IN_data.Neigh_file     + " system input files\n");}
    if(!INPUT_MAGNEIGH_FILE){throw std::runtime_error("ERROR: Missing " + IN_data.Mag_neigh_file + " system input files\n");}
    if(!INPUT_AREA_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.Area_file      + " system input files\n");}
    if(!INPUT_CL_FILE){      throw std::runtime_error("ERROR: Missing " + IN_data.CL_file        + " system input files\n");}
    if(!INPUT_GPV_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.GpV_file       + " system input files\n");}
    if(!INPUT_GPC_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.GpC_file       + " system input files\n");}
    if(!INPUT_GP_FILE){      throw std::runtime_error("ERROR: Missing " + IN_data.Gp_file        + " system input files\n");}
    if(!INPUT_GPEQ_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.GpEQ_file      + " system input files\n");}
    if(!INPUT_CHI_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.CHI_file       + " system input files\n");}
    if(!INPUT_ALPHA_FILE){   throw std::runtime_error("ERROR: Missing " + IN_data.ALPHA_file     + " system input files\n");}
    if(!INPUT_INT_MN_FILE){  throw std::runtime_error("ERROR: Missing " + IN_data.IpM_file       + " system input files\n");}
    if(!INPUT_INT_EN_FILE){  throw std::runtime_error("ERROR: Missing " + IN_data.IpE_file       + " system input files\n");}
    if(!INPUT_INT_W_FILE){   throw std::runtime_error("ERROR: Missing " + IN_data.IpW_file       + " system input files\n");}
    if(!INPUT_INT_HEXC_FILE){throw std::runtime_error("ERROR: Missing " + IN_data.IpH_file       + " system input files\n");}

//#####################################################################################//

    INPUT_VORO_FILE >> VORO.Centre_X >> VORO.Centre_Y
                    >> VORO.Vx_MAX >> VORO.Vy_MAX
                    >> VORO.Vx_MIN >> VORO.Vy_MIN
                    >> VORO.Input_grain_width >> VORO.Real_grain_width
                    >> VORO.obtainedMean >> VORO.obtainedStdDev
                    >> VORO.Average_contact_length >> VORO.Average_area;
    Structure.Grain_width = VORO.Input_grain_width;

    INPUT_ST_MAT_FILE >> VORO.Num_Grains >> Structure.Num_layers;
    Num_Grains = VORO.Num_Grains;
    Num_Layers = Structure.Num_layers;
    for(int LAYER=0;LAYER<Num_Layers;++LAYER){
        double tmp;
        INPUT_ST_MAT_FILE >> tmp;
    }
    total_grains = Num_Grains*Num_Layers;
    std::cout << "Tot grains = " << total_grains << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#################################IMPORTING STRUCTURE#################################//
    // Positions
    VORO.Pos_X_final.resize(Num_Grains);
    VORO.Pos_Y_final.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_POS_FILE >> VORO.Pos_X_final[grain_in_layer] >> VORO.Pos_Y_final[grain_in_layer];
    }
    // Vertices
    VORO.Vertex_X_final.resize(Num_Grains);
    VORO.Vertex_Y_final.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_VERT_FILE >> grain >> Vert_num;
        VORO.Vertex_X_final[grain_in_layer].resize(Vert_num);
        VORO.Vertex_Y_final[grain_in_layer].resize(Vert_num);
        for(int vert=0;vert<Vert_num;++vert){
            INPUT_VERT_FILE >> VORO.Vertex_X_final[grain_in_layer][vert] >> VORO.Vertex_Y_final[grain_in_layer][vert];
        }
        // Clear line with repeated initial vertex
        INPUT_VERT_FILE >> placeholder_dbl >> placeholder_dbl;
    }
    // Geo centre
    VORO.Geo_grain_centre_X.resize(Num_Grains);
    VORO.Geo_grain_centre_Y.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_GEO_FILE >> VORO.Geo_grain_centre_X[grain_in_layer] >> VORO.Geo_grain_centre_Y[grain_in_layer];
    }
    // Neighbours
    VORO.Neighbour_final.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_NEIGH_FILE >> grain >> Neigh_num;
        VORO.Neighbour_final[grain_in_layer].resize(Neigh_num);
        for(int neigh=0;neigh<Neigh_num;++neigh){
            INPUT_NEIGH_FILE >> VORO.Neighbour_final[grain_in_layer][neigh];
        }
    }
    // Magnetostatic neighbours
    VORO.Magnetostatic_neighbours.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_MAGNEIGH_FILE >> grain >> Mag_Neigh_num;
        VORO.Magnetostatic_neighbours[grain_in_layer].resize(Mag_Neigh_num);
        for(int Mneigh=0;Mneigh<Mag_Neigh_num;++Mneigh){
            INPUT_MAGNEIGH_FILE >> VORO.Magnetostatic_neighbours[grain_in_layer][Mneigh];
        }
    }
    // Areas
    VORO.Grain_Area.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_AREA_FILE >> VORO.Grain_Area[grain_in_layer];
    }
    // Contact length
    VORO.Contact_lengths.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        INPUT_CL_FILE >> grain >> CL_Num;
        VORO.Contact_lengths[grain_in_layer].resize(CL_Num);
        for(int CL=0;CL<CL_Num;++CL){
            INPUT_CL_FILE >> VORO.Contact_lengths[grain_in_layer][CL];
        }
    }
    // Diameters
    VORO.Grain_diameter.resize(Num_Grains);
    for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
        double tmp;
        INPUT_GP_FILE >> tmp >> VORO.Grain_diameter[grain_in_layer]
                      >> tmp >> tmp
                      >> tmp >> tmp;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    INPUT_VORO_FILE.close();        INPUT_ST_MAT_FILE.close(); 		INPUT_ALPHA_FILE.close();
    INPUT_POS_FILE.close();         INPUT_VERT_FILE.close();
    INPUT_GEO_FILE.close();         INPUT_NEIGH_FILE.close();
    INPUT_MAGNEIGH_FILE.close();    INPUT_AREA_FILE.close();
    INPUT_CL_FILE.close();          INPUT_GPV_FILE.close();
    INPUT_GPC_FILE.close();         INPUT_GP_FILE.close();
    INPUT_GPEQ_FILE.close();        INPUT_CHI_FILE.close();
    INPUT_INT_MN_FILE.close();      INPUT_INT_EN_FILE.close();
    INPUT_INT_W_FILE.close();       INPUT_INT_HEXC_FILE.close();

    return 0;
}
