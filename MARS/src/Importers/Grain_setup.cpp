/* Grain_setup.cpp
 *  Created on: 16 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** @file Grain_setup.cpp
 * @brief Function to define all grain data using mainly the material data structure. */

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <random>

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Distributions_generator.hpp"
#include "Globals.hpp"
#include "Io/Logger/Global_logger.hpp"

// TODO - Remove legacy Grain_setup

int Grain_setup(const unsigned int Num_grains,const unsigned int Num_Layers,
                const Material_t& Material, const std::vector<double>& Grain_area,
                const std::vector<double>& Grain_diameter, const double Temperature, std::vector<Grain_t>& Grains){

    Log.logandshow(Logger_t::LogLvl::INFO,"Assigning grain data...");
    unsigned int Total_grains = Num_grains * Num_Layers;
    double phi=0.0;
    double theta=0.0;
    double EAx=0.0;
    double EAy=0.0;
    double EAz=0.0;
    double EAx2=0.0;
    double EAy2=0.0;
    double EAz2=0.0;

    // Set number of grains
    Grains.clear();
    Grains.resize(Total_grains);

    /* NEED TO SET UP ANISOTROPY DISPERSION
     *         NEED TO ENSURE LIMIT IS [0:180) -> IF <0 COUNT BACK FROM 180 | IF >180 COUNT FORWARD FROM 180.
     *         GAUSSIAN DISTRIBUTION FOR POLAR ANGLE.
     *         NORMAL DISTRIBUTION FOR AZIMUTHAL ANGLE.
     *
     *         1. GENERATE UNIFORM DISTRIBUTION IN AZIMUTHAL ANGLE
     *                 std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
     *                 phi = 2.0*PI*uniformPHI;
     *         2. GENERATE GAUSSIAN DISTRIBUTION IN POLAR ANGLE [0:180)
     *                 deg*pi/180 = rad
     *                 Input 3 --> Input/180 should give correct upper limit for distribution.
     *                 std::uniform_real_distribution<double> uniformTHETA(0.0, Input/180.0);
     *                 theta = acos(1.0-2.0*uniformTHETA);
     *         3. CONVERT POLAR COORDINATES TO CARTESIAN COORDINATES
     *                 x = sin(theta)*cos(phi);
     *                 y = sin(theta)*sin(phi);
     *                 z = cos(phi);
     */


    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Layer*Num_grains;
        // Polar and azimuthal angle generator - different per layer.

        std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
        std::uniform_real_distribution<double> uniformTHETA(0.0, Material.Anis_angle[Layer]/180.0);
        std::normal_distribution<double> Norm(0.0,1.0);

        for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
            unsigned int Grain_in_system = Grain_in_Layer+Offset;
    //================== initial Magnetisation ==================//
            if(Material.Mag_Type[Layer]=="assigned"){
                Grains[Grain_in_system].m.x=Material.Initial_mag[Layer].x;
                Grains[Grain_in_system].m.y=Material.Initial_mag[Layer].y;
                Grains[Grain_in_system].m.z=Material.Initial_mag[Layer].z;
            }
            else if(Material.Mag_Type[Layer]=="random"){
                double X=Norm(Gen);
                double Y=Norm(Gen);
                double Z=Norm(Gen);
                double MAG = sqrt(X*X+Y*Y+Z*Z);
                // Normalise the magnetisation
                X /= MAG;
                Y /= MAG;
                Z /= MAG;
                Grains[Grain_in_system].m.x = X;
                Grains[Grain_in_system].m.y = Y;
                Grains[Grain_in_system].m.z = Z;
            }
    //=========================== Anisotropy direction ===========================//
            // Generate dispersion in anisotropy direction
            phi = M_2PI*uniformPHI(Gen);
            theta = acos(1.0-2.0*uniformTHETA(Gen));
            EAx=sin(theta)*cos(phi);
            EAy=sin(theta)*sin(phi);
            EAz=cos(theta);
            // Rotate easy axes to match desired initial direction
            EAx2 =  EAx*cos(Material.Easy_axis_polar[Layer]) + EAz*sin(Material.Easy_axis_polar[Layer]);
            EAy2 =  EAy;
            EAz2 = -EAx*sin(Material.Easy_axis_polar[Layer]) + EAz*cos(Material.Easy_axis_polar[Layer]);
            // Azimuthal rotation.
            Grains[Grain_in_system].Easy_axis.x = EAx2*cos(Material.Easy_axis_azimuth[Layer]) - EAy2*sin(Material.Easy_axis_azimuth[Layer]);
            Grains[Grain_in_system].Easy_axis.y = EAx2*sin(Material.Easy_axis_azimuth[Layer]) + EAy2*cos(Material.Easy_axis_azimuth[Layer]);
            Grains[Grain_in_system].Easy_axis.z = EAz2;
            Grains[Grain_in_system].Temp        = Temperature;
            Grains[Grain_in_system].diameter    = Grain_diameter[Grain_in_Layer];
            Grains[Grain_in_system].Vol         = Grain_area[Grain_in_Layer]*Material.dz[Layer];
            Grains[Grain_in_system].Ms          = Material.Ms[Layer];
            //================== Anisotropy T dependence ==================//
            Grains[Grain_in_system].Ani_method          = Material.Ani_method[Layer];
            Grains[Grain_in_system].Callen_power_range  = Material.Callen_power_range[Layer];
            Grains[Grain_in_system].Callen_power        = Material.Callen_power[Layer];
            Grains[Grain_in_system].Callen_factor_lowT  = Material.Callen_factor_lowT[Layer];
            Grains[Grain_in_system].Callen_factor_midT  = Material.Callen_factor_midT[Layer];
            Grains[Grain_in_system].Callen_factor_highT = Material.Callen_factor_highT[Layer];
            Grains[Grain_in_system].Callen_power_lowT   = Material.Callen_power_lowT[Layer];
            Grains[Grain_in_system].Callen_power_midT   = Material.Callen_power_midT[Layer];
            Grains[Grain_in_system].Callen_power_highT  = Material.Callen_power_highT[Layer];
            Grains[Grain_in_system].Callen_range_lowT   = Material.Callen_range_lowT[Layer];
            Grains[Grain_in_system].Callen_range_midT   = Material.Callen_range_midT[Layer];
    //================== Equilibrium magnetisation T dependence ==================//
            Grains[Grain_in_system].Crit_exp = Material.Crit_exp[Layer];
            Grains[Grain_in_system].mEQ_Type = Material.mEQ_Type[Layer];
            Grains[Grain_in_system].a0_mEQ   = Material.a0_mEQ[Layer];
            Grains[Grain_in_system].a1_mEQ   = Material.a1_mEQ[Layer];
            Grains[Grain_in_system].a2_mEQ   = Material.a2_mEQ[Layer];
            Grains[Grain_in_system].a3_mEQ   = Material.a3_mEQ[Layer];
            Grains[Grain_in_system].a4_mEQ   = Material.a4_mEQ[Layer];
            Grains[Grain_in_system].a5_mEQ   = Material.a5_mEQ[Layer];
            Grains[Grain_in_system].a6_mEQ   = Material.a6_mEQ[Layer];
            Grains[Grain_in_system].a7_mEQ   = Material.a7_mEQ[Layer];
            Grains[Grain_in_system].a8_mEQ   = Material.a8_mEQ[Layer];
            Grains[Grain_in_system].a9_mEQ   = Material.a9_mEQ[Layer];
            Grains[Grain_in_system].a1_2_mEQ = Material.a1_2_mEQ[Layer];
            Grains[Grain_in_system].b1_mEQ   = Material.b1_mEQ[Layer];
            Grains[Grain_in_system].b2_mEQ   = Material.b2_mEQ[Layer];
        }
    //================== Curie Temperature and Anisotropy distribution generators ==================//
        // Create distribution of Tc for grains in this layer.
        if(Material.Tc_Dist_Type[Layer]=="d"){
            double nu=Material.Tc_Ddist_nu[Layer];
            double d0=Material.Tc_Ddist_d0[Layer];
            double lambda = 1.0/nu;
            for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
                unsigned int Grain_in_system = Grain_in_Layer+Offset;
                // d0 and Grain diameter are both in [nm]
                Grains[Grain_in_system].Tc = Material.Tc_inf[Layer]*(1.0-pow((d0/(Grains[Grain_in_system].diameter)),lambda));
            }
        }
        else{
            std::vector<double> TcTmp;
            Dist_gen(Num_grains,Material.Tc_Dist_Type[Layer],Material.Avg_Tc[Layer],Material.StdDev_Tc[Layer],&TcTmp);
            for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
                unsigned int Grain_in_system = Grain_in_Layer+Offset;
                Grains[Grain_in_system].Tc = TcTmp[Grain_in_Layer];
            }
        }
        // Create distribution of K for grains in this layer.
        std::vector<double> KTmp;
        Dist_gen(Num_grains,Material.K_Dist_Type[Layer],Material.Avg_K[Layer],Material.StdDev_K[Layer],&KTmp);
        for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
            unsigned int Grain_in_system = Grain_in_Layer+Offset;
            Grains[Grain_in_system].K = KTmp[Grain_in_Layer];
        }
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"Complete.");
    return 0;
}

int Grain_setup(const unsigned int Num_Layers, const Voronoi_t& Voro,
                const Material_t& Material, const double Temperature, std::vector<Grain_t>& Grains){

    Log.logandshow(Logger_t::LogLvl::INFO,"Assigning grain data...");
    unsigned int Total_grains = Voro.Num_Grains * Num_Layers;
    double phi=0.0;
    double theta=0.0;
    double EAx=0.0;
    double EAy=0.0;
    double EAz=0.0;
    double EAx2=0.0;
    double EAy2=0.0;
    double EAz2=0.0;

    // Set number of grains
    Grains.clear();
    Grains.resize(Total_grains);

    /* NEED TO SET UP ANISOTROPY DISPERSION
     *         NEED TO ENSURE LIMIT IS [0:180) -> IF <0 COUNT BACK FROM 180 | IF >180 COUNT FORWARD FROM 180.
     *         GAUSSIAN DISTRIBUTION FOR POLAR ANGLE.
     *         NORMAL DISTRIBUTION FOR AZIMUTHAL ANGLE.
     *
     *         1. GENERATE UNIFORM DISTRIBUTION IN AZIMUTHAL ANGLE
     *                 std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
     *                 phi = 2.0*PI*uniformPHI;
     *         2. GENERATE GAUSSIAN DISTRIBUTION IN POLAR ANGLE [0:180)
     *                 deg*pi/180 = rad
     *                 Input 3 --> Input/180 should give correct upper limit for distribution.
     *                 std::uniform_real_distribution<double> uniformTHETA(0.0, Input/180.0);
     *                 theta = acos(1.0-2.0*uniformTHETA);
     *         3. CONVERT POLAR COORDINATES TO CARTESIAN COORDINATES
     *                 x = sin(theta)*cos(phi);
     *                 y = sin(theta)*sin(phi);
     *                 z = cos(phi);
     */


    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Layer*Voro.Num_Grains;

        // Polar and azimuthal angle generator - different per layer.

        std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
        std::uniform_real_distribution<double> uniformTHETA(0.0, Material.Anis_angle[Layer]/180.0);
        std::normal_distribution<double> Norm(0.0,1.0);

        for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Voro.Num_Grains;++Grain_in_Layer){
            unsigned int Grain_in_system = Grain_in_Layer+Offset;

            Grains[Grain_in_system].Layer=Layer;

            for(size_t v=0;v<Voro.Vertex_X_final[Grain_in_Layer].size();++v){
                Grains[Grain_in_system].Vertices.push_back(std::make_pair(Voro.Vertex_X_final[Grain_in_Layer][v],Voro.Vertex_Y_final[Grain_in_Layer][v]));
            }

            Grains[Grain_in_system].Pos = std::make_pair(Voro.Geo_grain_centre_X[Grain_in_Layer],Voro.Geo_grain_centre_Y[Grain_in_Layer]);

    //================== initial Magnetisation ==================//
            if(Material.Mag_Type[Layer]=="assigned"){
                Grains[Grain_in_system].m.x=Material.Initial_mag[Layer].x;
                Grains[Grain_in_system].m.y=Material.Initial_mag[Layer].y;
                Grains[Grain_in_system].m.z=Material.Initial_mag[Layer].z;
            }
            else if(Material.Mag_Type[Layer]=="random"){
                double X=Norm(Gen);
                double Y=Norm(Gen);
                double Z=Norm(Gen);
                double MAG = sqrt(X*X+Y*Y+Z*Z);
                // Normalise the magnetisation
                X /= MAG;
                Y /= MAG;
                Z /= MAG;
                Grains[Grain_in_system].m.x = X;
                Grains[Grain_in_system].m.y = Y;
                Grains[Grain_in_system].m.z = Z;
            }
    //=========================== Anisotropy direction ===========================//
            // Generate dispersion in anisotropy direction
            phi = M_2PI*uniformPHI(Gen);
            theta = acos(1.0-2.0*uniformTHETA(Gen));
            EAx=sin(theta)*cos(phi);
            EAy=sin(theta)*sin(phi);
            EAz=cos(theta);
            // Rotate easy axes to match desired initial direction
            EAx2 =  EAx*cos(Material.Easy_axis_polar[Layer]) + EAz*sin(Material.Easy_axis_polar[Layer]);
            EAy2 =  EAy;
            EAz2 = -EAx*sin(Material.Easy_axis_polar[Layer]) + EAz*cos(Material.Easy_axis_polar[Layer]);
            // Azimuthal rotation.
            Grains[Grain_in_system].Easy_axis.x = EAx2*cos(Material.Easy_axis_azimuth[Layer]) - EAy2*sin(Material.Easy_axis_azimuth[Layer]);
            Grains[Grain_in_system].Easy_axis.y = EAx2*sin(Material.Easy_axis_azimuth[Layer]) + EAy2*cos(Material.Easy_axis_azimuth[Layer]);
            Grains[Grain_in_system].Easy_axis.z = EAz2;
            Grains[Grain_in_system].Temp        = Temperature;
            Grains[Grain_in_system].diameter    = Voro.Grain_diameter[Grain_in_Layer];
            Grains[Grain_in_system].Vol         = Voro.Grain_Area[Grain_in_Layer]*Material.dz[Layer];
            Grains[Grain_in_system].Ms          = Material.Ms[Layer];
            //================== Anisotropy T dependence ==================//
            Grains[Grain_in_system].Ani_method          = Material.Ani_method[Layer];
            Grains[Grain_in_system].Callen_power_range  = Material.Callen_power_range[Layer];
            Grains[Grain_in_system].Callen_power        = Material.Callen_power[Layer];
            Grains[Grain_in_system].Callen_factor_lowT  = Material.Callen_factor_lowT[Layer];
            Grains[Grain_in_system].Callen_factor_midT  = Material.Callen_factor_midT[Layer];
            Grains[Grain_in_system].Callen_factor_highT = Material.Callen_factor_highT[Layer];
            Grains[Grain_in_system].Callen_power_lowT   = Material.Callen_power_lowT[Layer];
            Grains[Grain_in_system].Callen_power_midT   = Material.Callen_power_midT[Layer];
            Grains[Grain_in_system].Callen_power_highT  = Material.Callen_power_highT[Layer];
            Grains[Grain_in_system].Callen_range_lowT   = Material.Callen_range_lowT[Layer];
            Grains[Grain_in_system].Callen_range_midT   = Material.Callen_range_midT[Layer];
    //================== Equilibrium magnetisation T dependence ==================//
            Grains[Grain_in_system].Crit_exp = Material.Crit_exp[Layer];
            Grains[Grain_in_system].mEQ_Type = Material.mEQ_Type[Layer];
            Grains[Grain_in_system].a0_mEQ   = Material.a0_mEQ[Layer];
            Grains[Grain_in_system].a1_mEQ   = Material.a1_mEQ[Layer];
            Grains[Grain_in_system].a2_mEQ   = Material.a2_mEQ[Layer];
            Grains[Grain_in_system].a3_mEQ   = Material.a3_mEQ[Layer];
            Grains[Grain_in_system].a4_mEQ   = Material.a4_mEQ[Layer];
            Grains[Grain_in_system].a5_mEQ   = Material.a5_mEQ[Layer];
            Grains[Grain_in_system].a6_mEQ   = Material.a6_mEQ[Layer];
            Grains[Grain_in_system].a7_mEQ   = Material.a7_mEQ[Layer];
            Grains[Grain_in_system].a8_mEQ   = Material.a8_mEQ[Layer];
            Grains[Grain_in_system].a9_mEQ   = Material.a9_mEQ[Layer];
            Grains[Grain_in_system].a1_2_mEQ = Material.a1_2_mEQ[Layer];
            Grains[Grain_in_system].b1_mEQ   = Material.b1_mEQ[Layer];
            Grains[Grain_in_system].b2_mEQ   = Material.b2_mEQ[Layer];
        }
    //================== Curie Temperature and Anisotropy distribution generators ==================//
        // Create distribution of Tc for grains in this layer.
        if(Material.Tc_Dist_Type[Layer]=="d" || Material.Tc_Dist_Type[Layer]=="d_custom"){
            double nu=Material.Tc_Ddist_nu[Layer];
            double d0=Material.Tc_Ddist_d0[Layer];
            double lambda = 1.0/nu;
            for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Voro.Num_Grains;++Grain_in_Layer){
                unsigned int Grain_in_system = Grain_in_Layer+Offset;
                // d0 and Grain diameter are both in [nm]
                Grains[Grain_in_system].Tc = Material.Tc_inf[Layer]*(1.0-pow((d0/(Grains[Grain_in_system].diameter)),lambda));
            }
        }
        else{
            std::vector<double> TcTmp;
            Dist_gen(Voro.Num_Grains,Material.Tc_Dist_Type[Layer],Material.Avg_Tc[Layer],Material.StdDev_Tc[Layer],&TcTmp);
            for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Voro.Num_Grains;++Grain_in_Layer){
                unsigned int Grain_in_system = Grain_in_Layer+Offset;
                Grains[Grain_in_system].Tc = TcTmp[Grain_in_Layer];
            }
        }
        // Create distribution of K for grains in this layer.
        std::vector<double> KTmp;
        Dist_gen(Voro.Num_Grains,Material.K_Dist_Type[Layer],Material.Avg_K[Layer],Material.StdDev_K[Layer],&KTmp);
        for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Voro.Num_Grains;++Grain_in_Layer){
            unsigned int Grain_in_system = Grain_in_Layer+Offset;
            Grains[Grain_in_system].K = KTmp[Grain_in_Layer];
        }
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"Complete.");
    return 0;
}