/* Parameter_import.cpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** @file Structure_import.cpp
 * @brief Function to define structure specific values using configuration file data. */

#include <iostream>
#include <fstream>
#include <string>

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Simulation_globals.hpp"

int Structure_import(const ConfigFile& cfg, Structure_t*Structure){
    Log.logandshow(Logger_t::LogLvl::INFO,"Assigning Structure parameters...");

//####################Retrieve values from hash table####################//
    Structure->Dim_x = cfg.getValAndUnitOfKey<double>("struct:dim-x");
    Structure->Dim_y = cfg.getValAndUnitOfKey<double>("struct:dim-y");
    Structure->Num_layers = cfg.getValueOfKey<unsigned int>("struct:num_layers");
    Structure->Grain_width = cfg.getValAndUnitOfKey<double>("struct:avg_grain_width");
    Structure->StdDev_grain_pos = cfg.getValueOfKey<double>("struct:std_dev_grain_vol");
    Structure->Max_width_limit = cfg.getValueOfKey<double>("struct:Max_grain_width");
    Structure->TinyGrainChance = cfg.getValueOfKey<double>("struct:Tiny_Grain_chance");
    Structure->Packing_fraction = cfg.getValueOfKey<double>("struct:packing_fraction");
    Structure->cell_size = cfg.getValAndUnitOfKey<double>("struct:Pixel_size");

    Sim::NumLayers = Structure->Num_layers;

//####################Output parameter values to end-user####################//
    Log.log(Logger_t::LogLvl::INFO, "\tInput Structure parameters: ");
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tX dimension (dx)                           = \t" + Log.BY + to_string_exact(Structure->Dim_x) + " nm"       + Log.RST);
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tY dimension (dy)                           = \t" + Log.BY + to_string_exact(Structure->Dim_y) + " nm"       + Log.RST);
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tNumber of layers in the system is          = \t" + Log.BY + to_string_exact(Structure->Num_layers)          + Log.RST);
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tThe average grain width is                 = \t" + Log.BY + to_string_exact(Structure->Grain_width) + " nm" + Log.RST);
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tThe standard deviation of the grain volume = \t" + Log.BY + to_string_exact(Structure->StdDev_grain_pos)    + Log.RST);
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tThe chance of generating a tiny grain      = \t" + Log.BY + to_string_exact(Structure->TinyGrainChance)     + Log.RST);
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t\tThe packing fraction is                    = \t" + Log.BY + to_string_exact(Structure->Packing_fraction)    + Log.RST);
    Log.log(Logger_t::LogLvl::INFO,"======================================================================");

    return 0;
}



