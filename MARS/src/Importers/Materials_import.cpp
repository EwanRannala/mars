/* Materials_import.cpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** @file Materials_import.cpp
 * @brief Function to define material specific data from material configuration files. */

#include <iostream>
#include <vector>
#include <fstream>
#include <cstring>
#include <cmath>

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

int Materials_import(const ConfigFile& cfg, const unsigned int Num_Layers, std::vector<ConfigFile>*Material_Config, Material_t*Material){

    std::vector<double> temp_mag;
    std::vector<double> temp_EA;
    unsigned int Material_number=0;
    std::string unit_susceptibility;

    Log.logandshow(Logger_t::LogLvl::INFO,"Assigning material parameters...");
    Material->Initial_mag.resize(Num_Layers);     Material->Initial_anis.resize(Num_Layers);
    Material->z.push_back(0.0);

    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        // Determine Material number
        Material_number = Layer+1;
        std::string key_prefix = "Mat" + to_string_exact(Material_number) + ":";

        std::string FILE_LOC = "Input/Materials/" + cfg.getValueOfKey<std::string>(key_prefix+"File");
        ConfigFile MAT_FILE(FILE_LOC);
        Material_Config->push_back(MAT_FILE); // Need the config file for future use.

        // Import all variables
        Material->Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:type"));

        Material->Mag_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:Initial_mag_type"));
        if(Material->Mag_Type.back() != "assigned" && Material->Mag_Type.back() != "random"){
            Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown Mag type. -> " + Material->Mag_Type.back());
        }
        if(Material->Mag_Type.back()=="assigned"){        // This forces the input vector to be a unit vector
            temp_mag = MAT_FILE.getValueOfKey<std::vector<double>>("Mat:Initial_mag");
            double temp_MAG = sqrt(temp_mag[0]*temp_mag[0]+temp_mag[1]*temp_mag[1]+temp_mag[2]*temp_mag[2]);
            Material->Initial_mag[Layer].x = temp_mag[0]/temp_MAG;
            Material->Initial_mag[Layer].y = temp_mag[1]/temp_MAG;
            Material->Initial_mag[Layer].z = temp_mag[2]/temp_MAG;
        }
        Material->Easy_axis_polar.push_back(MAT_FILE.getValueOfKey<double>("Mat:easy_axis_polar"));
         Material->Easy_axis_azimuth.push_back(MAT_FILE.getValueOfKey<double>("Mat:easy_axis_azimuth"));

         Material->dz.push_back(MAT_FILE.getValueOfKey<double>("Mat:thickness"));
         if(Layer>0){Material->z.push_back(Material->z[Layer-1]+(Material->dz[Layer-1]+Material->dz[Layer])*0.5);}
         Material->Ms.push_back(MAT_FILE.getValueOfKey<double>("Mat:ms"));

        Material->K_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:k_Dist_type"));
        if(Material->K_Dist_Type.back() != "normal" && Material->K_Dist_Type.back() != "log-normal"){
            Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown K distribution type. -> " + Material->K_Dist_Type.back());
        }
        Material->Avg_K.push_back(MAT_FILE.getValueOfKey<double>("Mat:avg_k"));
        Material->StdDev_K.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_k"));
        Material->Tc_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:tc_dist_type"));
        if(Material->Tc_Dist_Type.back() != "normal" && Material->Tc_Dist_Type.back() != "log-normal" && Material->Tc_Dist_Type.back() != "d" && Material->Tc_Dist_Type.back() != "d_custom"){
            Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown Tc distribution type. -> " + Material->Tc_Dist_Type.back());
        }
        Material->Avg_Tc.push_back(MAT_FILE.getValueOfKey<double>("Mat:avg_tc"));
        Material->StdDev_Tc.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_tc"));
        if(Material->Tc_Dist_Type.back() == "d"){ // If "d" assume scaling parameters from "Hovorka, S. Devos, Q. Coopman, W.J. Fan, C.J. Aas, R.F.L. Evans, X. Chen, G. Ju, and R.W. Chantrell, Appl. Phys. Lett. 101, 052406 (2012)"
            Material->Tc_inf.push_back(MAT_FILE.getValueOfKey<double>("Mat:Tc_inf"));
            Material->Tc_Ddist_d0.push_back(0.71);
            Material->Tc_Ddist_nu.push_back(0.79);
        }
        else if(Material->Tc_Dist_Type.back() == "d_custom"){ // Let user input own scaling parameters
            Material->Tc_inf.push_back(MAT_FILE.getValueOfKey<double>("Mat:Tc_inf"));
            Material->Tc_Ddist_d0.push_back(MAT_FILE.getValueOfKey<double>("Mat:Tc_d0"));
            Material->Tc_Ddist_nu.push_back(MAT_FILE.getValueOfKey<double>("Mat:Tc_nu"));
        }

        Material->J_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:j_dist_type"));
        if(Material->J_Dist_Type.back() != "normal" && Material->J_Dist_Type.back() != "log-normal"){
            Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown J distribution type. -> " + Material->J_Dist_Type.back());
        }
        Material->StdDev_J.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_j"));
        Material->H_sat.push_back(MAT_FILE.getValueOfKey<double>("Mat:h_sat"));
        Material->Anis_angle.push_back(MAT_FILE.getValueOfKey<double>("Mat:anis_angle"));
        //#################### Temperature dependence of magnetisation ####################//
        std::string mEQstr = MAT_FILE.getValueOfKey<std::string>("Mat:mEQ_type");
        if(mEQstr!="bulk" && mEQstr!="polynomial"){
            Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown mEQ type. -> " + mEQstr);
        }
        else if(mEQstr=="bulk"){
            Material->mEQ_Type.push_back(Material_t::mEQTypes::Bulk);
            Material->Crit_exp.push_back(MAT_FILE.getValueOfKey<double>("Mat:Critical_Exponent"));
            Material->a0_mEQ.push_back  (0.0);
            Material->a1_mEQ.push_back  (0.0);
            Material->a2_mEQ.push_back  (0.0);
            Material->a3_mEQ.push_back  (0.0);
            Material->a4_mEQ.push_back  (0.0);
            Material->a5_mEQ.push_back  (0.0);
            Material->a6_mEQ.push_back  (0.0);
            Material->a7_mEQ.push_back  (0.0);
            Material->a8_mEQ.push_back  (0.0);
            Material->a9_mEQ.push_back  (0.0);
            Material->a1_2_mEQ.push_back(0.0);
            Material->b1_mEQ.push_back  (0.0);
            Material->b2_mEQ.push_back  (0.0);
        }
        else{ // Polynomial
            Material->mEQ_Type.push_back(Material_t::mEQTypes::Polynomial);
            Material->Crit_exp.push_back(0.0);
            Material->a0_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a0_mEQ"));
            Material->a1_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a1_mEQ"));
            Material->a2_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a2_mEQ"));
            Material->a3_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a3_mEQ"));
            Material->a4_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a4_mEQ"));
            Material->a5_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a5_mEQ"));
            Material->a6_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a6_mEQ"));
            Material->a7_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a7_mEQ"));
            Material->a8_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a8_mEQ"));
            Material->a9_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a9_mEQ"));
            Material->a1_2_mEQ.push_back(MAT_FILE.getValueOfKey<double>("Mat:a1_2_mEQ"));
            Material->b1_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:b1_mEQ"));
            Material->b2_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:b2_mEQ"));            
        }
        //#################### Temperature dependence of anisotropy (Callen-Callen) ####################//
        // Anisotropy model type
        std::string Anistr = MAT_FILE.getValueOfKey<std::string>("Mat:Ani_method");
        if(Anistr != "callen" && Anistr != "chi"){
            Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown Anisotropy_method. -> " + Anistr);
        }
        else if(Anistr=="callen"){
            Material->Ani_method.push_back(Material_t::AniMethods::Callen);
            // Temperature dependence of anisotropy (Callen-Callen)
            std::string Calstr = MAT_FILE.getValueOfKey<std::string>("Mat:Callen_power_range");
            if(Calstr != "single" && Calstr != "multiple"){
                Log.log(Logger_t::LogLvl::ERR," MAT_FILE error: Material " + to_string_exact(Material_number) + " Unknown Callen power range type. -> " + Calstr);
            }
            else if(Calstr=="single"){
                Material->Callen_power_range.push_back(Material_t::CallenMethods::Single);
                // Variables set to ensure no undesired usage occurs.
                Material->Callen_power.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
                Material->Callen_factor_lowT.push_back (1.000);
                Material->Callen_factor_midT.push_back (1.000);
                Material->Callen_factor_highT.push_back(1.000);
                Material->Callen_power_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
                Material->Callen_power_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
                Material->Callen_power_highT.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
                Material->Callen_range_lowT.push_back (1600.0);
                Material->Callen_range_midT.push_back (2000.0);
            }
            else{
                Material->Callen_power_range.push_back(Material_t::CallenMethods::Multiple);
                Material->Callen_power.push_back(0.0);
                Material->Callen_factor_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_factor_lowT"));
                Material->Callen_factor_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_factor_midT"));
                Material->Callen_factor_highT.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_factor_highT"));
                Material->Callen_power_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power_lowT"));
                Material->Callen_power_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power_midT"));
                Material->Callen_power_highT.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power_highT"));
                Material->Callen_range_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_range_lowT"));
                Material->Callen_range_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_range_midT"));
            }
        }
        else{ // These values won't be used as we are using Chi to determine the anisotropy
            Material->Ani_method.push_back(Material_t::AniMethods::Chi);
            Material->Callen_power_range.push_back(Material_t::CallenMethods::Single);
            Material->Callen_power.push_back(1.000);
            Material->Callen_factor_lowT.push_back (1.000);
            Material->Callen_factor_midT.push_back (1.000);
            Material->Callen_factor_highT.push_back(1.000);
            Material->Callen_power_lowT.push_back (1.000);
            Material->Callen_power_midT.push_back (1.000);
            Material->Callen_power_highT.push_back(1.000);
            Material->Callen_range_lowT.push_back (1600.0);
            Material->Callen_range_midT.push_back (2000.0);
        }
        if(Num_Layers>1){
            Material->Hexch_str_out_plane_UP.push_back(MAT_FILE.getValueOfKey<double>("Mat:Hexch_out_of_layer_UP"));
            if(Layer==0){
                Material->Hexch_str_out_plane_DOWN.push_back(0.0);
            }
            else if(Layer!=0){
                Material->Hexch_str_out_plane_DOWN.push_back(MAT_FILE.getValueOfKey<double>("Mat:Hexch_out_of_layer_DOWN"));
            }
        }
    }
//####################Output parameter values to end-user####################//
    Log.logandshow(Logger_t::LogLvl::INFO, Log.BY + to_string_exact(Num_Layers)+" Layer(s):" + Log.RST);
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        Material_number = Layer+1;
        Log.logandshow(Logger_t::LogLvl::INFO, "..............................Material " + to_string_exact(Material_number) + "..............................");
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tType                                       = \t"  + Log.BY + Material->Type[Layer] + " \t" + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC +  "\tThickness                                  = \t" + Log.BY + to_string_exact(Material->dz[Layer]) + " nm\t" + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC +  "\tMs                                         = \t" + Log.BY + to_string_exact(Material->Ms[Layer]) + " emu/cc\t" + Log.RST);
        /******************************************************************************/
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tM(T) type                                  = \t" + Log.BY + Material->mEQString(Material->mEQ_Type[Layer]) + " \t" + Log.RST);
        switch (Material->mEQ_Type[Layer])
        {
            case Material_t::mEQTypes::Bulk: // bulk
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     M critical exp                   = \t" + Log.BY + to_string_exact(Material->Crit_exp[Layer]) + " \t" + Log.RST);
            break;
            case Material_t::mEQTypes::Polynomial:    // Polynomial
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a0_mEQ                                = \t" + Log.BY + to_string_exact(Material->a0_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a1_mEQ                                = \t" + Log.BY + to_string_exact(Material->a1_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a2_mEQ                                = \t" + Log.BY + to_string_exact(Material->a2_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a3_mEQ                                = \t" + Log.BY + to_string_exact(Material->a3_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a4_mEQ                                = \t" + Log.BY + to_string_exact(Material->a4_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a5_mEQ                                = \t" + Log.BY + to_string_exact(Material->a5_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a6_mEQ                                = \t" + Log.BY + to_string_exact(Material->a6_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a7_mEQ                                = \t" + Log.BY + to_string_exact(Material->a7_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a8_mEQ                                = \t" + Log.BY + to_string_exact(Material->a8_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a9_mEQ                                = \t" + Log.BY + to_string_exact(Material->a9_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     a_1_2_mEQ                             = \t" + Log.BY + to_string_exact(Material->a1_2_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     b1_mEQ                                = \t" + Log.BY + to_string_exact(Material->b1_mEQ[Layer]) + " \t" + Log.RST);
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     b2_mEQ                                = \t" + Log.BY + to_string_exact(Material->b2_mEQ[Layer]) + " \t" + Log.RST);
            break;
        }
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tTc distribution type                       = \t" + Log.BY + Material->Tc_Dist_Type[Layer]  + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Average Tc                            = \t" + Log.BY + to_string_exact(Material->Avg_Tc[Layer]) + " K\t" + Log.RST);
        if(Material->Tc_Dist_Type[Layer]=="d" || Material->Tc_Dist_Type[Layer]=="d_custom"){
            Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Tc_inf                                = \t" + Log.BY + to_string_exact(Material->Tc_inf[Layer]) + " K\t" + Log.RST);
            Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     d0                                    = \t" + Log.BY + to_string_exact(Material->Tc_Ddist_d0[Layer]) + " \t" + Log.RST);
            Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     nu                                    = \t" + Log.BY + to_string_exact(Material->Tc_Ddist_nu[Layer]) + " \t" + Log.RST);
        }
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Standard deviation Tc                 = \t" + Log.BY + to_string_exact(Material->StdDev_Tc[Layer]) + " K\t" + Log.RST);
        
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tJ distribution type                        = \t" + Log.BY + Material->J_Dist_Type[Layer]  + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     H_Sat                                 = \t" + Log.BY + to_string_exact(Material->H_sat[Layer]) + " Oe\t" + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Standard deviation J                  = \t" + Log.BY + to_string_exact(Material->StdDev_J[Layer]) + " \t" + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tAnisotropy type                            = \t" + Log.BY + Material->AniString(Material->Ani_method[Layer]) + " \t" + Log.RST);
        switch (Material->Ani_method[Layer])
        {
            case Material_t::AniMethods::Callen:                
                switch(Material->Callen_power_range[Layer])
                {
                    case Material_t::CallenMethods::Single:
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen power                          = \t" + Log.BY + to_string_exact(Material->Callen_power[Layer]) + " \t" + Log.RST);
                        break;
                    case Material_t::CallenMethods::Multiple:
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen factor low                     = \t" + Log.BY + to_string_exact(Material->Callen_factor_lowT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen power low                      = \t" + Log.BY + to_string_exact(Material->Callen_power_lowT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen range low                      = \t" + Log.BY + to_string_exact(Material->Callen_range_lowT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen factor mid                     = \t" + Log.BY + to_string_exact(Material->Callen_factor_midT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen power mid                      = \t" + Log.BY + to_string_exact(Material->Callen_power_midT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen range mid                      = \t" + Log.BY + to_string_exact(Material->Callen_range_midT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen factor high                    = \t" + Log.BY + to_string_exact(Material->Callen_factor_highT[Layer]) + " \t" + Log.RST);
                        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Callen power high                     = \t" + Log.BY + to_string_exact(Material->Callen_power_midT[Layer]) + " \t" + Log.RST);
                        break;
                }
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tAnisotropy dispersion angle                = \t" + Log.BY + to_string_exact(Material->Anis_angle[Layer]) + " degrees\t" + Log.RST);
                break;
            case Material_t::AniMethods::Chi:
                break;
        }
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     K distribution type                   = \t" + Log.BY + Material->K_Dist_Type[Layer]  + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Average K                             = \t" + Log.BY + to_string_exact(Material->Avg_K[Layer]) + " erg/cc\t" + Log.RST);
        Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\t     Standard deviation K                  = \t" + Log.BY + to_string_exact(Material->StdDev_K[Layer]) + " erg/cc\t" + Log.RST);
        if(Num_Layers>1){
            Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tH exchange out of plane_UP                 = \t" + Log.BY + to_string_exact(Material->Hexch_str_out_plane_UP[Layer]) + " Oe\t" + Log.RST);
            if(Layer!=0){
                Log.logandshow(Logger_t::LogLvl::INFO, Log.BC + "\tH exchange out of plane_DOWN               = \t" + Log.BY + to_string_exact(Material->Hexch_str_out_plane_DOWN[Layer]) + " Oe\t" + Log.RST);
            }
        }
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"======================================================================");
    return 0;
}

