/* Distributions_generator.cpp
 *  Created on: 31 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Distributions_generator.cpp
 * \brief Function for distribution generation
 *
 * There are two key distributions in MARS a normal (Gaussian) and a log-normal distribution.<BR>
 * The normal distribution is:
 * \f[
 *        f(x) = \frac{1}{\sigma\sqrt{2\pi}}\exp\Bigg[-\frac{1}{2}\bigg(\frac{x-\mu}{\sigma}\bigg)^2\Bigg]
 * \f]
 *
 * The Log-normal distribution is:
 * \f[
 *        f(x) = \frac{1}{x\sigma_{log}\sqrt{2\pi}}\exp\Bigg[-\frac{(\ln(x)-\mu_{log})^2}{2\sigma_{log}^2}\Bigg]
 * \f]
 * The conversion of the mean and standard deviation for use in the log-normal distribution is
 * automatically handled by the function via:
 * \f[
 *         \mu_{log} = \log{\Bigg(\frac{\mu}{\sqrt{1+\frac{\sigma^2}{\mu^2}}}\Bigg)}
 * \f]
 * \f[
 *         \sigma_{log} = \sqrt{\log{\bigg(1+\frac{\sigma^2}{\mu^2}\bigg)}}
 * \f]
 *
 */
#include <random>
#include <vector>
#include <iostream>
#include <cmath>

#include "Globals.hpp"
#include "Lognormal_distribution_generator.hpp"

// TODO Get Tc distribution checked and tested
int Dist_gen(const unsigned int Num_grains,const std::string& Type, const double Mean, const double StdDev, std::vector<double>*Data){
    double Val=0.0;
    if(Type == "normal"){
        std::normal_distribution<double> Norm(Mean,StdDev);
        for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
            Val = Norm(Gen);
            Data->push_back(Val);
    }    }
    else{
        /* Input expected is arithmetic mean and logStdDev.
         * The lognormal generator requires mu (LogMean) and sigma (LogStdDev).
         * Calculating mu requires arithmetic StdDev. This is calculated using the relationship
         * sigma = ArthStdDev/ArthMeam. */
        double mu = log(Mean/sqrt(1.0+(((Mean*StdDev)*(Mean*StdDev))/(Mean*Mean))));
        *Data = Lognormal_generator(mu,StdDev,Num_grains);
    }
    return 0;
}

int Tc_dist_gen(const unsigned int Num_grains, const std::vector<double>& Grain_diameter, const double Tc_inf, std::vector<double>*Data){
    double D_arth_mu=0.0;
    double D_arth_sig=0.0;
    double nu=0.85;
    double d0=0.72;
    double lambda = 1.0/nu;
    /* Since each layer will have the same grain areas, it is sufficient to determine the mean and
     * standard deviation using the first layer values only. */
    for(unsigned int grain=0;grain<Num_grains;++grain){
        D_arth_mu += Grain_diameter[grain];
    }
    D_arth_mu/=Num_grains;
    for(unsigned int grain=0;grain<Num_grains;++grain){
        D_arth_sig += (Grain_diameter[grain]-D_arth_mu)*(Grain_diameter[grain]-D_arth_mu);
    }
    D_arth_sig/=Num_grains;
    D_arth_sig=sqrt(D_arth_sig);
    // determine lognormal distribution parameters
    double D_log_sig = sqrt(log(1.0+((D_arth_sig*D_arth_sig)/(D_arth_mu*D_arth_mu))));
    double D_log_mu  = log( D_arth_mu/sqrt(1.0+((D_arth_sig*D_arth_sig)/(D_arth_mu*D_arth_mu))));
    // Determine dTc distribution parameters
    double dTc_log_mu  = lambda*(log(d0*pow(Tc_inf,nu))-D_log_mu);
    double dTc_log_sig = lambda*D_log_sig;
    std::vector<double> dTc = Lognormal_generator(dTc_log_mu,dTc_log_sig,Num_grains);
    // Generate Tc distribution
    for(unsigned int grain=0;grain<Num_grains;++grain){Data->push_back(Tc_inf-dTc[grain]);}
    return 0;
}


