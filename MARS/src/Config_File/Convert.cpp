/* Convert.cpp
 *  Created on: 28 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Convert.cpp
 * \brief Function implementations for the Convert class. */

#include "Config_File/Convert.hpp"
#include <sstream>
#include <typeinfo>

const std::string Convert::BoldRedFont = "\033[1;4;31m";
const std::string Convert::ResetFont = "\033[0m";

/** Converts the input std::string to the declared type.
 * \tparam val `std::string`, `bool`, std::vector<double>` or `std::vector<int>`.
 * \param[in] FileName std::string indicating the name of the input file.
 * \param[in] val std::string variable to be converted.
 * \param[in] key std::string indicating the requested key.
 * \return val converted to a std::string. */
//@{
template <typename T>
T Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    // Convert a std::string to T.
    std::istringstream istr(val);
    T returnVal;
    if (!(istr >> returnVal)){
        throw std::runtime_error(BoldRedFont + "CFG error: Invalid " + (std::string)typeid(T).name() + " received from " + key + " in " + FileName + ResetFont + "\n");
    }
    return returnVal;
}

template double Convert::string_to_T(const std::string&, std::string const&, std::string const &);
template int Convert::string_to_T(const std::string&, std::string const&, std::string const &);

template <>
std::string Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    return val;
}

template <>
std::vector<std::string> Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    std::stringstream istr(val);
    std::string string;
    std::vector<std::string> Strings;
    while (istr >> string || !istr.eof()){
        if (istr.fail()){
            throw std::runtime_error(BoldRedFont + "CFG error: Invalid int received from " + key + " in " + FileName + ResetFont + "\n");
        }
        Strings.push_back(string);
    }
    return Strings;
}

template <>
unsigned int Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    std::istringstream istr(val);
    int returnVal=0;
    if (!(istr >> returnVal) || returnVal<0){
        throw std::runtime_error(BoldRedFont + "CFG error: Invalid unsigned int received from " + key + " in " + FileName + ResetFont + "\n");
    }
    return static_cast<unsigned int>(returnVal);
}

template <>
bool Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    if(val=="true"){
        return true;
    }
    if(val=="false"){
        return false;
    }
    throw std::runtime_error(BoldRedFont + "CFG error: Invalid Boolean received from " + key + " in " + FileName + ResetFont + "\n");
}

template <>
std::vector<double> Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    std::stringstream istr(val);
    double number=0.0;
    std::vector<double> Numbers;
    while (istr >> number || !istr.eof()){
        if (istr.fail()){
            throw std::runtime_error(BoldRedFont + "CFG error: Invalid double received from " + key + " in " + FileName + ResetFont + "\n");
        }
        Numbers.push_back(number);
    }
    return Numbers;
}

template <>
std::vector<int> Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    std::stringstream istr(val);
    int number=0;
    std::vector<int> Numbers;
    while (istr >> number || !istr.eof()){
        if (istr.fail()){
            throw std::runtime_error(BoldRedFont + "CFG error: Invalid int received from " + key + " in " + FileName + ResetFont + "\n");
        }
        Numbers.push_back(number);
    }
    return Numbers;
}

template <>
std::vector<unsigned int> Convert::string_to_T(const std::string& FileName, std::string const &val, std::string const &key){
    std::stringstream istr(val);
    int number=0;
    std::vector<unsigned int> Numbers;
    while (istr >> number || !istr.eof()){
        if (istr.fail() || number<0){
            throw std::runtime_error(BoldRedFont + "CFG error: Invalid unsigned int received from " + key + " in " + FileName + ResetFont + "\n");
        }
        Numbers.push_back(static_cast<unsigned int>(number));
    }
    return Numbers;
}
//@}


