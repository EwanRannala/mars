/* ConfigFile_import.cpp
 *  Created on: 27 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file ConfigFile_import.cpp
 * \brief Function implementations for the ConfigFile class. */

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <utility>


#include "Config_File/ConfigFile_import.hpp"

/** This function is called during construction. The routine opens the configuration file
 * and proceeds to remove comments and blank lines, and then parses the line. All
 * parameters from the configuration file are stored in a hash table once the entire
 * file has been parsed. */
ConfigFile::ConfigFile(std::string InFileName)
    : FileName(std::move(InFileName))
{
    ExtractKeys();
}

/** Extract Contents of the configuration file and generate the key-value container. Once this is performed
 * the configuration file is no longer used for I/O. */
void ConfigFile::ExtractKeys(){
    
    std::ifstream File(FileName);
    
    if(!File){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: " + FileName + " not found");
    }

    std::string line;
    unsigned int lineNum=0;
    
    while(std::getline(File,line)){
        lineNum++;
        std::string temp=line;

        // Remove all whitespace
        temp.erase(remove(temp.begin(), temp.end(), '\t'), temp.end());
        temp.erase(remove(temp.begin(), temp.end(), ' ' ), temp.end());
        
        // clear carriage return for dos formatted files
        temp.erase(remove(temp.begin(), temp.end(), '\r'), temp.end()); 
        
        // Remove comments
        removeComment(temp);
        
        // Format all non-empty lines 
        if(!temp.empty()){

            if(temp.find('{')!=std::string::npos){
                FormatVector(temp,lineNum);
            }
            
            lowerCase(temp);

            parseLine(temp,lineNum);
        }
    }
    File.close();
}

/** Removes the remaining line when ';' or '#' is encountered.
 *
 * @param[out] line Line of configuration file.
 */
void ConfigFile::removeComment(std::string &line){
    
    // If ; or # is present remove it and all characters following.
    size_t CommentPos = line.find('#');
    
    // Find the earliest comment marker
    if(size_t CommentPosB=line.find(';',0); CommentPosB<CommentPos){
        CommentPos = CommentPosB;
    }

    if(CommentPos != std::string::npos){
        line.erase(CommentPos);
    }
}

/** Remove braces used to specify vector and also replace comma separators with whitespace.
 *
 * @param[out] line Line of configuration file.
 * @param[in] lineNum Line number.
 */
void ConfigFile::FormatVector(std::string &line, size_t const lineNum) const{

    // The vector must be closed
    if(line.find('}') == std::string::npos){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Bad vector input, missing '}' on line " + to_string_exact(lineNum) + " in " + FileName);
    }

    // Remove braces
    line.erase(line.find('{'),1);
    line.erase(line.find('}'),1);

    // Replace commas with whitespace
    std::replace(line.begin(),line.end(), ',' , ' ');
}

/** Set all characters to lower case, except those surrounded by quotes.
 *
 * @param[out] line Line of configuration file.
 */
void ConfigFile::lowerCase(std::string &line){

    std::vector<size_t> QuotePositions (0);
    std::string::iterator It=line.begin();

    /* We want to iterate through the line looking for quotes and lowering the case of everything
     * between the iterator and that quote. We only erase after every odd number of quotes in order
     * to conserve casae between quote pairs. We also want to store the position of the quotes so that 
     * we can remove all quote pairs.*/
    while(It<line.end()){

        // Find opening quote
        std::string::iterator QuotePos = std::find(It,line.end(),'"');
        
        // Convert from iterator and save position for deletion
        size_t QposIdx = std::distance(line.begin(),QuotePos);
        if(QposIdx!=std::string::npos){ // Only save position if a quote has been found
            QuotePositions.emplace_back(QposIdx);
        }

        transform(It, QuotePos, It, 
          [](unsigned char c){ return std::tolower(c); });

        // Move the iterator past the opening quote
        It = QuotePos+1;

        // Find closing quote
        QuotePos = std::find(It,line.end(),'"');

        // Convert from iterator and save position for deletion
        QposIdx = std::distance(line.begin(),QuotePos);
        if(QposIdx!=std::string::npos){ // Only save position if a quote has been found
            QuotePositions.emplace_back(QposIdx);
        }

        // Move the iterator past the closing quote
        It = QuotePos+1;
        
    }

    // Erase all quotes
    for(std::vector<size_t>::reverse_iterator Rit = QuotePositions.rbegin(); Rit!=QuotePositions.rend(); ++Rit){
        line.erase(*Rit,1);
    }

}

/** Parse line searching for expected formatting, then extract the Contents of the line.
 *
 * @param line Line of configuration file.
 * @param lineNum Line number.
 */
void ConfigFile::parseLine(std::string &line, const unsigned int lineNum){

    if(validLine(line,lineNum)){
        extractContents(line, lineNum);
    }

}

/** Return boolean indicating if line is valid or not.
 *
 * @param[in] line Line of configuration file.
 */
bool ConfigFile::validLine(const std::string &line, const unsigned int lineNum){

    // We must have an = sign
    if(line.find('=')==std::string::npos){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Couldn't find separator on line " + to_string_exact(lineNum) + " in " + FileName);
        return false;
    }

    // Must have data on either side of the equals sign
    if(line[0]=='=' || line.back()=='='){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Bad format on line " + to_string_exact(lineNum) + " in " + FileName);
        return false;
    }

    return true;
}

/** Extract Contents of a line in the configuration file.
 * Check and ensure only unique keys are present and insert a key-value pair for that line.
 *
 * @param[in] line Line of configuration file.
 */
void ConfigFile::extractContents(const std::string &line, const unsigned int lineNum){
    
    size_t separator_pos = line.find('=');
    std::string key;
    std::string value;
    std::string unit;

    /* Here we want to extract everything before = as the key.
     * Everything after should be the value and unit */

    extractKey(key, separator_pos, line);
    extractValueAndUnit(value, unit, separator_pos, line);
    
    // Only unique keys are allowed
    if(!keyExists(key)){
        Contents.insert(std::pair<std::string,KeyValues_t>(key,KeyValues_t(value, unit, lineNum)));
    }
    else{
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Key name conflict, all keys must be unique. -> " + key + " on line " + to_string_exact(lineNum) + " in " + FileName );
    }
}

/** Extract the keys from configuration file for use in map.
 *
 * @param[out] key Configuration file key
 * @param[in] separator_pos Position of key and value separator
 * @param[in] line Line of configuration file.
 */
void ConfigFile::extractKey(std::string &key, size_t const &separator_pos, const std::string &line){
    // Set key as substring before '='.
    key=line.substr(0,separator_pos);
}

/** Extract the value and unit from configuration file for use in map.
 *
 * @param[out] value Value of configuration file key
 * @param[out] unit Unit of configuration file value
 * @param[in] separator_pos Position of key and value separator
 * @param[in] line Line of configuration file.
 */
void ConfigFile::extractValueAndUnit(std::string &value, std::string &unit, size_t const& separator_pos, const std::string& line){
    
    // extract the substring after '='
    std::string VUstr = line.substr(separator_pos+1);

    // Search for '!'
    std::size_t UnitPos = VUstr.find('!');
    if(UnitPos==std::string::npos){  // No unit given
        value = VUstr;
        unit  = "";
    }
    else{
        // Extract value and unit from VUstr
        value = VUstr.substr(0,UnitPos);
        unit  = VUstr.substr(UnitPos+1);
    }
}

/** Checks for duplicate parameter entries.
 *
 * @param[in] key Key to be added to the map.
 */
bool ConfigFile::keyExists(const std::string &key) const{

    std::string temp=key;
    lowerCase(temp);
    return Contents.find(temp) != Contents.end();
}

/** Returns the value for the associated key from the map table if it is present.
 * @tparam ValueType `std::string`, `double`, `int`, `std::vector<double>`, `std::vector<int>` or `bool`.
 * @param[in] key a pointer to the string indicating the requested key.
 * @return Value associated with the requested key or nothing. */
//@{
template <typename ValueType>
std::optional<ValueType> ConfigFile::getValueOfOptKey(const std::string &key) const{
    std::string temp=key;
    lowerCase(temp);
    if(keyExists(temp)){
        return Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp);
    }
    return std::nullopt;
}

template std::optional<std::string> ConfigFile::getValueOfOptKey<std::string>(const std::string&) const;
template std::optional<std::vector<std::string>> ConfigFile::getValueOfOptKey<std::vector<std::string>>(const std::string&) const;
template std::optional<std::vector<double>> ConfigFile::getValueOfOptKey<std::vector<double>>(const std::string&) const;
template std::optional<std::vector<unsigned int>> ConfigFile::getValueOfOptKey<std::vector<unsigned int>>(const std::string&) const;
template std::optional<std::vector<int>> ConfigFile::getValueOfOptKey<std::vector<int>>(const std::string&) const;
template std::optional<double> ConfigFile::getValueOfOptKey<double>(const std::string&) const;
template std::optional<unsigned int> ConfigFile::getValueOfOptKey<unsigned int>(const std::string&) const;
template std::optional<int> ConfigFile::getValueOfOptKey<int>(const std::string&) const;
template std::optional<bool> ConfigFile::getValueOfOptKey<bool>(const std::string&) const;
//@}

/** Returns the value for the associated key from the map.
 * @tparam ValueType `std::string`, `double`, `int`, `std::vector<double>`, `std::vector<int>` or `bool`.
 * @param[in] key a pointer to the string indicating the requested key.
 * @return Value associated with the requested key. */
//@{
template <typename ValueType>
ValueType ConfigFile::getValueOfKey(const std::string &key) const{

    std::string temp=key;
    lowerCase(temp);

    if (!keyExists(temp)){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Key not found -> " + temp + " in " + FileName);
    }

    return Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp);
}

template std::string ConfigFile::getValueOfKey<std::string>(const std::string&) const;
template std::vector<std::string> ConfigFile::getValueOfKey<std::vector<std::string>>(const std::string&) const;
template std::vector<double> ConfigFile::getValueOfKey<std::vector<double>>(const std::string&) const;
template std::vector<unsigned int> ConfigFile::getValueOfKey<std::vector<unsigned int>>(const std::string&) const;
template std::vector<int> ConfigFile::getValueOfKey<std::vector<int>>(const std::string&) const;
template double ConfigFile::getValueOfKey<double>(const std::string&) const;
template unsigned int ConfigFile::getValueOfKey<unsigned int>(const std::string&) const;
template int ConfigFile::getValueOfKey<int>(const std::string&) const;
template bool ConfigFile::getValueOfKey<bool>(const std::string&) const;
//@}

/** Returns the value for the associated key from the map table if it is present.
 * @tparam ValueType `std::string`, `double`, `int`, `std::vector<double>`, `std::vector<int>` or `bool`.
 * @param[in] key a pointer to the string indicating the requested key.
 * @return Value associated with the requested key or nothing. */
//@{
template <typename ValueType>
std::optional<std::pair<ValueType,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum(const std::string &key) const{
    std::string temp=key;
    lowerCase(temp);
    if(keyExists(temp)){
        return std::pair(Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp), std::get<2>(Contents.find(temp)->second));
    }
    return std::nullopt;
}

template std::optional<std::pair<std::string,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<std::string>(const std::string&) const;
template std::optional<std::pair<std::vector<std::string>,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<std::vector<std::string>>(const std::string&) const;
template std::optional<std::pair<std::vector<double>,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<std::vector<double>>(const std::string&) const;
template std::optional<std::pair<std::vector<unsigned int>,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<std::vector<unsigned int>>(const std::string&) const;
template std::optional<std::pair<std::vector<int>,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<std::vector<int>>(const std::string&) const;
template std::optional<std::pair<double,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<double>(const std::string&) const;
template std::optional<std::pair<unsigned int,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<unsigned int>(const std::string&) const;
template std::optional<std::pair<int,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<int>(const std::string&) const;
template std::optional<std::pair<bool,unsigned int>> ConfigFile::getValueOfOptKeyWLineNum<bool>(const std::string&) const;
//@}



// TODO check generated documentation

/** Returns the value and unit for the associated key from the map.
 * @tparam ValueType `double`, `int`, `unsigned int`, `std::vector<double>`, `std::vector<int>`, `std::vector<unsigned int>`.
 * @param[in] key a pointer to the string indicating the requested key.
 * @return Value and unit (if available) associated with the requested key. */
//@{
template<class ValueType> std::enable_if_t<!ConfigFile::is_vector<ValueType>::value, ValueType> ConfigFile::getValAndUnitOfKey(const std::string &key) const{

    std::string temp=key;
    lowerCase(temp);

    if (!keyExists(temp)){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Key not found -> " + temp + " in " + FileName);
    }

    // Extract the value and unit
    ValueType Val  = Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp);
    double Unit    = ConvertUnit(std::get<1>(Contents.find(temp)->second));

    // Return the converted value
    return static_cast<ValueType>(Val*Unit);
}
template double       ConfigFile::getValAndUnitOfKey<double>(const std::string&) const;
template unsigned int ConfigFile::getValAndUnitOfKey<unsigned int>(const std::string&) const;
template int          ConfigFile::getValAndUnitOfKey<int>(const std::string&) const;

template<class ValueType> std::enable_if_t<ConfigFile::is_vector<ValueType>::value, ValueType> ConfigFile::getValAndUnitOfKey(const std::string &key) const{

    std::string temp=key;
    lowerCase(temp);

    if (!keyExists(temp)){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Key not found -> " + temp + " in " + FileName);
    }

    // Extract the value and unit
    ValueType Val  = Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp);
    double Unit    = ConvertUnit(std::get<1>(Contents.find(temp)->second));

    // Convert vector elements
    for(unsigned int i=0;i<Val.size();++i){
        Val[i] *= Unit;
    }

    // Return the converted values
    return Val;
}
template std::vector<double>       ConfigFile::getValAndUnitOfKey<std::vector<double>>(const std::string &key) const;
template std::vector<unsigned int> ConfigFile::getValAndUnitOfKey<std::vector<unsigned int>>(const std::string &key) const;
template std::vector<int>          ConfigFile::getValAndUnitOfKey<std::vector<int>>(const std::string &key) const;
//@}


/** Returns the value and unit for the associated key from the map.
 * @tparam ValueType `double`, `int`, `unsigned int`, `std::vector<double>`, `std::vector<int>`, `std::vector<unsigned int>`.
 * @param[in] key a pointer to the string indicating the requested key.
 * @return Value and unit (if available) associated with the requested key. */
//@{
template<class ValueType> std::optional<std::enable_if_t<!ConfigFile::is_vector<ValueType>::value, ValueType>> ConfigFile::getValAndUnitOfOptKey(const std::string &key) const{

    std::string temp=key;
    lowerCase(temp);

    if (keyExists(temp)){

        // Extract the value and unit
        ValueType Val  = Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp);
        double Unit    = ConvertUnit(std::get<1>(Contents.find(temp)->second));

        // Return the converted value
        return static_cast<ValueType>(Val*Unit);
    }
    return std::nullopt;
}
template std::optional<double>       ConfigFile::getValAndUnitOfOptKey<double>(const std::string&) const;
template std::optional<unsigned int> ConfigFile::getValAndUnitOfOptKey<unsigned int>(const std::string&) const;
template std::optional<int>          ConfigFile::getValAndUnitOfOptKey<int>(const std::string&) const;

template<class ValueType> std::optional<std::enable_if_t<ConfigFile::is_vector<ValueType>::value, ValueType>> ConfigFile::getValAndUnitOfOptKey(const std::string &key) const{

    std::string temp=key;
    lowerCase(temp);

    if (keyExists(temp)){

        // Extract the value and unit
        ValueType Val  = Convert::string_to_T<ValueType>(FileName, std::get<0>(Contents.find(temp)->second), temp);
        double Unit    = ConvertUnit(std::get<1>(Contents.find(temp)->second));

        // Convert vector elements
        for(unsigned int i=0;i<Val.size();++i){
            Val[i] *= Unit;
        }

        // Return the converted values
        return Val;
    }
    return std::nullopt;
}
template std::optional<std::vector<double>>       ConfigFile::getValAndUnitOfOptKey<std::vector<double>>(const std::string &key) const;
template std::optional<std::vector<unsigned int>> ConfigFile::getValAndUnitOfOptKey<std::vector<unsigned int>>(const std::string &key) const;
template std::optional<std::vector<int>>          ConfigFile::getValAndUnitOfOptKey<std::vector<int>>(const std::string &key) const;
//@}

/** Return the conversion factor required to the input unit
 * @param[in] unit the name of the desired unit.
 * @return Conversion factor related to input units
 */
double ConfigFile::ConvertUnit(const std::string& unit) const{

    /* MARS uses: Oe, s, nm */

    if(unit==""){return 1.0;}
    /* Length */
    if(unit=="m") {return 1.0e9;}
    if(unit=="cm"){return 1.0e7;}
    if(unit=="mm"){return 1.0e6;}
    if(unit=="um"){return 1.0e3;}
/**/if(unit=="nm"){return 1.0;}
    /* Field */
    if(unit=="t") {return 1.0e4;}
/**/if(unit=="oe"){return 1.0;}
    /* Time */
    if(unit=="yr") {return 3.154e7;}
    if(unit=="mth"){return 2629756.8;}
    if(unit=="wk") {return 604800.0;}
    if(unit=="d")  {return 86400.0;}
    if(unit=="hr") {return 3600.0;}
    if(unit=="min"){return 60.0;}
/**/if(unit=="s")  {return 1.0;}
    if(unit=="ms") {return 1.0e-3;}
    if(unit=="us") {return 1.0e-6;}
    if(unit=="ns") {return 1.0e-9;}
    if(unit=="ps") {return 1.0e-12;}
    if(unit=="fs") {return 1.0e-15;}
    /* Speed */
    if(unit=="m/s") {return 1.0e9;}
/**/if(unit=="nm/s"){return 1.0;}
    /* Frequency */
/**/if(unit=="hz"){return 1.0;}
    if(unit=="kz"){return 1.0e3;}
    if(unit=="mz"){return 1.0e6;}
    if(unit=="Gz"){return 1.0e9;}
    if(unit=="Tz"){return 1.0e12;}
    /* Angle */
/**/if(unit=="rad"){return 1.0;}
    if(unit=="deg"){return M_PI_180;}
    return 1.0;
}