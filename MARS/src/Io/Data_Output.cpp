/*  Data_Output.cpp
 *  Created on: 29 Jun 2022
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include <optional>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <limits>

#include "Structures.hpp"
#include "Io/Data_Output.hpp"
#include "Io/Logger/Global_logger.hpp"
#include "Simulation_globals.hpp"

// TODO - Implement method to store K(T) and Ms(T) inside the grains whilst keeping K(0) and Ms(0)

Data_output_t::Data_output_t(const std::string& OutDir, const ConfigFile& cfg)
    : MainFileNamePrefix("MARS_output")
    , GrainFileNamePrefix("IdvGrain_")
    , Delimiter(" ")
    , FileExtension(".dat")
    , GrainOutNum(0)
    , MainFileName(MainFileNamePrefix+FileExtension)
    , GrainFileName(GrainFileNamePrefix+std::to_string(GrainOutNum)+FileExtension)
    , Width(10)
    , Precision(6)
    , Main_output(std::vector<OutFields>(0))
    , Grain_output(std::vector<OutFields>(0))
    , MainHeader(true)
    , GrainHeader(true)
    , GrainOutput(true)
    , MainOutCounter(0)
    , GrainOutCounter(0)
    , MainOutRate(std::numeric_limits<unsigned int>::max())
    , GrainOutRate(std::numeric_limits<unsigned int>::max())
    , OutFmt(Gnuplot)
    , GrainwVert(true)
    , Outputdirectory(OutDir)
{

    // Optional inputs
    std::optional<bool> InputBool;
    std::optional<unsigned int> InputUInt;
    std::optional<std::string> Inputstring;

    // --------------------------
    // Check for custom filenames
    // --------------------------
    Inputstring = cfg.getValueOfOptKey<std::string>("Output:MainFileName");
    if(Inputstring.has_value()){
        MainFileNamePrefix = Inputstring.value();
    }
    Inputstring = cfg.getValueOfOptKey<std::string>("Output:GrainFileName");
    if(Inputstring.has_value()){
        GrainFileNamePrefix = Inputstring.value();
    }

    // ------------------------------------------
    // Check for delimiter and set file extension
    // ------------------------------------------
    // TODO - Fix import of whitespace in cfg file. Currently all whitespace is removed before parsing
    Inputstring = cfg.getValueOfOptKey<std::string>("Output:Delimiter");
    if(Inputstring.has_value()){
        /* The delimiter requires single quotes (') to surround the
         * value as this prevents the configuration file importer from
         * flagging the input as missing a value (only whitespace after =).
         * So we must remove these quotes. */
        Delimiter = Inputstring.value();
        Delimiter.erase(remove( Delimiter.begin(), Delimiter.end(), '\'' ),Delimiter.end());
    }
    if(Delimiter==","){FileExtension=".csv";}
    else if(Delimiter=="\\t"){ // expect literal \t in input file. Need to convert this to a tab
        Delimiter="\t";
        FileExtension=".tsv";
    }
    else{FileExtension=".dat";}

    // -------------------------------
    // Check for specified fixed width
    // -------------------------------
    InputUInt = cfg.getValueOfOptKey<unsigned int>("Output:Width");
    if(InputUInt.has_value()){
        // Limit maximum width to 30
        if(InputUInt.value()>30){InputUInt=30;}
        Width = InputUInt.value();
    }

    // -----------------------------
    // Check for specified precision
    // -----------------------------
    InputUInt = cfg.getValueOfOptKey<unsigned int>("Output:Precision");
    if(InputUInt.has_value()){
        Precision = InputUInt.value();
    }

    // ------------------------------------------------
    // Check for desired output or Main and Grain files
    // ------------------------------------------------
    DesiredInput(cfg);

    // ------------------------
    // Get the output rates
    // ------------------------
    InputUInt = cfg.getValueOfOptKey<unsigned int>("Output:MainRate");
    if(InputUInt.has_value()){
        MainOutRate = InputUInt.value();
        if(MainOutRate>0){
            --MainOutRate;
        }
    }
    InputUInt = cfg.getValueOfOptKey<unsigned int>("Output:GrainRate");
    if(InputUInt.has_value()){
        GrainOutRate = InputUInt.value();
        if(GrainOutRate>0){
            --GrainOutRate;
        }
    }

    // ---------------------
    // Get the output format
    // ---------------------
    Inputstring = cfg.getValueOfOptKey<std::string>("Output:Format");
    if(Inputstring.has_value()){
        if(Inputstring=="jmp"){
            OutFmt = Format::JMP;
        }
        else{
            OutFmt = Format::Gnuplot;
        }
    }

    // --------------------
    // Are headers desired?
    // --------------------
    InputBool = cfg.getValueOfOptKey<bool>("Output:ColumnHeaders");
    if(InputBool.has_value()){
        if(InputBool.value()){
            MainHeader = InputBool.value();
            GrainHeader = MainHeader;
        }
    }

    // -----------------------------------
    // Is individual grain output desired?
    // -----------------------------------
    InputBool = cfg.getValueOfOptKey<bool>("Output:Individual_Grains");
    if(InputBool.has_value()){
        GrainOutput = InputBool.value();
    }

    // -----------------------------------
    // Do we want to output grain data per vertex or per grain?
    // -----------------------------------
    if(GrainOutput){
        InputBool = cfg.getValueOfOptKey<bool>("Output:Ouput_grain_vertices");
        if(InputBool.has_value()){
            GrainwVert = InputBool.value();
            Log.logandshow(Logger_t::LogLvl::INFO,"Vertex output "+std::to_string(GrainwVert));
        }
    }

    // -------------------------------------------------------------------------
    // Set the Filenames and open them as long as output data has been requested
    // -------------------------------------------------------------------------
    MainFileName  = MainFileNamePrefix+FileExtension;
    GrainFileName = GrainFileNamePrefix+std::to_string(GrainOutNum)+FileExtension;

    if(!Main_output.empty()){
        MainDataFile.open(Outputdirectory+"/"+MainFileName);
        Log.logandshow(Logger_t::LogLvl::INFO,"Created Main Output: "+Outputdirectory+"/"+MainFileName);
        if(!MainDataFile.is_open()){
            Log.log(Logger_t::LogLvl::ERR,"Output error: Cannot open "+Outputdirectory+"/"+MainFileName);
        }
    }
    if(GrainOutput && !Grain_output.empty()){
        GrainDataFile.open(Outputdirectory+"/"+GrainFileName);
        Log.logandshow(Logger_t::LogLvl::INFO,"Created Initial Grain Output: "+Outputdirectory+"/"+GrainFileName);
        if(!GrainDataFile.is_open()){
            Log.log(Logger_t::LogLvl::ERR,"Output error: Cannot open "+Outputdirectory+"/"+GrainFileName);
        }
    }
}

Data_output_t::~Data_output_t(){
    if(MainDataFile.is_open()){
        MainDataFile.close();
    }
    if(GrainDataFile.is_open()){
        GrainDataFile.close();
    }
}

void Data_output_t::DesiredInput(const ConfigFile& cfg){

    // Go through and check for every possible output

    std::optional<std::pair<bool,unsigned int>> InputBool;
    std::vector<std::pair<OutFields,unsigned int>> Output;

    // Extract field and linenum
    // Then use linenum to sort the list

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Magnetisation");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Magnetisation,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Layer_Magnetisation");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Layer_Magnetisation,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Magnetisation_Normalised");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Magnetisation_Normalised,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Layer_Magnetisation_Normalised");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Layer_Magnetisation_Normalised,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Magnetisation_Length");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Magnetisation_Length,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Layer_Magnetisation_Length");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Layer_Magnetisation_Length,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Easy_axis");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Easy_axis,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Temperature");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Temperature,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Anisotropy");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::K,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Anisotropy_StdDev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::K_stddev,InputBool.value().second));
        }
    }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Anisotropy_temperature");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Kt,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Anisotropy_temperature_StdDev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Kt_stddev,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Ms");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Ms,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Ms_StdDev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Ms_stddev,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Ms_temperature");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Mst,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Ms_temperature_StdDev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Mst_stddev,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Hk");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Hk,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Hk_StdDev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Hk_stddev,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Hk_temperature");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Hkt,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Hk_temperature_StdDev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Hkt_stddev,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Curie_Point");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Tc,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Curie_Point_Stddev");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Tc_stddev,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Field_applied");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::H_appl,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Field_anisotropy");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::H_ani,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Field_magneto");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::H_magneto,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Field_exchange");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::H_exchange,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Field_effective");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::H_eff,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Volume");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Vol,InputBool.value().second));
        }
    }

    InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Main:Time");
    if(InputBool.has_value()){
        if(InputBool.value().first){
            Output.push_back(std::make_pair(OutFields::Time,InputBool.value().second));
        }
    }

    // Sort Output and fill Main_output
    std::sort(Output.begin(), Output.end(), LineSort);
    for(const std::pair<OutFields,unsigned int> & Val : Output){
        Main_output.push_back(Val.first);
    }

    if(GrainOutput){

        // Ensure we don't include main output options in the grain output settings.
        Output.clear();

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Magnetisation");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Magnetisation,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Magnetisation_Normalised");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Magnetisation_Normalised,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Magnetisation_length");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Magnetisation_Length,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Easy_axis");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Easy_axis,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Temperature");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Temperature,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Anisotropy");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::K,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Anisotropy_temperature");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Kt,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Ms");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Ms,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Ms_temperature");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Mst,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Hk");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Hk,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Hk_temperature");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Hkt,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Curie_Point");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Tc,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Field_applied");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::H_appl,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Field_anisotropy");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::H_ani ,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Field_magneto");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::H_magneto,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Field_exchange");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::H_exchange,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Field_effective");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::H_eff,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Volume");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Vol,InputBool.value().second));
            }
        }

        InputBool = cfg.getValueOfOptKeyWLineNum<bool>("Output:Grain:Time");
        if(InputBool.has_value()){
            if(InputBool.value().first){
                Output.push_back(std::make_pair(OutFields::Time,InputBool.value().second));
            }
        }

        // Sort Output and fill Main_output
        std::sort(Output.begin(), Output.end(), LineSort);
        for(const std::pair<OutFields,unsigned int> & Val : Output){
            Grain_output.push_back(Val.first);
        }

    }

}


void Data_output_t::ChangeGrainFile(){

    // Can skip this if the previous file was never opened.
    if(GrainDataFile.is_open()){

        // Close the previous file, and generate the name for the next file.
        GrainDataFile.close();

        ++GrainOutNum;
        GrainFileName = GrainFileNamePrefix+std::to_string(GrainOutNum)+FileExtension;

        GrainDataFile.open(Outputdirectory+"/"+GrainFileName);
    }
}

bool Data_output_t::PrintNow(bool force/*=false*/){
    if(force || MainOutCounter==MainOutRate || (GrainOutput && GrainOutCounter==GrainOutRate) ){
        return true;
    }
    else{
        ++MainOutCounter;
        ++GrainOutCounter;
        return false;
    }
}

void Data_output_t::PrintData(const std::vector<Grain_t>& Grains, bool force/*=false*/){

    if(MainOutCounter==MainOutRate || force){

//        Log.logandshow(Logger_t::LogLvl::INFO,"Outputting Main Data...");
        std::stringstream FileStr;
        FileStr << std::setprecision(Precision);

        // Create the output stream
        DesiredOutput(Grains, FileStr, true);

        /* If writing headers we need to format them
         * For gnuplot # acts as a comment preventing headers from affecting parsing
         * We will also disable header output for the main file from now on.
         */
        if(MainHeader){
            if(OutFmt==Data_output_t::Format::Gnuplot){
                std::stringstream OutputStream;
                OutputStream << FileStr.rdbuf();
                FileStr << "# " << OutputStream.rdbuf();
            }
            MainHeader=false;
        }

        // Write the stream to the file
        FileOutput(FileStr,true);

        // Reset counter
        MainOutCounter=0;
//        Log.logandshow(Logger_t::LogLvl::INFO,"Complete.");

    }
    else{
        ++MainOutCounter;
    }

    if(GrainOutput && (GrainOutCounter==GrainOutRate || force)){

        // Need to disable the header after first output for EVERY grain file

        std::stringstream FileStr;
        FileStr << std::setprecision(Precision);

        // Perform Grain data output
        DesiredOutput(Grains, FileStr, false);

        // Write the stream to the file
        FileOutput(FileStr,false);

        // Create new grain file
        ChangeGrainFile();

        // Reset counter
        GrainOutCounter=0;
    }
    else{
        ++GrainOutCounter;
    }
}

void Data_output_t::FileOutput(const std::stringstream& OutStr, bool MainOut){

    if(MainOut){
        MainDataFile << OutStr.rdbuf() << "\n";
    }
    else{
        GrainDataFile << OutStr.rdbuf() << "\n";
    }

}

void Data_output_t::DesiredOutput(const std::vector<Grain_t>& Grains, std::stringstream& OutStr, bool MainOutput){

    // TODO - implement per layer output

    if(MainOutput){


        // Loop through all desired output fields and add corresponding output to the output stream
        for(const OutFields& Field : Main_output){

            switch (Field)
            {
            case Data_output_t::OutFields::Magnetisation:
            {
                double mx=0.0;
                double my=0.0;
                double mz=0.0;
                double ml=0.0;
                for(const Grain_t& Grain : Grains){
                    ml += sqrt(Grain.m.x*Grain.m.x+Grain.m.y*Grain.m.y+Grain.m.z*Grain.m.z);
                    mx += Grain.m.x;
                    my += Grain.m.y;
                    mz += Grain.m.z;
                }
                ml /= Grains.size();
                mx /= Grains.size();
                my /= Grains.size();
                mz /= Grains.size();

                output_value("m_x", mx, OutStr, MainHeader);
                output_value("m_y", my, OutStr, MainHeader);
                output_value("m_z", mz, OutStr, MainHeader);
                output_value("m_l", ml, OutStr, MainHeader);
                break;
            }
            case Data_output_t::OutFields::Layer_Magnetisation:
            {
                for (size_t l=0; l<Sim::NumLayers; ++l){

                    double mx=0.0;
                    double my=0.0;
                    double mz=0.0;
                    double ml=0.0;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mx += Grains[g].m.x;
                        my += Grains[g].m.y;
                        mz += Grains[g].m.z;
                        ml += sqrt(Grains[g].m.x*Grains[g].m.x+Grains[g].m.y*Grains[g].m.y+Grains[g].m.z*Grains[g].m.z);
                    }
                    mx /= Sim::GrainsperLayer;
                    my /= Sim::GrainsperLayer;
                    mz /= Sim::GrainsperLayer;
                    ml /= Sim::GrainsperLayer;

                    output_value("m_x", mx, OutStr, MainHeader);
                    output_value("m_y", my, OutStr, MainHeader);
                    output_value("m_z", mz, OutStr, MainHeader);
                    output_value("m_l", ml, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Magnetisation_Normalised:
            {
                double mx=0.0;
                double my=0.0;
                double mz=0.0;
                for(const Grain_t& Grain : Grains){
                    double ml = sqrt(Grain.m.x*Grain.m.x+Grain.m.y*Grain.m.y+Grain.m.z*Grain.m.z);
                    mx += Grain.m.x/ml;
                    my += Grain.m.y/ml;
                    mz += Grain.m.z/ml;
                }
                mx /= Grains.size();
                my /= Grains.size();
                mz /= Grains.size();

                output_value("m_x/m_l", mx, OutStr, MainHeader);
                output_value("m_y/m_l", my, OutStr, MainHeader);
                output_value("m_z/m_l", mz, OutStr, MainHeader);
                break;
            }
            case Data_output_t::OutFields::Layer_Magnetisation_Normalised:
            {
                // Obtain average for grains in layer
                for (size_t l=0; l<Sim::NumLayers; ++l){

                    double mx=0.0;
                    double my=0.0;
                    double mz=0.0;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        double One_o_ml = 1.0/(sqrt(Grains[g].m.x*Grains[g].m.x+Grains[g].m.y*Grains[g].m.y+Grains[g].m.z*Grains[g].m.z));
                        mx += Grains[g].m.x*One_o_ml;
                        my += Grains[g].m.y*One_o_ml;
                        mz += Grains[g].m.z*One_o_ml;
                    }
                    mx /= Sim::GrainsperLayer;
                    my /= Sim::GrainsperLayer;
                    mz /= Sim::GrainsperLayer;

                    output_value("m_x/m_l", mx, OutStr, MainHeader);
                    output_value("m_y/m_l", my, OutStr, MainHeader);
                    output_value("m_z/m_l", mz, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Magnetisation_Length:
            {
                double ml=0.0;
                for(const Grain_t& Grain : Grains){
                    ml += sqrt(Grain.m.x*Grain.m.x+Grain.m.y*Grain.m.y+Grain.m.z*Grain.m.z);
                }
                ml /= Grains.size();

                output_value("m_l", ml, OutStr, MainHeader);
                break;
            }
            case Data_output_t::OutFields::Layer_Magnetisation_Length:
            {
                for (size_t l=0; l<Sim::NumLayers; ++l){

                    double ml=0.0;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        ml += sqrt(Grains[g].m.x*Grains[g].m.x+Grains[g].m.y*Grains[g].m.y+Grains[g].m.z*Grains[g].m.z);
                    }
                    ml /= Sim::GrainsperLayer;

                    output_value("m_l", ml, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Easy_axis:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double ex=0.0;
                    double ey=0.0;
                    double ez=0.0;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        ex += Grains[g].Easy_axis.x;
                        ey += Grains[g].Easy_axis.y;
                        ez += Grains[g].Easy_axis.z;
                    }
                    ex /= Sim::GrainsperLayer;
                    ey /= Sim::GrainsperLayer;
                    ez /= Sim::GrainsperLayer;

                    output_value("EA_x", ex, OutStr, MainHeader);
                    output_value("EA_y", ey, OutStr, MainHeader);
                    output_value("EA_z", ez, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Temperature:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double Temp=0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        Temp += Grains[g].Temp;
                    }
                    Temp /= Sim::GrainsperLayer;

                    output_value("T", Temp, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::K:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double Anisotropy = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        Anisotropy += Grains[g].K;
                    }
                    Anisotropy /= Sim::GrainsperLayer;

                    output_value("K", Anisotropy, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::K_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean = 0.0;
                    double StdDev = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += Grains[g].K;
                    }
                    mean /= Sim::GrainsperLayer;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        StdDev += (Grains[g].K - mean)*(Grains[g].K - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;
                    StdDev = sqrt(StdDev);

                    output_value("K_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Kt:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double Anisotropy = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        Anisotropy += Grains[g].Kt();
                    }
                    Anisotropy /= Sim::GrainsperLayer;

                    output_value("K(T)", Anisotropy, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Kt_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean = 0.0;
                    double StdDev = 0.0;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += Grains[g].Kt();
                    }
                    mean /= Sim::GrainsperLayer;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        double Kt = Grains[g].Kt();
                        StdDev += (Kt - mean)*(Kt - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;
                    StdDev = sqrt(StdDev);
                    output_value("K(T)_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Ms:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double MSat=0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        MSat += Grains[g].Ms;
                    }
                    MSat /= Sim::GrainsperLayer;

                    output_value("Ms",MSat, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Ms_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean=0.0;
                    double StdDev = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += Grains[g].Ms;
                    }
                    mean /= Sim::GrainsperLayer;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        StdDev += (Grains[g].Ms - mean)*(Grains[g].Ms - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;
                    StdDev = sqrt(StdDev);
                    output_value("Ms_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Mst:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double MSat=0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        MSat += Grains[g].Mt();
                    }
                    MSat /= Sim::GrainsperLayer;

                    output_value("Ms(T)",MSat, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Mst_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean=0.0;
                    double StdDev = 0.0;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += Grains[g].Mt();
                    }
                    mean /= Sim::GrainsperLayer;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        double Mst = Grains[g].Mt();
                        StdDev += (Mst - mean)*(Mst - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;
                    StdDev = sqrt(StdDev);
                    output_value("Ms(T)_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Hk:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double Hk = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        Hk += 2.0*Grains[g].K/Grains[g].Ms;
                    }
                    Hk /= Sim::GrainsperLayer;
                    output_value("Hk", Hk, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Hk_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean = 0.0;
                    double StdDev = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += 2.0*Grains[g].K/Grains[g].Ms;
                    }
                    mean /= Sim::GrainsperLayer;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        StdDev += (2.0*Grains[g].K/Grains[g].Ms - mean)*(2.0*Grains[g].K/Grains[g].Ms - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;

                    output_value("Hk_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Hkt:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double Hk = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        Hk += 2.0*Grains[g].Kt()/Grains[g].Mt();
                    }
                    Hk /= Sim::GrainsperLayer;
                    output_value("Hk(T)", Hk, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Hkt_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean = 0.0;
                    double StdDev = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += 2.0*Grains[g].Kt()/Grains[g].Mt();
                    }
                    mean /= Sim::GrainsperLayer;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        StdDev += (2.0*Grains[g].Kt()/Grains[g].Mt() - mean)*(2.0*Grains[g].Kt()/Grains[g].Mt() - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;
                    output_value("Hk(T)_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Tc:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double Curie_point = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        Curie_point += Grains[g].Tc;
                    }
                    Curie_point /= Sim::GrainsperLayer;
                    output_value("Tc", Curie_point, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Tc_stddev:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double mean = 0.0;
                    double StdDev = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        mean += Grains[g].Tc;
                    }
                    mean /= Sim::GrainsperLayer;

                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        StdDev += (Grains[g].Tc - mean)*(Grains[g].Tc - mean);
                    }
                    StdDev /= Sim::GrainsperLayer;

                    output_value("Tc_stddev", StdDev, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::H_appl:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double HappX = 0.0;
                    double HappY = 0.0;
                    double HappZ = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        HappX += Grains[g].H_appl.x;
                        HappY += Grains[g].H_appl.y;
                        HappZ += Grains[g].H_appl.z;
                    }
                    HappX /= Sim::GrainsperLayer;
                    HappY /= Sim::GrainsperLayer;
                    HappZ /= Sim::GrainsperLayer;

                    output_value("Happl_x",HappX, OutStr, MainHeader);
                    output_value("Happl_y",HappY, OutStr, MainHeader);
                    output_value("Happl_z",HappZ, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::H_ani:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double HaniX = 0.0;
                    double HaniY = 0.0;
                    double HaniZ = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        HaniX += Grains[g].H_ani.x;
                        HaniY += Grains[g].H_ani.y;
                        HaniZ += Grains[g].H_ani.z;
                    }
                    HaniX /= Sim::GrainsperLayer;
                    HaniY /= Sim::GrainsperLayer;
                    HaniZ /= Sim::GrainsperLayer;

                    output_value("Hani_x",HaniX, OutStr, MainHeader);
                    output_value("Hani_y",HaniY, OutStr, MainHeader);
                    output_value("Hani_z",HaniZ, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::H_magneto:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double HmagX = 0.0;
                    double HmagY = 0.0;
                    double HmagZ = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        HmagX += Grains[g].H_magneto.x;
                        HmagY += Grains[g].H_magneto.y;
                        HmagZ += Grains[g].H_magneto.z;
                    }
                    HmagX /= Sim::GrainsperLayer;
                    HmagY /= Sim::GrainsperLayer;
                    HmagZ /= Sim::GrainsperLayer;

                    output_value("Hmag_x",HmagX, OutStr, MainHeader);
                    output_value("Hmag_y",HmagY, OutStr, MainHeader);
                    output_value("Hmag_z",HmagZ, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::H_exchange:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double HexchX = 0.0;
                    double HexchY = 0.0;
                    double HexchZ = 0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        HexchX += Grains[g].H_exchange.x;
                        HexchY += Grains[g].H_exchange.y;
                        HexchZ += Grains[g].H_exchange.z;
                    }
                    HexchX /= Sim::GrainsperLayer;
                    HexchY /= Sim::GrainsperLayer;
                    HexchZ /= Sim::GrainsperLayer;

                    output_value("Hexch_x",HexchX, OutStr, MainHeader);
                    output_value("Hexch_y",HexchY, OutStr, MainHeader);
                    output_value("Hexch_z",HexchZ, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::H_eff:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double HeffX = 0.0;
                    double HeffY = 0.0;
                    double HeffZ = 0.0;
                        for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        HeffX += Grains[g].H_eff.x;
                        HeffY += Grains[g].H_eff.y;
                        HeffZ += Grains[g].H_eff.z;
                    }
                    HeffX /= Sim::GrainsperLayer;
                    HeffY /= Sim::GrainsperLayer;
                    HeffZ /= Sim::GrainsperLayer;

                    output_value("Heff_x",HeffX, OutStr, MainHeader);
                    output_value("Heff_y",HeffY, OutStr, MainHeader);
                    output_value("Heff_z",HeffZ, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Vol:
            {
                for(size_t l=0; l<Sim::NumLayers; ++l){
                    double v=0.0;
                    for(size_t g=Sim::GrainsperLayer*l; g<Sim::GrainsperLayer*(l+1); ++g){
                        v += Grains[g].Vol;
                    }
                    v /= Sim::GrainsperLayer;
                    v *= 1e-21;
                    output_value("Vol", v, OutStr, MainHeader);
                }
                break;
            }
            case Data_output_t::OutFields::Time:
            {
                output_value("Time", Sim::Time, OutStr, MainHeader);
                break;
            }
            }
        }
    }
    else{ // Individual grain output
        bool TmpGrainHeader=GrainHeader;
        std::stringstream OutBuffer;

        /* The output file is closed after each output so if we want headers
         * we need to write to the file twice. This loop enables us to perform
         * the required multiple writes. */
        for(int OutItr=0;OutItr<1;++OutItr){

            /* If we are outputting headers then we will set the iterator to -1 to force a second iteration of the loop
             * which will add the data to the output file. */
            size_t TotIter = Grains.size();
            if(TmpGrainHeader){
                TotIter = 1; // Only want to output a single header row
                --OutItr;
            }

            for(size_t g=0;g<TotIter;++g){
                std::stringstream GrainStream; // Store the grain data

                for(const OutFields& Field : Grain_output){

                    switch (Field)
                    {
                    case Data_output_t::OutFields::Layer_Magnetisation:
                    case Data_output_t::OutFields::Layer_Magnetisation_Length:
                    case Data_output_t::OutFields::Layer_Magnetisation_Normalised:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Magnetisation:
                    {
                        output_value("m_x",Grains[g].m.x, GrainStream, TmpGrainHeader);
                        output_value("m_y",Grains[g].m.y, GrainStream, TmpGrainHeader);
                        output_value("m_z",Grains[g].m.z, GrainStream, TmpGrainHeader);
                        output_value("m_l",sqrt(Grains[g].m.x*Grains[g].m.x+Grains[g].m.y*Grains[g].m.y+Grains[g].m.z*Grains[g].m.z), GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Magnetisation_Normalised:
                    {
                        double ml = sqrt(Grains[g].m.x*Grains[g].m.x+Grains[g].m.y*Grains[g].m.y+Grains[g].m.z*Grains[g].m.z);
                        output_value("m_x/m_l",Grains[g].m.x/ml, GrainStream, TmpGrainHeader);
                        output_value("m_y/m_l",Grains[g].m.y/ml, GrainStream, TmpGrainHeader);
                        output_value("m_z/m_l",Grains[g].m.z/ml, GrainStream, TmpGrainHeader);
                    }
                    case Data_output_t::OutFields::Magnetisation_Length:
                    {
                        output_value("m_l",sqrt(Grains[g].m.x*Grains[g].m.x+Grains[g].m.y*Grains[g].m.y+Grains[g].m.z*Grains[g].m.z), GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Easy_axis:
                    {
                        output_value("EA_x",Grains[g].Easy_axis.x, GrainStream, TmpGrainHeader);
                        output_value("EA_y",Grains[g].Easy_axis.y, GrainStream, TmpGrainHeader);
                        output_value("EA_z",Grains[g].Easy_axis.z, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Temperature:
                    {
                        output_value("T",Grains[g].Temp, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::K:
                    {
                        output_value("K",Grains[g].K, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::K_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Kt:
                    {
                        output_value("K(T)",Grains[g].Kt(), GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Kt_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Ms:
                    {
                        output_value("Ms",Grains[g].Ms, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Ms_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Mst:
                    {
                        output_value("Ms(T)",Grains[g].Mt(), GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Mst_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Hk:
                    {
                        output_value("Hk",2.0*Grains[g].K/Grains[g].Ms, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Hk_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Hkt:
                    {
                        output_value("Hk(T)",2.0*Grains[g].Kt()/Grains[g].Mt(), GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Hkt_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::Tc:
                    {
                        output_value("Tc",Grains[g].Tc, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Tc_stddev:
                    {
                        break;
                    }
                    case Data_output_t::OutFields::H_appl:
                    {
                        output_value("Happl_x",Grains[g].H_appl.x, GrainStream, TmpGrainHeader);
                        output_value("Happl_y",Grains[g].H_appl.y, GrainStream, TmpGrainHeader);
                        output_value("Happl_z",Grains[g].H_appl.z, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::H_magneto:
                    {
                        output_value("Hmagneto_x", Grains[g].H_magneto.x, GrainStream, TmpGrainHeader);
                        output_value("Hmagneto_y", Grains[g].H_magneto.y, GrainStream, TmpGrainHeader);
                        output_value("Hmagneto_z", Grains[g].H_magneto.z, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::H_exchange:
                    {
                        output_value("Hexch_x", Grains[g].H_exchange.x, GrainStream, TmpGrainHeader);
                        output_value("Hexch_y", Grains[g].H_exchange.y, GrainStream, TmpGrainHeader);
                        output_value("Hexch_z", Grains[g].H_exchange.z, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::H_ani:
                    {
                        output_value("Hani_x", Grains[g].H_ani.x, GrainStream, TmpGrainHeader);
                        output_value("Hani_y", Grains[g].H_ani.y, GrainStream, TmpGrainHeader);
                        output_value("Hani_z", Grains[g].H_ani.z, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::H_eff:
                    {
                        output_value("Heff_x", Grains[g].H_eff.x, GrainStream, TmpGrainHeader);
                        output_value("Heff_y", Grains[g].H_eff.y, GrainStream, TmpGrainHeader);
                        output_value("Heff_z", Grains[g].H_eff.z, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Vol:
                    {
                        output_value("Vol", Grains[g].Vol*1e-21, GrainStream, TmpGrainHeader);
                        break;
                    }
                    case Data_output_t::OutFields::Time:
                    {
                        output_value("Time",Sim::Time, GrainStream, TmpGrainHeader);
                        break;
                    }
                    }
                }

                if(GrainwVert){
                    /* Here we output the data for each vertex of the grain
                        * By doing it this way we only have to collect the data for each grain once
                        * instead of for each vertext of each grain. */
                    for(size_t v=0; v<=Grains[g].Vertices.size(); ++v){
                        size_t w = v % Grains[g].Vertices.size();
                        std::stringstream AppOutBuff; // Used to append GrainID and vertices to output

                        // Add # to the header line for gnuplot formatting
                        if(TmpGrainHeader){
                            if(OutFmt==Data_output_t::Format::Gnuplot){
                                AppOutBuff <<"#";
                            }
                        }

                        output_value("GrainID", g, AppOutBuff, TmpGrainHeader);
                        output_value("Vx", Grains[g].Vertices[w].first, AppOutBuff, TmpGrainHeader);
                        output_value("Vy", Grains[g].Vertices[w].second, AppOutBuff, TmpGrainHeader);
                        AppOutBuff << GrainStream.str() << "\n";
                        OutBuffer << AppOutBuff.rdbuf();
                        // We only want to write one line if we are writing headers
                        if(TmpGrainHeader){break;}
                    }
                    // For Gnuplot formatting we separate the grains with two blank lines
                    if(!TmpGrainHeader){
                        switch (OutFmt)
                        {
                        case Gnuplot:
                            OutBuffer << "\n\n";
                            break;
                        case JMP:
                            break;
                        }
                    }
                }
                else{   // We output a single value per grain
                    std::stringstream AppOutBuff; // Used to append GrainID to output
                    output_value("GrainID", g, AppOutBuff, TmpGrainHeader);
                    AppOutBuff << GrainStream.str() << "\n";
                    OutBuffer << AppOutBuff.rdbuf();
                }

            }
            // At this point the headers must have be written if desired
            TmpGrainHeader = false;
        }
        OutStr << OutBuffer.rdbuf();
    }
}

template <typename T>
void Data_output_t::output_value(const std::string& ValName, const T Val, std::stringstream& stream, bool header){

    if(header){
        stream << std::left << std::setw(Width) << ValName << Delimiter;
    }
    else{
        stream << std::left << std::setw(Width) << Val << Delimiter;
    }

}