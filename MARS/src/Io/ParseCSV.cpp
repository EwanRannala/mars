#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

#include "Io/ParseCSV.hpp"
#include "Io/Logger/Global_logger.hpp"


// TODO: Update to allow for specification of data types to read in from CSV
void ParseCSV(std::string FileName, std::vector<int> Cols, bool Headers, std::vector<std::vector<double>>& OutVec)
{
    OutVec.resize(Cols.size());

    std::ifstream ImportFile(FileName.c_str());
    if(!ImportFile.is_open())
    {
        Log.logandshow(Logger_t::LogLvl::ERR,"Failed to open "+FileName);
    }

    // Get number of columns in CSV file
    std::string line;
    std::getline(ImportFile,line);
    int NumCols = std::count(line.begin(), line.end(), ',')+1;
    if(NumCols==0)
    {
        Log.logandshow(Logger_t::LogLvl::ERR,FileName+" contains no columns");
    }

    for(int& i: Cols)
    {
        if(i>NumCols)
        {
            Log.logandshow(Logger_t::LogLvl::ERR,FileName+" contains "+std::to_string(NumCols)+" columns. Yet col "+std::to_string(i)+" was requested");
        }
        if(i<0)
        {
            if(i<-NumCols)
            {
                Log.logandshow(Logger_t::LogLvl::ERR,FileName+" contains "+std::to_string(NumCols)+" columns. Yet col "+std::to_string(i)+" was requested");
            }
            i += NumCols;
        }
    }
    ImportFile.seekg(0);

    unsigned int lineNum = 0;
    // Get the line from the file
    while(std::getline(ImportFile, line))
    {
        ++lineNum;
        // Skip headers
        if(Headers && lineNum==1){continue;}

        std::istringstream RowStream(line);
        std::string cell;

        // Iterate through each column and extract data from the required columns
        int colNum = 0;
        std::vector<std::vector<double>> RowData(Cols.size());
        std::vector<bool> ObtainedData(Cols.size(),false);
        bool ObtainedAllCols = false;
        while(std::getline(RowStream, cell, ',') && !ObtainedAllCols)
        {
            // Check if the current column is one we want
            for(size_t i=0; i<Cols.size(); ++i)
            {
                if(colNum == Cols[i])
                {
                    RowData[i].push_back(atof(cell.c_str()));
                    ObtainedData[i] = true;
                }
            }
            ++colNum;
            // If we have read in all desired columns then read in a new line
            if(std::all_of(ObtainedData.begin(), ObtainedData.end(), [](bool v) { return v; })){continue;}
        }

        // Copy RowData into data vector
        for(size_t i=0; i<OutVec.size(); ++i)
        {
            for(double v : RowData[i])
            {
                OutVec[i].push_back(v);
            }
        }

        size_t SmallestDataSet=std::numeric_limits<size_t>::max();
        for(size_t i=0; i<RowData.size(); ++i)
        {
            if(RowData[i].size()==0)
            {
                Log.logandshow(Logger_t::LogLvl::ERR,"No data imported for column "+to_string_exact(Cols[i])+" from input data file!");
            }
            else if(OutVec[i].size()<SmallestDataSet){SmallestDataSet = OutVec[i].size();}
        }

        // Check if we have unequally sized data entries for each column
        if(!std::all_of(OutVec.begin(), OutVec.end(), [SmallestDataSet](const std::vector<double>& OutVec){return OutVec.size() == SmallestDataSet;}))
        {
            // Reduce all data sets to match the length of the smallest
            for(size_t i=0; i<OutVec.size(); ++i)
            {
                OutVec[i].resize(SmallestDataSet);
            }
            // Now stop reading the file as we have reached the end of a required data column
            break;
        }

    }
}