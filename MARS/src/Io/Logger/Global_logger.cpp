/* Global_logger.cpp
 *  Created on: 24 Feb 2022
 *      Author: Samuel Ewan Rannala
 */

/** \file Global_logger.cpp
 * \brief Source file for global logger definition. */

#include "Io/Logger/Global_logger.hpp"

Logger_t Log;             //!< Logging class

