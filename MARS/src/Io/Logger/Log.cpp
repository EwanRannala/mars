/*
 *  Logger.cpp
 *
 *  Created on: 23 Feb 2022
 */

#include "Io/Logger/Log.hpp"
#include <array>

const std::string Logger_t::SelfLvl ="[  INIT   ] ";

Logger_t::Logger_t()
    : LogLevel(LogLvl::INFO)
    , Delim("@")
{
}

Logger_t::~Logger_t(){
    if(Initialised){
        Logfile.close();
        Initialised=false;
    }
}

void Logger_t::Initialise(std::string ExecCmd, bool main/*="false"*/, LogLvl lvl/*=Logger_t::LogLvl::INFO*/){

    // Set Loglevel
    SetLevel(lvl);

    // We are creating the Logger for the executable
    if(main){
        // Extract the exectuable name from input string - expects this to be argv[0]
        #ifdef WINDOWS
            Exec = ExecCmd.erase(0, ExecCmd.find_last_of('\\')+1);
        #else
            Exec = ExecCmd.erase(0, ExecCmd.find_last_of('/')+1);
        #endif

        // Get hostname
        char loghostname [80];
        #ifdef WINDOWS
            DWORD sizehostname = 80;
            int GetHost=GetComputerName(loghostname, &sizehostname);
        #else
            int GetHost=gethostname(loghostname, 80);
        #endif
        if(GetHost!=0){
            std::cerr << BURed("Warning: Unable to retrieve hostname for logger.") << std::endl;
        }
        Hostname = loghostname;

        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::milliseconds now2 = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        struct tm currentLocalTime;
        localtime_r(&currentTime, &currentLocalTime);
        std::stringstream ss;
        ss << std::put_time(&currentLocalTime, "%Y%m%dT%H%M%S") << "_" << std::setw(3) << now2.count()%1000;
        TimeStr = ss.str();

        LogFileName = Exec+"_"+TimeStr+".log";
        Logfile.open(LogFileName);
        if(Logfile.is_open()){Initialised=true;}
        else{
            std::cerr << BURed("LOG ERROR: Could not open Log file. Exiting.") << std::endl;
            exit(EXIT_FAILURE);
        }
        // Set the log string to internal (only used by logger class itself).
        LvlStr = SelfLvl;

        Write("Logfile succesfully initiated"); // Error checking takes place within this function
        char Dir[512];
        #ifdef WINDOWS
            if(_getcwd(Dir,sizeof(Dir)) == NULL){
                std::cerr << "_wgetcwd error." << std::endl;
            }
        #else
            if(getcwd(Dir,sizeof(Dir)) == NULL){
                std::cerr << "getcwd error." << std::endl;
            }
        #endif
        Directory = Dir;

        Write("Executable: " + Exec);
        Write("Githash   : " + Githash);
        Write("Host name : " + Hostname);
        Write("Directory : " + Directory);
        switch (LogLevel)
        {
            case SILENT:
                Write("Log level : SILENT");
                break;
            case LogLvl::ERR:
                Write("Log level : ERROR ");
                break;
            case LogLvl::WARN:
                Write("Log level : WARN  ");
                break;
            case LogLvl::INFO:
                Write("Log level : INFO  ");
                break;
            case LogLvl::TIME:
                Write("Log level : TIME  ");
                break;
            case LogLvl::DEBUG:
                Write("Log level : DEBUG ");
                break;
            default:
                Write("Log level : UNKOWN");
                break;
        }
        Write("-----------------------------------------------");
    }
    else{ // We are creating a logger for another part of the code - WIP

        // ExecCmd is now treated as the name of the parent object using the logger
        Exec = ExecCmd;
        Hostname  = " N/A ";
        Directory = " N/A ";

        Logfile.open(Exec+".log");
        if(Logfile.is_open()){Initialised=true;}
        else{
            std::cerr << BURed("LOG ERROR: Could not open Log file. Exiting.") << std::endl;
            exit(EXIT_FAILURE);
        }
        Write("Logfile succesfully initiated"); // Error checking takes place within this function
        Write("This log is for " + Exec);

    }
}

void Logger_t::SetLevel(LogLvl Lvl){
    LogLevel = Lvl;
}

void Logger_t::SetLevelStr(LogLvl Lvl){
    switch (Lvl)
    {
        case SILENT:
            break;
        case LogLvl::ERR:
            LvlStr = "[  ERROR  ] ";
            break;
        case LogLvl::WARN:
            LvlStr = "[ WARNING ] ";
            break;
        case LogLvl::INFO:
            LvlStr = "[  INFO   ] ";
            break;
        case LogLvl::TIME:
            LvlStr = "[  TIME   ] ";
            break;
        case LogLvl::DEBUG:
            LvlStr = "[  DEBUG  ] ";
            break;
        default:
            LvlStr = "[         ] ";
            break;
    }
}

void Logger_t::log(LogLvl MessageLvl,std::string Message){

    // Only write message if the warning is at or below the desired level
    if(LogLevel>=MessageLvl){
        SetLevelStr(MessageLvl);
        Write(Message);
    }
    if(MessageLvl==LogLvl::ERR){
        // Unrecoverable error - exit
        std::cerr << BURed("ERROR: Unrecoverable error.") << std::endl;
        std::cerr << BUYraw << "\t"+Message << RSTraw <<std::endl;
        std::cerr << BURed("Please see logfile. Exiting.") << std::endl;
        exit(EXIT_FAILURE);
    }
}

    // I want this function to be able to do the same as log but also output to the terminal
    // May need to include the ability to strip formatting from the input string before calling
    // the log function.
void Logger_t::logandshow(LogLvl MessageLvl,std::string Message){

    if(LogLevel<MessageLvl){return;}

    std::string FormattedMessage=Message;
    std::string StrippedMessage=Message;
    size_t pos=Message.find(Delim,0);
    std::vector<size_t> vPos;

    // Find all occurances of the delimiter
    while(pos != std::string::npos){
        vPos.emplace_back(pos);
        pos = Message.find(Delim,pos+1);
    }

    if(vPos.size()%2==0){   // Delimiters must come in pairs

        for(size_t c=vPos.size()-1;c<std::numeric_limits<std::size_t>::max();c-=2){ // Extract substring
            // Starting pos = vPos[c-1]
            // Length = (vPos[c]-vPos[c-1]))+1
            std::string sub = Message.substr(vPos[c-1],(vPos[c]-vPos[c-1])+1);

            if(sub==BUR){sub=BURraw;}
            else if(sub==RST){sub=RSTraw;}
            else if(sub==BUY){sub=BUYraw;}
            else if(sub==BG){sub=BGraw;}
            else if(sub==BC){sub=BCraw;}
            else if(sub==BY){sub=BYraw;}
            // If we don't match the string then just leave it as it is
            // Replace substring
            FormattedMessage.replace(vPos[c-1],(vPos[c]-vPos[c-1])+1,sub);
            // Remove for output to log
            StrippedMessage.erase(vPos[c-1],(vPos[c]-vPos[c-1])+1);
        }
    }
    else{
        std::cerr << BURed("LOG ERROR: Malformed Message. Uneven number of Delimiters") << std::endl;
        std::cerr << BUY << "\t" << Message << std::endl;
        std::cerr << BURed("Exiting.") << std::endl;
        exit(EXIT_FAILURE);
    }

    // Print message to terminal
    std::cout << FormattedMessage << std::endl;
    // Print message to log
    log(MessageLvl,StrippedMessage);
}

void Logger_t::Write(std::string output){

    if(Initialised){
        if(LogLevel==SILENT){
            return;
        }
        // Get time
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::milliseconds now2 = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        struct tm currentLocalTime;
        localtime_r(&currentTime, &currentLocalTime);
        std::stringstream ss;
        ss << std::put_time(&currentLocalTime, "%Y-%b-%d %T (%z) ");
        TimeStr = ss.str();

        // Insert TimeStr at every occurance of \n in output
        std::string NewLine="\n";
        size_t pos=output.find(NewLine,0);
        std::vector<size_t> vPos;
        while(pos != std::string::npos){
            vPos.emplace_back(pos);
            pos = output.find(NewLine,pos+1);
        }
        // Inserting changes the size of the string so it is best to insert from end to start
        for(auto it = vPos.rbegin(); it != vPos.rend(); ++it){
            output.insert(*it+1,std::string().append(LvlStr).append(TimeStr));
        }

        Logfile << LvlStr << TimeStr << output << std::endl;
    }
    else{
        std::cerr << BURed("LOG ERROR: Log not initialised. Exiting.") << std::endl;
        exit(EXIT_FAILURE);
    }

}

std::string Logger_t::Name() const{
    return LogFileName;
}

template <typename T>
std::string to_string_exact(T x) {
    std::ostringstream os;
    os << std::setprecision(std::numeric_limits<T>::max_digits10-1) << x;
    return os.str();
}

std::string to_string_exact(double x) {
    std::ostringstream os;
    os << std::setprecision(8) << x;
    return os.str();
}

template std::string to_string_exact(bool );
template std::string to_string_exact(char );
template std::string to_string_exact(signed char );
template std::string to_string_exact(unsigned char );
template std::string to_string_exact(wchar_t );
template std::string to_string_exact(char16_t );
template std::string to_string_exact(char32_t );
template std::string to_string_exact(short );
template std::string to_string_exact(unsigned short );
template std::string to_string_exact(int );
template std::string to_string_exact(unsigned int );
template std::string to_string_exact(long );
template std::string to_string_exact(unsigned long );
template std::string to_string_exact(long long );
template std::string to_string_exact(unsigned long long );
template std::string to_string_exact(float );
template std::string to_string_exact(long double );

