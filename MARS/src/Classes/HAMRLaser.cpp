/*
 * HAMR_laser.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file HAMRLaser.cpp
 * @brief Definitions of HAMR_Laser class methods. */

#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <optional>
#include "Classes/HAMRLaser.hpp"

/** the constructor initialises the class using data from the configuration file. */
HAMR_Laser::HAMR_Laser(const ConfigFile&cfg):
    Temp_Min(300.0),
    Temp_Cur(0.0),
    Type(Laser_t::gaussiandouble)
{
    std::optional<double> Tmin = cfg.getValueOfKey<double>("Laser:Min_temperature");
    if(Tmin.has_value()){Temp_Min=Tmin.value();}
    Temp_Max = cfg.getValueOfKey<double>("Laser:Max_temperature");
    Cooling_time = cfg.getValAndUnitOfKey<double>("Laser:Cooling_time");
    std::optional<std::string> Laser_type = cfg.getValueOfOptKey<std::string>("Laser:Type");
    if(Laser_type.has_value()){
        if(Laser_type.value()=="linear"){Type = Laser_t::Linear;}
        if(Laser_type.value()=="double-gaussian"){Type = Laser_t::gaussiandouble;}
        if(Laser_type.value()=="gaussian"){Type = Laser_t::gaussian;}
    }
}
/** Update laser temperature and apply pulse to all grains within the system.
 *
 * @param[in] Time Current time w.r.t. start to laser pulse.
 * @param[out] Grains The temperature value is updated for all grains.
 */
void HAMR_Laser::ApplyT(const double Time, std::vector<Grain_t>& Grains){

    switch (Type)
    {
        case Laser_t::Linear:
            std::cout << "Linear temperature pulse not yet implemented " << std::endl;
            exit(1);
        break;
        case Laser_t::gaussian:
            Temp_Cur = Temp_Min + (getPeakTemp()-Temp_Min)*exp(-(Time)*(Time)/((getCoolingTime())*(getCoolingTime())));
        break;
        case Laser_t::gaussiandouble:
            Temp_Cur = Temp_Min + (getPeakTemp()-Temp_Min)*exp( -pow( ((Time-3*getCoolingTime())/getCoolingTime()),2.0 ) );
        break;
    }

    for(auto & Grain : Grains){
        Grain.Temp = Temp_Cur;
    }
}

/** Sets the CoolingTime. */
void HAMR_Laser::setCoolingTime(double value){
    Cooling_time = value;
}

/** Returns current temperature of the laser. */
double HAMR_Laser::getCurTemp() const {return Temp_Cur;}
/** Returns maximum laser temperature. */
double HAMR_Laser::getPeakTemp() const {return Temp_Max;}
/** Returns cooling time of the laser (This is one sixth the total pulse time. */
double HAMR_Laser::getCoolingTime() const {return Cooling_time;}
/** Returns entire duration of the laser pulse. */
double HAMR_Laser::getProfileTime() const {return 6.0*Cooling_time;}
