/*
 * ReadHead.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadHead.cpp
 * @brief Definitions of ReadHead class methods. */

#include<iostream>

#include "Classes/ReadHead.hpp"

/** The constructor initialises the class using the data already present it the RecLayer class.*/
ReadHead::ReadHead(const RecLayer& RecordingLayer)
    : PosX(0.0)
    , PosY(0.0)
    , Sensitvity_type(SensType::Box)
    , SizeX(RecordingLayer.getBitSizeX()) // old method would set SizeX = 1 Pixel
    , SizeY(RecordingLayer.getBitSizeY())
    , sX(0)
    , sY(0)
    , Normalise(false)
    , Resolution(0)
    , Sfunc(0)
{
}

/** The constructor initialises using RecLayer data or the configuration file. */
ReadHead::ReadHead(const RecLayer& RecordingLayer, const ConfigFile& cfg)
    : PosX(0.0)
    , PosY(0.0)
    , Sensitvity_type(SensType::Box)
    , SizeX(RecordingLayer.getBitSizeX()) // old method would set SizeX = 1 Pixel
    , SizeY(RecordingLayer.getBitSizeY())
    , sX(0)
    , sY(0)
    , Normalise(false)
    , Resolution(0)
    , Sfunc(0)
{
    auto Custom = cfg.getValueOfOptKey<bool>("ReadHead:CustomSettings");
    if(Custom.value_or(false)){
        auto NormTmp = cfg.getValueOfOptKey<bool>("ReadHead:Normalise");
        if(NormTmp.value_or(false)){
            Normalise=true;
        }
        std::string tmp = cfg.getValueOfKey<std::string>("ReadHead:Sensitivity_function");
        if(tmp!="box" && tmp!="gaussian"){
            Log.log(Logger_t::LogLvl::ERR, "CFG error: Read head sensitivity function selection invalid");
            exit(EXIT_FAILURE);
        }
        else{
            if(tmp=="box"){Sensitvity_type=SensType::Box;}
            else if(tmp=="gaussian"){Sensitvity_type=SensType::Gauss;}
        }

        switch (Sensitvity_type)
        {
        case SensType::Box:
            SizeX=cfg.getValAndUnitOfKey<double>("ReadHead:SizeX");
            SizeY=cfg.getValAndUnitOfKey<double>("ReadHead:SizeY");
            break;
        case SensType::Gauss:
            // sigma = FWHM/(2*sqrt(2*ln(2))) = FWHM/2.354820045030949 = FWHM*0.4246609001440095
            sX=cfg.getValAndUnitOfKey<double>("ReadHead:FWHMx")*0.4246609001440095;
            sY=cfg.getValAndUnitOfKey<double>("ReadHead:FWHMy")*0.4246609001440095;
            break;
        case SensType::Custom:
            Log.log(Logger_t::LogLvl::ERR,"CFG error: Read head custom sensitivity is not yet implemented");
        }
    }
}

/** The constructor initialises the class using the data from the configuration file.
 * The read head needs to know the sizes of the bits and tracks in order to average the
 * magnetisation over the correct size to create the data signal.
 */
ReadHead::ReadHead(const ConfigFile& cfg)
    : PosX(0.0)
    , PosY(0.0)
    , Sensitvity_type(SensType::Gauss)
    , SizeX(0.0)
    , SizeY(0.0)
    , sX(0)
    , sY(0)
    , Normalise(false)
    , Resolution(0)
    , Sfunc(0)
{
    auto NormTmp = cfg.getValueOfOptKey<bool>("ReadHead:Normalise");
    if(NormTmp.value_or(false)){
        Normalise=true;
    }
    std::string tmp = cfg.getValueOfKey<std::string>("ReadHead:Sensitivity_function");
    if(tmp!="box" && tmp!="gaussian"){
        Log.log(Logger_t::LogLvl::ERR, "CFG error: Read head sensitivity function selection invalid");
        exit(EXIT_FAILURE);
    }
    else{
        if(tmp=="box"){Sensitvity_type=SensType::Box;}
        else if(tmp=="gaussian"){Sensitvity_type=SensType::Gauss;}
    }

    switch (Sensitvity_type)
    {
    case SensType::Box:
        SizeX=cfg.getValAndUnitOfKey<double>("ReadHead:SizeX");
        SizeY=cfg.getValAndUnitOfKey<double>("ReadHead:SizeY");
        break;
    case SensType::Gauss:
        // sigma = FWHM/(2*sqrt(2*ln(2))) = FWHM/2.354820045030949 = FWHM*0.4246609001440095
        sX=cfg.getValAndUnitOfKey<double>("ReadHead:FWHMx")*0.4246609001440095;
        sY=cfg.getValAndUnitOfKey<double>("ReadHead:FWHMy")*0.4246609001440095;
        break;
    case SensType::Custom:
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Read head custom sensitivity is not yet implemented");
    }


}

void ReadHead::Create_S_function(SensType type, double Res){
    Resolution=Res;
    unsigned int NumY = std::ceil(SizeY/Resolution);
    unsigned int NumX = std::ceil(SizeX/Resolution);
    Sfunc.resize(NumY,std::vector<double>(NumX,0.0));

    switch (type)
    {
    case SensType::Box:
        for(std::vector<double> & y : Sfunc){
            for(double & x : y){
                x=1.0;
            }
        }
        break;
    default:
        Log.log(Logger_t::LogLvl::ERR,"Sfunc not yet defined. Exiting.");
        exit(EXIT_FAILURE);
    }
}

/** Returns the size of the read head in the x-dimension. */
double ReadHead::getSizeX() const {return SizeX;}
/** Returns the size of the read head in the y-dimension. */
double ReadHead::getSizeY() const {return SizeY;}
/** Returns the positions of the read head in the x-dimension. */
double ReadHead::getPosX() const {return PosX;}
/** Returns the positions of the read head in the y-dimension. */
double ReadHead::getPosY() const {return PosY;}
/** Returns the geomtrical size each value in the sensitivity function covers. */
double ReadHead::getRes() const {return Resolution;}
/** Returns the type of the sensitivity function. */
ReadHead::SensType ReadHead::getSensitivityFunction() const {return Sensitvity_type;}
/** Returns if the signal should be normalised w.r.t. the number of pixels. */
bool ReadHead::Norm() const {return Normalise;}
/** Sets the position of the read head in the x-direction. */
void ReadHead::setPosX(double x){PosX = x;}
/** sets the position of the read head in the y-position. */
void ReadHead::setPosY(double y){PosY = y;}
/** Returns value of sensitivity function at specified coordinates w.r.t. the centre. */
double ReadHead::ApplySfunc(const double x, const double y) const {

    double dispX = x - PosX;
    double dispY = y - PosY;

    switch (Sensitvity_type)
    {
        case SensType::Box:
            if( (fabs(dispX)<=(SizeX*0.5)) && (fabs(dispY)<=(SizeY*0.5)) ){
                return 1.0;
            }
            return 0.0;
        case SensType::Gauss:
            return exp( -((dispX*dispX)/(2.0*sX*sX)) - ((dispY*dispY)/(2.0*sY*sY)) );
        case SensType::Custom:
            // Here we want to use a pixel map to determine the sensitivity function.
            return 0.0;
    }
    return 0.0;
} 
