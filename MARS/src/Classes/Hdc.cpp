/*
 * Hdc.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Hdc.cpp
 * @brief Definitions of Hdc class methods. */

#define _USE_MATH_DEFINES
#include <cmath>
#include <limits>
#include "Classes/Hdc.hpp"
#include "Globals.hpp"
/** The constructor initialises the class using the data from the configuration file.
 * the field is always initialised at the minimum strength in the off state.
 *
 * @param[in] cfg Configuration file data.
 */
Hdc::Hdc(const ConfigFile&cfg)
    : state(HdcConditions::min)
    , On(false)
    , CurTime(0.0)
    , mag(0.0)
    , SwitchTimer(std::numeric_limits<double>::max())
{
        std::vector<double> dirn_tmp = cfg.getValueOfKey<std::vector<double>>("FieldDC:Unit_vector");
        double tmpMag = sqrt(dirn_tmp[0]*dirn_tmp[0]+dirn_tmp[1]*dirn_tmp[1]+dirn_tmp[2]*dirn_tmp[2]);
        dirn.x = dirn_tmp[0]/tmpMag;
        dirn.y = dirn_tmp[1]/tmpMag;
        dirn.z = dirn_tmp[2]/tmpMag;

        min  = cfg.getValAndUnitOfKey<double>("FieldDC:Minimum_strength");
        max  = cfg.getValAndUnitOfKey<double>("FieldDC:Maximum_strength");
        overshootAmp = fabs(cfg.getValAndUnitOfKey<double>("FieldDC:Overshoot_strength"));
        maxTotal = max+overshootAmp;
        if(min>max){
            Log.log(Logger_t::LogLvl::ERR, "HDC error: Minimum field is greater than maximum.");
        }
        else if(maxTotal-min>1e-5){
            rise_time = cfg.getValAndUnitOfKey<double>("FieldDC:Ramp_time");
            if(rise_time==0){ // Instantaneous field change does not allow for overshoots.
                RTrate=0.0;
                overshootDur=0.0;
                OSrate=0.0;
                maxTotal=max;
            }
            else{
                if(maxTotal-max>1e-5){
                    overshootDur = cfg.getValAndUnitOfKey<double>("FieldDC:Overshoot_Duration");
                    OSrate = (maxTotal-max)/overshootDur;
                }
                else{
                    overshootDur=0.0;
                    OSrate=0.0;
                }
                RTrate = (maxTotal-min)/rise_time;
            }
        }
        else{
            rise_time = 1.0;
            RTrate = 0.0;
            overshootDur = 0.0;
            OSrate = 0.0;
        }

        updateField(0.0);
    }
/** Updates the field's magnitude and 3-vector.
 *
 * @param[in] dt Timestep
 */
void Hdc::updateField(double dt){
     updateTime(dt);
     TimedSwitch();
     updateMag(dt);
     field=dirn*mag;
 }
/** Toggles the fields power state. */
void Hdc::Switch(){
    On=!On;
}
/** Turns the field on. */
 void Hdc::turnOn(){
     On=true;
 }
/** Turns the field off. */
void Hdc::turnOff(){
    On=false;
}
/** Set field to turn on for specified duration. */
void Hdc::turnOnfor(const double Timer){
    On=true;
    setTimer(Timer);
    CurTime=0.0;
}
/** Set field to turn off for specified duration. */
void Hdc::turnOfffor(const double Timer){
    On=false;
    setTimer(Timer);
    CurTime=0.0;
}
/** Set the internal timer. */
void Hdc::setTimer(const double Timer){
    SwitchTimer = Timer;
}
/** Performs switch based on internal timer. */
void Hdc::TimedSwitch(){
    if(CurTime>=SwitchTimer){
        Switch();
        CurTime=0.0;
        SwitchTimer=std::numeric_limits<double>::max();
    }
}
/** Updates the internal time of the field. */
void Hdc::updateTime(const double dt){
    CurTime += dt;
}
/** Updates the field state based on the current magnitude. */
void Hdc::updateState(){
    switch(state)
    {
    case HdcConditions::min:
        if(On){state = HdcConditions::increasing;}
        break;
    case HdcConditions::increasing:
        if(mag>=maxTotal){
            if(overshootDur>0.0){
                state = HdcConditions::overshootDec;
            }
            else{
                state = HdcConditions::max;
            }
        }
        break;
    case HdcConditions::overshootDec:
        if(mag<=max){state = HdcConditions::max;}
        break;
    case HdcConditions::max:
        if(!On){state = HdcConditions::decreasing;}
        break;
    case HdcConditions::decreasing:
        if(mag<=min){state = HdcConditions::min;}
        break;
    }
}
/** Updates the fields magnitude according to the current state.
 *
 * @param[in] dt Timestep
 */
void Hdc::updateMag(double dt){
    switch(state)
    {
    case HdcConditions::min:
        mag=min;
        break;
    case HdcConditions::max:
        mag=max;
        break;
    case HdcConditions::increasing:
        if(RTrate==0){mag=max;}
        else{mag += dt*RTrate;}
        break;
    case HdcConditions::decreasing:
        if(RTrate==0){mag=min;}
        else{mag -= dt*RTrate;}
        break;
    case HdcConditions::overshootDec:
        // Overshoot is not possible when rate==0;
        mag -= dt*OSrate;
        break;
    }
    updateState();
}
/** Sets the field direction vector.
 *
 * @param[in] Direction Field direction vector.
 */
 void Hdc::setDirn(Vec3 Direction){
    if(Direction.x!=0.0 || Direction.y!=0.0 || Direction.z!=0.0){
        Direction = Direction/sqrt(Direction*Direction);
    }
    dirn = Direction;
 }

// TODO update setters to set the correct rate based on new field strengths.

/** Sets the field's maximum strength.
 *
 * @param[in] Maximum field strength.
 */
void Hdc::setMax(double Maximum){
    max = Maximum;
}
/** Sets the field's minimum strength.
 *
 * @param[in] Minimum field strength.
 */
void Hdc::setMin(double Minimum){
    min = Minimum;
}
/** Sets the field's overshoot amplitude.
 *
 * @param[in] Overshoot field strength.
 */
void Hdc::setOvershoot(double Overshoot){
    overshootDur = Overshoot;
}
/** Returns the Field's 3-vector. */
Vec3 Hdc::getField() const {return field;}
/** Returns the field's magnitude. */
double Hdc::getMag() const {return mag;}
/** Returns the field's current state. */
HdcConditions Hdc::getState() const {return state;}
/** Returns the field's direction vector. */
Vec3 Hdc::getDirn() const {return dirn;}
/** Returns the fields minimum strength. */
double Hdc::getMin() const {return min;}
/** Returns the fields maximum strength. */
double Hdc::getMax() const {return max;}
/** Return the overshoot of the field. */
double Hdc::getOvershoot() const {return overshootAmp;}
/** Returns the time required to switch between field strengths. */
double Hdc::getRampTime() const {return rise_time;}
/** Returns the rate of change of the field (inverse of ramp_time). */
double Hdc::getRate() const {return RTrate;}
/** Returns boolean indicating if the field is on. */
bool Hdc::isOn() const {return On;}
