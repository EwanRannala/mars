/*
 * Solver_kMC.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** \file Solver_kMC.cpp
 * \brief kMC solver function, used to set up and call kMC core. */

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <string>

#include "Classes/Solver.hpp"
/** Wrapper function for the kMC numerical solver. This function handles determination of the inclusion zone
 * and grain parameters (e.g. Exchange and magnetostatic fields, etc.).
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Interac   Magnetic interactions data structure.
 * @param[in] VORO      Voronoi data structure.
 * @param[in] Centre_x Centre of integration on a-axis (provides position for inclusion zone).
 * @param[in] Centre_y Centre of integration on y-axis (provides position for inclusion zone).
 * @param[out] Grain Grains data structure.
 */
int Solver_t::KMC(const unsigned int Num_Layers, const Interaction_t& Interac,
                const Voronoi_t& VORO, std::vector<Grain_t>& Grains){

    unsigned int Tot_grains=VORO.Num_Grains*Num_Layers;
    m_EQ.resize(Tot_grains);

    // Determine Ms(t) and K(t)
    std::vector<double> K_t(Tot_grains,0.0);
    std::vector<double> Ms_t(Tot_grains,0.0);
    Chi_para.resize(Tot_grains);
    Chi_perp.resize(Tot_grains);

/* ####################### WORKING WITH INCLUSION ZONES #######################
 * Due to the inclusion zone, there are multiple arrays to loop through.
 * There are two inclusion zone related arrays "Included_grains_in_layer"
 * and "Included_grains_in_system" for grains per layer and per system.
 * The other two arrays are for the entire system, again per layer and per
 * system.
 * All loops APART from the anisotropy loop only require iteration through
 * "Included_grains_in_system" as there is no difference in behaviour between
 * layers.
 * The anisotropy loop requires iteration through "Included_grains_in_layer" as
 * the ani_method can change from layer to layer.
 * In all loops there must be a conversion from the inclusion zone arrays to
 * the general system arrays, in order to access grain values correctly.
 *
 * This can get confusing so to try and make things less difficult the
 * iterators are names as so:
 *
 * G_in_sys_in_incl - Only grains within inclusion zone but for ALL layers
 *
 * G_in_layer_in_incl - Only grains within inclusion zone but for a single
 *                      layers
 *
 * grain_in_sys - Grains in the entire system for ALL layers
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    if(!T_variation){    // Case where values are constant with temperature.
        for(unsigned int grain_in_sys=0; grain_in_sys<Tot_grains;++grain_in_sys){
            K_t[grain_in_sys]  = Grains[grain_in_sys].K;
            Ms_t[grain_in_sys] = Grains[grain_in_sys].Ms;
            std::fill(m_EQ.begin(), m_EQ.end(), 1.0);
        }
    }
    else{    // Need to temperature determine parameters.
        // mEQ and Ms
        for(unsigned int grain_in_sys=0; grain_in_sys<Tot_grains;++grain_in_sys){
            m_EQ[grain_in_sys] = Grains[grain_in_sys].mEQ();
            Ms_t[grain_in_sys] = Grains[grain_in_sys].Mt();
        }
        // Anisotropy - required for grains in inclusion zone only.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset = Integratable_grains_in_layer*Layer;
            for(unsigned int G_in_layer_in_incl=0;G_in_layer_in_incl<Integratable_grains_in_layer;++G_in_layer_in_incl){
                unsigned int G_in_sys_in_incl = G_in_layer_in_incl+Offset;
                unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
                K_t[G_in_sys_in_incl] = Grains[grain_in_sys].Kt();
            }
        }
    }

//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        /* The W matrix is ordered with decreasing granularity. Grains first then subsegments and finally segments.
         * Due to the order we must keep access updated based on previous access. */
        size_t WmatPos=0;
        Grains[grain_in_sys].H_magneto=0.0;
        // Neighbouring grains
        while(WmatPos<Interac.Magneto_neigh_list_grains[grain_in_sys].size()){
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_grains[grain_in_sys][WmatPos];
            Grains[grain_in_sys].H_magneto.x += Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            Grains[grain_in_sys].H_magneto.y += Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            Grains[grain_in_sys].H_magneto.z += Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            ++WmatPos;
        }
        // Calculates mean field for truncation method
        for(size_t p=0;p<Interac.MFWxx[grain_in_sys].size();++p){
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.Magneto_MeanField_grains[grain_in_sys]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.MFWxx[grain_in_sys][p]*mx+
                                                               Interac.MFWxy[grain_in_sys][p]*my+
                                                               Interac.MFWxz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.MFWxy[grain_in_sys][p]*mx+
                                                               Interac.MFWyy[grain_in_sys][p]*my+
                                                               Interac.MFWyz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.MFWxz[grain_in_sys][p]*mx+
                                                               Interac.MFWyz[grain_in_sys][p]*my+
                                                               Interac.MFWzz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
        }
        // Neighbouring Subsegments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_subseg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_subseg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.subsegments[NEIGHBOUR_ID]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
        }

        //Neighbouring Segments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_seg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_seg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            unsigned int count=0;
            for(unsigned int subseg : Interac.segments[NEIGHBOUR_ID]){
                for(unsigned int grain : Interac.subsegments[subseg]){
                    mx += Grains[grain].m.x*m_EQ[grain];
                    my += Grains[grain].m.y*m_EQ[grain];
                    mz += Grains[grain].m.z*m_EQ[grain];
                    ++count;
                }
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/count;
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/count;
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/count;
        }
    }
    // Exchange
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].H_exchange = 0.0;
        for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_sys].size();neigh++){
            unsigned int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_sys][neigh];
            Grains[grain_in_sys].H_exchange+=Interac.H_exch_str[grain_in_sys][neigh]*Grains[NEIGHBOUR_ID].m*m_EQ[NEIGHBOUR_ID];
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].H_eff = Grains[grain_in_incl].H_appl+Grains[grain_in_sys].H_magneto+Grains[grain_in_sys].H_exchange;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    for(unsigned int G_in_sys_in_incl=0;G_in_sys_in_incl<Integratable_grains_in_system;++G_in_sys_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain

        double Hk = (2.0*K_t[G_in_sys_in_incl])/Ms_t[G_in_sys_in_incl];
        double KV_over_kBT = (K_t[G_in_sys_in_incl]*Grains[grain_in_sys].Vol*1.0e-21)/(KB*Grains[grain_in_sys].Temp);
        double MsVHk_over_kBT = (Ms_t[G_in_sys_in_incl]*Grains[grain_in_sys].Vol*1.0e-21*Hk)/(KB*Grains[grain_in_sys].Temp);
        KMC_core(Grains[grain_in_sys].Easy_axis,(Grains[grain_in_sys].H_eff/Hk),KV_over_kBT,MsVHk_over_kBT,Grains[grain_in_sys].m);
        // Output magnetisation values with correct lengths if desired -- allows for easier transfer between KMC and LLB solvers
        if(M_length_variation){
            Grains[grain_in_sys].m *= m_EQ[G_in_sys_in_incl];
        }
    }
    return 0;
}