/*
 * Solver_LLG.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** \file Solver_LLG.cpp
 * \brief Function for Landau-Lifshitz-Gilbert solver
 */

#include <iostream>
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <unordered_set>


#include "Classes/Solver.hpp"

/** This function acts on all grains within the system.
 * <P>
 * This function performs a single time step using the LLG equation of motion. This model requires normalisation of the magnetisation thus
 * it breaks down when \f$T \xrightarrow{} T_c\f$ as the ferromagnetic ordering is lost.
 * As a result any simulations which require high temperatures must use the LLB solver instead. <BR>
 * The LLG solver is useful for temperature independent simulations as it utilises larger time steps than the LLB thus reducing computation
 * time. <BR>
 * The maximum possible time step is \f$3ps\f$, however \f$1ps\f$ is the most reliable. (See LLG time step test). <BR>
 * <P> The equation of motion is:
 * \f[
 *         \frac{\partial{\vec{m}^i}}{\partial{t}} = -\frac{\gamma_e}{1+\alpha^2} \left(\vec{m}^i \times \vec{H_{eff}}^i \right)-\frac{\alpha\gamma_e}{M_s\left(1+\alpha^2\right)} \vec{m}^i \times \left(\vec{m}^i \times \vec{H_{eff}}^i \right)
 * \f]
 * \f$\gamma_e\f$ is the electron gyromagnetic ratio. \f$|\gamma_e|=1.760859644\times10^7 \frac{rad}{Oes}\f$. <BR>
 * \f$\alpha\f$ is the damping constant. <BR>
 * \f$M_{s}\f$ is the saturation magnetisation in \f$\frac{emu}{cc}\f$. <BR>
 * \f$H_{eff}\f$ is the effective field in Oe, which accounts for thermal effects, interactions and applied fields. <BR>
 * \f$\vec{m}\f$ is the magnetisation unit vector. <BR>
 * </P>
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Interac Magnetic interactions data structure.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Centre_x Centre of integration on a-axis (provides position for inclusion zone).
 * @param[in] Centre_y Centre of integration on y-axis (provides position for inclusion zone).
 * @param[out] Grain Grains data structure.
 *
 */
int Solver_t::LLG(const unsigned int Num_Layers, const Interaction_t& Interac,
                const Voronoi_t& VORO, std::vector<Grain_t>& Grains){

    // TODO implement the Ml variation (use a normalisation here to fix m<1).
    // TODO add a notification that LLG will not use Chi if LLG and Chi are selected
    double One_o_dt = 1.0/dt_LLG;
    std::normal_distribution<double> Gauss(0.0,1.0);
    unsigned int Tot_grains=VORO.Num_Grains*Num_Layers;
    m_EQ.resize(Tot_grains);

    // Internal variables
    std::vector<double> H_ani (Integratable_grains_in_system,0.0);
    std::vector<Vec3> M_initial (Integratable_grains_in_system);
    std::vector<Vec3> dmdt1 (Integratable_grains_in_system);
    std::vector<Vec3> dmdt2 (Integratable_grains_in_system);
    std::vector<Vec3> Therm_field (Integratable_grains_in_system);

    //-- READ IN MAGNETISATION DATA
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        M_initial[grain_in_incl] = Grains[grain_in_sys].m;
    }

    // Determine Ms(t) and K(t)
    std::vector<double> K_t(Tot_grains,0.0);
    std::vector<double> Ms_t(Tot_grains,0.0);
    Chi_para.resize(Tot_grains);
    Chi_perp.resize(Tot_grains);
    Susceptibilities(VORO.Num_Grains, Num_Layers, Included_grains_in_layer, Grains);

//################################################################# DETERMINE TEMPERATURE DEPENDENT PARAMETERS ##########################################################//
    if(!Inclusion){
        // mEQ
        if(!T_variation){        // Case where experimental values are used. - NO Ms(t) and K(t)
            std::fill(m_EQ.begin(), m_EQ.end(), 1.0);
        }
        else{
            // Determine mEQ for all grains
            for(unsigned int grain_in_sys=0; grain_in_sys<Tot_grains;++grain_in_sys){
                m_EQ[grain_in_sys] = Grains[grain_in_sys].mEQ();
            }
        }
        // K_t and Ms_t
        // Determine K,Ms,mEQ for all grain in zone.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset(VORO.Num_Grains*Layer);
            for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_layer;++grain_in_incl){
                unsigned int grain_in_sys = Included_grains_in_layer[grain_in_incl]+Offset;

                if(!T_variation){        // Case where experimental values are used. - NO Ms(t) and K(t)
                    K_t[grain_in_sys]  = Grains[grain_in_sys].K;
                    Ms_t[grain_in_sys] = Grains[grain_in_sys].Ms;
                }
                else{
                    K_t[grain_in_sys]  = Grains[grain_in_sys].Kt();
                    Ms_t[grain_in_sys] = Grains[grain_in_sys].Mt();
                }
            }
        }
    }
    else{
        std::unordered_set<unsigned int> Neigh_grains; // Keep track of grains required for integration step
        // Determine K,Ms,mEQ for all grain in zone.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset(VORO.Num_Grains*Layer);
            for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_layer;++grain_in_incl){
                unsigned int grain_in_sys = Included_grains_in_layer[grain_in_incl]+Offset;
                Neigh_grains.emplace(grain_in_sys);

                if(!T_variation){        // Case where experimental values are used. - NO Ms(t) and K(t)
                    K_t[grain_in_sys] = Grains[grain_in_sys].K;
                    Ms_t[grain_in_sys]= Grains[grain_in_sys].Ms;
                    m_EQ[grain_in_sys]=1.0;
                }
                else{
                    m_EQ[grain_in_sys] = Grains[grain_in_sys].mEQ();
                    K_t[grain_in_sys]  = Grains[grain_in_sys].Kt();
                    Ms_t[grain_in_sys] = Grains[grain_in_sys].Mt();
                }
            }
        }
        // Second determine mEQ for all meighbours of the simulated grains
        for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

            for(unsigned int NEIGHBOUR_ID : Interac.Magneto_neigh_list_grains[grain_in_sys]){
                // Place Neighbour ID into set. If successful then determine mEQ for that neighbour.
                if(Neigh_grains.emplace(NEIGHBOUR_ID).second){
                    if(!T_variation){
                        m_EQ[NEIGHBOUR_ID]=1.0;
                    }
                    else{
                        m_EQ[NEIGHBOUR_ID] = Grains[NEIGHBOUR_ID].mEQ();
                    }
                }
            }
            // Exchange
            for(unsigned int NEIGHBOUR_ID : Interac.Exchange_neigh_list[grain_in_sys]){
                // Place Neighbour ID into set. If successful then determine mEQ for that neighbour.
                if(Neigh_grains.emplace(NEIGHBOUR_ID).second){
                    if(!T_variation){
                        m_EQ[NEIGHBOUR_ID]=1.0;
                    }
                    else{
                        m_EQ[NEIGHBOUR_ID] = Grains[NEIGHBOUR_ID].mEQ();
                    }
                }
            }
        }
    }
//################################################## DETERMINE STOCHASTIC TERMS #########################################################################################//
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Integratable_grains_in_layer*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
            unsigned int grain_in_incl = grain_in_layer+Offset;
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

            double RNG_MAG = sqrt( ((2.0*Alpha[Layer]*KB*Grains[grain_in_sys].Temp)/(Gamma*Ms_t[grain_in_incl]*(Grains[grain_in_sys].Vol*1e-21)))*One_o_dt );

            Therm_field[grain_in_incl].x = Gauss(Gen)*RNG_MAG;
            Therm_field[grain_in_incl].y = Gauss(Gen)*RNG_MAG;
            Therm_field[grain_in_incl].z = Gauss(Gen)*RNG_MAG;

            H_ani[grain_in_incl] = (2.0*K_t[grain_in_incl])/Ms_t[grain_in_incl];
        }
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        double MdotE=0.0;
        switch (Grains[grain_in_sys].Ani_method)
        {
            case Material_t::AniMethods::Callen:
                MdotE = (Grains[grain_in_sys].m*Grains[grain_in_sys].Easy_axis);
                Grains[grain_in_sys].H_eff = Grains[grain_in_sys].H_appl+H_ani[grain_in_incl]*MdotE*Grains[grain_in_sys].Easy_axis;
            break;
            case Material_t::AniMethods::Chi:
            const double Chi_perp_inv = 1.0 / Chi_perp[grain_in_sys];
                Grains[grain_in_sys].H_eff.x = Grains[grain_in_sys].H_appl.x-Grains[grain_in_sys].m.x*Chi_perp_inv;
                Grains[grain_in_sys].H_eff.y = Grains[grain_in_sys].H_appl.y-Grains[grain_in_sys].m.y*Chi_perp_inv;
                Grains[grain_in_sys].H_eff.z = Grains[grain_in_sys].H_appl.z;
            break;
        }
    }
    /* Magnetostatics
     * Segmentation will determine grains first, then subsegments and finally segments
     * If another method is in use then only the loop over grains is used.
     */
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        /* The W matrix is ordered with decreasing granularity. Grains first then subsegments and finally segments.
         * Due to the order we must keep access updated based on previous access. */
        size_t WmatPos=0;
        Grains[grain_in_sys].H_magneto=0.0;
        // Neighbouring grains
        while(WmatPos<Interac.Magneto_neigh_list_grains[grain_in_sys].size()){
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_grains[grain_in_sys][WmatPos];
            Grains[grain_in_sys].H_magneto.x += Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            Grains[grain_in_sys].H_magneto.y += Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            Grains[grain_in_sys].H_magneto.z += Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            ++WmatPos;
        }
        // Calculates mean field for truncation method
        for(size_t p=0;p<Interac.MFWxx[grain_in_sys].size();++p){
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.Magneto_MeanField_grains[grain_in_sys]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.MFWxx[grain_in_sys][p]*mx+
                                                               Interac.MFWxy[grain_in_sys][p]*my+
                                                               Interac.MFWxz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.MFWxy[grain_in_sys][p]*mx+
                                                               Interac.MFWyy[grain_in_sys][p]*my+
                                                               Interac.MFWyz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.MFWxz[grain_in_sys][p]*mx+
                                                               Interac.MFWyz[grain_in_sys][p]*my+
                                                               Interac.MFWzz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
        }
        // Neighbouring Subsegments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_subseg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_subseg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.subsegments[NEIGHBOUR_ID]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
        }

        //Neighbouring Segments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_seg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_seg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            unsigned int count=0;
            for(unsigned int subseg : Interac.segments[NEIGHBOUR_ID]){
                for(unsigned int grain : Interac.subsegments[subseg]){
                    mx += Grains[grain].m.x*m_EQ[grain];
                    my += Grains[grain].m.y*m_EQ[grain];
                    mz += Grains[grain].m.z*m_EQ[grain];
                    ++count;
                }
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/count;
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/count;
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/count;
        }

    }
    // Exchange
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].H_exchange = 0.0;
        for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_sys].size();neigh++){
            unsigned int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_sys][neigh];
            Grains[grain_in_sys].H_exchange+=Interac.H_exch_str[grain_in_sys][neigh]*Grains[NEIGHBOUR_ID].m*m_EQ[NEIGHBOUR_ID];
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].H_eff+=Grains[grain_in_sys].H_magneto+Grains[grain_in_sys].H_exchange+Therm_field[grain_in_incl];
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### FIRST HEUN STEP ###########################################################################//
    // Loop over layers as Alpha is a single value per layer
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        double alp=Alpha[Layer];
        double alpSQ=alp*alp;
        unsigned int Offset = Integratable_grains_in_layer*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
            unsigned int grain_in_incl = grain_in_layer+Offset;
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

            // Vec3 MxH   = Grains[grain_in_sys].m%Grains[grain_in_sys].H_eff;
            double MxH_x = (Grains[grain_in_sys].m.y*Grains[grain_in_sys].H_eff.z - Grains[grain_in_sys].m.z*Grains[grain_in_sys].H_eff.y);
            double MxH_y = (Grains[grain_in_sys].m.z*Grains[grain_in_sys].H_eff.x - Grains[grain_in_sys].m.x*Grains[grain_in_sys].H_eff.z);
            double MxH_z = (Grains[grain_in_sys].m.x*Grains[grain_in_sys].H_eff.y - Grains[grain_in_sys].m.y*Grains[grain_in_sys].H_eff.x);

            // Vec3 MxMxH = Grains[grain_in_sys].m%MxH;
            double MxMxH_x = (Grains[grain_in_sys].m.y*MxH_z - Grains[grain_in_sys].m.z*MxH_y);
            double MxMxH_y = (Grains[grain_in_sys].m.z*MxH_x - Grains[grain_in_sys].m.x*MxH_z);
            double MxMxH_z = (Grains[grain_in_sys].m.x*MxH_y - Grains[grain_in_sys].m.y*MxH_x);

            // dmdt1[grain_in_incl] = (-Gamma/(1.0+Alpha[Layer]*Alpha[Layer]))*(MxH+Alpha[Layer]*MxMxH);
            dmdt1[grain_in_incl].x = (-Gamma/(1.0+alpSQ))*(MxH_x+alp*MxMxH_x);
            dmdt1[grain_in_incl].y = (-Gamma/(1.0+alpSQ))*(MxH_y+alp*MxMxH_y);
            dmdt1[grain_in_incl].z = (-Gamma/(1.0+alpSQ))*(MxH_z+alp*MxMxH_z);

            //Grains[grain_in_sys].m += dmdt1[grain_in_incl]*dt_LLG;
            Grains[grain_in_sys].m.x += dmdt1[grain_in_incl].x*dt_LLG;
            Grains[grain_in_sys].m.y += dmdt1[grain_in_incl].y*dt_LLG;
            Grains[grain_in_sys].m.z += dmdt1[grain_in_incl].z*dt_LLG;
            // Normalise
            double MagM = sqrt(Grains[grain_in_sys].m*Grains[grain_in_sys].m);
            Grains[grain_in_sys].m /= MagM;
        }
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        double MdotE=0.0;
        switch (Grains[grain_in_sys].Ani_method)
        {
            case Material_t::AniMethods::Callen:
                MdotE = (Grains[grain_in_sys].m*Grains[grain_in_sys].Easy_axis);
                Grains[grain_in_sys].H_eff = Grains[grain_in_sys].H_appl+H_ani[grain_in_incl]*MdotE*Grains[grain_in_sys].Easy_axis;
            break;
            case Material_t::AniMethods::Chi:
                const double Chi_perp_inv = 1.0 / Chi_perp[grain_in_sys];
                Grains[grain_in_sys].H_eff.x = Grains[grain_in_sys].H_appl.x-Grains[grain_in_sys].m.x*Chi_perp_inv;
                Grains[grain_in_sys].H_eff.y = Grains[grain_in_sys].H_appl.y-Grains[grain_in_sys].m.y*Chi_perp_inv;
                Grains[grain_in_sys].H_eff.z = Grains[grain_in_sys].H_appl.z;
            break;
        }
    }
    /* Magnetostatics
     * Segmentation will determine grains first, then subsegments and finally segments
     * If another method is in use then only the loop over grains is used.
     */
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        /* The W matrix is ordered with decreasing granularity. Grains first then subsegments and finally segments.
         * Due to the order we must keep access updated based on previous access. */
        size_t WmatPos=0;
        Grains[grain_in_sys].H_magneto=0.0;
        // Neighbouring grains
        while(WmatPos<Interac.Magneto_neigh_list_grains[grain_in_sys].size()){
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_grains[grain_in_sys][WmatPos];
            Grains[grain_in_sys].H_magneto.x += Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            Grains[grain_in_sys].H_magneto.y += Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            Grains[grain_in_sys].H_magneto.z += Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            ++WmatPos;
        }
        // Calculates mean field for truncation method
        for(size_t p=0;p<Interac.MFWxx[grain_in_sys].size();++p){
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.Magneto_MeanField_grains[grain_in_sys]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.MFWxx[grain_in_sys][p]*mx+
                                                               Interac.MFWxy[grain_in_sys][p]*my+
                                                               Interac.MFWxz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.MFWxy[grain_in_sys][p]*mx+
                                                               Interac.MFWyy[grain_in_sys][p]*my+
                                                               Interac.MFWyz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.MFWxz[grain_in_sys][p]*mx+
                                                               Interac.MFWyz[grain_in_sys][p]*my+
                                                               Interac.MFWzz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
        }
        // Neighbouring Subsegments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_subseg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_subseg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.subsegments[NEIGHBOUR_ID]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
        }

        //Neighbouring Segments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_seg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_seg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            unsigned int count=0;
            for(unsigned int subseg : Interac.segments[NEIGHBOUR_ID]){
                for(unsigned int grain : Interac.subsegments[subseg]){
                    mx += Grains[grain].m.x*m_EQ[grain];
                    my += Grains[grain].m.y*m_EQ[grain];
                    mz += Grains[grain].m.z*m_EQ[grain];
                    ++count;
                }
            }
            Grains[grain_in_sys].H_magneto.x += (Ms_t[grain_in_sys]*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/count;
            Grains[grain_in_sys].H_magneto.y += (Ms_t[grain_in_sys]*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/count;
            Grains[grain_in_sys].H_magneto.z += (Ms_t[grain_in_sys]*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/count;
        }

    }
    // Exchange
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].H_exchange = 0.0;
        for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_sys].size();neigh++){
            unsigned int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_sys][neigh];
            Grains[grain_in_sys].H_exchange+=Interac.H_exch_str[grain_in_sys][neigh]*Grains[NEIGHBOUR_ID].m*m_EQ[NEIGHBOUR_ID];
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].H_eff+=Grains[grain_in_sys].H_magneto+Grains[grain_in_sys].H_exchange+Therm_field[grain_in_incl];
    }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### SECOND HEUN STEP ##########################################################################//
        // Loop over layers as Alpha is a single value per layer
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        double alp=Alpha[Layer];
        double alpSQ=alp*alp;
        unsigned int Offset = Integratable_grains_in_layer*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
            unsigned int grain_in_incl = grain_in_layer+Offset;
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

            // Vec3 MxH   = Grains[grain_in_sys].m%Grains[grain_in_sys].H_eff;
            double MxH_x = (Grains[grain_in_sys].m.y*Grains[grain_in_sys].H_eff.z - Grains[grain_in_sys].m.z*Grains[grain_in_sys].H_eff.y);
            double MxH_y = (Grains[grain_in_sys].m.z*Grains[grain_in_sys].H_eff.x - Grains[grain_in_sys].m.x*Grains[grain_in_sys].H_eff.z);
            double MxH_z = (Grains[grain_in_sys].m.x*Grains[grain_in_sys].H_eff.y - Grains[grain_in_sys].m.y*Grains[grain_in_sys].H_eff.x);

            // Vec3 MxMxH = Grains[grain_in_sys].m%MxH;
            double MxMxH_x = (Grains[grain_in_sys].m.y*MxH_z - Grains[grain_in_sys].m.z*MxH_y);
            double MxMxH_y = (Grains[grain_in_sys].m.z*MxH_x - Grains[grain_in_sys].m.x*MxH_z);
            double MxMxH_z = (Grains[grain_in_sys].m.x*MxH_y - Grains[grain_in_sys].m.y*MxH_x);

            // dmdt2[grain_in_incl] = (-Gamma/(1.0+Alpha[Layer]*Alpha[Layer]))*(MxH+Alpha[Layer]*MxMxH);
            dmdt2[grain_in_incl].x = (-Gamma/(1.0+alpSQ))*(MxH_x+alp*MxMxH_x);
            dmdt2[grain_in_incl].y = (-Gamma/(1.0+alpSQ))*(MxH_y+alp*MxMxH_y);
            dmdt2[grain_in_incl].z = (-Gamma/(1.0+alpSQ))*(MxH_z+alp*MxMxH_z);

            // Grains[grain_in_sys].m = M_initial[grain_in_incl] + (dmdt1[grain_in_incl]+dmdt2[grain_in_incl])*dt_LLG*0.5;
            Grains[grain_in_sys].m.x = M_initial[grain_in_incl].x + (dmdt1[grain_in_incl].x+dmdt2[grain_in_incl].x)*dt_LLG*0.5;
            Grains[grain_in_sys].m.y = M_initial[grain_in_incl].y + (dmdt1[grain_in_incl].y+dmdt2[grain_in_incl].y)*dt_LLG*0.5;
            Grains[grain_in_sys].m.z = M_initial[grain_in_incl].z + (dmdt1[grain_in_incl].z+dmdt2[grain_in_incl].z)*dt_LLG*0.5;

            // Normalise
            double MagM = sqrt(Grains[grain_in_sys].m*Grains[grain_in_sys].m);
            Grains[grain_in_sys].m /= MagM;

            if(M_length_variation){
                Grains[grain_in_sys].m *= m_EQ[grain_in_sys];
            }
        }
    }
    return 0;
}
