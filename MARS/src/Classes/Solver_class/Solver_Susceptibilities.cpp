/*
 * Solver_Susceptibilities.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Solver_Susceptibilities.cpp
 * \brief This function determines the susceptibility for use in the LLB solver. */

#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <string>
#include <vector>

#include "Classes/Solver.hpp"

/** Multiple fitting methods are available within this function. The desired fit is selected in
 *  the material configuration file with the <B>Mat:susceptibility_type</B> parameter. <BR>
 *  The available options are: "default", "kazantseva_susceptibility_fit",
 *  "vogler_scusceptibility_fit" or "inv_scusceptibility_fit".<BR>
 *    <B>If none of these options are found the function will always use the Kazantseva fitting method.</B><BR>
 *  <P>
 *  The kazantseva fitting method originates from "Dynamic response of the magnetisation to picosecond
 *  heat pulses" Natalia Kazantseva 2008 <BR>
 *  The Susceptibilities are given by:
 *  \f[
 *      \tilde{\chi}_{\parallel} =\left \{
 *      \begin{aligned}
 *          a_0 + \sum_{i=1}^{9} &a_i(T_c-T)^i, && \text{if } T < T_c \\
 *             &b_0\frac{1}{4\pi}\frac{T}{T-T_c}, && \text{if } T > T_c
 *           \end{aligned} \right.
 *    \f]
 *    \f[
 *         \tilde{\chi}_{\bot} =\left \{
 *      \begin{aligned}
 *          a_0 + \sum_{i=1}^{9} &a_i\bigg(1-\frac{T}{1.068T_c}\bigg)^i, && \text{if } T < 1.065T_c \\
 *          &b_0\frac{1}{4\pi}\frac{T}{T-T_c}, && \text{if } T > 1.065T_c
 *         \end{aligned} \right.
 *  \f]
 * <B>If</B><EM> "Default"</EM> is used the following parameters are applied:
 * <center>
 * | Parameter | \f$\chi_{\parallel}\f$ | \f$\chi_{\bot}\f$        |
 * | :-------: | :--------------------: | :----------------------: |
 * |\f$a_0\f$  |\f$2.11\times10^4\f$    |\f$1.21\times10^4\f$      |
 * |\f$a_1\f$  |\f$1.10\times10^6\f$    |\f$-2.20\f$               |
 * |\f$a_2\f$  |\f$-8.55\times10^6\f$   |\f$0.0\f$                 |
 * |\f$a_3\f$  |\f$3.42\times10^6\f$    |\f$1.95\times10^{-6}\f$   |
 * |\f$a_4\f$  |\f$-7.85\times10^7\f$   |\f$-1.3\times10^{-10}\f$  |
 * |\f$a_5\f$  |\f$1.03\times10^8\f$    |\f$0.0\f$                 |
 * |\f$a_6\f$  |\f$-6.86\times10^6\f$   |\f$-4.00\times10^{-16}\f$ |
 * |\f$a_7\f$  |\f$7.97\times10^6\f$    |\f$0.0\f$                 |
 * |\f$a_8\f$  |\f$1.54\times10^7\f$    |\f$0.0\f$                 |
 * |\f$a_9\f$  |\f$-6.27\times10^6\f$   |\f$-6.51\times10^{-25}\f$ |
 * |\f$b_0\f$  |\f$4.85\times10^4\f$    |\f$2.12\times10^4\f$      |
 * </center>
 * <EM>"kazantseva_susceptibility_fit", "vogler_scusceptibility_fit" <EM>and</EM>
 * "inv_scusceptibility_fit"</EM> options use parameters defined in the material configuration files.
 * </P>
 *  The Vogler fitting method originates from Vogler et al, J. Appl. Phys. 119, 223903 (2016) https://doi.org/10.1063/1.4953390 <BR>
 *  The Susceptibilities are given by:
 *  \f[
 *      \chi_{\parallel} =\left \{
 *      \begin{aligned}
 *          &\frac{a_0}{T_c-T}, && \text{if } T < T_c \\
 *             &\frac{b_0}{T-T_c}, && \text{if } T \geq T_c
 *           \end{aligned} \right.
 *    \f]
 *    \f[
 *         \chi_{\bot} =\left \{
 *      \begin{aligned}
 *          &a_0, && \text{if } T < T_c \\
 *          &b_0, && \text{if } T \geq T_c
 *         \end{aligned} \right.
 *  \f]
 * <P>
 *    The Inverse susceptibility fit originates from "Simulations of magnetic reversal properties in
 *    granular recording media" Matthew Ellis 2015<BR>
 *  The Susceptibilities are given by:
 *  \f[
 *      \frac{1}{\chi_{\parallel}} =\left \{
 *      \begin{aligned}
 *          a_0 + a_{\frac{1}{2}}\bigg(\frac{T_c-T}{T_c}\bigg)^{\frac{1}{2}} &+ \sum_{i=1}^{9} a_i\bigg(\frac{T_c-T}{T_c}\bigg)^i, && \text{if } T < T_c \\
 *          &b_0 + \sum_{i=1}^{4} b_i\bigg(\frac{T-T_c}{T_c}\bigg)^i, && \text{if } T \geq T_c
 *           \end{aligned} \right.
 *    \f]
 *    \f[
 *      \frac{1}{\chi_{\bot}} =\left \{
 *      \begin{aligned}
 *          a_0 + a_{\frac{1}{2}}\bigg(\frac{|T_c-T|}{Tc}\bigg)^{\frac{1}{2}} &+ \sum_{i=1}^{9} a_i\bigg(\frac{T_c-T}{T_c}\bigg)^i, && \text{if } T < T_c \\
 *          &b_0 + \sum_{i=1}^{4} b_i\bigg(\frac{T-T_c}{T_c}\bigg)^i, && \text{if } T \geq T_c
 *         \end{aligned} \right.
 *  \f]
 *    </P>
 *    <P>
 *    \param[in] Num_Grains Number of grains per layer.
 *    \param[in] Num_Layers Number of layers in the system.
 *    \param[in] Included_grains_in_layer List of grains this function will loop through.
 *    \param Grains List of all grains.
 */
int Solver_t::Susceptibilities(const unsigned int Num_Grains, const unsigned int Num_Layers,
                               const std::vector<unsigned int>& Included_grains_in_layer, const std::vector<Grain_t>& Grains){

    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Num_Grains*Layer; // Needs to be the number of grains per layer
        int Susceptibility_type_DUMMY = Susceptibility_Type[Layer];
        // Temporary variables to make code more clean
        double t_Chi_scaling_factor = Chi_scaling_factor[Layer];

        for(unsigned int grain_in_sys : Included_grains_in_layer){
            grain_in_sys += Offset;
            double Temperature = Grains[grain_in_sys].Temp;
            double Tc = Grains[grain_in_sys].Tc;

            double t_a0_PARA   = Ga0_PARA[grain_in_sys];
            double t_a1_PARA   = Ga1_PARA[grain_in_sys];
            double t_a2_PARA   = Ga2_PARA[grain_in_sys];
            double t_a3_PARA   = Ga3_PARA[grain_in_sys];
            double t_a4_PARA   = Ga4_PARA[grain_in_sys];
            double t_a5_PARA   = Ga5_PARA[grain_in_sys];
            double t_a6_PARA   = Ga6_PARA[grain_in_sys];
            double t_a7_PARA   = Ga7_PARA[grain_in_sys];
            double t_a8_PARA   = Ga8_PARA[grain_in_sys];
            double t_a9_PARA   = Ga9_PARA[grain_in_sys];
            double t_a1_2_PARA = Ga1_2_PARA[grain_in_sys];
            double t_b0_PARA   = Gb0_PARA[grain_in_sys];
            double t_b1_PARA   = Gb1_PARA[grain_in_sys];
            double t_b2_PARA   = Gb2_PARA[grain_in_sys];
            double t_b3_PARA   = Gb3_PARA[grain_in_sys];
            double t_b4_PARA   = Gb4_PARA[grain_in_sys];
            double t_a0_PERP   = Ga0_PERP[grain_in_sys];
            double t_a1_PERP   = Ga1_PERP[grain_in_sys];
            double t_a2_PERP   = Ga2_PERP[grain_in_sys];
            double t_a3_PERP   = Ga3_PERP[grain_in_sys];
            double t_a4_PERP   = Ga4_PERP[grain_in_sys];
            double t_a5_PERP   = Ga5_PERP[grain_in_sys];
            double t_a6_PERP   = Ga6_PERP[grain_in_sys];
            double t_a7_PERP   = Ga7_PERP[grain_in_sys];
            double t_a8_PERP   = Ga8_PERP[grain_in_sys];
            double t_a9_PERP   = Ga9_PERP[grain_in_sys];
            double t_a1_2_PERP = Ga1_2_PERP[grain_in_sys];
            double t_b0_PERP   = Gb0_PERP[grain_in_sys];
            double t_b1_PERP   = Gb1_PERP[grain_in_sys];
            double t_b2_PERP   = Gb2_PERP[grain_in_sys];
            double t_b3_PERP   = Gb3_PERP[grain_in_sys];
            double t_b4_PERP   = Gb4_PERP[grain_in_sys];


//####################################### DETERMINE THE FITTING METHOD #######################################//
            double Tc1p68(0.0);
            switch (Susceptibility_type_DUMMY)
            {
                case Material_t::SusTypes::Default:
                case Material_t::SusTypes::Kazantseva:
                //==================== If susceptibility is default or Kazantseva_susceptibility_fit ========================//
                    if(Temperature<Tc){
                        double Tc_m_T = Tc-Temperature;
                        Chi_para[grain_in_sys]  = t_a0_PARA * (Tc/(4.0*M_PI*Tc_m_T) )
                                                + t_a1_PARA * Tc_m_T
                                                + t_a2_PARA * Tc_m_T*Tc_m_T
                                                + t_a3_PARA * Tc_m_T*Tc_m_T*Tc_m_T
                                                + t_a4_PARA * Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T
                                                + t_a5_PARA * Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T
                                                + t_a6_PARA * Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T
                                                + t_a7_PARA * Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T
                                                + t_a8_PARA * Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T
                                                + t_a9_PARA * Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T*Tc_m_T;
                    }
                    else{
                        Chi_para[grain_in_sys] = t_b0_PARA * (Tc/(4.0*M_PI*(Temperature-Tc)) );
                    }
                    // Chi_para must not be a negative value.
                    if(Chi_para[grain_in_sys]<0){
                        Chi_para[grain_in_sys] -= 1.1*Chi_para[grain_in_sys];
                    }
                    Tc1p68=1.068*Tc;
                    if(Temperature<Tc1p68){
                        double one_m_T_o_Tc1p68 = 1.0 - Temperature/Tc1p68;
                        Chi_perp[grain_in_sys]  = t_a0_PERP
                                                + t_a1_PERP*one_m_T_o_Tc1p68
                                                + t_a2_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a3_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a4_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a5_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a6_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a7_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a8_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68
                                                + t_a9_PERP*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68*one_m_T_o_Tc1p68;
                    }
                    else{
                        Chi_perp[grain_in_sys]= t_b0_PERP * (Tc/ (4.0*M_PI*(Temperature-Tc)) );
                    }
                    Chi_para[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                    Chi_perp[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                    break;
                case Material_t::SusTypes::Vogler:
                //==================== If susceptibility_type == vogler_scusceptibility_fit ==================================//
                    // Determine first the inverse of susceptibility
                    if(Temperature<Tc){
                        double mEQ_dummy = m_EQ[grain_in_sys];
                        Chi_para[grain_in_sys]  = t_a0_PARA/(Tc-Temperature);
                        Chi_perp[grain_in_sys]  = t_a0_PERP
                                                + t_a1_PERP*mEQ_dummy
                                                + t_a2_PERP*mEQ_dummy*mEQ_dummy
                                                + t_a3_PERP*mEQ_dummy*mEQ_dummy*mEQ_dummy
                                                + t_a4_PERP*mEQ_dummy*mEQ_dummy*mEQ_dummy*mEQ_dummy;
                    }
                    // if T>=Tc
                    else if(Temperature>=Tc){
                        Chi_para[grain_in_sys] = t_b0_PARA/(Temperature-Tc);
                        Chi_perp[grain_in_sys] = t_b0_PERP/(Temperature-Tc);
                    }
                    // Convert susceptibility in CGS units passing from 1/T to 1/Oe by *1e-4 T/Oe and rescaling by the scaling factor if any
                    Chi_para[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                    Chi_perp[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                    break;
                case Material_t::SusTypes::Inverse:
                //==================== If susceptibility_type == inv_scusceptibility_fit =====================================//
                    // Determine first the inverse of the susceptibility
                    if(Temperature<Tc){
                        double Tc_m_T_o_Tc = (Tc-Temperature)/Tc;
                        Chi_para[grain_in_sys]  = t_a0_PARA
                                                + t_a1_PARA  * Tc_m_T_o_Tc
                                                + t_a2_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a3_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a4_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a5_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a6_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a7_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a8_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a9_PARA  * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a1_2_PARA * pow(Tc_m_T_o_Tc,0.5);

                        Chi_perp[grain_in_sys]  = t_a0_PERP
                                                + t_a1_2_PERP * pow(Tc_m_T_o_Tc,0.5)
                                                + t_a1_PERP   * Tc_m_T_o_Tc
                                                + t_a2_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a3_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a4_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a5_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a6_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a7_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a8_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)
                                                + t_a9_PERP   * (Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc)*(Tc_m_T_o_Tc);
                    }
                    // if T>=Tc
                    else{
                        double T_m_Tc_o_Tc = (Temperature-Tc)/Tc;
                        Chi_para[grain_in_sys]  = t_b0_PARA
                                                + t_b1_PARA * T_m_Tc_o_Tc
                                                + t_b2_PARA * (T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)
                                                + t_b3_PARA * (T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)
                                                + t_b4_PARA * (T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)*(T_m_Tc_o_Tc);

                        Chi_perp[grain_in_sys]  = t_b0_PERP
                                                + t_b1_PERP * T_m_Tc_o_Tc
                                                + t_b2_PERP * (T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)
                                                + t_b3_PERP * (T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)
                                                + t_b4_PERP * (T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)*(T_m_Tc_o_Tc)*(T_m_Tc_o_Tc);
                    }
                    // Calculate susceptibility by 1/Chi_para and 1/Chi_perp
                    Chi_para[grain_in_sys] = 1.0/Chi_para[grain_in_sys];
                    Chi_perp[grain_in_sys] = 1.0/Chi_perp[grain_in_sys];
                    // Convert susceptibility in CGS units passing from 1/T to 1/Oe by *1e-4 T/Oe and rescaling by the scaling factor
                    Chi_para[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                    Chi_perp[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                    if(Chi_perp[grain_in_sys]<0.0){Chi_perp[grain_in_sys]=0.0;}
                    break;
            }

            if(Chi_perp[grain_in_sys]<0.0 || Chi_para[grain_in_sys]<0.0){
                Log.log(Logger_t::LogLvl::ERR,"Negative Susceptibility Grain:"+to_string_exact(grain_in_sys)+" Temp: "+to_string_exact(Temperature)+" Chi_para: "+
                                     to_string_exact(Chi_para[grain_in_sys])+" Chi_perp: "+to_string_exact(Chi_perp[grain_in_sys]));
            }
        }
    }
    return 0;
}