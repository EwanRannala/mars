/*
 * Solver_LLB.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Solver_LLB.cpp
 * \brief Function for Landau-Lifshitz-Bloch solver
 */

#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <random>
#include <iomanip>
#include <unordered_set>

#include "Classes/Solver.hpp"

/** This function acts on all grains within the system.
 * <P>
 * The LLB requires multiple divisions by \f$(T-T_c)\f$ thus if \f$T=T_c\f$ a zero division occurs producing undefined behaviour.
 * To prevent crashes the temperature is compared to the Curie point and incremented by 0.00001K if they are equal.
 * This may produce undesired behaviour or negatively impact simulation results, as such a warning is output to inform the user this has
 * occurred. To prevent this behaviour it is best to specify the Curie point to a high degree of accuracy (i.e. 3+ decimal places).
 * </P>
 * <P>
 * The LLB solver is valid for all temperatures. The specific version implemented here is the
 * sLLB-II (See R.F.L. Evans et al Phys. Rev. B 85, 014433 https://doi.org/10.1103/PhysRevB.85.014433).
 * This version of the LLB provides the correct Boltzmann distribution for all temperatures, whereas
 * the sLLB-I fails to do so at \f$T=T_c\f$. While the LLB is valid for all temperature ranges, it requires
 * greater calculations per time step and also smaller time steps than the LLG. <BR>
 * The most stable time step is \f$1fs\f$ (See LLB time step test.)
 * </P>
 * <P> The sLLB-II is:
 * \f[
 *         \frac{\partial{\vec{m}^i}}{\partial{t}}=
 *             \gamma_e \left( {\vec{m}^i} \times \vec{Heff}^i \right)
 *           -
 *           \frac{\gamma_e\alpha_{\parallel}}{{m^i}^2} \left(\vec{m}^i \cdot \vec{Heff}^i \right) \vec{m}^i
 *           +
 *           \frac{\gamma_e\alpha_{\bot}}{{m^i}^2} \left[ \vec{m}^i \times \left( \vec{m}^i \times \left(\vec{Heff}^i
 *           +
 *           \vec{\zeta}_{\bot}\right) \right) \right] + \vec{\zeta}_{ad}
 * \f]
 * \f$\vec{m}\f$ is the magnetisation unit vector. <BR>
 * \f$\vec{e}\f$ is the easy axis unit vector. <BR>
 * \f$\gamma_e\f$ is the electron gyromagnetic ratio. \f$|\gamma_e|=1.760859644\times10^7 \frac{rad}{Oes}\f$. <BR>
 * \f$\alpha_{\parallel}\f$ and \f$\alpha_{\bot}\f$ are the parallel and perpendicular damping constants:
 * \f[
 *     \alpha_{\parallel} = \frac{2}{3}\frac{T}{T_c}\lambda\quad\text{ and }\quad \left \{
 *           \begin{aligned}
 *               \alpha_{\bot} = \lambda\left(1-\frac{T}{3T_c}\right), && \text{if } T \leq T_c \\
 *               \alpha_{\bot} = \alpha_{\parallel} = \frac{2}{3}\frac{T}{T_c}\lambda, && \text{otherwise. }
 *           \end{aligned} \right.
 * \f]
 * \f$H_{eff}\f$ is the effective field in Oe, which accounts for intragrain exchange, interactions and applied fields. <BR>
 * \f[
 *         H_{intragrain} = \left \{
 *           \begin{aligned}
 *               \frac{1}{2\chi_{\parallel}}(1-\frac{m^2}{m_e^2}) && \text{if } T \leq T_c \\
 *                -\bigg(\frac{1}{\chi_{\parallel}}+\frac{3T_c}{5(T-T_c)\chi_{\parallel}}m^2\bigg) && \text{otherwise.}
 *           \end{aligned} \right.
 * \f]
 * \f$\chi_{\parallel}\f$ is the parallel susceptibility. <BR>
 * For more information see
 * int Solver_t::Susceptibilities(const unsigned int Num_Grains, const unsigned int Num_Layers, const std::vector<unsigned int>*Included_grains_in_system, const Grain_t*Grain) <BR>
 * The thermal dependency of anisotropy and magnetisation can be accounted for via two methods. Both methods
 * are implemented by varying the anisotropy field directly. <BR>
 * <P> Method one: Callen-Callen scailing. <BR>
 * The anisotropy field is given by:
 * \f[
 *         H_{ani} = \frac{2K}{M_s}
 * \f]
 *
 * The temperature dependence of anisotropy is described via Callen-Callen scailing:
 * \f[
 *         K(T) = K_0m_{e}^\gamma
 * \f]
 * \f$m_e\f$ is the equilibrium magnetisation and \f$\gamma\f$ is the material dependent critical exponent.
 * Combining these equations results in:
 * \f[
 *         H_{ani}(T) = \frac{2K}{M_s}m_{e}^{\gamma-1}
 * \f]
 * Finally the vector form for the anisotropy magnetisation used here is:
 * \f[
 *         \vec{H_{ani}} = H_{ani} (\vec{m}\bullet \vec{e}) \vec{e}
 * \f]
 * </P>
 * <P> Method two: Transverse susceptibility. <BR>
 * The Anisotropy field is directed along the z-axis and is given by:.
 * \f[
 *          \vec{H_{ani}} = \frac{-(m_x+m_y)}{\chi_{\bot}}
 * \f]
 * </P>
 * <P><B>
 * While the user is given the option to use either method, it should be noted that Callen-Callen
 * scailing produces a fictitious longitudinal component of the anisotropy at elevated temperatures.
 * Thus the transverse susceptibility method is optimal, however for soft systems where obtaining
 * the transverse susceptibility is not possible Callen-Callen scailing provides a simple and useful alternative.
 * </B></P>
 * The equilibrium magnetisation \f$m_e\f$ is determined via:
 * \f[
 *         m_e(T) = \left \{
 *          \begin{aligned}
 *                   \bigg(1-\frac{T}{T_c}\bigg)^{\beta}, && \text{if } T \leq T_c \\
 *                   0, && \text{otherwise.}
 *          \end{aligned} \right.
 * \f]
 * \f$\zeta_{ad}\f$ and \f$\zeta_{\bot}\f$ are the diffusion coefficients:
 * \f[
 *         \begin{aligned}
 *             <\zeta_{ad}^{i}(t)\zeta_{ad}^{j}(t-t^\prime)> = \frac{2k_{B}T\alpha_{\parallel}}{\gamma MsV}\delta_{ij}\delta_{ad}\delta(t) \\
 *          <\zeta_{\bot}^{i}(t)\zeta_{\bot}^{j}(t-t^\prime)> = \frac{2k_{B}T(\alpha_{\bot}-\alpha_{\parallel})}{\gamma MsV\alpha_{\bot}^2}\delta_{ij}\delta_{ad}\delta(t)
 *      \end{aligned}
 * \f]
 * \f$M_s\f$ is the saturation magnetisation in emu/cc. <BR>
 * \f$V\f$ is the grain volume in cc. <BR>
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Interac Magnetic interactions data structure.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Centre_x Centre of integration on a-axis (provides position for inclusion zone).
 * @param[in] Centre_y Centre of integration on y-axis (provides position for inclusion zone).
 * @param[out] Grain Grains data structure.
 *
 */
int Solver_t::LLB(const unsigned int Num_Layers,const Interaction_t& Interac,
                  const Voronoi_t& VORO, std::vector<Grain_t>& Grains){

//    auto start = std::chrono::system_clock::now();
    std::normal_distribution<double> Gauss(0.0,1.0);
//    auto end = std::chrono::system_clock::now();
//    auto elapsed1 = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start);

    // Local parameters
    std::vector<Vec3> M_initial (Integratable_grains_in_system);
    std::vector<Vec3> dmdt1 (Integratable_grains_in_system);
    std::vector<Vec3> dmdt2 (Integratable_grains_in_system);
    std::vector<Vec3> RNG_PARA (Integratable_grains_in_system);
    std::vector<Vec3> RNG_PERP (Integratable_grains_in_system);
    std::vector<Vec3> RNG_PARA_NUM (Integratable_grains_in_system);
    std::vector<Vec3> RNG_PERP_NUM (Integratable_grains_in_system);
    std::vector<Vec3> H_eff (Integratable_grains_in_system);
    std::vector<Vec3> H_magneto (Integratable_grains_in_system);
    std::vector<Vec3> H_exchange (Integratable_grains_in_system);
    std::vector<double> Internal_Exchange_field (Integratable_grains_in_system,0.0);
    std::vector<double> H_ani (Integratable_grains_in_system,0.0);
    unsigned int Tot_grains=VORO.Num_Grains*Num_Layers;
    // Resize LLB vectors
    Chi_para.resize(Tot_grains);
    Chi_perp.resize(Tot_grains);
    Alpha_PARA.resize(Tot_grains);
    Alpha_PERP.resize(Tot_grains);
    m_EQ.resize(Tot_grains);

    //-- READ IN MAGNETISATION DATA
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        M_initial[grain_in_incl] = Grains[grain_in_sys].m;
        // Save random numbers
        RNG_PARA_NUM[grain_in_incl].x = Gauss(Gen);
        RNG_PARA_NUM[grain_in_incl].y = Gauss(Gen);
        RNG_PARA_NUM[grain_in_incl].z = Gauss(Gen);
        RNG_PERP_NUM[grain_in_incl].x = Gauss(Gen);
        RNG_PERP_NUM[grain_in_incl].y = Gauss(Gen);
        RNG_PERP_NUM[grain_in_incl].z = Gauss(Gen);
    }
//################################################################# PREVENT TEMPERATURE EQUAL TO TC #########################################################//
    // The LLB fails when T=Tc, this condition ensures T=/=Tc. However this may cause unexpected behaviour so a warning needs to be displayed
    // each time this condition is met. The best way to overcome this limitation of the LLB is to define Tc to a high precision.
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        // Ensure T != Tc as solution will be infinity.
        if(Grains[grain_in_sys].Temp==Grains[grain_in_sys].Tc){
            Log.logandshow(Logger_t::LogLvl::WARN,"WARNING: Temp=Tc for grain '" + to_string_exact(grain_in_sys) + "' increasing grain temperature by 0.00001K.");
            Grains[grain_in_sys].Temp+=0.00001;
        }
    }
//    start = std::chrono::system_clock::now();
    Susceptibilities(VORO.Num_Grains, Num_Layers, Included_grains_in_layer, Grains);
//    end = std::chrono::system_clock::now();
//    auto elapsed2 = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start);
//###########################################################################################################################################################//
// STEP ONE
//################################################################# DETERMINE TEMPERATURE DEPENDENT PARAMETERS ##########################################################//
//    start = std::chrono::system_clock::now();
    if(!Inclusion){
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset(VORO.Num_Grains*Layer);
            for(size_t grain_in_incl=0;grain_in_incl<VORO.Num_Grains;++grain_in_incl){
                unsigned int grain_in_sys = grain_in_incl+Offset;
                double Temperature = Grains[grain_in_sys].Temp;
                double Tc = Grains[grain_in_sys].Tc;
                if(Temperature<Tc){
                    switch (Grains[grain_in_sys].mEQ_Type)
                    {
                    case Material_t::mEQTypes::Bulk:
                        m_EQ[grain_in_sys] = pow(1.0-(Temperature/Tc),Grains[grain_in_sys].Crit_exp); // PAPER
                        break;
                    case Material_t::mEQTypes::Polynomial:
                        double Tc_m_T_o_Tc=(Tc-Temperature)/Tc;
                            m_EQ[grain_in_sys] = Grains[grain_in_sys].a0_mEQ +
                                                Grains[grain_in_sys].a1_2_mEQ*pow(Tc_m_T_o_Tc,0.5) +
                                                Grains[grain_in_sys].a1_mEQ*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a2_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a3_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a4_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a5_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a6_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a7_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a8_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a9_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc;
                        break;
                    }
                    Alpha_PARA[grain_in_sys] = Alpha[Layer]*0.666666666666666*Temperature/Tc;
                    Alpha_PERP[grain_in_sys] = Alpha[Layer]*(1.0-(Temperature/(3.0*Tc)));
                }
                else{
                    switch (Grains[grain_in_sys].mEQ_Type)
                    {
                    case Material_t::mEQTypes::Bulk:
                        m_EQ[grain_in_sys] = 0.01; //  To avoid numerical issues
                        break;
                    case Material_t::mEQTypes::Polynomial:
                        m_EQ[grain_in_sys] = 1.0/(1.0/Grains[grain_in_sys].a0_mEQ + Grains[grain_in_sys].b1_mEQ*((Temperature-Tc)/Tc) + Grains[grain_in_sys].b2_mEQ*pow((Temperature-Tc)/Tc,2));
                        break;
                    }
                    Alpha_PARA[grain_in_sys] = Alpha[Layer]*0.666666666666666*Temperature/Tc;
                    Alpha_PERP[grain_in_sys] = Alpha_PARA[grain_in_sys];
                }
            }
        }
    }
    else{
        // First determine the mEQ and alpha for the grains to be simulated.
        // The unordered set is used to prevent duplication
        std::unordered_set<unsigned int> Neigh_grains;
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset(VORO.Num_Grains*Layer);
            for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_layer;++grain_in_incl){
                unsigned int grain_in_sys = Included_grains_in_layer[grain_in_incl]+Offset;
                Neigh_grains.emplace(grain_in_sys);
                double Temperature = Grains[grain_in_sys].Temp;
                double Tc = Grains[grain_in_sys].Tc;
                if(Temperature<Tc){
                    switch (Grains[grain_in_sys].mEQ_Type)
                    {
                    case Material_t::mEQTypes::Bulk:
                        m_EQ[grain_in_sys] = pow(1.0-(Temperature/Tc),Grains[grain_in_sys].Crit_exp); // PAPER
                        break;
                    case Material_t::mEQTypes::Polynomial:
                        double Tc_m_T_o_Tc=(Tc-Temperature)/Tc;
                            m_EQ[grain_in_sys] = Grains[grain_in_sys].a0_mEQ +
                                                Grains[grain_in_sys].a1_2_mEQ*pow(Tc_m_T_o_Tc,0.5) +
                                                Grains[grain_in_sys].a1_mEQ*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a2_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a3_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a4_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a5_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a6_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a7_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                Grains[grain_in_sys].a8_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                Grains[grain_in_sys].a9_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc;
                        break;
                    }
                    Alpha_PARA[grain_in_sys] = Alpha[Layer]*0.666666666666666*Temperature/Tc;
                    Alpha_PERP[grain_in_sys] = Alpha[Layer]*(1.0-(Temperature/(3.0*Tc)));
                }
                else{
                    switch (Grains[grain_in_sys].mEQ_Type)
                    {
                    case Material_t::mEQTypes::Bulk:
                        m_EQ[grain_in_sys] = 0.01; //  To avoid numerical issues
                        break;
                    case Material_t::mEQTypes::Polynomial:
                        m_EQ[grain_in_sys] = 1.0/(1.0/Grains[grain_in_sys].a0_mEQ + Grains[grain_in_sys].b1_mEQ*((Temperature-Tc)/Tc) + Grains[grain_in_sys].b2_mEQ*pow((Temperature-Tc)/Tc,2));
                        break;
                    }
                    Alpha_PARA[grain_in_sys] = Alpha[Layer]*0.666666666666666*Temperature/Tc;
                    Alpha_PERP[grain_in_sys] = Alpha_PARA[grain_in_sys];
                }
            }
        }
        // Second determine mEQ for all meighbours of the simulated grains
        for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

            // Magneto
            for(unsigned int NEIGHBOUR_ID : Interac.Magneto_neigh_list_grains[grain_in_sys]){
                // Place Neighbour ID into set. If successful then determine mEQ for that neighbour.
                if(Neigh_grains.emplace(NEIGHBOUR_ID).second){

                    double Temperature=Grains[NEIGHBOUR_ID].Temp;
                    double Tc=Grains[NEIGHBOUR_ID].Tc;
                    if(Temperature<Tc){
                        switch (Grains[NEIGHBOUR_ID].mEQ_Type)
                        {
                        case Material_t::mEQTypes::Bulk:
                            m_EQ[NEIGHBOUR_ID] = pow(1.0-(Temperature/Tc),Grains[NEIGHBOUR_ID].Crit_exp); // PAPER
                            break;
                        case Material_t::mEQTypes::Polynomial:
                            double Tc_m_T_o_Tc=(Tc-Temperature)/Tc;
                            m_EQ[NEIGHBOUR_ID] = Grains[NEIGHBOUR_ID].a0_mEQ +
                                                    Grains[NEIGHBOUR_ID].a1_2_mEQ*pow(Tc_m_T_o_Tc,0.5) +
                                                    Grains[NEIGHBOUR_ID].a1_mEQ*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a2_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a3_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a4_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a5_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a6_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a7_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a8_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a9_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc;
                        break;
                        }
                    }
                    else{
                        switch (Grains[NEIGHBOUR_ID].mEQ_Type)
                        {
                        case Material_t::mEQTypes::Bulk:
                            m_EQ[NEIGHBOUR_ID] = 0.01; //  To avoid numerical issues
                            break;
                        case Material_t::mEQTypes::Polynomial:
                            m_EQ[NEIGHBOUR_ID] = 1.0/(1.0/Grains[NEIGHBOUR_ID].a0_mEQ + Grains[NEIGHBOUR_ID].b1_mEQ*((Temperature-Tc)/Tc) + Grains[NEIGHBOUR_ID].b2_mEQ*pow((Temperature-Tc)/Tc,2));
                            break;
                        }
                    }
                }
            }
            // Exchange
            for(unsigned int NEIGHBOUR_ID : Interac.Exchange_neigh_list[grain_in_sys]){
                // Place Neighbour ID into set. If successful then determine mEQ for that neighbour.
                if(Neigh_grains.emplace(NEIGHBOUR_ID).second){

                    double Temperature=Grains[NEIGHBOUR_ID].Temp;
                    double Tc=Grains[NEIGHBOUR_ID].Tc;
                    if(Temperature<Tc){
                        switch (Grains[NEIGHBOUR_ID].mEQ_Type)
                        {
                        case Material_t::mEQTypes::Bulk:
                            m_EQ[NEIGHBOUR_ID] = pow(1.0-(Temperature/Tc),Grains[NEIGHBOUR_ID].Crit_exp); // PAPER
                            break;
                        case Material_t::mEQTypes::Polynomial:
                            double Tc_m_T_o_Tc=(Tc-Temperature)/Tc;
                            m_EQ[NEIGHBOUR_ID] = Grains[NEIGHBOUR_ID].a0_mEQ +
                                                    Grains[NEIGHBOUR_ID].a1_2_mEQ*pow(Tc_m_T_o_Tc,0.5) +
                                                    Grains[NEIGHBOUR_ID].a1_mEQ*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a2_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a3_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a4_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a5_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a6_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a7_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc+
                                                    Grains[NEIGHBOUR_ID].a8_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc +
                                                    Grains[NEIGHBOUR_ID].a9_mEQ*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc*Tc_m_T_o_Tc;
                        break;
                        }
                    }
                    else{
                        switch (Grains[NEIGHBOUR_ID].mEQ_Type)
                        {
                        case Material_t::mEQTypes::Bulk:
                            m_EQ[NEIGHBOUR_ID] = 0.01; //  To avoid numerical issues
                            break;
                        case Material_t::mEQTypes::Polynomial:
                            m_EQ[NEIGHBOUR_ID] = 1.0/(1.0/Grains[NEIGHBOUR_ID].a0_mEQ + Grains[NEIGHBOUR_ID].b1_mEQ*((Temperature-Tc)/Tc) + Grains[NEIGHBOUR_ID].b2_mEQ*pow((Temperature-Tc)/Tc,2));
                            break;
                        }
                    }
                }
            }
        }
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE STOCHASTIC TERMS ######################################################################//
    double m_MAG_SQ(0.0);
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

        double Temperature = Grains[grain_in_sys].Temp;
        double Tc = Grains[grain_in_sys].Tc;
        double m_EQ_dummy = m_EQ[grain_in_sys];
        double Chi_PARA_dummy = Chi_para[grain_in_sys];
        double Alpha_PARA_dummy = Alpha_PARA[grain_in_sys];
        double Alpha_PERP_dummy = Alpha_PERP[grain_in_sys];
        double Ms = Grains[grain_in_sys].Ms;
        double Vol = Grains[grain_in_sys].Vol*1e-21;
        double Hk = 2.0*Grains[grain_in_sys].K/Ms;

        double RNG_PARA_MAG = sqrt((Gamma*two_KB*Temperature*Alpha_PARA_dummy) / (dt_LLB*Ms*Vol));
        RNG_PARA[grain_in_incl] = RNG_PARA_NUM[grain_in_incl]*RNG_PARA_MAG;
        double RNG_PERP_MAG = sqrt( (two_KB*Temperature*(Alpha_PERP_dummy-Alpha_PARA_dummy)) / (Gamma*Ms*Vol*dt_LLB*(Alpha_PERP_dummy*Alpha_PERP_dummy)) );
        RNG_PERP[grain_in_incl] = RNG_PERP_NUM[grain_in_incl]*RNG_PERP_MAG;

        m_MAG_SQ = Grains[grain_in_sys].m*Grains[grain_in_sys].m;

        if (Temperature <= Grains[grain_in_sys].Callen_range_lowT){
            H_ani[grain_in_incl] = Hk * Grains[grain_in_sys].Callen_factor_lowT * pow(m_EQ_dummy,Grains[grain_in_sys].Callen_power_lowT-1.0);
        }
        else if (Temperature > Grains[grain_in_sys].Callen_range_midT){
            H_ani[grain_in_incl] = Hk * Grains[grain_in_sys].Callen_factor_highT * pow(m_EQ_dummy,Grains[grain_in_sys].Callen_power_highT-1.0);
        }
        else{ //(Temperature > Grain.Callen_range_lowT[grain_in_sys] && Temperature <= Grain.Callen_range_midT[grain_in_sys])
            H_ani[grain_in_incl] = Hk * Grains[grain_in_sys].Callen_factor_midT * pow(m_EQ_dummy,Grains[grain_in_sys].Callen_power_midT-1.0);
        }

        if(Temperature <= Tc){
            Internal_Exchange_field[grain_in_incl] = (1.0/(2.0*Chi_PARA_dummy))*(1.0-(m_MAG_SQ/(m_EQ_dummy*m_EQ_dummy)));
        }
        else{
            Internal_Exchange_field[grain_in_incl] = (-1.0/Chi_PARA_dummy)*(1.0+((3.0*Tc)/(5.0*(Temperature-Tc)))*m_MAG_SQ);
        }
    }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    double MdotE(0.0);
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        switch (Grains[grain_in_sys].Ani_method)
        {
            case Material_t::AniMethods::Callen:
                MdotE = (Grains[grain_in_sys].m*Grains[grain_in_sys].Easy_axis)/sqrt(Grains[grain_in_sys].m*Grains[grain_in_sys].m);
                H_eff[grain_in_incl] = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m+Grains[grain_in_sys].H_appl+H_ani[grain_in_incl]*MdotE*Grains[grain_in_sys].Easy_axis;
            break;
            case Material_t::AniMethods::Chi:
            double Chi_perp_inv = 1.0 / Chi_perp[grain_in_sys];
                if(Chi_perp[grain_in_sys]<1.0e-10){Chi_perp_inv=0.0;}
                H_eff[grain_in_incl].x = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m.x+Grains[grain_in_sys].H_appl.x-Grains[grain_in_sys].m.x*Chi_perp_inv;
                H_eff[grain_in_incl].y = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m.y+Grains[grain_in_sys].H_appl.y-Grains[grain_in_sys].m.y*Chi_perp_inv;
                H_eff[grain_in_incl].z = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m.z+Grains[grain_in_sys].H_appl.z;
            break;
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        /* The W matrix is ordered with decreasing granularity. Grains first then subsegments and finally segments.
         * Due to the order we must keep access updated based on previous access. */
        size_t WmatPos=0;
        H_magneto[grain_in_incl]=0.0;
        // Neighbouring grains
        while(WmatPos<Interac.Magneto_neigh_list_grains[grain_in_sys].size()){
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_grains[grain_in_sys][WmatPos];
            H_magneto[grain_in_incl].x += Grains[grain_in_sys].Ms*(Interac.Wxx[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            H_magneto[grain_in_incl].y += Grains[grain_in_sys].Ms*(Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wyy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            H_magneto[grain_in_incl].z += Grains[grain_in_sys].Ms*(Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wzz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            ++WmatPos;
        }
        // Calculates mean field for truncation method
        for(size_t p=0;p<Interac.MFWxx[grain_in_sys].size();++p){
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.Magneto_MeanField_grains[grain_in_sys]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            H_magneto[grain_in_incl].x += (Grains[grain_in_sys].Ms*(Interac.MFWxx[grain_in_sys][p]*mx+
                                                               Interac.MFWxy[grain_in_sys][p]*my+
                                                               Interac.MFWxz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            H_magneto[grain_in_incl].y += (Grains[grain_in_sys].Ms*(Interac.MFWxy[grain_in_sys][p]*mx+
                                                               Interac.MFWyy[grain_in_sys][p]*my+
                                                               Interac.MFWyz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            H_magneto[grain_in_incl].z += (Grains[grain_in_sys].Ms*(Interac.MFWxz[grain_in_sys][p]*mx+
                                                               Interac.MFWyz[grain_in_sys][p]*my+
                                                               Interac.MFWzz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
        }
        // Neighbouring Subsegments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_subseg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_subseg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.subsegments[NEIGHBOUR_ID]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            H_magneto[grain_in_incl].x += (Grains[grain_in_sys].Ms*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            H_magneto[grain_in_incl].y += (Grains[grain_in_sys].Ms*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            H_magneto[grain_in_incl].z += (Grains[grain_in_sys].Ms*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
        }

        //Neighbouring Segments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_seg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_seg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            unsigned int count=0;
            for(unsigned int subseg : Interac.segments[NEIGHBOUR_ID]){
                for(unsigned int grain : Interac.subsegments[subseg]){
                    mx += Grains[grain].m.x*m_EQ[grain];
                    my += Grains[grain].m.y*m_EQ[grain];
                    mz += Grains[grain].m.z*m_EQ[grain];
                    ++count;
                }
            }
            H_magneto[grain_in_incl].x += (Grains[grain_in_sys].Ms*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/count;
            H_magneto[grain_in_incl].y += (Grains[grain_in_sys].Ms*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/count;
            H_magneto[grain_in_incl].z += (Grains[grain_in_sys].Ms*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/count;
        }

    }
    // Exchange
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        H_exchange[grain_in_incl] = 0.0;
        for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_sys].size();neigh++){
            unsigned int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_sys][neigh];
            H_exchange[grain_in_incl]+=Interac.H_exch_str[grain_in_sys][neigh]*Grains[NEIGHBOUR_ID].m*m_EQ[NEIGHBOUR_ID];
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        H_eff[grain_in_incl]+=H_magneto[grain_in_incl]+H_exchange[grain_in_incl];
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

        double mx = Grains[grain_in_sys].m.x;
        double my = Grains[grain_in_sys].m.y;
        double mz = Grains[grain_in_sys].m.z;

        double Heffx = H_eff[grain_in_incl].x;
        double Heffy = H_eff[grain_in_incl].y;
        double Heffz = H_eff[grain_in_incl].z;

        double RNGPERPx = RNG_PERP[grain_in_incl].x;
        double RNGPERPy = RNG_PERP[grain_in_incl].y;
        double RNGPERPz = RNG_PERP[grain_in_incl].z;

        double Heff_p_PERP_x = Heffx+RNGPERPx;
        double Heff_p_PERP_y = Heffy+RNGPERPy;
        double Heff_p_PERP_z = Heffz+RNGPERPz;

        double one_o_mSQ = (1.0/(mx*mx + my*my + mz*mz));

        dmdt1[grain_in_incl].x = Gamma*(-1.0*(my*Heffz - mz*Heffy)
                               + (Alpha_PARA[grain_in_sys]*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mx
                               - (Alpha_PERP[grain_in_sys]*one_o_mSQ*(my*(mx*(Heff_p_PERP_y) - my*(Heff_p_PERP_x)) - mz*(mz*(Heff_p_PERP_x) - mx*(Heff_p_PERP_z)))))
                               + RNG_PARA[grain_in_incl].x;

        dmdt1[grain_in_incl].y = Gamma*(-1.0*(mz*Heffx - mx*Heffz)
                               + (Alpha_PARA[grain_in_sys]*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*my
                               - (Alpha_PERP[grain_in_sys]*one_o_mSQ*(mz*(my*(Heff_p_PERP_z) - mz*(Heff_p_PERP_y)) - mx*(mx*(Heff_p_PERP_y) - my*(Heff_p_PERP_x)))))
                               + RNG_PARA[grain_in_incl].y;

        dmdt1[grain_in_incl].z = Gamma*(-1.0*(mx*Heffy - my*Heffx)
                               + (Alpha_PARA[grain_in_sys]*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mz
                               - (Alpha_PERP[grain_in_sys]*one_o_mSQ*(mx*(mz*(Heff_p_PERP_x) - mx*(Heff_p_PERP_z)) - my*(my*(Heff_p_PERP_z) - mz*(Heff_p_PERP_y)))))
                               + RNG_PARA[grain_in_incl].z;
    }

//***************************************************************************************************************
//****************************************************************************************************************
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].m += dmdt1[grain_in_incl]*dt_LLB;
        if(Grains[grain_in_sys].m.x > 1.1 || Grains[grain_in_sys].m.y > 1.1 || Grains[grain_in_sys].m.z>1.1){
            Log.log(Logger_t::LogLvl::ERR,"LLB error: magnetisation exceeding unity. ");
        }
    }
//    end = std::chrono::system_clock::now();
//    auto elapsed3 = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start);
//###########################################################################################################################################################//
// STEP TWO
//##################################################################### DETERMINE STOCHASTIC TERMS ######################################################################//
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain

        double Temperature = Grains[grain_in_sys].Temp;
        double Tc = Grains[grain_in_sys].Tc;
        double m_EQ_dummy = m_EQ[grain_in_sys];
        double Chi_PARA_dummy = Chi_para[grain_in_sys];
        double Ms = Grains[grain_in_sys].Ms;
        double Hk = 2.0*Grains[grain_in_sys].K/Ms;

        m_MAG_SQ = Grains[grain_in_sys].m*Grains[grain_in_sys].m;

        if (Temperature <= Grains[grain_in_sys].Callen_range_lowT){
            H_ani[grain_in_incl] = Hk * Grains[grain_in_sys].Callen_factor_lowT * pow(m_EQ_dummy,Grains[grain_in_sys].Callen_power_lowT-1.0);
        }
        else if (Temperature > Grains[grain_in_sys].Callen_range_midT){
            H_ani[grain_in_incl] = Hk * Grains[grain_in_sys].Callen_factor_highT * pow(m_EQ_dummy,Grains[grain_in_sys].Callen_power_highT-1.0);
        }
        else{ //(Temperature > Grain.Callen_range_lowT[grain_in_sys] && Temperature <= Grain.Callen_range_midT[grain_in_sys])
            H_ani[grain_in_incl] = Hk * Grains[grain_in_sys].Callen_factor_midT * pow(m_EQ_dummy,Grains[grain_in_sys].Callen_power_midT-1.0);
        }

        if(Temperature <= Tc){
            Internal_Exchange_field[grain_in_incl] = (1.0/(2.0*Chi_PARA_dummy))*(1.0-(m_MAG_SQ/(m_EQ_dummy*m_EQ_dummy)));
        }
        else{
            Internal_Exchange_field[grain_in_incl] = (-1.0/Chi_PARA_dummy)*(1.0+((3.0*Tc)/(5.0*(Temperature-Tc)))*m_MAG_SQ);
        }
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        switch (Grains[grain_in_sys].Ani_method)
        {
            case Material_t::AniMethods::Callen:
                MdotE = (Grains[grain_in_sys].m*Grains[grain_in_sys].Easy_axis)/sqrt(Grains[grain_in_sys].m*Grains[grain_in_sys].m);
                H_eff[grain_in_incl] = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m+Grains[grain_in_sys].H_appl+H_ani[grain_in_incl]*MdotE*Grains[grain_in_sys].Easy_axis;
            break;
            case Material_t::AniMethods::Chi:
            const double Chi_perp_inv = 1.0 / Chi_perp[grain_in_sys];
                H_eff[grain_in_incl].x = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m.x+Grains[grain_in_sys].H_appl.x-Grains[grain_in_sys].m.x*Chi_perp_inv;
                H_eff[grain_in_incl].y = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m.y+Grains[grain_in_sys].H_appl.y-Grains[grain_in_sys].m.y*Chi_perp_inv;
                H_eff[grain_in_incl].z = Internal_Exchange_field[grain_in_incl]*Grains[grain_in_sys].m.z+Grains[grain_in_sys].H_appl.z;
            break;
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        /* The W matrix is ordered with decreasing granularity. Grains first then subsegments and finally segments.
         * Due to the order we must keep access updated based on previous access. */
        size_t WmatPos=0;
        H_magneto[grain_in_incl]=0.0;
        // Neighbouring grains
        while(WmatPos<Interac.Magneto_neigh_list_grains[grain_in_sys].size()){
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_grains[grain_in_sys][WmatPos];
            H_magneto[grain_in_incl].x += Grains[grain_in_sys].Ms*(Interac.Wxx[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            H_magneto[grain_in_incl].y += Grains[grain_in_sys].Ms*(Interac.Wxy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wyy[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            H_magneto[grain_in_incl].z += Grains[grain_in_sys].Ms*(Interac.Wxz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.x*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wyz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.y*m_EQ[NEIGHBOUR_ID]+
                                                                   Interac.Wzz[grain_in_sys][WmatPos]*Grains[NEIGHBOUR_ID].m.z*m_EQ[NEIGHBOUR_ID]);
            ++WmatPos;
        }
        // Calculates mean field for truncation method
        for(size_t p=0;p<Interac.MFWxx[grain_in_sys].size();++p){
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.Magneto_MeanField_grains[grain_in_sys]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            H_magneto[grain_in_incl].x += (Grains[grain_in_sys].Ms*(Interac.MFWxx[grain_in_sys][p]*mx+
                                                               Interac.MFWxy[grain_in_sys][p]*my+
                                                               Interac.MFWxz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            H_magneto[grain_in_incl].y += (Grains[grain_in_sys].Ms*(Interac.MFWxy[grain_in_sys][p]*mx+
                                                               Interac.MFWyy[grain_in_sys][p]*my+
                                                               Interac.MFWyz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
            H_magneto[grain_in_incl].z += (Grains[grain_in_sys].Ms*(Interac.MFWxz[grain_in_sys][p]*mx+
                                                               Interac.MFWyz[grain_in_sys][p]*my+
                                                               Interac.MFWzz[grain_in_sys][p]*mz))/Interac.Magneto_MeanField_grains[grain_in_sys].size();
        }
        // Neighbouring Subsegments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_subseg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_subseg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            for(unsigned int grain : Interac.subsegments[NEIGHBOUR_ID]){
                mx += Grains[grain].m.x*m_EQ[grain];
                my += Grains[grain].m.y*m_EQ[grain];
                mz += Grains[grain].m.z*m_EQ[grain];
            }
            H_magneto[grain_in_incl].x += (Grains[grain_in_sys].Ms*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            H_magneto[grain_in_incl].y += (Grains[grain_in_sys].Ms*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
            H_magneto[grain_in_incl].z += (Grains[grain_in_sys].Ms*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/Interac.subsegments[NEIGHBOUR_ID].size();
        }

        //Neighbouring Segments
        for(size_t neigh=0;neigh<Interac.Magneto_neigh_list_seg[grain_in_sys].size();++neigh){
            ++WmatPos;
            unsigned int NEIGHBOUR_ID = Interac.Magneto_neigh_list_seg[grain_in_sys][neigh];
            // Determine magnetisation.
            double mx=0.0;
            double my=0.0;
            double mz=0.0;
            unsigned int count=0;
            for(unsigned int subseg : Interac.segments[NEIGHBOUR_ID]){
                for(unsigned int grain : Interac.subsegments[subseg]){
                    mx += Grains[grain].m.x*m_EQ[grain];
                    my += Grains[grain].m.y*m_EQ[grain];
                    mz += Grains[grain].m.z*m_EQ[grain];
                    ++count;
                }
            }
            H_magneto[grain_in_incl].x += (Grains[grain_in_sys].Ms*(Interac.Wxx[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wxy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wxz[grain_in_sys][WmatPos]*mz))/count;
            H_magneto[grain_in_incl].y += (Grains[grain_in_sys].Ms*(Interac.Wxy[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyy[grain_in_sys][WmatPos]*my+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*mz))/count;
            H_magneto[grain_in_incl].z += (Grains[grain_in_sys].Ms*(Interac.Wxz[grain_in_sys][WmatPos]*mx+
                                                              Interac.Wyz[grain_in_sys][WmatPos]*my+
                                                              Interac.Wzz[grain_in_sys][WmatPos]*mz))/count;
        }

    }
    // Exchange
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        H_exchange[grain_in_incl] = 0.0;
        for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_sys].size();neigh++){
            unsigned int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_sys][neigh];
            H_exchange[grain_in_incl]+=Interac.H_exch_str[grain_in_sys][neigh]*Grains[NEIGHBOUR_ID].m*m_EQ[NEIGHBOUR_ID];
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        H_eff[grain_in_incl]+=H_magneto[grain_in_incl]+H_exchange[grain_in_incl];
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        double mx = Grains[grain_in_sys].m.x;
        double my = Grains[grain_in_sys].m.y;
        double mz = Grains[grain_in_sys].m.z;

        double Heffx = H_eff[grain_in_incl].x;
        double Heffy = H_eff[grain_in_incl].y;
        double Heffz = H_eff[grain_in_incl].z;

        double RNGPERPx = RNG_PERP[grain_in_incl].x;
        double RNGPERPy = RNG_PERP[grain_in_incl].y;
        double RNGPERPz = RNG_PERP[grain_in_incl].z;

        double Heff_p_PERP_x = Heffx+RNGPERPx;
        double Heff_p_PERP_y = Heffy+RNGPERPy;
        double Heff_p_PERP_z = Heffz+RNGPERPz;

        double one_o_mSQ = (1.0/(mx*mx + my*my + mz*mz));

        dmdt2[grain_in_incl].x = Gamma*(-1.0*(my*Heffz - mz*Heffy)
                               + (Alpha_PARA[grain_in_sys]*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mx
                               - (Alpha_PERP[grain_in_sys]*one_o_mSQ*(my*(mx*(Heff_p_PERP_y) - my*(Heff_p_PERP_x)) - mz*(mz*(Heff_p_PERP_x) - mx*(Heff_p_PERP_z)))))
                               + RNG_PARA[grain_in_incl].x;

        dmdt2[grain_in_incl].y = Gamma*(-1.0*(mz*Heffx - mx*Heffz)
                               + (Alpha_PARA[grain_in_sys]*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*my
                               - (Alpha_PERP[grain_in_sys]*one_o_mSQ*(mz*(my*(Heff_p_PERP_z) - mz*(Heff_p_PERP_y)) - mx*(mx*(Heff_p_PERP_y) - my*(Heff_p_PERP_x)))))
                               + RNG_PARA[grain_in_incl].y;

        dmdt2[grain_in_incl].z = Gamma*(-1.0*(mx*Heffy - my*Heffx)
                               + (Alpha_PARA[grain_in_sys]*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mz
                               - (Alpha_PERP[grain_in_sys]*one_o_mSQ*(mx*(mz*(Heff_p_PERP_x) - mx*(Heff_p_PERP_z)) - my*(my*(Heff_p_PERP_z) - mz*(Heff_p_PERP_y)))))
                               + RNG_PARA[grain_in_incl].z;
    }
//***************************************************************************************************************
//****************************************************************************************************************

    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grains[grain_in_sys].m = M_initial[grain_in_incl] + (dmdt1[grain_in_incl]+dmdt2[grain_in_incl])*dt_LLB*0.5;
        if(std::isnan(Grains[grain_in_sys].m.x) || std::isnan(Grains[grain_in_sys].m.y) || std::isnan(Grains[grain_in_sys].m.z)){
            Log.log(Logger_t::LogLvl::ERR,"LLB error: NaN magnetisation.");
        }
    }
    return 0;
}



