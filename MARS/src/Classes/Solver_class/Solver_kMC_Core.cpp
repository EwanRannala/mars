/*
 * Solver_kMC_Core.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Sergiu Ruta
 *      Editions by: Samuel Ewan Rannala
 */

/** \file Solver_kMC_Core.cpp
 * \brief kMC core function.
 */
constexpr double PP = (1.0/3.0);           //!< ???
constexpr double ANGTOL = 1.0e-4;          //!< ???
constexpr double INFTY = 1.0e10;           //!< Value used to act as infinity
constexpr double FERRO_EBCUTOFF = 500.0;    //!< Limiting value for E/kBT to determine when to skip direct computation of the exponent and set value to 0
// TODO decipher what FERRO_EBCUTOFF is

#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>
#include <random>
#include <iostream>

#include "Globals.hpp"
#include "Classes/Solver.hpp"
/** Performs integration step for a single grain.
 *  <P>
 *  This function first finds two possible orientations of spins given the anisotropy vector and the
 *  external field, which both define the line along which the new moment orientation will lie.
 *  The energy barrier is then calculated, separating the minima for the up and down states. To calculate
 *  the energy barrier the following form of the Stoner-Wohlfarth energy is assumed:
 *  \f[
 *      e = \frac{KV}{k_BT}\sin(\theta)^2 - \frac{M_sVH_{eff}}{k_BT}\cos(\theta-\theta_h);
 *  \f]
 *  The implementation uses the normalisation:
 *  \f[
 *      e = \frac{KV}{k_BT}\sin(\theta)^2 - \frac{M_sVH_k}{k_BT}h_{eff}\cos(\theta-\theta_h);
 *  \f]
 *  </P>
 *
 *  \param[in] Easy_axis Anisotropy vector component of a grain. <BR>
 *  \param[in] h_eff effective field vector components (Normalised by \f$H_k = \frac{2K}{M_s}\f$) <BR>
 *  \param[in] KV_over_kBT \f$\frac{KV}{k_BT}\f$. <BR>
 *  \param[in] MsVHk_over_kBT \f$\frac{M_sVH_K}{k_BT}\f$. <BR>
 *  \param[out] Spin Grain's magnetisation vector component.<BR>
 */
int Solver_t::KMC_core(const Vec3& Easy_axis, const Vec3& h_eff, const double KV_over_kBT, const double MsVHk_over_kBT, Vec3& Spin) const {

    /*   Internal variables
     *   eminust -> energy associated with the energy of the minus state
     *   eplust -> energy associated with the energy of the plus state
     *   de -> energy barrier - difference between max energy and the energy
     *        of the closer of the two minima
     *   dele -> difference between the energies of the minima
     */
    std::uniform_real_distribution<double> Random(0.0,1.0);
    // Decide whether the initial state is "+" (diro = 1) or "-" (diro = -1)
    double vecmag = Easy_axis*Spin;
    int diro = -1;
    if (vecmag >= 0.0){diro=1;}

    double ht=0.0;
    Vec3 htot;
    Vec3 snew;
    Vec3 sdn;
    Vec3 sup;

//------------------------------The "up" state------------------------------//
    Spin = Easy_axis;
    double err_abs = INFTY;
    while(err_abs > ANGTOL){
        vecmag = Easy_axis*Spin;
        htot = vecmag*Easy_axis+h_eff;
        ht = sqrt(htot*htot);
        if(ht > 0.0){snew=htot/ht;}
        else{snew = Spin;}
        err_abs = sqrt((snew.x-Spin.x)*(snew.x-Spin.x)+(snew.y-Spin.y)*(snew.y-Spin.y)+(snew.z-Spin.z)*(snew.z-Spin.z));
        Spin=snew;
    }
    sup=Spin;

//-----------------------------The "down" state-----------------------------//
    Spin = -Easy_axis;
    err_abs = INFTY;
    while(err_abs > ANGTOL){
        vecmag = Easy_axis*Spin;
        htot = vecmag*Easy_axis+h_eff;
        ht = sqrt(htot*htot);
        if(ht > 0.0){snew = htot/ht;}
        else{snew=Spin;}
        err_abs = sqrt((snew.x-Spin.x)*(snew.x-Spin.x)+(snew.y-Spin.y)*(snew.y-Spin.y)+(snew.z-Spin.z)*(snew.z-Spin.z));
        Spin=snew;
    }
    sdn=Spin;
//==========================================================================//

    // Calculate the energy barrier for reversal against the field.
    // First set the direction to the old value.
    double hnorm=sqrt(h_eff*h_eff);
    Vec3 hu;
    if(hnorm != 0.0){hu = h_eff/hnorm;}
    else{hu = 0.0;}

    int dirn = diro;
    double coshe = hu*Easy_axis;
    double pp1 = pow(coshe*coshe,PP);
    double pp2 = pow(1.0-coshe*coshe,PP);
    double hebsy = pow(pp1+pp2,-1.5);
    // TODO Check gebsy, should it be / or * ?
    double gebsy = 0.86+1.14/hebsy;
    double hcon = hnorm/hebsy;
    double de=0.0;
    double eb=0.0;
    double dele=0.0;
    double ebdu=0.0;
    double ebud=0.0;
    double eplust=0.0;
    double eminust=0.0;
    double cosang=0.0;
    double fielddirn=0.0;

    if(hcon >= 1.0){de = 0.0;}
    else{
        eb = pow(1.0-hcon, gebsy);
        de = KV_over_kBT*eb;
    }

    // energy of up state
    eplust = -MsVHk_over_kBT*(sup*h_eff);
    cosang = Easy_axis*sup;
    eplust = eplust+KV_over_kBT*(1.0-cosang*cosang);

    // energy of down state
    eminust = -MsVHk_over_kBT*(sdn*h_eff);
    cosang = (Easy_axis*sdn);
    eminust = eminust+KV_over_kBT*(1.0-cosang*cosang);

    dele = eminust-eplust;

    fielddirn = h_eff*Easy_axis;
    if(fielddirn > 0.0){
        ebdu = de;
        ebud = de+dele;
    }
    else{
        ebud = de;
        ebdu = de-dele;
    }

    // If outside of the SW-asteroid...
    if(hcon >= 1.0){
        if(fielddirn > 0.0){dirn = 1;}
        else{dirn = -1;}

        // Assign new moment direction
        Spin=sup;
        if(dirn < 0){
            Spin=sdn;
        }
    }
    else{
        // calculation of relaxation times
        double towiv=0.0;
        double towud=0.0;
        double ttot=0.0;
        double xvar=0.0;
        double prot=0.0;
        double test = 0.0;
        if(fabs(ebud) > FERRO_EBCUTOFF){
            towiv = 0.0;
            if(ebud < 0.0){test=1.0;}
        }
        else{towiv = f0*exp(-ebud);}

        if(fabs(ebdu) > FERRO_EBCUTOFF){
            towud = 0.0;
            if(ebdu < 0.0){test = 1.0;}
        }
        else{towud = f0*exp(-ebdu);}

        ttot = towiv+towud;
        prot = 1.0-exp(-dt_kMC*ttot);
        if(test > 0.5){prot = 1.0;}
        double ran = Random(Gen);
        if(prot > ran){
            dirn = 1;
            ran = Random(Gen);
            if(dele > FERRO_EBCUTOFF){xvar = 0.0;}
            else{
                if(dele < -FERRO_EBCUTOFF){xvar = 1.0;}
                else{
                    xvar = exp(-dele);
                    xvar = xvar/(xvar+1.0);
                }
            }
            if(xvar > ran){dirn = -1;}
        }
        // Assign new moment direction
        if(dirn > 0){Spin=sup;}
        else{Spin=sdn;}
    }
    return 0;
}
