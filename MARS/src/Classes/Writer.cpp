/*
 * Writer.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Writer.cpp
 * @brief Definitions of Writer class methods. */

#include <unordered_set>
#include "Classes/Writer.hpp"
#include "Classes/PixelMap.hpp"
/** The constructor initialises the sub and parent classes using the data from the configuration file.
 *
 * @param[in] cfg Configuration file data.
 */
Writer::Writer(const ConfigFile&cfg)
    : Hdc(cfg)
    , Pos_X(0.0)
    , Pos_Y(0.0)
{
    H_width_X = cfg.getValAndUnitOfKey<double>("WriteHead:Field_Width_X");
    H_width_Y = cfg.getValAndUnitOfKey<double>("WriteHead:Field_Width_Y");
    speed     = cfg.getValAndUnitOfKey<double>("WriteHead:Speed");
    Skew      = cfg.getValAndUnitOfKey<double>("WriteHead:Skew");
    CosSkew = cos(Skew);
    SinSkew = sin(Skew);
}
/** Sets the position of the write head in the x-dimension.
 *
 * @param[in] x X-coordinate.
 */
void Writer::setX(double x){Pos_X=x;}
/** Sets the position of the write head in the y-dimension.
 *
 * @param[in] y Y-coordinate.
 */
void Writer::setY(double y){Pos_Y=y;}
/** Increments the x-position of the write head by dx.
 *
 * @param[in] dx Displacement in x-direction.
 */
void Writer::moveX(double dx){Pos_X += dx;}
/** Increments the y-position of the write head by dy.
 *
 * @param[in] dy Displacement in y-direction.
 */
void Writer::moveY(double dy){Pos_Y += dy;}
/** Apply field to grains within the the profile. The Distance between the centre of the profile and the
 * grains is checked to be within the limits of the field profile. Grains within the profile have their field
 * data updated equal to the applied field, all other grains have their field data set to zero.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] dt Timestep.
 * @param[in] Included_grains_in_layer Vector containing all included grains.
 * @param[out] Grains Grain.H_appl is updated for all the grains in the system.
 */
void Writer::ApplyHspatial(const Voronoi_t&VORO, const unsigned int Num_Layers,
                           const double dt, std::vector<Grain_t>& Grains){

    updateField(dt);

    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
        double Disp_X = Pos_X-VORO.Pos_X_final[grain_in_layer];
        double Disp_Y = Pos_Y-VORO.Pos_Y_final[grain_in_layer];
        // Apply rotatiom due to Skew to field box
        double Xrot = Disp_X*CosSkew - Disp_Y*SinSkew;
        double Yrot = Disp_X*SinSkew + Disp_Y*CosSkew;
        if(fabs(Xrot)<=(H_width_X*0.5) && fabs(Yrot)<=(H_width_Y*0.5)){
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains[grain_in_system].H_appl = getField();
            }
        } else {
            // Reset all the other grains not in range of writing head to zero applied field
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains[grain_in_system].H_appl = Vec3(0.0,0.0,0.0);
            }
        }
    }
}
/** Apply field to grains within the the profile while taking into account the inclusion zone limits.
 * The Distance between the centre of the profile and the grains is checked to be within the limits 
 * of the field profile. Grains within the profile have their field data updated equal to the applied
 * field, all other grains have their field data set to zero.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] dt Timestep.
 * @param[out] Grains Grain.H_appl is updated for all the grains in the system.
 */
void Writer::ApplyHspatial(const Voronoi_t&VORO, const unsigned int Num_Layers,
                           const double dt, const std::vector<unsigned int>&Included_grains_in_layer,
                           std::vector<Grain_t>& Grains){
    updateField(dt);

    for(unsigned int grain_in_layer : Included_grains_in_layer){
        double Disp_X = Pos_X-VORO.Pos_X_final[grain_in_layer];
        double Disp_Y = Pos_Y-VORO.Pos_Y_final[grain_in_layer];
        // Apply rotatiom due to Skew to field box
        double Xrot = Disp_X*CosSkew - Disp_Y*SinSkew;
        double Yrot = Disp_X*SinSkew + Disp_Y*CosSkew;
        if(fabs(Xrot)<=(H_width_X*0.5) && fabs(Yrot)<=(H_width_Y*0.5)){
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains[grain_in_system].H_appl = getField();
            }
        } else {
            // Reset all the other grains not in range of writing head to zero applied field
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains[grain_in_system].H_appl = Vec3(0.0,0.0,0.0);
            }
        }
    }
}
/** Apply field to pixels in the pixel map and then produce average field for each grain.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] dt Timestep.
 * @param[out] Grains Grain.H_appl is updated for all the grains in the system.
 * @param PixMap PixelMap.Field is updated for all pixels.
 */
 void Writer::ApplyHspatialPM(const Voronoi_t&VORO, const unsigned int Num_Layers, const double dt,
                              std::vector<Grain_t>& Grains, PixelMap& PixMap){

     updateField(dt);
     PixMap.ClearH();
     for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
         unsigned int offset = Layer*VORO.Num_Grains;
         for(unsigned int grain=0;grain<VORO.Num_Grains;++grain){
             unsigned int grain_in_system = grain+offset;
             Grains[grain_in_system].H_appl = Vec3(0.0,0.0,0.0);
         }
     }
     std::vector<unsigned int> PixelList;
     std::unordered_set<unsigned int> ImpactedGrains;
     PixMap.InRect((Pos_X-H_width_X*0.5), (Pos_Y-H_width_Y*0.5), (Pos_X+H_width_X*0.5),
                   (Pos_Y+H_width_Y*0.5),PixelList,ImpactedGrains);
     // Apply field to all pixels
     for(size_t Pix : PixelList){
         PixMap.Access(Pix).set_H(getField());
     }
     // Determine grain average field
     for(const auto& grain : ImpactedGrains){
         Vec3 H(0,0,0);
         for(unsigned int px : Grains[grain].Pixel){
             H += PixMap.Access(px).get_H();
         }
         H /= Grains[grain].Pixel.size();
         for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
             Grains[grain+(VORO.Num_Grains*Layer)].H_appl=H;
         }
     }
}
/** Returns the x-position for the write head. */
double Writer::getX() const {return Pos_X;}
/** Returns the y-position for the write head. */
double Writer::getY() const {return Pos_Y;}
/** Returns the x-width of the field profile of the write head. */
double Writer::getHwidthX() const {return H_width_X;}
/** Returns the y-width of the field profile of the write head. */
double Writer::getHwidthY() const {return H_width_Y;}
/** Returns the speed of the write head. */
double Writer::getSpeed() const {return speed;}
/** Returns the skew angle of the write head. */
double Writer::getSkew() const {return Skew;}
/** Returns the cosine of write head skew angle. */
double Writer::getCosSkew() const {return CosSkew;}
/** Returns the sine of write head skew angle. */
double Writer::getSinSkew() const {return SinSkew;}
/** Returns the skew angle, in degrees, of the write head. */
double Writer::getSkewDEG() const {return Skew*(57.29577951308232);}
