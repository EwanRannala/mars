/*
 * ReadLayer.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadLayer.cpp
 * @brief Definitions of ReadLayer class methods. */

#include "Classes/ReadLayer.hpp"
/** The constructor initialises the class using the data already present it the RecLayer class.*/
ReadLayer::ReadLayer(const RecLayer& RecordingLayer){
    Track_spacing = RecordingLayer.getBitSpacingY();
    Track_size    = RecordingLayer.getBitSizeY();
    Track_number  = RecordingLayer.getBitsY();
    Padding_Y     = RecordingLayer.getPadding();
}
/** The constructor initialises the class using the configuration file data. */
ReadLayer::ReadLayer(const ConfigFile&cfg)
    :Track_spacing(0.0)
    ,Track_size(0)
    ,Track_number(0)
    ,Padding_Y(0)
{
    Track_spacing = cfg.getValAndUnitOfKey<double>("ReadingLayer:Track_spacing");
    Track_size    = cfg.getValAndUnitOfKey<double>("ReadingLayer:Bit_width");
    Track_number  = cfg.getValAndUnitOfKey<unsigned int>("ReadingLayer:Tracks");
    Padding_Y     = cfg.getValAndUnitOfKey<double>("ReadingLayer:Padding");
}
/** Returns the track spacing. */
double ReadLayer::getBitSpacingY() const {return Track_spacing;}
/** Returns the number of tracks. */
unsigned int ReadLayer::getBitsY() const {return Track_number;}
/** Returns the size of the tracks (y-dimension). */
double ReadLayer::getBitSizeY() const {return Track_size;}
/** Returns Track padding. */
double ReadLayer::getPadding() const {return Padding_Y;}
