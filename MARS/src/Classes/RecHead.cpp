/*
 * RecHead.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file RecHead.cpp
 * @brief Definitions of RecHead class methods. */

// TODO Update class to provide position relative to laser spot centre instead of field.

#include "Classes/RecHead.hpp"
/** The constructor initialises the sub and parent classes using the data within the configuration file.
 *
 * @param cfg Configuration file data.
 */
RecHead::RecHead(const ConfigFile&cfg)
    : Writer(cfg)
    , HAMR_Laser(cfg)
{
    Pulsing    = cfg.getValueOfKey<bool>("Recordinghead:Pulsing");
    NPS      = fabs(cfg.getValAndUnitOfKey<double>("Recordinghead:NPS"));
    FWHM_X   = cfg.getValAndUnitOfKey<double>("Recordinghead:FWHM_X");
    FWHM_Y   = cfg.getValAndUnitOfKey<double>("Recordinghead:FWHM_Y");
    // sqrt(8.0*log(2.0))  = 2.354820045030949
    // 1/2.354820045030949 = 0.42466090014400959
    // Sigma = (FWHM * 0.42466090014400959)
    // 2.0*0.42466090014400959*0.42466090014400959 = 0.36067376022224096
    two_SigmaX_SQ = FWHM_X*FWHM_X*0.36067376022224097;
    two_SigmaY_SQ = FWHM_Y*FWHM_Y*0.36067376022224097;

}
/** Apply the temperature to grains within the system. The laser is assumed to be always
 * applied in this function. The distance between the centre of the profile and
 * the grains is determined and used to calculate the temperature applied to each grain.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time Current time w.r.t. the head.
 * @param[out] Temperature Current laser temperature.
 * @param[out] Grains Temperature data is updated for all grains.
 */
void RecHead::ApplyTspatialCont(const unsigned int Num_Layers, const Voronoi_t&VORO, const double Environ_Temp,
                                const double Time, double& Temperature, std::vector<Grain_t>& Grains){

    // TODO check if setCoolingTime is actually required.
    setCoolingTime(0.0);

    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
        const double disp_X = VORO.Pos_X_final[grain_in_layer]-(getSpeed()*Time + NPS*getCosSkew());
        const double disp_Y = VORO.Pos_Y_final[grain_in_layer]-(getY()          - NPS*getSinSkew());
        const double Xrot = disp_X*getCosSkew() - disp_Y*getSinSkew();
        const double Yrot = disp_X*getSinSkew() + disp_Y*getCosSkew();

        double Expo = exp(-( (Xrot*Xrot)/(two_SigmaX_SQ) + (Yrot*Yrot)/(two_SigmaY_SQ) ));
        Temperature = Environ_Temp + (getCurTemp()-Environ_Temp)*Expo;

        // Assign temperature to each layer.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
            Grains[grain_in_system].Temp = Temperature;
        }
    }
}
/** Apply the temperature to grains within the system while taking into account the inclusion zone limits.
 * The laser is assumed to be always applied in this function. The distance between the centre of the profile
 * and the grains is determined and used to calculate the temperature applied to each grain.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time Current time w.r.t. the head.
 * @param[out] Temperature Current laser temperature.
 * @param[in] Included_grains_in_layer List of grain the function loops through.
 * @param[out] Grains Temperature data is updated for all grains.
 */
void RecHead::ApplyTspatialCont(const unsigned int Num_Layers, const Voronoi_t&VORO, const double Environ_Temp,
                                const double Time, double& Temperature, const std::vector<unsigned int>& Included_grains_in_layer,
                                std::vector<Grain_t>& Grains){

    double Temporal_temp=0.0;
    double Spatial_temp=0.0;

// ONLY NEED SIGMA^2 can optimise this here

    setCoolingTime(0.0);

    for(unsigned int grain_in_layer : Included_grains_in_layer){

        double Tmp = VORO.Pos_X_final[grain_in_layer] - (getSpeed()*Time+NPS);
        Temporal_temp = exp(-(Tmp*Tmp)/two_SigmaX_SQ);
        Tmp = VORO.Pos_Y_final[grain_in_layer] - getY();
        Spatial_temp  = exp(-(Tmp*Tmp)/two_SigmaY_SQ);
        Temperature = Environ_Temp + (getCurTemp()-Environ_Temp)*Temporal_temp*Spatial_temp;
        // Assign temperature to each layer.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
            Grains[grain_in_system].Temp = Temperature;
        }
    }
}
/** Apply the mean temperature to the grains within the system, by first assigning the temperatures to
 * each pixel within the input pixelmap. The laser is assumed to be always applied in this
 * function. The temperature values of these pixels within each grain is then
 * averaged over to produce a mean temperature for each grain.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time Current time w.r.t. the laser.
 * @param[out] Temperature Current laser temperature.
 * @param[out] Grains Temperature data is updated for all grains.
 * @param[out] PixMap Temperature data is updated for all pixels.
 */
void RecHead::ApplyTspatialContPM(const unsigned int Num_Layers, const Voronoi_t& VORO, const double Environ_Temp,
                                  const double Time, double& Temperature, std::vector<Grain_t>& Grains, PixelMap& PixMap){

    double Temporal_temp=0.0;
    double Spatial_temp=0.0;

    setCoolingTime(0.0);
    // Set Pixel values
    for(unsigned int y=0;y<PixMap.getRows();++y){
        for(unsigned int x=0;x<PixMap.getCols();++x){
            double Px = PixMap.Access(x, y).get_Coords().first;
            double Py = PixMap.Access(x, y).get_Coords().second;

            double Tmp = Px - (getSpeed()*Time+NPS);
            Temporal_temp = exp(-(Tmp*Tmp)/(two_SigmaX_SQ));
            Tmp = (Py - getY());
            Spatial_temp  = exp(-(Tmp*Tmp)/(two_SigmaY_SQ));
            Temperature = Environ_Temp + (getCurTemp()-Environ_Temp)*Temporal_temp*Spatial_temp;
            PixMap.Access(x,y).set_Temp(Temperature);
        }
    }
    // Set Grain values
    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
        double T=0.0;
        for(unsigned int Pix : Grains[grain_in_layer].Pixel){
            T+=PixMap.Access(Pix).get_Temp();
        }
        T /= Grains[grain_in_layer].Pixel.size();
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            Grains[grain_in_layer+(VORO.Num_Grains*Layer)].Temp=T;
        }
    }
}
/** Apply temperature to the grains within the system. The laser is assumed to be pulsed
 * in this function. The distance between the centre of the profile and
 * the grains is determined and used to calculate the temperature applied to each grain.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers within the system.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time current time w.r.t. laser.
 * @param[out] Spatial_Temp Current temperature produced by the laser.
 * @param[out] Grain Temperature data is updated for all grains.
 */
void RecHead::ApplyTspatial(const Voronoi_t& VORO, const unsigned int Num_Layers,
                            const double Environ_Temp, const double Time,
                            double& Temporal_Temp, std::vector<Grain_t>& Grains){

    Temporal_Temp = (getCurTemp()-Environ_Temp)*exp(-pow(((Time-3*getCoolingTime())/getCoolingTime()),2.0));

    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
        double Grain_X=VORO.Pos_X_final[grain_in_layer];
        double Grain_Y=VORO.Pos_Y_final[grain_in_layer];

        //########## Generic definition of the laser profile.
        double Spatial_Temp = Environ_Temp + Temporal_Temp * exp(-(((Grain_X-getX())*(Grain_X-getX()))/(two_SigmaX_SQ) + ((Grain_Y-getY())*(Grain_Y-getY()))/(two_SigmaY_SQ)));
        // Assign temperature to each layer.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
            Grains[grain_in_system].Temp = Spatial_Temp;
        }
    }
    Temporal_Temp += Environ_Temp;
}
/** Apply the mean temperature to the grains within the system, by first assigning the temperatures to
 * each pixel within the input pixelmap. The laser is assumed to be pulsed in this
 * function. The temperature values of these pixels within each grain is then averaged over
 * to produce a mean temperature for each grain.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers within the system.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time current time w.r.t. laser.
 * @param[out] Spatial_Temp Current temperature produced by the laser.
 * @param[out] Grains Temperature data is updated for all grains.
 * @param[out] PixMap Temperature data is updated for all pixels.
 */
void RecHead::ApplyTspatialPM(const Voronoi_t& VORO, const unsigned int Num_Layers, const double Environ_Temp,
                            const double Time, double& Temporal_Temp, std::vector<Grain_t>& Grains, PixelMap& PixMap){

    Temporal_Temp = (getCurTemp()-Environ_Temp)*exp(-pow(((Time-3*getCoolingTime())/getCoolingTime()),2.0));

    // Set Pixel values
    for(unsigned int y=0;y<PixMap.getRows();++y){
        for(unsigned int x=0;x<PixMap.getCols();++x){
            double Px = PixMap.Access(x, y).get_Coords().first;
            double Py = PixMap.Access(x, y).get_Coords().second;

            //########## Generic definition of the laser profile.
            double Spatial_Temp = Environ_Temp + Temporal_Temp * exp(-(((Px-getX())*(Px-getX()))/(two_SigmaX_SQ) + ((Py-getY())*(Py-getY()))/(two_SigmaY_SQ)));
            PixMap.Access(x,y).set_Temp(Spatial_Temp);
        }
    }
    // Set Grain values
    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
        double T=0.0;
        for(unsigned int Pix : Grains[grain_in_layer].Pixel){
            T+=PixMap.Access(Pix).get_Temp();
        }
        T /= Grains[grain_in_layer].Pixel.size();
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            Grains[grain_in_layer+(VORO.Num_Grains*Layer)].Temp=T;
        }
    }
    Temporal_Temp += Environ_Temp;
}
/** Returns boolean indicating if the laser is set to pulse. */
bool RecHead::getPulsing()const{return Pulsing;}
/** Return NFT to write head spacing. */
double RecHead::getNPS()const{return NPS;}
/** Return FWHMx */
double RecHead::getFWHMx()const{return FWHM_X;}
/** Return FWHMx */
double RecHead::getFWHMy()const{return FWHM_Y;}
