/*
 * Globals.cpp
 *
 *  Created on: 5 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Globals.cpp
 * @brief Source file for global variables definitions. */

#include "Globals.hpp"

const double M_2PI   =6.28318530717958647692;
const double M_PI_180=0.01745329251994329577;
const double M_180_PI=57.295779513082320876798;

const double KB=1.38064900000000e-16;
const double two_KB=2.76129800000000e-16;

std::mt19937_64 Gen;
std::mt19937_64 VoroGen;

std::string InputcfgFile="MARS_input.cfg";
/* Stores information about the version of the code.
 * This value is filled in via CMake. CMake will check the
 * current git hash and if it has changed since the last build
 * it will copy Globals.cpp.in and replace the value between @
 * with the hash.
 * ####################################################
 * #####     If changes are made to this file     #####
 * ##### they must also be made to Globals.cpp.in ##### */
std::string Githash = "@GIT_HASH@";

std::string Delim=" ";
std::string OutDir="Output/";
