/*
 * HAMR_Write_Data_continuous.cpp
 *
 *  Created on: 28 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */
/** \file HAMR_Write_Data_continuous.cpp
 * \brief  Simulation of HAMR writing process with a continuously moving write head.
 *
 * <P> Data is written to the system via heat assisted magnetic recording. The write head is set to move
 * continuously applying a magnetic field once the head is in the position of a bit. The laser pulse
 * follows the write head and can be set to be either applied constantly or only when the head is
 * writing a bit, similar to the magnetic field. The system is first equilibrated for
 * <B><EM> HAMR:Equilibration_time </EM></B>, after this, all bit positions are calculated. If a
 * track is too long for the system then the number of bits per track is reduced, the same is done
 * for the number of tracks. A list of grains in each bit is also determined, allowing for individual
 * data for each bit to be output. </P>
 * <P> Each time step consists of an LLB integration, a movement of the write head and, if the head is
 * positions within a bit, the application of a magnetic field. Once all bits in a track have been
 * written the write head is moved back to its initial x-position and set to the centre of the
 * track in the y-direction. The process is then repeated for all required tracks. </P>
 * <P> Once all data has been written the system is exported for use with read back simulations. if
 * inclusion has been used then the system is made to equilibrate (forcing any non-integrated grains
 * to respond the change in the system). The system is then exported again. If
 * <B><EM> HAMR:ReadBack </EM></B> is set to <B> True </B> then a read back simulation is performed
 * before the simulation exits. </P> */

#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <string>
#include <chrono>


#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include "Classes/RecHead.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Grain_setup.hpp"
#include "Importers/Materials_import.hpp"
#include "Importers/Read_in_import.hpp"
#include "Importers/Create_system_from_output.hpp"

#include "Voronoi.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Simulations/Read_back.hpp"

#include "Data_output/HAMR_grain_output.hpp"
#include "Data_output/HAMR_layer_output.hpp"
#include "Data_output/HAMR_switching_output.hpp"
#include "Data_output/Export_system.hpp"

#include "Classes/PixelMap.hpp"
#include "Classes/Solver.hpp"


#include <chrono>

/** Performs HAMR data write over an entire system with a continuously moving write head.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 *         <UL>
 *            <LI> Type <LI> SEED
 *        </UL>
 * <LI> Struct:
 *         <UL>
 *             <LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 *             <LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 *         </UL>
 * <LI> LLB:
 *         <UL>
 *             <LI> dt <LI> Inclusion_zone
 *             <LI> <STRONG> If Inclusion_zone == True </STRONG>
 *             <UL>
 *                 <LI> Inclusion_range_negX <LI> Inclusion_range_posX <LI> Inclusion_range_negY <LI> Inclusion_range_posY
 *             </UL>
 *         </UL>
 * <LI> HAMR:
 *         <UL>
 *             <LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 *             <LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 *             <LI> Measurement_time <LI> equilibration_time <LI> Field_X_width <LI> Field_Y_width
 *             <LI> laser_fwhm_x <LI> laser_fwhm_y <LI> Thermal_gradient <LI> Bit_width
 *             <LI> Bit_spacing <LI> Bit_spacing_X <LI> Bit_spacing_Y <LI> Bit_number_in_X
 *             <LI> Bit_number_in_Y <LI> Data_Binary <LI> Write_head_speed
 *         </UL>
 * <LI> EXPT:
 *         <UL>
 *             <LI> ReadBack
 *             <LI> <STRONG> If ReadBack == True </STRONG>
 *                 <UL>
 *                     <LI> Read_Head_speed <LI> Read_Head_record_interval <LI> Read_Head_width_X <LI> Read_Head_width_Y
 *                 </UL>
 *         </UL>
 * </UL>
 * */

int HAMR_write_data_cont(ConfigFile&cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a HAMR continuous writing simulation");
    Log.SetLevel(Logger_t::LogLvl::TIME);

    auto Equilibration_time = cfg.getValAndUnitOfKey<double>("HAMRWC:Equilibration_time");
    auto Environ_Temp       = cfg.getValueOfKey<double>("HAMRWC:Environment_Temp");
    auto ReadBack           = cfg.getValueOfKey<bool>("HAMRWC:ReadBack_after_write");
    auto ReadInput          = cfg.getValueOfKey<bool>("HAMRWC:Read_input_structure");
    auto import_PixMap      = cfg.getValueOfKey<bool>("HAMRWC:Import_PixelMap");
    auto Time_evolution     = cfg.getValueOfKey<bool>("HAMRWC:Time_evolution");
    double Time_evol=0.0;
    double Evol_temp=Environ_Temp;
    if(Time_evolution){
        Time_evol=cfg.getValAndUnitOfKey<double>("HAMRWC:Evolution_length");
        std::optional<double> In_Evol_temp=cfg.getValueOfOptKey<double>("HAMRWC:Evolution_temp");
        if(In_Evol_temp.has_value()){Evol_temp=In_Evol_temp.value();}
    }
    // Declare all required variables
    Data_output_t DataOut(OutDir,cfg);
    Data_input_t  IN_DATA;
    Voronoi_t     Voronoi_data;
    Structure_t   Structure;
    Material_t    Materials;
    Interaction_t Int_system;
    std::vector<Grain_t> Grains;
    std::vector<ConfigFile> Materials_Config;
    std::vector<unsigned int> Grain_output_list;

    double Time = 0.0;
    double Profile_time=0.0;
    double Downtrack_time=0.0;
    double temperature=0.0;
    double Head_X_init=0.0;
    int File_count=0;
    unsigned int Steps_until_output=0;
    unsigned int Bit=0;
    unsigned int Bit_X=0;
    unsigned int Tot_bits=0;
    unsigned int output_steps=0;
    std::string FILENAME_idv;
    std::string Output_switch_file_loc = OutDir+"/HAMR_Grain_switching.dat";
    std::vector<std::string> Output_switch_buffer;
    std::string OUTPUT = OutDir+"/HAMR_multi_layer.dat";
    std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this file appends

    std::ofstream HEAD_OUTPUT (OutDir+"/Head_position.dat");
    std::ofstream BIT_OUTPUT (OutDir+"/Bit_desired_position.dat");

//####################IMPORT ALL REQUIRED DATA####################//
    Structure_import(cfg, &Structure);
    RecHead  RecordingHead(cfg);
    RecLayer RecordingLayer(cfg,RecordingHead.getDirn().z);
    Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Environ_Temp,Grains);
    Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Grains,Int_system);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    if(Time_evolution){
        Solver.Force_specific_solver_construction(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config,Solvers::kMC); // Ensure kMC data is imported from configuration file
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    if(ReadInput){
        Log.logandshow(Logger_t::LogLvl::INFO,"Reading input file list for system import...");
        Read_in_import(cfg, &IN_DATA);
        Log.logandshow(Logger_t::LogLvl::INFO,"Reading in old system parameters...");
        Log.logandshow(Logger_t::LogLvl::INFO,"\tSolver : " + to_string_exact(Solver.get_dt()));
        Read_in_system(IN_DATA,Structure,Voronoi_data,Materials,Grains,Int_system,Solver);
        Log.logandshow(Logger_t::LogLvl::INFO,"\tGrain width = " + to_string_exact(Structure.Grain_width));
        Log.logandshow(Logger_t::LogLvl::INFO,"\tNum layers = "  + to_string_exact(Structure.Num_layers));
        Log.logandshow(Logger_t::LogLvl::INFO,"\tUse previously created system");
        Log.logandshow(Logger_t::LogLvl::INFO,"\tExporting system after importing from old data");
        Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"InCopy");
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    const Vec3 Hap_unit = RecordingHead.getDirn();
    // If no custom PM dimensions are specified then use the system vertices.
    PixelMap PixMap(Voronoi_data.Vx_MIN,Voronoi_data.Vy_MIN,Voronoi_data.Vx_MAX,Voronoi_data.Vy_MAX,Structure.cell_size,cfg,import_PixMap);
    if(import_PixMap){
        Log.logandshow(Logger_t::LogLvl::INFO,"Exporting pixel map to file");
        PixMap.PrintwUpdate("PixelMap_from_input", Grains, DataType::ALL);
    }
    else{
        PixMap.Discretise(Voronoi_data, Grains);
    }
    // Best to check for suitable input before the simulation has been performed
    ReadLayer ReadingLayer(RecordingLayer);
    ReadHead ReadingHead(RecordingLayer,cfg);
    Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"20");

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    if(RecordingHead.getRampTime()<Solver.get_dt()){
        Log.log(Logger_t::LogLvl::ERR,"Ramp time less than timestep!");
    }
//###################DETERMINE IMPORTANT VALUES###################//
    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
    Tot_bits = RecordingLayer.getBitsA();
    if(Tot_bits==0){
        Log.log(Logger_t::LogLvl::ERR,"No bits present! X: " + to_string_exact(RecordingLayer.getBitsX()) +
                             " Y: " + to_string_exact(RecordingLayer.getBitsY()));
    }
    output_steps = Solver.getOutputSteps();
    temperature = Environ_Temp;

    double RecHeadHalfWidthX=0.0;//RecordingHead.getFWHMx()*0.5;
    double RecHeadHalfWidthY=RecordingHead.getFWHMy()*0.5;

    Head_X_init   = Voronoi_data.Vx_MIN - RecordingHead.getNPS() - RecHeadHalfWidthX;
    RecordingHead.setX(Head_X_init);
    RecordingHead.setY(RecordingLayer.getBitSizeY()*0.5+RecordingLayer.getPadding());

    double TOTAL_profile_time = RecordingLayer.getBitSizeX()/RecordingHead.getSpeed();
    double TOTAL_track_time   = Voronoi_data.Vx_MAX/RecordingHead.getSpeed();
    double Adj_Downtrack_time = 0.0; // Adjusted downtrack time is used to start the laser pulse before the system.
    double Cooling_time = TOTAL_profile_time/6.0;
    double Tot_time = RecordingLayer.getBitsY()*TOTAL_track_time;

    Log.logandshow(Logger_t::LogLvl::INFO,"Speed\t\t" + to_string_exact(RecordingHead.getSpeed()) + " nm/s");
    Log.logandshow(Logger_t::LogLvl::INFO,"Prof_tot_time\t" + to_string_exact(TOTAL_profile_time) + "s");
    Log.logandshow(Logger_t::LogLvl::INFO,"Cool_time\t" + to_string_exact(Cooling_time) + "s");
    Log.logandshow(Logger_t::LogLvl::INFO,"Tot_time\t" + to_string_exact(Tot_time) + "s");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##########Equilibrate the system for Equilibration_time#########//
    Log.logandshow(Logger_t::LogLvl::INFO,"Equilibrating the system for " + to_string_exact(Equilibration_time) + " s");

    if(Solver.get_Inclusion()){
        Log.logandshow(Logger_t::LogLvl::INFO,"Temporarily disabling Inclusion zone feature for equilibration");
        Solver.disableInclusion();
        // Remove laser pulse and applied field
        for(Grain_t & Grain  : Grains){
            Grain.Temp = Environ_Temp;
            Grain.H_appl = 0.0;
        }
        while(Time<Equilibration_time){
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
            Time += Solver.get_dt();
        }
        Log.logandshow(Logger_t::LogLvl::INFO,"Equilibration terminated: resetting Inclusion zone for the rest of simulation");
        Solver.enableInclusion();
    }
    else{
        // Remove laser pulse and applied field
        for(Grain_t & Grain  : Grains){
            Grain.Temp = Environ_Temp;
            Grain.H_appl = 0.0;
        }
        while(Time<Equilibration_time){
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
            Time += Solver.get_dt();
        }
    }
    // Reset time to zero
    Log.logandshow(Logger_t::LogLvl::INFO,"Resetting time to t = 0.0 s");
    Time = 0.0;

    HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grains,Materials.dz,&Output_switch_buffer);
    // Print initial configuration
    FILENAME_idv = to_string_exact(File_count) + ".dat";
    Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
    HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
    PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grains, DataType::ALL);
    ++File_count;

    Log.logandshow(Logger_t::LogLvl::INFO,"Output frequency: " + to_string_exact(output_steps) + " steps");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###############Generate a list of bit positions#################//
    Log.logandshow(Logger_t::LogLvl::INFO,"Generating Bit positions");
    double bit_position_X=  RecordingLayer.getBitSizeX()*0.5 + RecordingLayer.getBitSpacingX();
    double bit_position_Y=  RecordingLayer.getBitSizeY()*0.5 + RecordingLayer.getPadding();
    std::vector<std::pair<double,double>> Bit_positions;
    std::pair<double,double> XY_coords;
    for(unsigned int Bit_y=0;Bit_y<RecordingLayer.getBitsY();++Bit_y){
        for(unsigned int Bit_x=0;Bit_x<RecordingLayer.getBitsX();++Bit_x){
            XY_coords = std::make_pair(bit_position_X,bit_position_Y);
            Bit_positions.push_back(XY_coords);
            bit_position_X += RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX();
        }
        bit_position_X = Bit_positions[0].first;
        bit_position_Y += RecordingLayer.getBitSizeY()+RecordingLayer.getBitSpacingY();
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//######Generate a list of the grains contained in each bit#######//
    Log.logandshow(Logger_t::LogLvl::INFO,"Generating grains in bit");
    std::vector<std::vector<unsigned int>> Bit_grain_list (Tot_bits);
    for(unsigned int Bit_number= 0;Bit_number<Tot_bits;++Bit_number){
        bit_position_X=Bit_positions[Bit_number].first;
        bit_position_Y=Bit_positions[Bit_number].second;

        for(unsigned int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
            double grain_pos_X=Voronoi_data.Pos_X_final[grain_in_layer];
            double grain_pos_Y=Voronoi_data.Pos_Y_final[grain_in_layer];

            if(grain_pos_X>(bit_position_X-RecordingLayer.getBitSizeX()*0.5) && grain_pos_X<(bit_position_X+RecordingLayer.getBitSizeX()*0.5)){
                if(grain_pos_Y>(bit_position_Y-RecordingLayer.getBitSizeY()*0.5) && grain_pos_Y<(bit_position_Y+RecordingLayer.getBitSizeY()*0.5)){
                    Bit_grain_list[Bit_number].push_back(grain_in_layer);
                }
            }
        }
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########Check that total bits and tracks will fit in the system########//
    if(RecordingLayer.getBitsX()*(RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX()) > Voronoi_data.Vx_MAX){
        Log.logandshow(Logger_t::LogLvl::WARN,Log.BUY+"System too small for number of bits in a single track!"+Log.RST);
        int Bit_num_X = int(Voronoi_data.Vx_MAX/(RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX()));
        // Need to remove Bit from arrays
        for(unsigned Y=0;Y<RecordingLayer.getBitsY();++Y){
            Bit_positions.erase(Bit_positions.begin()+Bit_num_X+Y*Bit_num_X);
            Bit_grain_list.erase(Bit_grain_list.begin()+Bit_num_X+Y*Bit_num_X);
        }
        Log.logandshow(Logger_t::LogLvl::WARN,Log.BUY+"\tReducing bits per track from " + to_string_exact(RecordingLayer.getBitsX()) + " to " + to_string_exact(Bit_num_X)+Log.RST);
        RecordingLayer.setBitsX(Bit_num_X);
    }
    if(RecordingLayer.getBitsY()*(RecordingLayer.getBitSizeY()+RecordingLayer.getBitSpacingY())-RecordingLayer.getBitSpacingY() + 2.0*RecordingLayer.getPadding() > Voronoi_data.Vy_MAX){
        Log.logandshow(Logger_t::LogLvl::WARN,Log.BUY+"System too small for number of tracks!"+Log.RST);
        int Bit_num_Y = int(Voronoi_data.Vy_MAX/(RecordingLayer.getBitSizeY()+RecordingLayer.getBitsY()));
        if(Bit_num_Y<=0){Log.log(Logger_t::LogLvl::ERR,"System Y too small!");}
        // Need to remove Bit from arrays
        Bit_positions.resize(Bit_num_Y*RecordingLayer.getBitsX());
        Bit_grain_list.resize(Bit_num_Y*RecordingLayer.getBitsX());
        Log.logandshow(Logger_t::LogLvl::WARN,Log.BUY+"\tReducing tracks from " + to_string_exact(RecordingLayer.getBitsY()) + " to " + to_string_exact(Bit_num_Y)+Log.RST);
        RecordingLayer.setBitsY(Bit_num_Y);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################PRINT OUT BIT POSITIONS############################//

    for(std::pair<double,double> BIT : Bit_positions){
        BIT_OUTPUT << (BIT.first-RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second-RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first-RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second+RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first+RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second+RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first+RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second-RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first-RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second-RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << "\n" << std::endl;
    }
    BIT_OUTPUT.close();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//############################SIMULATE WRITING###########################//
    unsigned int Track_num_offset=0;
    auto SkipBits  = cfg.getValueOfKey<unsigned int>("HAMRWC:Skip_bits");
    auto WriteBits = cfg.getValueOfKey<unsigned int>("HAMRWC:Write_bits");

    if(SkipBits>=Tot_bits){
        Log.log(Logger_t::LogLvl::ERR,"Set to skip all bits.");
    }
    if(WriteBits==0){
        Log.log(Logger_t::LogLvl::ERR,"Number of Write bits set to zero.");
        return EXIT_FAILURE;
    }

    // Create a list of bits we want to write to
    std::vector<std::pair<double, double>> Writable_Bit_positions;
    unsigned int limit = (WriteBits < Bit_positions.size()) ? WriteBits : Bit_positions.size();
    for(size_t b=SkipBits; b<limit; ++b){
        Writable_Bit_positions.push_back(Bit_positions[b]);
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"Writing " + to_string_exact(Writable_Bit_positions.size()) + " bits. Starting from bit " + to_string_exact(SkipBits));

    // Set track number we will be writing to
    unsigned int Track_num = SkipBits/RecordingLayer.getBitsX();

    // We will now set the starting position of the head
    RecordingHead.setX( (Writable_Bit_positions[0].first-RecordingLayer.getBitSizeX()*0.5) - RecordingHead.getNPS() - RecHeadHalfWidthX );
    RecordingHead.setY(  Writable_Bit_positions[0].second);
    // Need to use an adjusted time to correctly apply the laser pulse
    Adj_Downtrack_time = RecordingHead.getX()/RecordingHead.getSpeed();

    // Output the data to be written to the system
    Log.logandshow(Logger_t::LogLvl::INFO,"Data to be written...");
    for(unsigned i=0;i<Writable_Bit_positions.size();++i){
        Log.logandshow(Logger_t::LogLvl::INFO,"    "+to_string_exact(i)+" | "+to_string_exact(RecordingLayer.getWritableData(Bit+i)));
    }

    // Output the bits positions to be written
    BIT_OUTPUT.open(OutDir+"/Bit_desired_position_selected.dat");
    for(std::pair<double,double> BIT : Writable_Bit_positions){
        BIT_OUTPUT << (BIT.first-RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second-RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first-RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second+RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first+RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second+RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first+RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second-RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << (BIT.first-RecordingLayer.getBitSizeX()*0.5) << Delim << (BIT.second-RecordingLayer.getBitSizeY()*0.5) << Delim << BIT.first << Delim << BIT.second << "\n";
        BIT_OUTPUT << "\n" << std::endl;
    }
    BIT_OUTPUT.close();

    // Ensure we start with no write field -- Possibly already done by default
    RecordingHead.setDirn(Vec3(0.0,0.0,0.0));

    // Output Column headers
    Log.logandshow(Logger_t::LogLvl::INFO,"Step Time Profile_Time Downtrack_Time Head_X Head_Y Bit_x Bit_y Bit Field");

    auto start = std::chrono::high_resolution_clock::now();
    auto end   = std::chrono::high_resolution_clock::now();
    auto dur   = std::chrono::duration_cast<std::chrono::seconds>(end-start);

    // This loop will continue until we have written the final bit
    unsigned int Bits_written = 0;
    bool WritingBit=false;
    unsigned int STEPS=0;
    bool Runoff = false; // Runoff is when the head is being moved beyond the end of the final bit
    while(Bits_written<Writable_Bit_positions.size() || Runoff){

        // What to do when outside of bit
        if(RecordingHead.getX() >= Writable_Bit_positions[Bits_written].first-RecordingLayer.getBitSizeX()*0.5 && RecordingHead.getX() <= Writable_Bit_positions[Bits_written].first+RecordingLayer.getBitSizeX()*0.5){
            // Set up head for writing
            if(!WritingBit){
                RecordingHead.setDirn(Vec3(0.0,0.0,RecordingLayer.getWritableData(Bits_written)));
                RecordingHead.Hdc::turnOnfor(RecordingLayer.getBitSizeX()/RecordingHead.getSpeed());
                WritingBit=true;
            }
        }
        else{ // outside of the bit
            // Have we just written a bit?
            if(WritingBit){
                ++Bits_written;
                WritingBit=false;
            }
            // Are we at the end of the track
            // Determine end of track position
            double EndofTrackPos = Bit_positions[(RecordingLayer.getBitsX()-1)].first+RecordingLayer.getBitSizeX();
            if(RecordingHead.getX() >= EndofTrackPos+RecordingHead.getFWHMx()){
                // Reached the end of track - need to move head back to the start
                RecordingHead.setX(Head_X_init);
                double Track_position = RecordingHead.getY() + RecordingLayer.getBitSizeY();
                RecordingHead.setY(Track_position);
                Adj_Downtrack_time = RecordingHead.getX()/RecordingHead.getSpeed();
                for(Grain_t & g : Grains){
                    g.Temp=Environ_Temp;
                }
                ++Track_num;
            }
        }

        // Do writing
        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,RecordingHead.getX(),RecordingHead.getY());
        RecordingHead.ApplyHspatial(Voronoi_data,Structure.Num_layers,Solver.get_dt(),Solver.Included_grains_in_layer,Grains);
        RecordingHead.ApplyTspatialCont(Structure.Num_layers,Voronoi_data,Environ_Temp,Adj_Downtrack_time,temperature,Solver.Included_grains_in_layer,Grains);
        Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
        ++STEPS;
        Time += Solver.get_dt();
        Downtrack_time += Solver.get_dt();
        Adj_Downtrack_time += Solver.get_dt();
        Profile_time += Solver.get_dt();
        RecordingHead.moveX(Solver.get_dt()*RecordingHead.getSpeed());
        if(STEPS==output_steps){
            // Output write head position
            HEAD_OUTPUT << (RecordingHead.getX()-RecHeadHalfWidthX) << Delim << (RecordingHead.getY()-RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // BL
            HEAD_OUTPUT << (RecordingHead.getX()-RecHeadHalfWidthX) << Delim << (RecordingHead.getY()+RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // TL
            HEAD_OUTPUT << (RecordingHead.getX()+RecHeadHalfWidthX) << Delim << (RecordingHead.getY()+RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // TR
            HEAD_OUTPUT << (RecordingHead.getX()+RecHeadHalfWidthX) << Delim << (RecordingHead.getY()-RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // TR
            HEAD_OUTPUT << (RecordingHead.getX()-RecHeadHalfWidthX) << Delim << (RecordingHead.getY()-RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // BL
            HEAD_OUTPUT << "\n" << std::endl;

            FILENAME_idv = to_string_exact(File_count) + ".dat";
            Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
            HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
//                    PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grains, DataType::ALL);

            Log.logandshow(Logger_t::LogLvl::INFO,to_string_exact(File_count)+"    "+to_string_exact(Time)+"    "+to_string_exact(Profile_time)+"    "+to_string_exact(Downtrack_time)+"    "+
                                to_string_exact(RecordingHead.getX())+"    "+to_string_exact(RecordingHead.getY())+"    "+to_string_exact(Bit_X)+"    "+
                                to_string_exact(Track_num)+"    "+to_string_exact(Bits_written)+"    "+to_string_exact(RecordingHead.getField().z));
            ++File_count;
            Steps_until_output=0;
            end = std::chrono::high_resolution_clock::now();
            dur  = std::chrono::duration_cast<std::chrono::seconds>(end-start);
            Log.logandshow(Logger_t::LogLvl::TIME,to_string_exact(dur.count())+"s");
            start = std::chrono::high_resolution_clock::now();
            STEPS=0;
        }


        // Check if we have finished writing, ensure head completely passes the final written bit before ending
        if(Bits_written==Writable_Bit_positions.size()){
            // TODO: this currently doesn't seem to work for larger FWHM, not sure why but I need to look into this.
            if(RecordingHead.getX() <= Writable_Bit_positions.back().first+RecordingLayer.getBitSizeX()*0.5+RecordingHead.getFWHMx()){
                Runoff=true;
            }
            else{
                Runoff=false;
            }

        }

    }
    // End of system

    Log.logandshow(Logger_t::LogLvl::INFO,"Bits written: " + to_string_exact(Bits_written) + " of " + to_string_exact(Tot_bits));
    Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"0");
    // Perform simulation of entire completed system until more outputs are achieved.
    // ONLY required IF LLB_inclusion is used:
    if(Solver.get_Inclusion()){
        Log.logandshow(Logger_t::LogLvl::INFO,"Simulating entire system for an extra 1ns, due to use of inclusion zone.");
        Log.logandshow(Logger_t::LogLvl::INFO,"Step Time Profile_Time Downtrack_Time Head_X Head_Y Bit_x Bit_y Bit Field");
        Solver.disableInclusion();
        int Output_counter=0;
        // Remove laser pulse and applied field
        for(Grain_t & Grain : Grains){
            Grain.Temp = Environ_Temp;
            Grain.H_appl = 0.0;
        }
        // Integrate the whole system for extra 1ns
        double Time_final = Time;
        while(Time-Time_final<=1.0e-9){
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
            Steps_until_output++;
            Time += Solver.get_dt();

            // Only output every X steps
            if(Steps_until_output==output_steps){
                // Output write head position
                HEAD_OUTPUT << (RecordingHead.getX()-RecHeadHalfWidthX) << Delim << (RecordingHead.getY()-RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // BL
                HEAD_OUTPUT << (RecordingHead.getX()-RecHeadHalfWidthX) << Delim << (RecordingHead.getY()+RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // TL
                HEAD_OUTPUT << (RecordingHead.getX()+RecHeadHalfWidthX) << Delim << (RecordingHead.getY()+RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // TR
                HEAD_OUTPUT << (RecordingHead.getX()+RecHeadHalfWidthX) << Delim << (RecordingHead.getY()-RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // TR
                HEAD_OUTPUT << (RecordingHead.getX()-RecHeadHalfWidthX) << Delim << (RecordingHead.getY()-RecHeadHalfWidthY) << Delim << RecordingHead.getX() << Delim << RecordingHead.getY() << "\n";  // BL
                HEAD_OUTPUT << "\n" << std::endl;

                FILENAME_idv = to_string_exact(File_count) + ".dat";
                Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
                HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
//                PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grains, DataType::ALL);

                Log.logandshow(Logger_t::LogLvl::INFO,to_string_exact(File_count)+"    "+to_string_exact(Time)+"    "+to_string_exact(Profile_time)+"    "+to_string_exact(Downtrack_time)+"    "+
                                    to_string_exact(RecordingHead.getX())+"    "+to_string_exact(RecordingHead.getY())+"    "+to_string_exact(Bit_X)+"    "+
                                    to_string_exact(Track_num+Track_num_offset)+"    "+to_string_exact(Bits_written)+"    "+to_string_exact(RecordingHead.getField().z));
                ++File_count;
                Steps_until_output=0;
                ++Output_counter;
            }
        }
        // Output final system configuration at the end of extra 1ns independently of output steps
        FILENAME_idv = to_string_exact(File_count) + ".dat";
        Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
        ++File_count;
        HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
        PixMap.PrintwUpdate("PixelMap_Final_"+FILENAME_idv, Grains, DataType::ALL);
        // Export final system
        Log.logandshow(Logger_t::LogLvl::INFO,"Entire system evaluated");
        HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,Output_switch_buffer,Output_switch_file_loc);
        Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"1");

    }
    if(ReadBack){
        data_read_back(Grains,Voronoi_data,ReadingLayer,"Read_back.dat",std::vector<double>{Voronoi_data.Vx_MIN,Voronoi_data.Vy_MIN,Voronoi_data.Vx_MAX,Voronoi_data.Vy_MAX,Structure.cell_size},ReadingHead);
    }
    if(Time_evolution){
        Log.logandshow(Logger_t::LogLvl::INFO,"Performing time evolution...");
        Solver.disableInclusion();
        double Evol_time=0.0;
        for(Grain_t & Grain : Grains){
            Grain.Temp=Evol_temp;
        }

        Solver.set_Solver(Solvers::kMC);

        Solver.set_dt(1e-3);
        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
        Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
        Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"aTE");

        Solver.set_dt((Time_evol)*0.001);
        Log.logandshow(Logger_t::LogLvl::INFO,"\tdt = "+to_string_exact(Solver.get_dt())+" Total time = "+to_string_exact(Time_evol));
        while(Evol_time<Time_evol){
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
            Evol_time += Solver.get_dt();
        }
        Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"TE");
        FILENAME_idv = to_string_exact(File_count) + ".dat";
        Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
        Log.logandshow(Logger_t::LogLvl::INFO,"Completed.");
    }
    return 0;
}
