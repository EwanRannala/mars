/*
 * Thermoremanence.cpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

/** @file Thermoremanence.cpp
 * @brief Simulation of thermoremanence. */

#include <iostream>
#include <fstream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <cmath>

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include "Classes/Hdc.hpp"
#include "Classes/BasicLaser.hpp"
#include "Classes/Solver.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Grain_setup.hpp"
#include "Importers/Materials_import.hpp"
#include "Voronoi.hpp"
#include "Data_output/HAMR_grain_output.hpp"
#include "Interactions/Generate_interactions.hpp"

static int Determine_averages(unsigned int Num_layers, unsigned int Num_Grains, std::vector<Grain_t>& Grains,
        std::vector<double>& Average_Mx, std::vector<double>& Average_My, std::vector<double>& Average_Mz, std::vector<double>& Probability){

    std::vector<double> SUM_MsV(Num_layers+1,0.0);
    double SUM_MsV_all_layers=0.0;
    double Mx_all_layers=0.0;
    double My_all_layers=0.0;
    double Mz_all_layers=0.0;
    double Prob_all_layers=0.0;
    double Tot_grains = Num_Grains*Num_layers;

    Average_Mx.resize(Num_layers+1,0.0);
    Average_My.resize(Num_layers+1,0.0);
    Average_Mz.resize(Num_layers+1,0.0);
    Probability.resize(Num_layers+1,0.0);

    //##############################DETERMINE AVERAGE MAGNETISATIONS##############################//
    // Experimentally they measure Ms*V, thus the output needs to match that.
    // Determine normalisation values.
    for(unsigned int layer=0;layer<Num_layers;++layer){
        double dummy_SUM_MsV=0.0;
        double offset=Num_Grains*layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+offset;
            dummy_SUM_MsV += Grains[grain_in_system].Ms*Grains[grain_in_system].Vol;
        }
        SUM_MsV[layer]=dummy_SUM_MsV;
        SUM_MsV_all_layers+=SUM_MsV[layer];
    }
    SUM_MsV.back() = SUM_MsV_all_layers;

    // Determine average magnetisations.
    for(unsigned int layer=0;layer<Num_layers;++layer){
        double offset=Num_Grains*layer;
        Average_Mx.at(layer)=Average_My.at(layer)=Average_Mz.at(layer)=Probability.at(layer)=0.0;
        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+offset;
            Average_Mx.at(layer)  += Grains[grain_in_system].Ms*Grains[grain_in_system].Vol*Grains[grain_in_system].m.x;
            Average_My.at(layer)  += Grains[grain_in_system].Ms*Grains[grain_in_system].Vol*Grains[grain_in_system].m.y;
            Average_Mz.at(layer)  += Grains[grain_in_system].Ms*Grains[grain_in_system].Vol*Grains[grain_in_system].m.z;
            double Pol = (Grains[grain_in_system].m.z/fabs(Grains[grain_in_system].m.z));
            if(Pol>0){++Probability.at(layer);}
        }
        Mx_all_layers      += Average_Mx.at(layer);
        My_all_layers      += Average_My.at(layer);
        Mz_all_layers      += Average_Mz.at(layer);
        Prob_all_layers    += Probability.at(layer);
        Average_Mx.at(layer)  /= SUM_MsV[layer];
        Average_My.at(layer)  /= SUM_MsV[layer];
        Average_Mz.at(layer)  /= SUM_MsV[layer];
        Probability.at(layer) /= Num_Grains;

    }
    // Full system values
    Average_Mx.back() = Mx_all_layers/SUM_MsV.back();
    Average_My.back() = My_all_layers/SUM_MsV.back();
    Average_Mz.back() = Mz_all_layers/SUM_MsV.back();
    Probability.back() = Prob_all_layers/Tot_grains;

    return 0;
}
/**
 * @brief Simulation of thermoremanence
 * <P>
 * The granular system is placed under a constant magnetic field and a laser is applied
 * to heat the system for a specified duration. The laser is then removed and the
 * magnetisation is recorded. The system is then reset to its initial condition and
 * another laser pulse, with a different maximum temperature, is applied.
 * This is repeated until all requested laser pulses have been performed.
 * <BR>
 * If the pulse duration \f$ \geq 1\mu s\f$ then the Kinetic Monte Carlo solver is
 * automatically used for simulating the application phase. The solver specified in the
 * configuration file is always used for the heating and cooling phases.
 * </P>
 * <P>
 * This simulation will output the average magnetisation and probability of switching
 * per layer for each laser pulse peak temperature.
 * If "Thermoremanence:Output_individual_Grain_data" is set to true then magnetisation,
 * probability per laser pulse peak temperature is output for a number of specified grains
 * as determined by the value given to "Thermoremanence:Output_every_x_grain" in the
 * configuration file.
 * </P>
 *
 * @param cfg Configuration file data
 */
int Thermoremanence(const ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a Thermoremanence simulation");

    // Required input
    auto Environ_Temp = cfg.getValueOfKey<double>("Thermoremanence:Environment_Temp");
    auto Laser_Temp_MIN = cfg.getValueOfKey<double>("Thermoremanence:Laser_min");
    auto Laser_Temp_MAX = cfg.getValueOfKey<double>("Thermoremanence:Laser_max");
    auto Laser_Temp_interval = cfg.getValueOfKey<double>("Thermoremanence:Laser_interval");

    auto Equilibration_time = cfg.getValAndUnitOfKey<double>("Thermoremanence:Initialisation_time");
    auto Application_time = cfg.getValAndUnitOfKey<double>("Thermoremanence:Application_time");

    auto OUTPUT_GRAINS = cfg.getValueOfKey<bool>("Thermoremanence:Output_individual_Grain_data");
    unsigned int RES_grain=0;
    if(OUTPUT_GRAINS){
        RES_grain = cfg.getValueOfKey<unsigned int>("Thermoremanence:Output_every_x_grain");
    }

    Data_output_t DataOut(OutDir,cfg);
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t Int_system;
    std::vector<Grain_t> Grains;
    std::vector<Grain_t> Grains_BACKUP;

    double Time=0.0;
    std::string FILENAME_idv;

    std::ofstream OUTPUT_FILE_in_time;
    std::ofstream OUTPUT_FILE_final_values;
    std::ofstream MAG_DIST_OUT;
    std::ofstream TC_DIST_CHECK_FILE(OutDir+"/TC_dist_check.dat");
    TC_DIST_CHECK_FILE << "Grain Tc tot_grains" << std::endl;

//##########################################IMPORT ALL REQUIRED DATA#########################################//
    Structure_import(cfg, &Structure);
    Hdc FieldDC(cfg);
    BasicLaser LaserPulse(cfg,Environ_Temp);
    Materials_import(cfg,Structure.Num_layers, &Materials_Config, &Materials);
    Voronoi(Structure,&Voronoi_data);
    Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Environ_Temp,Grains);
    Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Grains,Int_system);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    Solver.Force_specific_solver_construction(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config,Solvers::kMC); // Ensure kMC data is imported from configuration file
    Solver.disableInclusion(); // No use for inclusion zone in this sim
    unsigned int Output_Steps = Solver.getOutputSteps();
    unsigned int Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;

    for(Grain_t & Grain : Grains){
        Grain.H_appl = FieldDC.getMax()*FieldDC.getDirn();
    }
    int Iterations = static_cast<int>((Laser_Temp_MAX - Laser_Temp_MIN)/Laser_Temp_interval + 1);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###########Make a copy of every grain in order to reset the system after each laser pulse##################//
    Grains_BACKUP = Grains;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################################PERFORM LASER PULSES#########################################//
    Solvers input_Solver=Solver.get_Solver();
    double input_dt=Solver.get_dt();
    double reduced_dt=input_dt;

    Log.logandshow(Logger_t::LogLvl::INFO,"Iterations: "+to_string_exact(Iterations));

    for(int runs=0;runs<Iterations;++runs){
        unsigned int steps=0;
        LaserPulse.setTempMax(Laser_Temp_MIN+runs*Laser_Temp_interval);
        // Ensure at least 100 steps are performed during the heating/cooling phases
        if(LaserPulse.getRate()>0.0){
            if(LaserPulse.getCoolingTime()<100.0*Solver.get_dt()){
                reduced_dt = LaserPulse.getCoolingTime()*0.01;
                Solver.set_dt(reduced_dt);
                Log.logandshow(Logger_t::LogLvl::WARN,"Reduced dt to "+to_string_exact(reduced_dt)+" for heating and cooling phases.");
            }
        }
        //#########################################SET UP OUTPUT FILES#########################################//
        std::vector<double> Average_Mx;
        std::vector<double> Average_My;
        std::vector<double> Average_Mz;
        std::vector<double> Probability;
        std::string FILENAME_pre_T = "Temp_" + to_string_exact(LaserPulse.getTempMax()) + "_";
        Log.logandshow(Logger_t::LogLvl::INFO,"Pulse temperature = "+to_string_exact(LaserPulse.getTempMax()));

        for(unsigned int layer=0;layer<=Structure.Num_layers;++layer){
            std::string FILENAME_pre_1 = "Layer_" + to_string_exact(layer) + "_";
            OUTPUT_FILE_in_time.open((OutDir+"/" + FILENAME_pre_1 + FILENAME_pre_T + "Average_M.dat").c_str());
            OUTPUT_FILE_in_time << "Mx My Mz Prob Temp time" << std::endl;
            OUTPUT_FILE_in_time.close();

            if(layer==0){
                if(OUTPUT_GRAINS){
                    for(unsigned int OUT_grain=0;OUT_grain<Voronoi_data.Num_Grains;OUT_grain+=RES_grain){
                        std::string FILENAME_pre_1b = "Grain_" + to_string_exact(OUT_grain) + "_";
                        OUTPUT_FILE_in_time.open((OutDir+"/GRAINS/" + FILENAME_pre_1 + FILENAME_pre_1b + FILENAME_pre_T + "_M.dat").c_str());
                        OUTPUT_FILE_in_time << "Mx My Mz Temp time" << std::endl;
                        OUTPUT_FILE_in_time.close();
        }   }   }   }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        Log.logandshow(Logger_t::LogLvl::INFO,"Run "+to_string_exact(runs)+" of "+to_string_exact(Iterations));
        //#########################################Equilibration phase#########################################//
        Log.logandshow(Logger_t::LogLvl::INFO,"Equilibrating...");
        while(Time<Equilibration_time){
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,Voronoi_data.Centre_X,Voronoi_data.Centre_Y);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
            Time += Solver.get_dt();
        }
        Log.logandshow(Logger_t::LogLvl::INFO,"Done.");
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        //##########################################Apply Laser pulse##########################################//
        LaserPulse.turnOn();
        Time = 0.0;
        double Time_dur_app=0.0;
        do{
#ifdef RealTimePrint
            std::cout << std::setprecision(10) << Time << " | | " << LaserPulse.getCurTemp() << " " << Solver.get_dt() << "\r" << std::flush;
#endif
            // Perform simulation
            LaserPulse.updateLaser(Solver.get_dt());
            for(unsigned int grain=0;grain<Tot_grains;++grain){Grains[grain].Temp = LaserPulse.getTemp();}
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
            ++steps;
            Time += Solver.get_dt();

            if(LaserPulse.isMax()){
                Time_dur_app+=Solver.get_dt();

                if(Application_time<=1.0e-6){
                    // Reset timesteps for application phase
                    Solver.set_dt(input_dt);
                }
                // Utilise kMC solver if application time exceeds 1 microsecond
                else if(Solver.get_Solver()!=Solvers::kMC){
                    Solver.set_Solver(Solvers::kMC);
                    Log.logandshow(Logger_t::LogLvl::INFO,"Enablng kMC solver | dt = "+to_string_exact(Solver.get_dt()));
                }
                // Revert solver once application is completed & set reduced timesteps for cooling phase
                if(Time_dur_app>=Application_time){
                    Solver.set_Solver(input_Solver);
                    Solver.set_dt(reduced_dt);
                    LaserPulse.turnOff();
                    Log.logandshow(Logger_t::LogLvl::INFO,"Resetting solver type");
                }
            }
            //#############################################Data Output#############################################//
            if(steps==Output_Steps){
                steps=0;
                Determine_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,Average_Mx,Average_My,Average_Mz,Probability);
                // OUTPUT for time resolved data
                for(unsigned int layer=0;layer<=Structure.Num_layers;++layer){
                    std::string FILENAME_pre_1 = "Layer_" + to_string_exact(layer) + "_";
                    OUTPUT_FILE_in_time.open((OutDir+"/" + FILENAME_pre_1 + FILENAME_pre_T + "Average_M.dat").c_str(),std::ofstream::app);
                    OUTPUT_FILE_in_time <<std::setprecision(8) << Average_Mx[layer] << Delim << Average_My[layer]  << Delim << Average_Mz[layer]  << Delim << Probability[layer]  << Delim << Grains[0].H_appl.z << Delim << Grains[0].Temp << Delim << Time << std::endl;
                    OUTPUT_FILE_in_time.close();

                    unsigned int Offset=layer*Voronoi_data.Num_Grains;
                    if(layer==0){
                        if(OUTPUT_GRAINS){
                            for(unsigned int OUT_grain=0;OUT_grain<Voronoi_data.Num_Grains;OUT_grain+=RES_grain){
                                unsigned int grain_in_system = OUT_grain+Offset;
                                std::string FILENAME_pre_1b = "Grain_" + to_string_exact(OUT_grain) + "_";
                                OUTPUT_FILE_in_time.open((OutDir+"/GRAINS/" + FILENAME_pre_1 + FILENAME_pre_1b + FILENAME_pre_T + "_M.dat").c_str(),std::ofstream::app);
                                OUTPUT_FILE_in_time << std::setprecision(15) << Grains[grain_in_system].m << Delim << Grains[grain_in_system].Temp << Delim << Time << std::endl;
                                OUTPUT_FILE_in_time.close();
                }   }   }   }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        }
        while(LaserPulse.isOn() || !LaserPulse.isMin());
        std::cout << std::endl;
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        //################################ OUTPUT MAGNETISATION DISTRIBUTION ##################################//

        for(unsigned int layer=0;layer<Structure.Num_layers;++layer){
            std::string FILENAME_pre_L = "Layer_" + to_string_exact(layer) + "_";
            MAG_DIST_OUT.open((OutDir+"/"+FILENAME_pre_L+FILENAME_pre_T+"Mag_dist.dat").c_str());
            double offset=Voronoi_data.Num_Grains*layer;
            for(unsigned int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = grain_in_layer+offset;
                MAG_DIST_OUT << Voronoi_data.Num_Grains << Delim << grain_in_system << Delim
                             << Grains[grain_in_system].m.x << Delim << Grains[grain_in_system].m.y << Delim
                             << Grains[grain_in_system].m.z << Delim << LaserPulse.getTempMax() << Delim << Time << std::endl;
            }
            MAG_DIST_OUT.close();
        }
        // Output for thermo-remanence data
        Determine_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,Average_Mx,Average_My,Average_Mz,Probability);
        for(unsigned int layer=0;layer<=Structure.Num_layers;++layer){
            std::string FILENAME_pre_L = "Layer_" + to_string_exact(layer) + "_";
            OUTPUT_FILE_final_values.open((OutDir+"/" + FILENAME_pre_L + "Final_magnetisation.dat").c_str(),std::ofstream::app);
            OUTPUT_FILE_final_values << Average_Mx[layer] << Delim << Average_My[layer]  << Delim << Average_Mz[layer]  << Delim << Probability[layer]  << Delim <<  LaserPulse.getTempMax() << Delim << Time << std::endl;
            OUTPUT_FILE_final_values.close();
        }
        // Output final system configuration
        Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,".dat",FILENAME_pre_T+"Final_config_");
        Grains = Grains_BACKUP;
        Time = 0.0;
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    }
    return 0;
}
