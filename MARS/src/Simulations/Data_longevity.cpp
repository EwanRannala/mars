/*
 * Data_longevity.cpp
 *
 *  Created on: 14 Nov 2019
 *      Author: Samuel Ewan Rannala
 */

/** @file Data_longevity.cpp
 * @brief Simulation for the read back of bits form a system after extended periods of time. */

#include <string>
#include <iostream>
#include <algorithm>

#include "Structures.hpp"
#include "Classes/ReadLayer.hpp"
#include "Classes/ReadHead.hpp"
#include "Classes/Solver.hpp"
#include "Importers/Read_in_import.hpp"
#include "Importers/Create_system_from_output.hpp"
#include "Simulations/Read_back.hpp"
#include "Data_output/HAMR_grain_output.hpp"
#include "Config_File/ConfigFile_import.hpp"

#include "Importers/Structure_import.hpp"
#include "Importers/Materials_import.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"

#include "Simulation_globals.hpp"

int Data_longevity(const ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a time evolution (data longevity) simulation");

    Data_output_t DataOut(OutDir,cfg);
    Data_input_t IN_DATA;
    Structure_t Structure;
    Voronoi_t VORO;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    std::vector<Grain_t> Grains;
    Interaction_t INT;

    auto Total_snaps   = cfg.getValueOfKey<unsigned int>("DataLongevity:Total_snapshots");
    auto Time_limit    = cfg.getValAndUnitOfKey<std::vector<double>>("DataLongevity:Times");
    auto Environ_Temp  = cfg.getValueOfKey<double>("DataLongevity:Environment_Temp");
    auto Importsystem  = cfg.getValueOfKey<bool>("DataLongevity:Import_System");
    auto Variabledt    = cfg.getValueOfKey<bool>("DataLongevity:Variable_timestep");
    if(Total_snaps>Time_limit.size()){
        Total_snaps=Time_limit.size();
    }
    std::sort(Time_limit.begin(),Time_limit.end());
    Sim::Time=0.0;
    unsigned int Snap=0;

    std::vector<double> PM_lims(5,0.0); // minX, minY, maxX, maxY, cellsize
    PM_lims[4] = cfg.getValAndUnitOfKey<double>("struct:Pixel_size");

//    std::string FILENAME_idv = to_string_exact(Snap)+".dat";
    std::string Read_back_FILENAME = "DL_Read_back_Img_"+to_string_exact(Snap)+".dat";
    Solver_t Solver(cfg);

    if(Importsystem){
        Read_in_import(cfg, &IN_DATA);
        Read_in_system(IN_DATA,Structure,VORO,Materials,Grains,INT,Solver);
    }
    else{
        Structure_import(cfg, &Structure);
        Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
        Voronoi(Structure, &VORO);
        Grain_setup(Structure.Num_layers,VORO,Materials,Environ_Temp,Grains);
        Generate_interactions(cfg,Structure.Num_layers,Materials,VORO,Grains,INT);
        // This forces the import of the susceptibility data
        Solver.Force_specific_solver_construction(VORO.Num_Grains,Structure.Num_layers,cfg,Materials_Config,Solvers::LLB);
        std::cout << Solver.get_SusType(0) << std::endl;
        PM_lims[0] = VORO.Vx_MIN;
        PM_lims[1] = VORO.Vy_MIN;
        PM_lims[2] = VORO.Vx_MAX;
        PM_lims[3] = VORO.Vy_MAX;
    }
    Solver.disableInclusion();

    ReadHead ReadingHead(cfg);
    ReadLayer ReadingLayer(cfg);
    double Input_dt = Solver.get_dt();
    for(Grain_t & Grain : Grains){
        Grain.Temp = Environ_Temp;
    }
    // Initial t=0 system read back
    data_read_back(Grains, VORO, ReadingLayer, Read_back_FILENAME, PM_lims, ReadingHead);
    // Output system image
//    Idv_Grain_output(Structure.Num_layers,VORO,Grains,Sim::Time,FILENAME_idv,"DL_");


    // Loop over each required snapshot
    for(unsigned int snapshot=1;snapshot<=Total_snaps;++snapshot){
        // Select time_limit from input
        // TODO Consider always setting dt to 100th
        double snap_time = Time_limit[snapshot-1];
        if(Variabledt){
            if(Solver.get_dt()<(snap_time-Sim::Time)*0.01){
                Log.logandshow(Logger_t::LogLvl::INFO,"Increasing time step to " + to_string_exact((snap_time-Sim::Time)*0.01) +"s");
                Solver.set_dt((snap_time-Sim::Time)*0.01);
            }
            else if(Solver.get_dt()>=(snap_time-Sim::Time)){
                Log.logandshow(Logger_t::LogLvl::INFO,"Decreasing time step to " + to_string_exact((snap_time-Sim::Time)*0.01) + "s");
                Solver.set_dt((snap_time-Sim::Time)*0.01);
            }
        }
        Log.logandshow(Logger_t::LogLvl::INFO,"Evolving system over "+to_string_exact(snap_time)+" seconds.");
        Read_back_FILENAME = "Read_back_Img_DL" + to_string_exact(snapshot) + ".dat";
//        FILENAME_idv = to_string_exact(snapshot) + ".dat";
        // Perform kMC until reach limit time
        while(Sim::Time<snap_time){
            Solver.Apply_Inclusion_zone(VORO,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(VORO,Structure.Num_layers,INT,Grains,DataOut);
            Sim::Time += Solver.get_dt();
        }
        data_read_back(Grains, VORO, ReadingLayer, Read_back_FILENAME, PM_lims, ReadingHead);
//        Idv_Grain_output(Structure.Num_layers,VORO,Grains,Sim::Time,FILENAME_idv,"DL_");
        Solver.set_dt(Input_dt);
    }
    return 0;
}
