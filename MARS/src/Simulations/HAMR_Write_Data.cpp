/*
 * HAMR_Write_Data.cpp
 *
 *  Created on: 28 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file HAMR_Write_Data.cpp
 * \brief Simulation of HAMR writing process with an instantaneously moving write head.
 *
 * <P> Data is written to the system via heat assisted magnetic recording. The write head is positioned
 * at the centre of a bit and the field and laser pulses are applied. Once a bit has been written the
 * write head is moved instantaneously to the centre of the next bit. </P>
 * <P> The system is first allowed to equilibrate for <B><EM> HAMR:Equilibration_time </EM></B>, once
 * the equilibration is complete a list of all bit positions is created. A list of grains in each bit
 * is also determined, allowing for individual data for each bit to be output. </P>
 * <P> Each time step consists of an LLB integration and the application of a magnetic field and laser
 * pulse. Once all bits and tracks have been written the final system is exported. If
 * <B><EM> HAMR:Readback </EM></B> is set to <B> True </B> then a read back simulation is performed
 * before the simulations exits. </P> */

#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <string>
#include <chrono>

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include "Classes/RecHead.hpp"
#include "Classes/Solver.hpp"
#include "Classes/PixelMap.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Grain_setup.hpp"
#include "Importers/Materials_import.hpp"
#include "Voronoi.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Simulations/Read_back.hpp"
#include "Data_output/HAMR_grain_output.hpp"
#include "Data_output/HAMR_layer_output.hpp"
#include "Data_output/HAMR_switching_output.hpp"
#include "Data_output/Export_system.hpp"

/** Performs HAMR writing process with an instantaneously moving write head.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 *         <UL>
 *            <LI> Type <LI> SEED
 *        </UL>
 * <LI> Struct:
 *         <UL>
 *             <LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 *             <LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 *         </UL>
 * <LI> LLB:
 *         <UL>
 *             <LI> dt <LI> Inclusion_zone
 *             <LI> <STRONG> If Inclusion_zone == True </STRONG>
 *             <UL>
 *                 <LI> Inclusion_range_negX <LI> Inclusion_range_posX <LI> Inclusion_range_negY <LI> Inclusion_range_posY
 *             </UL>
 *         </UL>
 * <LI> HAMR:
 *         <UL>
 *             <LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 *             <LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 *             <LI> Measurement_time <LI> equilibration_time <LI> Field_X_width <LI> Field_Y_width
 *             <LI> laser_fwhm_x <LI> laser_fwhm_y <LI> Thermal_gradient <LI> Bit_width
 *             <LI> Bit_spacing <LI> Bit_spacing_X <LI> Bit_spacing_Y <LI> Bit_number_in_X
 *             <LI> Bit_number_in_Y <LI> Data_Binary
 *         </UL>
 * <LI> EXPT:
 *         <UL>
 *             <LI> ReadBack
 *             <LI> <STRONG> If ReadBack == True </STRONG>
 *                 <UL>
 *                     <LI> Read_Head_speed <LI> Read_Head_record_interval <LI> Read_Head_width_X <LI> Read_Head_width_Y
 *                 </UL>
 *         </UL>
 * </UL>
 * */
int HAMR_write_data(ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a HAMR (instantly moving write head) simulation");

    auto Equilibration_time = cfg.getValAndUnitOfKey<double>("HAMRWD:Equilibration_time");
    auto Environ_Temp = cfg.getValueOfKey<double>("HAMRWD:Environment_Temp");
    auto ReadBack = cfg.getValueOfKey<bool>("HAMRWD:ReadBack_after_write");

    // Declare all required variables
    Data_output_t DataOut(OutDir,cfg);
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    Interaction_t Int_system;
    std::vector<Grain_t> Grains;
    std::vector<ConfigFile> Materials_Config;
    std::vector<double> Normaliser;
    std::vector<double> Mx;
    std::vector<double> My;
    std::vector<double> Mz;
    std::vector<double> Ml;
    std::vector<unsigned int> Grain_output_list;

    int Bit=0;
    unsigned int File_count=0;
    unsigned int Sim_time_predictor=0;
    unsigned int file_writes=0;
    unsigned int output_steps=0;
    unsigned int Tot_bits=0;
    double Temperature=0.0;
    double Time=0.0;
    double Time_dur_pulse=0.0;
    double Tot_time=0.0;
    std::string FILENAME_idv;
    std::string Output_switch_file_loc = OutDir+"/HAMR_Grain_switching.dat";
    std::vector<std::string> Output_switch_buffer;
    std::string OUTPUT = OutDir+"/HAMR_multi_layer.dat";
    std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends

//####################IMPORT ALL REQUIRED DATA####################//
    Structure_import(cfg, &Structure);
//    HAMR_writing_expt_import(cfg, "HAMR_write_data",&Expt_data,&Expt_laser,&Expt_field,&Expt_timings);
//    Read_Back_import(cfg, &Expt_data_read);
    Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Environ_Temp,Grains);
    Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Grains,Int_system);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    RecHead RecordingHead(cfg);
    RecLayer RecordingLayer(cfg,RecordingHead.getDirn().z);
    PixelMap PixMap(Voronoi_data.Vx_MIN,Voronoi_data.Vy_MIN,Voronoi_data.Vx_MAX,Voronoi_data.Vy_MAX,(Structure.Grain_width/10.0));
    PixMap.Discretise(Voronoi_data, Grains);
    // Best to check for suitable input before the simulation has been performed
    ReadLayer ReadingLayer(RecordingLayer);
    ReadHead ReadingHead(RecordingLayer);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    if(RecordingHead.getRampTime()<Solver.get_dt()){
        Log.log(Logger_t::LogLvl::ERR,"Ramp time less than timestep!");
    }
//###################DETERMINE IMPORTANT VALUES###################//

    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
    Tot_bits = RecordingLayer.getBitsA();
    output_steps = Solver.getOutputSteps();
    Temperature = Environ_Temp;
// Want these to be assigned from input.
    RecordingHead.setX(RecordingLayer.getBitSizeX()*0.5+RecordingLayer.getBitSpacingX());
    RecordingHead.setY(RecordingLayer.getBitSizeY()*0.5+RecordingLayer.getBitSpacingY());

    Tot_time = RecordingHead.getProfileTime()*Tot_bits;
    file_writes = static_cast<unsigned int>(round(Tot_time/Solver.getMeasTime()));
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##########Equilibrate the system for Equilibration_time#########//

    Log.logandshow(Logger_t::LogLvl::INFO,"Equilibrating the system for "+to_string_exact(Equilibration_time)+"s");
    while(Time<Equilibration_time){
        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,RecordingHead.getX(),RecordingHead.getY());
        Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
        Time += Solver.get_dt();
    }
    // Reset time to zero
    Log.logandshow(Logger_t::LogLvl::INFO,"Resetting time to t = 0.0 s");
    Time = 0.0;

    HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
    HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grains,Materials.dz,&Output_switch_buffer);

    Log.logandshow(Logger_t::LogLvl::INFO,"Output steps: "+to_string_exact(output_steps));
    Log.logandshow(Logger_t::LogLvl::INFO,"Simulation length: "+to_string_exact(Tot_time));
    auto start = std::chrono::system_clock::now();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###############Generate a list of bit positions#################//
    Log.logandshow(Logger_t::LogLvl::INFO,"Generating Bit positions");
    double bit_position_X=RecordingLayer.getBitSizeX()*0.5+RecordingLayer.getBitSpacingX();
    double bit_position_Y=RecordingLayer.getBitSizeY()*0.5+RecordingLayer.getBitSpacingY();
    std::vector<std::pair<double,double>> Bit_positions;
    std::pair<double,double> XY_coords;
    for(unsigned int Bit_y=0;Bit_y<RecordingLayer.getBitsY();++Bit_y){
        for(unsigned int Bit_x=0;Bit_x<RecordingLayer.getBitsX();++Bit_x){
            XY_coords = std::make_pair(bit_position_X,bit_position_Y);
            Bit_positions.push_back(XY_coords);
            bit_position_X += RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX();
        }
        bit_position_X = Bit_positions[0].first;
        bit_position_Y += RecordingLayer.getBitSizeY()+RecordingLayer.getBitSpacingY();
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//######Generate a list of the grains contained in each bit#######//
    Log.logandshow(Logger_t::LogLvl::INFO,"Generating grains in bit");
    RecordingLayer.setGrainsinBit(Voronoi_data.Num_Grains,Voronoi_data.Pos_X_final,Voronoi_data.Pos_Y_final,Bit_positions);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#########################Perform writing########################//
    // Simulation will run until all bits are written.
    for(unsigned int Bit_number=0;Bit_number<Tot_bits;++Bit_number){
        RecordingHead.setX(Bit_positions[Bit_number].first);
        RecordingHead.setY(Bit_positions[Bit_number].second);
        RecordingHead.setDirn(Vec3(0.0,0.0,RecordingLayer.getWritableData(Bit)));
        Log.logandshow(Logger_t::LogLvl::INFO,"Head Position: "+to_string_exact(RecordingHead.getX())+" , "+to_string_exact(RecordingHead.getY()));

        // For a single bit write
        RecordingHead.turnOnfor(RecordingHead.getProfileTime()-RecordingHead.getRampTime());
        while(Time_dur_pulse<RecordingHead.getProfileTime()){
            for(unsigned int Steps_until_output=0;Steps_until_output<output_steps;Steps_until_output++){
                RecordingHead.ApplyTspatial(Voronoi_data,Structure.Num_layers,Environ_Temp,Time_dur_pulse,Temperature,Grains);
                RecordingHead.ApplyHspatial(Voronoi_data,Structure.Num_layers,Solver.get_dt(),Grains);
                Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,RecordingHead.getX(),RecordingHead.getY());
                Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
                Time += Solver.get_dt();
                Time_dur_pulse += Solver.get_dt();
                std::cout << "Time: " << Time << "\r";
            }
            /*
             * Could do with having layer specific file outputs?
             */
            FILENAME_idv = to_string_exact(File_count) + ".dat";
            Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
            HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
            ++File_count;

            // Predict simulation run time
            ++Sim_time_predictor;
            if(Sim_time_predictor==10){
                auto end = std::chrono::system_clock::now();
                std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
                double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
                Log.logandshow(Logger_t::LogLvl::INFO,"Predicted sim time: "+to_string_exact(Predicted_Sim_time)+"s");
        }    }
        Time_dur_pulse = 0.0;
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"Bits written: "+to_string_exact(Tot_bits));
    HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,Output_switch_buffer,Output_switch_file_loc);
    Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grains,Solver,"1");
    Log.logandshow(Logger_t::LogLvl::INFO,"Finished data write");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###########################Read Back############################//
    if(ReadBack){
        data_read_back(Grains,Voronoi_data,ReadingLayer,"Read_back.dat",std::vector<double>{Voronoi_data.Vx_MIN,Voronoi_data.Vy_MIN,Voronoi_data.Vx_MAX,Voronoi_data.Vy_MAX,Structure.cell_size},ReadingHead);
    }
    return 0;
}
