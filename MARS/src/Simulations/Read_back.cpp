/*
 * Read_back.cpp
 *
 *  Created on: 30 Jul 2019
 *      Author: Ewan Rannala
 */

/** \file Read_back.cpp
 * \brief Function used for read back simulations.*/

#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>

#include "Simulations/Read_back.hpp"
#include "Importers/Read_in_import.hpp"
#include "Importers/Create_system_from_output.hpp"
#include "Data_output/HAMR_grain_output.hpp"
#include "Classes/PixelMap.hpp"
#include "Data_output/Export_system.hpp"

/** Function to determine the signal obtained from reading the magnetisation of the granular system using the discretised grid system.
 * \param[in] Dx Size of read head in x-dimension.
 * \param[in] Dy Size of read head in y-dimension.
 * \param[in] Cx Centre of read head x-coordinate.
 * \param[in] Cy Centre of read head y-coordinate.
 * \param[out] Cells Pointer to 2D vector of cells that make up the grid.
 */
static double Determine_signal(const ReadHead& ReadingHead,PixelMap& PixMap){

    double Signal=0.0;
    unsigned int Tot_pixels=0;
    for(unsigned int y=0;y<PixMap.getRows();++y){
        for(unsigned int x=0;x<PixMap.getCols();++x){
            if(PixMap.Access(x,y).get_Grain()>=0){ // Ignore any cells outside of a grain
                double SFunc=ReadingHead.ApplySfunc(PixMap.Access(x,y).get_Coords().first,PixMap.Access(x,y).get_Coords().second);
                Signal += PixMap.Access(x, y).get_M().z * SFunc;
                // Normalisation: we will normalise the signal by the number of pixels within the sensitivty function.
                if(ReadingHead.getSensitivityFunction()==ReadHead::SensType::Box){
                    // Only pixels within the sensitivty function will be counted.
                    if(fabs(SFunc-1.0)<1e-5){
                        ++Tot_pixels;
                    }
                }
            }
        }
    }
    if(ReadingHead.Norm() && ReadingHead.getSensitivityFunction()==ReadHead::SensType::Box){
        Signal /= Tot_pixels > 0 ? Tot_pixels : 1.0;
    }
    return Signal;
}

/** Function performs read back of magnetisation of a system. First a Pixel map of the system is generated
 * and then a read head is scanned along the tracks and the magnetisation is recorded. A rolling 5-point average of
 * the generated signal is created to reduce noise expected in the true signal.
 *
 * \param[in] Grains Pointer to grain data.
 * \param[in] VORO Pointer to Voronoi data.
 * \param[in] ReadingLayer Read layer containing required read back simulation data.
 * \param[in] FILENAME String used as the output files name.
 * \param[in] ReadingHead Read head used for the simulation.
 */
int data_read_back(const std::vector<Grain_t>& Grains, const Voronoi_t&VORO, const ReadLayer& ReadingLayer, const std::string& FILENAME, const std::vector<double>& PM_Limits, ReadHead ReadingHead){
//######################################CREATE DISCRETISED SYSTEM##########################//
    PixelMap PixMap(PM_Limits[0], PM_Limits[1], PM_Limits[2], PM_Limits[3], PM_Limits[4]);
    PixMap.Discretise(VORO, Grains);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###################################READ BACK MAGNETISATION###############################//
    ReadingHead.setPosX(PM_Limits[0]);
    ReadingHead.setPosY(ReadingLayer.getBitSizeY()*0.5+ReadingLayer.getPadding());

    std::ofstream READ_OUTPUT((OutDir+"/" + FILENAME).c_str());

    std::vector<std::vector<double>> Signals(ReadingLayer.getBitsY(),std::vector<double>(PixMap.getCols(),0.0));
    std::vector<double> x(PixMap.getCols(),0.0);
    std::vector<double> y(ReadingLayer.getBitsY(),0.0);

    for(unsigned int Track=0;Track<ReadingLayer.getBitsY();++Track){
        Log.logandshow(Logger_t::LogLvl::INFO,"Head Position: "+to_string_exact(ReadingHead.getPosX())+" , "+to_string_exact(ReadingHead.getPosY()));
        y[Track]=ReadingHead.getPosY();
        for(size_t it=0;it<Signals[Track].size();++it){
            if(Track==0){x[it]=ReadingHead.getPosX();} // Same for all track so only do this once.
            Signals[Track][it] = Determine_signal(ReadingHead, PixMap);
            ReadingHead.setPosX(ReadingHead.getPosX()+PM_Limits[4]);
        }
        ReadingHead.setPosY(ReadingHead.getPosY()+ReadingLayer.getBitSizeY()+ReadingLayer.getBitSpacingY());
        ReadingHead.setPosX(PM_Limits[0]);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################PRINT DATA TO OUTPUT FILE##################################//
    READ_OUTPUT << "#Track" << Delim << "x" << Delim << "y" << Delim << "signal" << std::endl;
    for(size_t track=0;track<Signals.size();++track){
        for(size_t i=0;i<Signals[track].size();++i){
            READ_OUTPUT << std::setprecision(5) << track << Delim
                        << x[i] << Delim << y[track] << Delim << Signals[track][i] << std::endl;
        }
        READ_OUTPUT << std::endl << std::endl;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    return 0;
}
/** Function performs read back simulation on a previously generated system using the
 *Read_in_system(const Data_input_t IN_data, Structure_t*Structure, Voronoi_t*VORO, Material_t*Materials, Grain_t*Grain, Interaction_t*Int, Solver_t*Solver) function.
 */
int data_read_back_from_import(ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a Read Back simulation from imported system");

    Data_input_t IN_data;
    Structure_t Structure;
    Voronoi_t VORO;
    Material_t Materials;
    std::vector<Grain_t> Grains;
    Interaction_t Int;
    std::string OUTPUT_FILE = "Read_back_from_input.dat";

    Read_in_import(cfg, &IN_data);
    ReadHead ReadingHead(cfg);
    ReadLayer ReadingLayer(cfg);
    Solver_t Solver(cfg);
    Read_in_system(IN_data,Structure,VORO,Materials,Grains,Int,Solver);
    Export_system(Structure.Num_layers,Materials.dz,VORO,Int,Grains,Solver,"0");
    std::vector<double> PM_lims(5,0.0);
    PM_lims[0]=VORO.Vx_MIN;
    PM_lims[1]=VORO.Vy_MIN;
    PM_lims[2]=VORO.Vx_MAX;
    PM_lims[3]=VORO.Vy_MAX;
    PM_lims[4]=cfg.getValAndUnitOfKey<double>("Struct:Pixel_size");
    data_read_back(Grains,VORO,ReadingLayer,OUTPUT_FILE,PM_lims,ReadingHead);

    return 0;
}