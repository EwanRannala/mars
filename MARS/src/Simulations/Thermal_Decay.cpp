/*
 * Thermal_Decay.cpp
 *
 *  Created on: 16 Nov 2022
 *      Author: Samuel Ewan Rannala
 */

/** @file Thermal_Decay.cpp
 * @brief Simulation for the thermal decay of magnetisation. */

#include <string>
#include <iostream>
#include <algorithm>

#include "Structures.hpp"
#include "Classes/ReadLayer.hpp"
#include "Classes/ReadHead.hpp"
#include "Classes/Solver.hpp"
#include "Classes/Hdc.hpp"
#include "Importers/Read_in_import.hpp"
#include "Importers/Create_system_from_output.hpp"
#include "Simulations/Read_back.hpp"
#include "Data_output/HAMR_grain_output.hpp"
#include "Config_File/ConfigFile_import.hpp"

#include "Importers/Structure_import.hpp"
#include "Importers/Materials_import.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"

#include "Simulation_globals.hpp"

int Thermal_Decay(const ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a thermal decay simulation");

    Data_output_t DataOut(OutDir,cfg);
    Data_input_t IN_DATA;
    Structure_t Structure;
    Voronoi_t VORO;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t INT;

    auto Total_time    = cfg.getValueOfKey<double>("ThermalDecay:Total_Time");
    auto Environ_Temp  = cfg.getValueOfKey<double>("ThermalDecay:Environment_Temp");
    auto LaserStart    = cfg.getValueOfKey<double>("ThermalDecay:Laser_Start_Time");
    auto Laser_Temp    = cfg.getValueOfKey<double>("ThermalDecay:Laser_Temp");

    Sim::Time=0.0;

    std::vector<double> PM_lims(5,0.0); // minX, minY, maxX, maxY, cellsize
    PM_lims[4] = cfg.getValAndUnitOfKey<double>("struct:Pixel_size");

    Solver_t Solver(cfg);
    Structure_import(cfg, &Structure);
    Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);

    // Want to repeat Voronoi construction until we get the desired distribution values
    int limit=1000;
    int iter=0;
    auto TimerSTART = std::chrono::steady_clock::now();

    double ExpMean   = sqrt(1+Structure.StdDev_grain_pos*Structure.StdDev_grain_pos)*exp(log(Structure.Grain_width/sqrt(1.0+((pow(Structure.Grain_width*Structure.StdDev_grain_pos,2))/(pow(Structure.Grain_width,2))))));
    double ExpStdDev = Structure.StdDev_grain_pos;
    Structure.Grain_width *= 0.9;
    Structure.StdDev_grain_pos *= 1.10;
    do
    {
        Voronoi(Structure, &VORO);
        Log.logandshow(Logger_t::LogLvl::INFO,"Mean: "+to_string_exact(ExpMean)+" sigma: "+to_string_exact(ExpStdDev));
        Log.logandshow(Logger_t::LogLvl::INFO,"Mean: "+to_string_exact(VORO.obtainedMean)+" sigma: "+to_string_exact(VORO.obtainedStdDev));
        Log.logandshow(Logger_t::LogLvl::INFO,"Mean: "+to_string_exact(fabs(ExpMean-VORO.obtainedMean))+" sigma: "+to_string_exact(fabs(ExpStdDev-VORO.obtainedStdDev)));
        ++iter;
        auto TimerEND = std::chrono::steady_clock::now();
        auto TimerDUR = std::chrono::duration_cast<std::chrono::seconds>(TimerEND-TimerSTART);

        if(iter>=limit || TimerDUR.count()>600)
        {
            Log.logandshow(Logger_t::LogLvl::INFO,"Cannot obtain distrbution");
            return 0;
        }
    }while(fabs(ExpMean-VORO.obtainedMean)>0.05 || fabs(ExpStdDev-VORO.obtainedStdDev)>0.001);

    Grain_setup(Structure.Num_layers,VORO,Materials,Environ_Temp,Sim::Grains);
    Generate_interactions(cfg,Structure.Num_layers,Materials,VORO,Sim::Grains,INT);
    Solver.Force_specific_solver_construction(VORO.Num_Grains,Structure.Num_layers,cfg,Materials_Config,Solvers::LLB);
    std::cout << Solver.get_SusType(0) << std::endl;
    PM_lims[0] = VORO.Vx_MIN;
    PM_lims[1] = VORO.Vy_MIN;
    PM_lims[2] = VORO.Vx_MAX;
    PM_lims[3] = VORO.Vy_MAX;
    Solver.disableInclusion();

    Hdc DCField(cfg);

    // Check distributions
    double meanV=0.0;
    double meanD=0.0;
    double meanK=0.0;
    double varianceV=0.0;
    double varianceD=0.0;
    double varianceK=0.0;
    for(const Grain_t& Grain : Sim::Grains){
        meanV += Grain.Vol;
        meanD += Grain.diameter;
        meanK += Grain.K;
    }
    meanV /= Sim::Grains.size();
    meanD /= Sim::Grains.size();
    meanK /= Sim::Grains.size();
    for(const Grain_t& Grain : Sim::Grains){
        varianceV += pow(Grain.Vol-meanV,2.0);
        varianceD += pow(Grain.diameter-meanD,2.0);
        varianceK += pow(Grain.K-meanK,2.0);
    }
    varianceV /= (Sim::Grains.size());
    varianceD /= (Sim::Grains.size());
    varianceK /= (Sim::Grains.size());

    Log.logandshow(Logger_t::LogLvl::INFO, "V (lognormal) arithmetic mean = " + to_string_exact(meanV) + " with logarithmic stddev = " + to_string_exact(sqrt(varianceV)/meanV));
    Log.logandshow(Logger_t::LogLvl::INFO, "D (lognormal) arithmetic mean = " + to_string_exact(meanD) + " with logarithmic stddev = " + to_string_exact(sqrt(varianceD)/meanD));
    Log.logandshow(Logger_t::LogLvl::INFO, "K (lognormal) arithmetic mean = " + to_string_exact(meanK) + " with logarithmic stddev = " + to_string_exact(sqrt(varianceK)/meanK));

    // Output distributions
    std::ofstream DISTFILE(OutDir+"/Sim_Dists_at_1K.dat");
    for(const Grain_t & Grain : Sim::Grains){
        DISTFILE << Grain.K << " " << Grain.Vol << " " << Grain.diameter << " " << Grain.Tc << " " << Grain.Ms << " " << (Grain.K*Grain.Vol*1e-21)/(KB) << "\n";
    }
    DISTFILE.close();

    // Output RT distributions
    for(Grain_t & Grain : Sim::Grains){
        Grain.Temp = Environ_Temp;
    }
    DISTFILE.open(OutDir+"/Sim_Dists_at_RT.dat");
    for(const Grain_t & Grain : Sim::Grains){
        DISTFILE << Grain.Kt() << " " << Grain.Vol << " " << Grain.diameter << " " << Grain.Tc << " " << Grain.Mt() << " " << (Grain.Kt()*Grain.Vol*1e-21)/(KB*Environ_Temp) << "\n";
    }
    DISTFILE.close();

    // Output Laser T distributions
    for(Grain_t & Grain : Sim::Grains){
        Grain.Temp = Laser_Temp;
    }
    DISTFILE.open(OutDir+"/Sim_Dists_at_LaserT.dat");
    for(const Grain_t & Grain : Sim::Grains){
        DISTFILE << Grain.Kt() << " " << Grain.Vol << " " << Grain.diameter << " " << Grain.Tc << " " << Grain.Mt() <<" " << (Grain.Kt()*Grain.Vol*1e-21)/(KB*Laser_Temp) << "\n";
    }
    DISTFILE.close();

    // Reset temperature
    for(Grain_t & Grain : Sim::Grains){
        Grain.Temp = Environ_Temp;
    }

    // Set initial grain temperatures
    bool Laser_applied=false;
    if(Solver.get_dt() > LaserStart){
        // If timestep is greater than start time for heating set the grains to be heated from the start
        for(Grain_t & Grain : Sim::Grains){
            Grain.Temp = Laser_Temp;
        }
        Laser_applied=true;
    }
    else{
        // Set environmental temperature
        for(Grain_t & Grain : Sim::Grains){
            Grain.Temp = Environ_Temp;
        }
    }

    // Set simulation seed
    std::optional<int> SimSeed = cfg.getValueOfKey<int>("ThermalDecay:SimSEED");
    if(SimSeed.has_value()){
        Gen.seed(SimSeed.value());
        const int RNG_warmup=700000;
        Gen.discard(RNG_warmup); // warm up the RNG
    }
    DCField.turnOn();
    DCField.updateField(0.0);
    DCField.updateField(0.0);
    for(Grain_t& Grain : Sim::Grains){
        Grain.H_appl = DCField.getField();
    }
    while(Sim::Time<Total_time){

        Solver.Apply_Inclusion_zone(VORO,Structure.Num_layers,0.0,0.0);
        Solver.Integrate(VORO,Structure.Num_layers,INT,Sim::Grains,DataOut);
        Sim::Time += Solver.get_dt();

        if(!Laser_applied && Sim::Time > LaserStart){
            for(Grain_t & Grain : Sim::Grains){
                Grain.Temp = Laser_Temp;
            }
            Laser_applied=true;
        }
    }
    return 0;
}
