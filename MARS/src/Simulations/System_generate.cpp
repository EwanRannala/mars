/*
 * System_generate.cpp
 *
 *  Created on: 5 Sep 2018
 *      Author: Samuel Ewan Rannalas
 */

/** @file System_generate.cpp
 * @brief Function to just generate a granular system via the Voronoi construction. */

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Importers/Structure_import.hpp"
#include "Voronoi.hpp"


/* Only generates a system via the Voronoi construction.
 * Use this function to identify the suitable value of
 * "Struct:Std_Dev_Grain_Vol" for your desired system.
 */
int Gen_sys(ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a system generation simulation");


    // Declare all required variables
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;

//####################IMPORT ALL REQUIRED DATA####################//
    Structure_import(cfg, &Structure);
    // Construct system
    Voronoi(Structure, &Voronoi_data);

    return 0;
}


