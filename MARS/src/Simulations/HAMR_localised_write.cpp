/*
 * HAMR_localised_write.cpp
 *
 *  Created on: 2 Oct 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file HAMR_localised_write.cpp
 * \brief Simulation of HAMR writing process for a system with a single centralised bit.
 *
 * <P> A single heat assisted magnetic recording write process is performed on a bit located at the
 * centre of the granular system. The system is first equilibrated for
 * <B><EM> HAMR:Equilibration_time </EM></B>. The simulation is performed until at least the write
 * process has been completed. However if <B><EM> HAMR:Run_time </EM></B> &gt;
 * <B><EM> 6&times;HAMR:Cooling_time </EM></B> then the simulation will continue after the write process is
 * completed until <B><EM> HAMR:Run_time </EM></B> is reached. </P>
 */

#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <string>
#include <chrono>

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include "Classes/RecHead.hpp"
#include "Classes/Solver.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Grain_setup.hpp"
#include "Importers/Materials_import.hpp"
#include "Voronoi.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Data_output/HAMR_grain_output.hpp"
#include "Data_output/HAMR_layer_output.hpp"
#include "Data_output/HAMR_switching_output.hpp"

/** Performs HAMR data write over an entire system with a continuously moving write head.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 *         <UL>
 *            <LI> Type <LI> SEED
 *        </UL>
 * <LI> Struct:
 *         <UL>
 *             <LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 *             <LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 *         </UL>
 * <LI> LLB:
 *         <UL>
 *             <LI> dt <LI> Inclusion_zone
 *             <LI> <STRONG> If Inclusion_zone == True </STRONG>
 *             <UL>
 *                 <LI> Inclusion_range_negX <LI> Inclusion_range_posX <LI> Inclusion_range_negY <LI> Inclusion_range_posY
 *             </UL>
 *         </UL>
 * <LI> HAMR:
 *         <UL>
 *             <LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 *             <LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 *             <LI> Measurement_time <LI> equilibration_time <LI> Field_X_width <LI> Field_Y_width
 *             <LI> laser_fwhm_x <LI> laser_fwhm_y <LI> Thermal_gradient <LI> Run_time
 *         </UL>
 * </UL>
 * */
int HAMR_localised_write(ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a HAMR localised write (no bits) simulation");


    auto Environ_Temp = cfg.getValueOfKey<double>("HAMRLW:Environment_Temp");
    auto Equilibration_time = cfg.getValAndUnitOfKey<double>("HAMRLW:Equilibration_time");
    auto Run_time = cfg.getValAndUnitOfKey<double>("HAMRLW:Run_time");
    // Declare all required variables
    Data_output_t DataOut(OutDir,cfg);
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    Interaction_t Int_system;
    std::vector<Grain_t> Grains;
    std::vector<ConfigFile> Materials_Config;
    std::vector<double> Normaliser;
    std::vector<double> Mx;
    std::vector<double> My;
    std::vector<double> Mz;
    std::vector<double> Ml;
    std::vector<unsigned int> Grain_output_list;
    unsigned int output_steps=0;
    unsigned int file_writes=0;
    unsigned int File_count=1;
    unsigned int Sim_time_predictor=0;
    double Temperature=Environ_Temp;
    double Time=0.0;
    double Profile_tot_time=0.0;
    std::string FILENAME_idv;
    std::string Output_switch_file_loc = OutDir+"/HAMR_Grain_switching.dat";
    std::vector<std::string> Output_switch_buffer;
    std::string OUTPUT = OutDir+"/HAMR_multi_layer.dat";
    std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends to it

//####################IMPORT ALL REQUIRED DATA####################//
    Structure_import(cfg, &Structure);
    Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Environ_Temp,Grains);
    Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Grains,Int_system);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    RecHead RecordingHead(cfg);
    RecordingHead.setX(Voronoi_data.Centre_X);
    RecordingHead.setY(Voronoi_data.Centre_Y);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    if(RecordingHead.getRampTime()<Solver.get_dt()){
        Log.log(Logger_t::LogLvl::ERR,"Ramp time less than timestep!");
    }
//###################DETERMINE IMPORTANT VALUES###################//

    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
    output_steps = Solver.getOutputSteps();
    Profile_tot_time = RecordingHead.getProfileTime();
    if(Run_time > Profile_tot_time){file_writes = static_cast<unsigned int>(round(Run_time/Solver.getMeasTime()));}
    else{file_writes = static_cast<unsigned int>(round(Profile_tot_time/Solver.getMeasTime()));}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Equilibrate the system for Equilibration_time
    Log.logandshow(Logger_t::LogLvl::INFO,"Equilibrating the system for "+to_string_exact(Equilibration_time)+"s");
    Solver.disableInclusion();
    while(Time<Equilibration_time){
        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
        Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);
        Time += Solver.get_dt();
    }
    Solver.enableInclusion();
    // Reset time to zero
    Log.logandshow(Logger_t::LogLvl::INFO,"Resetting time to t = 0.0 s");
    Time = 0.0;

    HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
    HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grains,Materials.dz,&Output_switch_buffer);
    Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,"0.dat");

    Log.logandshow(Logger_t::LogLvl::INFO,"Output steps: "+to_string_exact(output_steps));
    auto start = std::chrono::system_clock::now();

    // Run the simulation
    RecordingHead.turnOnfor(RecordingHead.getProfileTime()-RecordingHead.getRampTime());
    while(Time<Run_time){

        for(unsigned int Steps=0;Steps<output_steps;Steps++){
            RecordingHead.ApplyHspatial(Voronoi_data,Structure.Num_layers,Solver.get_dt(),Grains);
            RecordingHead.ApplyTspatial(Voronoi_data,Structure.Num_layers,Environ_Temp,Time,Temperature,Grains);
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,Voronoi_data.Centre_X,Voronoi_data.Centre_Y);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Grains,DataOut);

            #ifdef RealTimePrint
                std::cout << std::setw(30) << Time << "\t\r" << std::flush;
            #endif
            Time += Solver.get_dt();
        }

        FILENAME_idv = to_string_exact(File_count) + ".dat";
        Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grains,Time,FILENAME_idv);
        HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
        ++File_count;

        // Predict simulation run time
        ++Sim_time_predictor;
        if(Sim_time_predictor==10){
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
            double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
            Log.logandshow(Logger_t::LogLvl::INFO,"Predicted sim time: "+to_string_exact(Predicted_Sim_time)+"s");
        }
    }
    HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grains,Output_switch_buffer,Output_switch_file_loc);
    return 0;
}



