/*
 * HAMR.cpp
 *
 *  Created on: 21 Aug 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file HAMR.cpp
 * \brief This simulation applies the temperature profile and field profile on an entire system.
 *
 * <P> The system is first allowed to equilibrate for a specified time. After equilibration the initial
 * magnetisation is recorded then the thermal and field profiles are activated. The system is evaluated
 * until <B><EM> HAMR:Run_time </EM></B> is reached with data recorded at the interval defined by
 * <B><EM> HAMR:Measurement_time. </EM></B>
 * </P>
 */

#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <string>
#include <sstream>
#include <chrono>

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include "Classes/Hdc.hpp"
#include "Classes/HAMRLaser.hpp"
#include "Classes/Solver.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Grain_setup.hpp"
#include "Importers/Materials_import.hpp"
#include "Voronoi.hpp"
#include "Interactions/Generate_interactions.hpp"

#include "Data_output/HAMR_grain_output.hpp"
#include "Data_output/HAMR_layer_output.hpp"
#include "Data_output/HAMR_switching_output.hpp"
#include "Io/Data_Output.hpp"

#include "Simulation_globals.hpp"

#include "cuda/Classes/CudaSolver.cuh"


double Gaussian(const double Tmin, const double Tmax, const double CoolingTime, const double time)
{
    return Tmin + (Tmax-Tmin)*exp(-(time*time)/(CoolingTime*CoolingTime));
}
double dbl_Gaussian(const double Tmin, const double Tmax, const double CoolingTime, const double time)
{
    return Tmin + (Tmax-Tmin)*exp( -pow( ((time-3*CoolingTime)/CoolingTime),2.0 ) );
}

/* This is a simulation for applying temperature and field profiles to
 * an entire system to investigate their effects. */

/** Applies thermal and field profiles onto an entire granular system.
 *
 * \param[in] cfg Configuration file. <BR>
 * */
int HAMR(ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running a HAMR (switching probability) simulation");

    auto Equilibration_time = cfg.getValueOfKey<double>("HAMR:Equilibration_time");

    auto Tmin = cfg.getValueOfKey<double>("HAMR:Tmin");
    auto Tmax = cfg.getValueOfKey<double>("HAMR:Tmax");
    auto Cooling_Rate = cfg.getValueOfKey<double>("HAMR:Cooling_rate")/1.0e-9; // Convert from K/ns to K/s
    // Calculate CoolingTime
    double Cooling_Time = -0.857763884960706*((Tmin-Tmax)/(Cooling_Rate));
    // Calculate Run_time
    //double Run_time = sqrt( Cooling_Time*Cooling_Time*log( (-Tmax+Tmin)/(-1.0) ) );
    // Double Gaussian
    double Run_time = 6.0*Cooling_Time;
    Log.logandshow(Logger_t::LogLvl::INFO,"Cooling time set to " + to_string_exact(Cooling_Time)+"s");
    Log.logandshow(Logger_t::LogLvl::INFO,"Run time set to " + to_string_exact(Run_time)+"s");


    // Declare all required variables
    Data_output_t DataOut(OutDir,cfg);
    Structure_t Structure;
    Material_t Materials;
    Voronoi_t Voronoi_data;
    Interaction_t Int_system;
    std::vector<ConfigFile> Materials_Config;
    std::vector<double> Normaliser;
    std::vector<double> Mx;
    std::vector<double> My;
    std::vector<double> Mz;
    std::vector<double> Ml;
    std::vector<unsigned int> Grain_output_list;
    double Temperature=Tmin;
    Sim::Time = 0.0;

//####################IMPORT ALL REQUIRED DATA####################//
    Structure_import(cfg, &Structure);
    Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Tmax,Sim::Grains);
    Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Sim::Grains,Int_system);
    #ifdef CUDA
        cuda::CudaSolver_t CudaSolver(cfg,Materials_Config,Int_system);
    #else
        Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    #endif
    Hdc FieldDC(cfg);
    //HAMR_Laser  LaserPulse(cfg);

    // Output Tc data
    std::vector<double> TcData(Sim::Grains.size(),0.0);
    std::vector<double> KData(Sim::Grains.size(),0.0);
    for(size_t l=0; l<Structure.Num_layers; ++l){
        for(size_t g=0; g<Voronoi_data.Num_Grains; ++g){
            TcData[l*Voronoi_data.Num_Grains+g]=Sim::Grains[l*Voronoi_data.Num_Grains+g].Tc;
            KData[l*Voronoi_data.Num_Grains+g]=Sim::Grains[l*Voronoi_data.Num_Grains+g].K;
        }
    }
    std::ofstream TCFile(OutDir+"/Tc.dat");
    std::ofstream KFile(OutDir+"/K.dat");
    for(size_t g=0; g<Voronoi_data.Num_Grains; ++g){
        std::stringstream ssTc(std::ios_base::app | std::ios_base::out);
        std::stringstream ssK(std::ios_base::app | std::ios_base::out);
        for(size_t l=0; l<Structure.Num_layers; ++l){
            ssTc << TcData[l*Voronoi_data.Num_Grains+g] << " ";
            ssK  << KData[l*Voronoi_data.Num_Grains+g] << " ";
        }
        TCFile << ssTc.str() << "\n";
        KFile  << ssK.str() << "\n";
    }
    TCFile.close();
    KFile.close();


    // TODO: Decide if this is even needed
    #ifdef CUDA
        if(FieldDC.getRampTime()<CudaSolver.get_dt()){
            Log.log(Logger_t::LogLvl::ERR,"Ramp time less than timestep!");
        }
    #else
        if(FieldDC.getRampTime()<Solver.get_dt()){
            Log.log(Logger_t::LogLvl::ERR,"Ramp time less than timestep!");
        }
    #endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    // Generate output list for entire system
    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    // Measure time per integration step
    bool Intrgration_step_timed = false;
    double Time_per_step=0.0;
    unsigned int steps=0;

    // Equilibrate the system for Equilibration_time
    FieldDC.turnOn();
    if(Equilibration_time>0.0){
        Intrgration_step_timed = true;
        Log.logandshow(Logger_t::LogLvl::INFO,"Equilibrating the system for "+to_string_exact(Equilibration_time)+" s");
        for(auto & Grain : Sim::Grains){Grain.Temp = Temperature;}
        while(Sim::Time<Equilibration_time){
            #ifdef CUDA
                FieldDC.updateField(CudaSolver.get_dt());
            #else
                FieldDC.updateField(Solver.get_dt());
            #endif
            for(Grain_t & Grain : Sim::Grains){
                Grain.H_appl = FieldDC.getField();
            }
            auto IntSTART = std::chrono::steady_clock::now();
            #ifdef CUDA
                CudaSolver.Integrate(Sim::Grains,DataOut);
                Sim::Time += CudaSolver.get_dt();
            #else
                Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains,DataOut);
                Sim::Time += Solver.get_dt();
            #endif
            auto IntEND = std::chrono::steady_clock::now();
            auto SimDur = std::chrono::duration_cast<std::chrono::microseconds>(IntEND-IntSTART);
            Time_per_step+=SimDur.count();
            ++steps;
        }
        Log.logandshow(Logger_t::LogLvl::INFO, "Equilibration done.");
        Time_per_step /= steps;
        Log.logandshow(Logger_t::LogLvl::INFO, "Integration step takes " + to_string_exact(Time_per_step) + "us");
        // Reset time to zero
        Log.logandshow(Logger_t::LogLvl::INFO,"Resetting time to t = 0.0 s");
        Sim::Time = 0.0;
    }

    std::string Output_switch_file_loc = OutDir+"/HAMR_Grain_switching.dat";
    std::vector<std::string> Output_switch_buffer;
    HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Sim::Grains,Materials.dz,&Output_switch_buffer);

    auto start = std::chrono::system_clock::now();

    // Predict simulation time (requires equilibration to have been performed)
    if(Intrgration_step_timed){
        double Predicted_sim_time = Run_time;
        #ifdef CUDA
            Predicted_sim_time /= CudaSolver.get_dt();
        #else
            Predicted_sim_time /= Solver.get_dt();
        #endif
        Predicted_sim_time *= Time_per_step*1e-6;
        Log.logandshow(Logger_t::LogLvl::INFO,"Predicted sim time (ignoring data transfers) "+to_string_exact(Predicted_sim_time)+"s");
    }
    // Run the simulation
    //FieldDC.turnOnfor(LaserPulse.getProfileTime()-FieldDC.getRampTime());
    //FieldDC.turnOn();
    while(Sim::Time<Run_time){
        // Determine Temperature and Field.
        //LaserPulse.ApplyT(Environ_Temp,Sim::Time,Temperature,Sim::Grains);
        #ifdef CUDA
            FieldDC.updateField(CudaSolver.get_dt());
        #else
            FieldDC.updateField(Solver.get_dt());
        #endif
        for(Grain_t & Grain : Sim::Grains){
            Grain.H_appl = FieldDC.getField();
            Grain.Temp   = Temperature;
        }
        #ifdef CUDA
            CudaSolver.Integrate(Sim::Grains,DataOut);
            Sim::Time += CudaSolver.get_dt();
        #else
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains,DataOut);
            Sim::Time += Solver.get_dt();
        #endif
            Temperature = dbl_Gaussian(Tmin, Tmax, Cooling_Time, Sim::Time);

    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    Log.logandshow(Logger_t::LogLvl::INFO,"Simulation took "+to_string_exact(elapsed.count())+"s. Now obtaining switching data.");
    HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Sim::Grains,Output_switch_buffer,Output_switch_file_loc);
    return 0;
}
