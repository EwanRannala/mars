/*
 * FMR.cpp
 *
 *  Created on: 20 May 2020
 *      Author: Ewan Rannala
 */

/** @file FMR.cpp
 * @brief Simulation for ferromagnetic resonance. */

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include "Classes/Hac.hpp"
#include "Classes/Hdc.hpp"
#include "Classes/Solver.hpp"
#include "cuda/Classes/CudaSolver.cuh"

#include "Interactions/Generate_interactions.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Importers/Materials_import.hpp"
#include "Importers/Structure_import.hpp"
#include "FftComplex.hpp"

#include "Globals.hpp"

#include "Data_output/Export_system.hpp"


#include "Importers/Read_in_import.hpp"
#include "Importers/Create_system_from_output.hpp"
#include "Importers/Import_structure.hpp"



#include <complex>
typedef std::complex<double> cd;

void Hann_window(std::vector<double>&a){
    for(unsigned int i=0;i<a.size();++i){
        double multiplier = 0.5*(1-cos(M_2PI*i/(a.size()-1)));
        a[i] *= multiplier;
    }
}

void Hamming_window(std::vector<double>&a){
    for(unsigned int i=0;i<a.size();++i){
        double multiplier = 0.54-0.46*cos(M_2PI*i/(a.size()-1));
        a[i] *= multiplier;
    }
}

void BlackmanHarris7(std::vector<double>&a){
    for(unsigned int i=0;i<a.size();++i){

        double multiplier = 0.27105140069342*cos(2*M_2PI*0*i/(a.size()-1))
                          - 0.43329793923448*cos(2*M_2PI*1*i/(a.size()-1))
                          + 0.21812299954311*cos(2*M_2PI*2*i/(a.size()-1))
                          - 0.06592544638803*cos(2*M_2PI*3*i/(a.size()-1))
                          + 0.01081174209837*cos(2*M_2PI*4*i/(a.size()-1))
                          - 0.00077658482522*cos(2*M_2PI*5*i/(a.size()-1))
                          + 0.00001388721735*cos(2*M_2PI*6*i/(a.size()-1));
        a[i] *= multiplier;
    }
}

double SemiAnalytic(double hgt, double D, double f, double B0, double Gamma, double alpha,
                    double f0,  double T, double Tc, double beta){
    // Convert nm to cm
    hgt *= 1e-7;
    D   *= 1e-7;
    double C = (M_PI*hgt)/16.0;
    double m = pow(1.0 - T/Tc,beta);
    double tmp = (C*m*D*D)*((f*f*B0*B0*Gamma*alpha)/(alpha*alpha*f0*f0 + pow(f-f0,2.0)));
    return tmp;
}

double Omega0(double Gamma, double Bz, double m, double Chi_perp){
    return Gamma*(Bz + m/Chi_perp);
}

double Tau(double Lambda, double T, double Tc, double Omega0, double Gamma,
           double HeffZ, double m){
    return m/(Lambda*((1.0-T/(3.0*Tc))*Omega0 - (2.0*Gamma*T*HeffZ)/(3.0*Tc)) );
}

double Hintra(double T, double Tc, double Chi_para, double m, double me){
    double field=0.0;
    if(T<=Tc){
        field = (1.0/(2.0*Chi_para))*(1.0-((m*m)/(me*me)));
    }
    else{
        field = (-1.0/Chi_para)*(1.0+((3.0*Tc)/(5.0*(T-Tc)))*(m*m));
    }
    return field*m;
}

/** Performs frequency or field swept FMR simulation.
 *
 * \param[in] cfg Configuration file. <BR>
 */
int FMR(const ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running an FMR simulation");
    std::vector<double> Frequencies, Fields;
    int Num_iter=1;
    double Res=0.0, MaxFreq=0.0;
    std::string Analyt_pre=OutDir+"/";

    double Equilibration_time = cfg.getValueOfKey<double>("FMR:Equilibrium_time");
    double Run_time = cfg.getValueOfKey<double>("FMR:Run_time");
    double Start_time = cfg.getValueOfKey<double>("FMR:Start_recording_time");
    double Environ_Temp = cfg.getValueOfKey<double>("FMR:Environment_Temperature");
    std::string Type = cfg.getValueOfKey<std::string>("FMR:Sweep_Type");
    bool Set_Res = cfg.getValueOfKey<bool>("FMR:Set_resolution");
    if(Set_Res){Res=cfg.getValueOfKey<double>("FMR:Resolution");}

    Data_output_t DataOut(OutDir,cfg);
    Data_input_t IN_data;
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t Int_system;
    std::vector<Grain_t> Grains_backup;

    Hac AC(cfg);
    Hdc DC(cfg);

    std::ofstream FMROUT;

    bool ReadIN=cfg.getValueOfKey<bool>("FMR:ReadStructInput");
    bool ReadINparams=cfg.getValueOfKey<bool>("FMR:ReadParamsInput");


    if(!ReadIN){
        Structure_import(cfg,&Structure);
        Materials_import(cfg,Structure.Num_layers,&Materials_Config,&Materials);
        Voronoi(Structure,&Voronoi_data);
        Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Environ_Temp,Sim::Grains);
        Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Sim::Grains,Int_system);
    }
    else if(ReadIN && !ReadINparams){
        std::cout << "Importing structure only." << std::endl;
        Data_input_t IN_DATA;
        Read_in_import(cfg, &IN_DATA);
        Read_in_structure(IN_DATA, Structure, Voronoi_data);

        std::cout << "\tImported grain size distribution: \n\t" <<
                     "\tmu: "      << Voronoi_data.obtainedMean <<
                     "\tsigma: "   << Voronoi_data.obtainedStdDev <<
                     "\tMean[D]: " << Voronoi_data.Real_grain_width << "nm\n" << std::endl;
        Materials_import(cfg,Structure.Num_layers,&Materials_Config,&Materials);
        Grain_setup(Structure.Num_layers,Voronoi_data,Materials,Environ_Temp,Sim::Grains);
        Generate_interactions(cfg,Structure.Num_layers,Materials,Voronoi_data,Sim::Grains,Int_system);
    }
    #ifdef CUDA
        cuda::CudaSolver_t CudaSolver(cfg,Materials_Config,Int_system);
    #endif
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    #ifndef CUDA
    if(ReadINparams){
        std::cout << "Importing grain data." << std::endl;
        Data_input_t IN_DATA;
        Read_in_import(cfg, &IN_DATA);
        Read_in_system(IN_DATA, Structure, Voronoi_data, Materials, Sim::Grains, Int_system, Solver);
        // Set correct Grain Temperature
        for(unsigned g=0;g<Sim::Grains.size();++g){
            Sim::Grains[g].Temp=Environ_Temp;
        }
    }
    #endif

//######################
    std::ofstream File1;
    File1.open(OutDir+"/Tc_dist.dat");
    for(size_t g=0;g<Sim::Grains.size();++g){
        File1 << Sim::Grains[g].Tc << std::endl;
    }
    File1.close();
    File1.open(OutDir+"/VsT.dat");

    double EH0=-1.0;

    double InitTemp = Sim::Grains[0].Temp;

// NOTE I THINK THIS ESTIMATION IS WRONG ANYWAY
// ToDo Need to sort out use of File1
    if(Materials.Ani_method.back()!=Material_t::AniMethods::Callen){
        // Want to fill K vector with correct values!
        std::vector<unsigned> grains;
        for(unsigned g=0;g<Voronoi_data.Num_Grains;++g){grains.push_back(g);}
        for(unsigned L=1;L<Structure.Num_layers;++L){
            unsigned offset=Voronoi_data.Num_Grains*L;
            for(unsigned g=0;g<Voronoi_data.Num_Grains;++g){grains.push_back(g+offset);}
        }

        for(unsigned T=0;T<750;T+=1){
            double mEQAVG=0.0, HkAVG=0.0;

            Solver.set_Chi_size(Voronoi_data.Num_Grains*Structure.Num_layers);
            Solver.Susceptibilities(Voronoi_data.Num_Grains, Structure.Num_layers, grains, Sim::Grains);
            for(unsigned int grain_in_sys=0;grain_in_sys<Voronoi_data.Num_Grains;++grain_in_sys){
                Sim::Grains[grain_in_sys].Temp = T;
                double Tc= Sim::Grains[grain_in_sys].Tc, m_EQ=0.0;
                if(T>=Tc){
                    if(Sim::Grains[grain_in_sys].mEQ_Type==Material_t::mEQTypes::Bulk){m_EQ=0.0;}
                    else{
                        m_EQ = 1.0/(1.0/Sim::Grains[grain_in_sys].a0_mEQ + Sim::Grains[grain_in_sys].b1_mEQ*((Tc-T)/Tc) + Sim::Grains[grain_in_sys].b2_mEQ*pow((Tc-T)/Tc,2));
                    }
                }
                else {
                    if(Sim::Grains[grain_in_sys].mEQ_Type==Material_t::mEQTypes::Bulk){m_EQ = pow(1.0-(T/Tc),Sim::Grains[grain_in_sys].Crit_exp);}
                    else{
                        m_EQ = Sim::Grains[grain_in_sys].a0_mEQ + Sim::Grains[grain_in_sys].a1_2_mEQ*pow((Tc-T)/Tc,0.5) + Sim::Grains[grain_in_sys].a1_mEQ*pow((Tc-T)/Tc,1) +
                               Sim::Grains[grain_in_sys].a2_mEQ*pow((Tc-T)/Tc,2) + Sim::Grains[grain_in_sys].a3_mEQ*pow((Tc-T)/Tc,3) + Sim::Grains[grain_in_sys].a4_mEQ*pow((Tc-T)/Tc,4) +
                               Sim::Grains[grain_in_sys].a5_mEQ*pow((Tc-T)/Tc,5) + Sim::Grains[grain_in_sys].a6_mEQ*pow((Tc-T)/Tc,6) + Sim::Grains[grain_in_sys].a7_mEQ*pow((Tc-T)/Tc,7) +
                               Sim::Grains[grain_in_sys].a8_mEQ*pow((Tc-T)/Tc,8) + Sim::Grains[grain_in_sys].a9_mEQ*pow((Tc-T)/Tc,9);
                    }
                }
                double ChiPerp = Solver.get_ChiPerp(grain_in_sys);
                if(ChiPerp<1.0e-10){Sim::Grains[grain_in_sys].K=0.0;}
                else{Sim::Grains[grain_in_sys].K=(Sim::Grains[grain_in_sys].Ms*m_EQ*m_EQ)/(2.0*Solver.get_ChiPerp(grain_in_sys));}
                HkAVG += 2.0*Sim::Grains[grain_in_sys].K/Sim::Grains[grain_in_sys].Ms;
                mEQAVG+=m_EQ;
            }
            mEQAVG/=Voronoi_data.Num_Grains;
            HkAVG/=Voronoi_data.Num_Grains;
            double B0 = ((M_2PI*13.7e9)/Solver.get_Gamma())-HkAVG;
            File1 << T << " " << mEQAVG << " " << HkAVG << " " << B0 << std::endl;
            if(T==Environ_Temp){EH0=B0;}
        }
        File1.close();
    }
    else{
        for(unsigned T=0;T<750;T+=1){
            double mEQAVG=0.0, HkAVG=0.0;
            for(unsigned g=0;g<Voronoi_data.Num_Grains;++g){
                double Hk0 = ((2.0*Sim::Grains[g].K)/Sim::Grains[g].Ms);
                double mEQ, Hk;
                if(T<Sim::Grains[g].Tc){
                    mEQ = pow(1.0-(T/Sim::Grains[g].Tc),Sim::Grains[g].Crit_exp);
                    Hk = Hk0*pow(mEQ,Sim::Grains[g].Callen_power_lowT-1.0);
                }
                else{
                    mEQ = 0.01;
                    Hk = Hk0*pow(mEQ,Sim::Grains[g].Callen_power_lowT-1.0);
                }
                mEQAVG+=mEQ;
                HkAVG+=Hk;
            }
            mEQAVG/=Voronoi_data.Num_Grains;
            HkAVG/=Voronoi_data.Num_Grains;
            double B0 = ((M_2PI*13.7e9)/Solver.get_Gamma())-HkAVG;
            File1 << T << " " << mEQAVG << " " << HkAVG << " " << B0 << std::endl;
            if(T==Environ_Temp){EH0=B0;}
        }
        File1.close();
    }
    std::cout << "****************" << std::endl;
    std::cout << "\tExpected H0: " << EH0 << std::endl;
    std::cout << "****************" << std::endl;

    for(unsigned int grain_in_sys=0;grain_in_sys<Voronoi_data.Num_Grains;++grain_in_sys){
        Sim::Grains[grain_in_sys].Temp = InitTemp;
    }

//######################

    Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Sim::Grains,Solver,"0");
    if(cfg.getValueOfKey<bool>("FMR:Export_only")){
        // Output system distributions as a function to temperature
        std::ofstream File;

        File.open(OutDir+"/Tc_dist.dat");
        for(unsigned g=0;g<Voronoi_data.Num_Grains;++g){
            File << Sim::Grains[g].Tc << std::endl;
        }
        File.close();

        for(unsigned T=500;T<=500;T+=20){
            std::cout << "Generating distribution for T=" << T << std::endl;
            Grains_backup = Sim::Grains;
            for(Grain_t & Grain : Sim::Grains){
                Grain.H_appl = Vec3(0.0,0.0,0.0);
                Grain.Temp   = T;
            }
            double t=0.0;
            while(t<1.0e-10){
                #ifdef CUDA
                    CudaSolver.Integrate(Sim::Grains);
                    std::cout << std::setw(10) << t << "\r";
                    t += CudaSolver.get_dt();
                #else
                    Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                    Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains,DataOut);
                    std::cout << std::setw(10) << t << "\r";
                    t += Solver.get_dt();
                #endif
            }
            #ifdef CUDA
                CudaSolver.CopyToHost(Sim::Grains);
            #endif
            File.open(OutDir+"/K_Ms_dist_"+std::to_string(T)+"_.dat");
            File << "#K Ms" << std::endl;
            for(unsigned g=0;g<Sim::Grains.size();++g){
                if(Sim::Grains[g].Temp<Sim::Grains[g].Tc){
                    File << 1.0/Solver.get_ChiPerp(g) << " "
                         << sqrt(Sim::Grains[g].m*Sim::Grains[g].m) << " "
                         << sqrt(Sim::Grains[g].m*Sim::Grains[g].m)*Sim::Grains[g].Ms << " "
                         << Sim::Grains[g].Tc << std::endl;
                }
            }
            Sim::Grains = Grains_backup;
            #ifdef CUDA
                CudaSolver.Reinitialise();
            #endif
            File.close();
        }

        std::cout << "Exporting only set to true. Exiting..." << std::endl;
        exit(0);
    }

    #ifdef CUDA
        CudaSolver.Reinitialise();
    #endif
    //##########Equilibrate the system for Equilibration_time#########//
    std::cout << "\nEquilibrating the system for " << Equilibration_time << "s" << std::endl;
    for(Grain_t & Grain : Sim::Grains){
        Grain.H_appl = Vec3(0.0,0.0,0.0);
    }

    while(Sim::Time<Equilibration_time){
        #ifdef CUDA
            CudaSolver.Integrate(Sim::Grains);
            Sim::Time += CudaSolver.get_dt();
        #else
            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains,DataOut);
            Sim::Time += Solver.get_dt();
        #endif
#ifdef RealTimePrint
        std::cout << Sim::Time << "\t\r" << std::flush;
#endif
    }
    #ifdef CUDA
        CudaSolver.CopyToHost(Sim::Grains);
        CudaSolver.Reinitialise();
    #endif
    // Reset time to zero
    Log.logandshow(Logger_t::LogLvl::INFO,"Resetting time to t = 0.0s");

    Sim::Time = 0.0;

    Grains_backup = Sim::Grains;
///////////////////////////////////////////////////////////////////////////////////////////////

    if(Type!="frequency" && Type!="field" && Type!="none"){
        std::cout << "Unknown sweep type selected."; exit(EXIT_FAILURE);
    }
    else if(Type=="frequency"){
        Analyt_pre = "H"+std::to_string(DC.getMin())+"Oe_";
        std::vector<double> FreqRange = cfg.getValueOfKey<std::vector<double>>("fmr:Freq_Range");
        if(FreqRange.size()!=3){
            std::cout << "Incorrect frequency range vector. Requires {min, max, step}."; exit(EXIT_FAILURE);
        }
        // Fill Frequencies vector
        double temp=FreqRange[0];
        while(temp <= FreqRange[1]){
            Frequencies.push_back(temp);
            temp += FreqRange[2];
        }
        Num_iter=Frequencies.size();
        MaxFreq=Frequencies.back();
    }
    else if(Type=="field"){
        Analyt_pre = "F"+std::to_string(AC.getFreq()*1e-9)+"GHz_";
        MaxFreq=AC.getFreq();
        std::vector<double> FieldRange = cfg.getValueOfKey<std::vector<double>>("fmr:Field_Range");
        if(FieldRange.size()!=3){
            std::cout << "Incorrect field range vector. Requires {min, max, step}."; exit(EXIT_FAILURE);
        }
        // Fill Fields vector
        double temp=FieldRange[0];
        while(temp <= FieldRange[1]){
            Fields.push_back(temp);
            temp += FieldRange[2];
        }
        Num_iter = Fields.size();
    }

//################################# Determine that aliasing won't occur ################################//
    double SampleFreqMIN = MaxFreq*2.0;
    double dt;
    #ifdef CUDA
        dt = CudaSolver.get_dt();
    #else
        dt = Solver.get_dt();
    #endif
    double MeasTime = Solver.getMeasTime();

    if((1.0/dt)<=SampleFreqMIN){
        Log.logandshow(Logger_t::LogLvl::WARN,"Solver timestep will cause aliasing, adjusting timestep...");
        while((1.0/dt)<=SampleFreqMIN){
            dt *= 0.1;
            #ifdef CUDA
                CudaSolver.set_dt(dt);
            #else
                Solver.set_dt(dt);
                Solver.setMeasTime(dt,Solver.get_Solver());
            #endif
        }
        Log.logandshow(Logger_t::LogLvl::WARN,"New timestep "+to_string_exact(dt));
    }
    else if((1.0/MeasTime)<=SampleFreqMIN){
        MeasTime = dt;
        Solver.setMeasTime(dt, Solver.get_Solver());
    }
    uint64_t OutputSteps = MeasTime<dt ? 1 : round(MeasTime/dt);

    std::ofstream SAnalytic_OUT(Analyt_pre+"Semi_analytic_results.dat");

//################################# Set desired frequency resolution ###################################//
    unsigned int length = Run_time/MeasTime;
    if(Set_Res){
        std::cout << "Adjusting Run time to set FFT resolution." << std::endl;
        std::cout << "\tSample size: " << length << std::endl;
        std::cout << "\tSim time: " << MeasTime*length << std::endl;
        std::cout << "\tBin width: " << (1.0/MeasTime)/length << std::endl;
        length = (1.0/MeasTime)/Res;
        std::cout << "\t#### New values ####" << std::endl;
        std::cout << "\tSample size: " << length << std::endl;
        std::cout << "\tSim time: " << MeasTime*length << std::endl;
        std::cout << "\tBin width: " << (1.0/MeasTime)/length << std::endl;
    }
//################################# Determine superparamagnetic grains ###############################//
    std::vector<int> Ferro_grains;
    for(size_t g=0; g<Sim::Grains.size(); ++g){
        if(Sim::Grains[g].Tc>Environ_Temp){Ferro_grains.push_back(g);}
    }
    std::cout << "Ferro Grains: " << Ferro_grains.size() << " Superparamagnetic grains: " << Sim::Grains.size()-Ferro_grains.size() << std::endl;

//################################# Loop over desired frequencies/fields ###############################//
    for(int iter=0;iter<Num_iter;++iter){
        std::vector<double> mx_t;
        std::string filename_PRE = OutDir+"/FMR_";
        std::string filename;
        if(Type=="field"){
            filename = std::to_string(Fields[iter]) + "_" + std::to_string(AC.getFreq()) + "_";
            DC.setMin(Fields[iter]);
            DC.updateField(0.0);
        }
        else if(Type=="frequency"){
            filename = std::to_string(DC.getMin()) + "_" + std::to_string(Frequencies[iter]) + "_";
            AC.setFreq(Frequencies[iter]);
        }
        else{
            filename = std::to_string(DC.getMin()) + "_" + std::to_string(AC.getFreq()) + "_";
        }

        std::cout << "\nIteration " << iter+1 << " of " << Num_iter << std::endl;

        FMROUT.open(filename_PRE + filename + ".dat");

        for(Grain_t & Grain : Sim::Grains){
            Grain.H_appl = AC.getField()+DC.getField();
        }

        //######################################## Perform FMR #########################################//
        Sim::Time = 0.0;
        unsigned int steps=0, it=0;
        std::vector<std::vector<double>> Grain_mx(Ferro_grains.size());
        while(it<length){ // Perform simulation until the correct number of samples have been obtained
            //auto start = std::chrono::system_clock::now();

            while(steps<OutputSteps)
            {
                //std::cout << "A" << std::endl;
                #ifdef CUDA
                    //std::cout << "GPU" << std::endl;
                    CudaSolver.Integrate(Sim::Grains);
                    Sim::Time += CudaSolver.get_dt();
                #else
                    //std::cout << "CPU" << std::endl;
                    Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                    Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains,DataOut);
                    Sim::Time += Solver.get_dt();
                #endif
                //std::cout << "B" << std::endl;
                ++steps;
#ifdef RealTimePrint
                std::cout << Sim::Time << "\t\r" << std::flush;
#endif
                // Update field - only AC field
                AC.updateField(Sim::Time);
                for(Grain_t & Grain : Sim::Grains){Grain.H_appl = AC.getField()+DC.getField();}
            }
#if 0
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
            std::cout << "\n\n*****\n Predicted sim time: " << elapsed.count() << " s\n*****\n\n" << std::endl;
#endif
            steps=0;
            #ifdef CUDA
                CudaSolver.CopyToHost(Sim::Grains);
            #endif

            Vec3 m;
            // Ignore superparamagnetic grains
            for(unsigned grain=0;grain<Ferro_grains.size();++grain){
                if(std::isnan(Sim::Grains[Ferro_grains[grain]].m.x) || std::isnan(Sim::Grains[Ferro_grains[grain]].m.y) || std::isnan(Sim::Grains[Ferro_grains[grain]].m.z))
                {
                    std::cout << Ferro_grains[grain] << " " << Sim::Grains[Ferro_grains[grain]].m << std::endl;
                    exit(EXIT_FAILURE);
                }
                m += Sim::Grains[Ferro_grains[grain]].m;
            }
            m /= Ferro_grains.size();

            if(Sim::Time>Start_time){
                mx_t.push_back(m.x);
                FMROUT << std::setprecision(7) << m << "\t" << DC.getField() << "\t"
                        << AC.getField() << "\t" << Sim::Time << std::endl;
                ++it;
            }
        }
        FMROUT.close();

//####################################### Obtain and output FFT ########################################//
        std::vector<cd> input(mx_t.begin(), mx_t.end());
        Fft::transform(input,false);
        // Output FFT
        filename_PRE = OutDir+"/FFT_";
        std::ofstream FFTOUT(filename_PRE + filename + ".dat");
        // Second half of FFT is a mirror image of the first half, so it can be ignored.
        for(unsigned int i=0;i<input.size()*0.5;++i){
            if( (i/MeasTime)/length > 1.0e12 ){break;}
            FFTOUT << (i/MeasTime)/length << "\t"
	            << (2.0/input.size())*std::abs(input[i]) << std::endl;
        }
        FFTOUT.close();

        // Reset grains
        Sim::Grains=Grains_backup;
        #ifdef CUDA
            CudaSolver.Reinitialise();
        #endif
    }
    SAnalytic_OUT.close();
    return 0;
}
