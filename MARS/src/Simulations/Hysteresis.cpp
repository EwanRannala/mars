
#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <iomanip>

#include "Globals.hpp"
#include "Simulation_globals.hpp"

#include "Structures.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Materials_import.hpp"

#include "Classes/Solver.hpp"
#include "cuda/Classes/CudaSolver.cuh"

#include "Io/ParseCSV.hpp"

// TODO: Move and make PrintProgress accessible to all simulations
// TODO: Implement multiple steps for dynamic solvers

void PrintProgress(double progress, double* prev)
{
    if(progress>1.0){progress=1.0;}
    if(fabs(progress-*prev)>=0.05)
    {
        int barWidth = 70;

        std::cout << "[";
        int pos = barWidth * progress;
        for (int i = 0; i < barWidth; ++i) {
            if (i < pos) std::cout << "=";
            else if (i == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "] " << int(progress * 100.0) << " %\r";
        std::cout.flush();
        std::cout << std::endl;
        *prev=progress;
    }
}

int Hysteresis(const ConfigFile& cfg){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running Hysteresis simulation");

    Data_output_t DataOut(OutDir,cfg);
    Structure_t   Structure;
    Voronoi_t     Voro;
    Material_t    Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t Interactions;

    std::ofstream Output(OutDir+"/hysteresis.dat");

    // Simulation specific parameters
    int Inter_steps   = cfg.getValueOfKey<double>("Hyst:Integration_steps");

    auto Environ_Temp = cfg.getValueOfKey<double>("Hyst:Temperature");
    auto thetaH       = cfg.getValueOfKey<double>("Hyst:Field_polar");
    auto phiH         = cfg.getValueOfKey<double>("Hyst:Field_azimuth");
    std::optional<bool> importField  = cfg.getValueOfOptKey<bool>("Hyst:Import_Field");
    double HapplMax = 0.0;
    double Hdelta = 0.0;
    double SweepRate = std::numeric_limits<double>::quiet_NaN();
    std::vector<std::vector<double>> ImportData;
    if(importField.has_value() && importField.value())
    {
        auto ImportFileName = cfg.getValueOfKey<std::string>("Hyst:Import_File");
        auto TimeCol    = cfg.getValueOfKey<int>("Hyst:Import_Time_Col");
        auto FieldCol   = cfg.getValueOfKey<int>("Hyst:Import_Field_Col");
        auto Headers    = cfg.getValueOfKey<bool>("Hyst:Import_Headers");
        std::vector<int> Cols = {TimeCol, FieldCol};

        ParseCSV(ImportFileName, Cols, Headers, ImportData);
    }
    else
    {
        HapplMax     = cfg.getValueOfKey<double>("Hyst:Field_magnitude_limit");
        Hdelta       = cfg.getValueOfKey<double>("Hyst:Field_step");
        SweepRate    = cfg.getValueOfKey<double>("Hyst:Field_sweep_rate");
    }

    // Set field direction
    Vec3 HapplUnit;
    HapplUnit.x = sin(thetaH*M_PI_180) * cos(phiH*M_PI_180);
    HapplUnit.y = sin(thetaH*M_PI_180) * sin(phiH*M_PI_180);
    HapplUnit.z = cos(thetaH*M_PI_180);

    double HapplMAG = HapplMax;
    double HapplMin = -HapplMax;

    Structure_import(cfg,&Structure);
    Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);

    // Want to repeat Voronoi construction until we get the desired distribution values
    int limit=1000;
    int iter=0;
    auto TimerSTART = std::chrono::steady_clock::now();

    double ExpMean   = sqrt(1+Structure.StdDev_grain_pos*Structure.StdDev_grain_pos)*exp(log(Structure.Grain_width/sqrt(1.0+((pow(Structure.Grain_width*Structure.StdDev_grain_pos,2))/(pow(Structure.Grain_width,2))))));
    double ExpStdDev = Structure.StdDev_grain_pos;
    Structure.Grain_width *= 0.9;
    Structure.StdDev_grain_pos *= 1.10;
    // TODO: Add option to disable repeated Voronoi
    do
    {
        Voronoi(Structure, &Voro);
        Log.logandshow(Logger_t::LogLvl::INFO,"Desired | Mean: "+to_string_exact(ExpMean)+" sigma: "+to_string_exact(ExpStdDev));
        Log.logandshow(Logger_t::LogLvl::INFO,"Obtained| Mean: "+to_string_exact(Voro.obtainedMean)+" sigma: "+to_string_exact(Voro.obtainedStdDev));
        Log.logandshow(Logger_t::LogLvl::INFO,"Difference| Mean: "+to_string_exact(fabs(ExpMean-Voro.obtainedMean))+" sigma: "+to_string_exact(fabs(ExpStdDev-Voro.obtainedStdDev)));
        ++iter;
        auto TimerEND = std::chrono::steady_clock::now();
        auto TimerDUR = std::chrono::duration_cast<std::chrono::seconds>(TimerEND-TimerSTART);

        if(iter>=limit || TimerDUR.count()>600)
        {
            Log.logandshow(Logger_t::LogLvl::INFO,"Cannot obtain distrbution");
            return 0;
        }
    }while(fabs(ExpMean-Voro.obtainedMean)>0.05 || fabs(ExpStdDev-Voro.obtainedStdDev)>0.001);

    Grain_setup(Structure.Num_layers, Voro, Materials, Environ_Temp, Sim::Grains);
    Generate_interactions(cfg, Structure.Num_layers, Materials, Voro, Sim::Grains, Interactions);
    #ifdef CUDA
        cuda::CudaSolver_t CudaSolver(cfg,Materials_Config,Interactions);
        CudaSolver.set_dt(Hdelta/(SweepRate*Inter_steps));
    #else
        Solver_t Solver(Voro.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
        Solver.set_dt(Hdelta/(SweepRate*Inter_steps));
    #endif

    // Set solver seed
    std::optional<int> SimSeed = cfg.getValueOfKey<int>("Hyst:SimSEED");
    if(SimSeed.has_value()){
        Gen.seed(SimSeed.value());
        const int RNG_warmup=700000;
        Gen.discard(RNG_warmup); // warm up the RNG
    }

    if(!importField.has_value() || !importField.value())
    {
        Log.logandshow(Logger_t::LogLvl::INFO,"dt = "+to_string_exact(Hdelta/(SweepRate*Inter_steps)));
        for(Grain_t & Grain : Sim::Grains){
            Grain.H_appl = HapplUnit*HapplMAG;
            Grain.Temp = Environ_Temp;
        }
    }
    else
    {
        HapplMAG = ImportData[1][0]*1000.0;
        for(Grain_t & Grain : Sim::Grains)
        {
            Grain.H_appl = HapplUnit*HapplMAG;
        }
        Sim::Time = ImportData[0][0];
    }

    // Output Ms(300)
    Log.logandshow(Logger_t::LogLvl::INFO,"Ms("+to_string_exact(Environ_Temp)+") = "+to_string_exact(Sim::Grains[0].Mt()));

    // INITIAL OUTPUT TO FILE
    Vec3 Avg_m;
    for(const Grain_t& Grain : Sim::Grains){Avg_m += Grain.m;}
    Avg_m /= Voro.Num_Grains;

    Output << Avg_m << Delim << HapplUnit << Delim
           << HapplMAG << Delim << Avg_m*HapplUnit << Delim
           << Environ_Temp << Delim << SweepRate << Delim << Sim::Time << "\n";

    // Calculate expected integration steps
    unsigned int ExpectedSteps;
    unsigned int OutputAtStep;
    if(importField.has_value() && importField.value())
    {
        ExpectedSteps = ImportData[0].size();
        OutputAtStep  = ExpectedSteps*0.01;
    }
    else
    {
        ExpectedSteps = (HapplMax*4.0)/(Hdelta/Inter_steps);
        OutputAtStep  = ExpectedSteps*0.01;
    }

    Log.logandshow(Logger_t::LogLvl::INFO, "Total required steps "+to_string_exact(ExpectedSteps));
    unsigned int step=1;
    double prog=0.0;
    double* prev_prog = new double(-1.0);

    // Print out granular data
    std::ofstream GrainFile(OutDir+"/GrainA.dat");
    for(const Grain_t& Grain : Sim::Grains)
    {
        for(size_t v=0; v<=Grain.Vertices.size(); ++v)
        {
            size_t w = v % Grain.Vertices.size();
            GrainFile << Grain.Vertices[w].first << Delim << Grain.Vertices[w].second << Delim
                      << Grain.H_appl << Delim << Grain.m << "\n";
        }
        GrainFile << "\n\n";
    }
    GrainFile.flush();
    GrainFile.close();

    if(importField.has_value() && importField.value())
    {
        // Loop through all desried field values and perform integration
        for(size_t i=1; i<ImportData[0].size(); ++i)
        {
            double dt       = ImportData[0][i]-ImportData[0][i-1];
            double HapplMAG = ImportData[1][i]*1000;
            for(Grain_t & Grain : Sim::Grains){Grain.H_appl = HapplUnit*HapplMAG;}

            // Integrate Structuretem
            #ifdef CUDA
                CudaSolver.set_dt(dt);
                CudaSolver.Integrate(Sim::Grains);
                CudaSolver.CopyToHost(Sim::Grains);
                Sim::Time += CudaSolver.get_dt();
            #else
                Solver.set_dt(dt);
                Solver.Apply_Inclusion_zone(Voro,Structure.Num_layers,0.0,0.0);
                Solver.Integrate(Voro, Structure.Num_layers, Interactions, Sim::Grains);
                Sim::Time += Solver.get_dt();
            #endif

            Vec3 Avg_m;
            for(const Grain_t& Grain : Sim::Grains){Avg_m += Grain.m;}
            Avg_m /= Voro.Num_Grains;

            Output << Avg_m << Delim << HapplUnit << Delim
                << HapplMAG << Delim << Avg_m*HapplUnit << Delim
                << Environ_Temp << Delim << SweepRate << Delim << Sim::Time << "\n";

            PrintProgress(prog, prev_prog);

            if(++step == OutputAtStep)
            {
                step=0;
                prog += 0.01;
            }

        }
    }
    else
    {
        while(HapplMAG>=HapplMin)
        {
            // Update field magnitude and apply to grains
            HapplMAG -= (Hdelta/Inter_steps);
            for(Grain_t & Grain : Sim::Grains){Grain.H_appl = HapplUnit*HapplMAG;}

            // Integrate Structuretem
            #ifdef CUDA
                CudaSolver.Integrate(Sim::Grains);
                CudaSolver.CopyToHost(Sim::Grains);
                Sim::Time += CudaSolver.get_dt();
            #else
                Solver.Apply_Inclusion_zone(Voro,Structure.Num_layers,0.0,0.0);
                Solver.Integrate(Voro, Structure.Num_layers, Interactions, Sim::Grains);
                Sim::Time += Solver.get_dt();
            #endif

            Vec3 Avg_m;
            for(const Grain_t& Grain : Sim::Grains){Avg_m += Grain.m;}
            Avg_m /= Voro.Num_Grains;

            Output << Avg_m << Delim << HapplUnit << Delim
                << HapplMAG << Delim << Avg_m*HapplUnit << Delim
                << Environ_Temp << Delim << SweepRate << Delim << Sim::Time << "\n";

            PrintProgress(prog, prev_prog);

            if(++step == OutputAtStep)
            {
                //std::cout << HapplMAG << "Oe" << std::endl;
                step=0;
                prog += 0.01;
            }
        }
        // Print out granular data
        GrainFile.open(OutDir+"/GrainB.dat");
        for(const Grain_t& Grain : Sim::Grains)
        {
            for(size_t v=0; v<=Grain.Vertices.size(); ++v)
            {
                size_t w = v % Grain.Vertices.size();
                GrainFile << Grain.Vertices[w].first << Delim << Grain.Vertices[w].second << Delim
                        << Grain.H_appl << Delim << Grain.m << "\n";
            }
            GrainFile << "\n\n";
        }
        GrainFile.flush();
        GrainFile.close();
        while(HapplMAG<=HapplMax)
        {
            // Update field magnitude and apply to grains
            HapplMAG += (Hdelta/Inter_steps);
            for(Grain_t & Grain : Sim::Grains){Grain.H_appl = HapplUnit*HapplMAG;}

            // Integrate Structuretem
            #ifdef CUDA
                CudaSolver.Integrate(Sim::Grains);
                CudaSolver.CopyToHost(Sim::Grains);
                Sim::Time += CudaSolver.get_dt();
            #else
                Solver.Apply_Inclusion_zone(Voro,Structure.Num_layers,0.0,0.0);
                Solver.Integrate(Voro, Structure.Num_layers, Interactions, Sim::Grains);
                Sim::Time += Solver.get_dt();
            #endif
            Vec3 Avg_m;
            for(const Grain_t& Grain : Sim::Grains){Avg_m += Grain.m;}
            Avg_m /= Voro.Num_Grains;

            Output << Avg_m << Delim << HapplUnit << Delim
                << HapplMAG << Delim << Avg_m*HapplUnit << Delim
                << Environ_Temp << Delim << SweepRate << Delim << Sim::Time << "\n";

            PrintProgress(prog, prev_prog);

            if(++step == OutputAtStep)
            {
                //std::cout << HapplMAG << "Oe" << std::endl;
                step=0;
                prog += 0.01;
            }
        }
        // Print out granular data
        GrainFile.open(OutDir+"/GrainC.dat");
        for(const Grain_t& Grain : Sim::Grains)
        {
            for(size_t v=0; v<=Grain.Vertices.size(); ++v)
            {
                size_t w = v % Grain.Vertices.size();
                GrainFile << Grain.Vertices[w].first << Delim << Grain.Vertices[w].second << Delim
                        << Grain.H_appl << Delim << Grain.m << "\n";
            }
            GrainFile << "\n\n";
        }
        GrainFile.flush();
        GrainFile.close();
    }
    Output.close();
    delete prev_prog;
    return 0;
}