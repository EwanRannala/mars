/* Test_hyst.cpp
 *  Created on: 10 Dec 2019
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** @file Test_hyst.cpp
 * @brief Test simulation for zero Kelvin hysteresis. */

// System header files
#include <fstream>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <chrono>

// M.A.R.S. specific header files
#include "Classes/Solver.hpp"
#include "Structures.hpp"
#include "Importers/Structure_import.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Importers/Materials_import_Test_layers.hpp"
#include "Io/Data_Output.hpp"

#include "Simulation_globals.hpp"

#include "cuda/Classes/CudaSolver.cuh"

// TODO: Update output

int Test_hyst(){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running hysteresis test");

    const ConfigFile TEST_CFG_FILE("Tests/Hysteresis/test_parameters.cfg");

    Data_output_t DataOut(OutDir,TEST_CFG_FILE);
    Structure_t Structure;
    Material_t Material;
    Voronoi_t Voronoi_data;
    Interaction_t Int_system,Int_systemB;
    std::vector<ConfigFile> Materials_Config;
    std::fstream OUTPUT,OUTPUTB;

    const std::string MAT_FILE_LOC="Tests/Hysteresis";
    Structure_import(TEST_CFG_FILE, &Structure);
    Materials_import_Test_layers(MAT_FILE_LOC,Structure.Num_layers, &Materials_Config, &Material);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Sim::Grains);
    std::vector<Grain_t> Grains;
    Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Grains);
    Generate_interactions(TEST_CFG_FILE,Structure.Num_layers,Material,Voronoi_data,Sim::Grains,Int_system);
    Generate_interactions(TEST_CFG_FILE,Structure.Num_layers,Material,Voronoi_data,Grains,Int_systemB);

    // Steps
    unsigned int Steps_per_H=10000;//0000;
    unsigned int H_steps    =  100;
    unsigned int Angle_steps=    7;

    #ifdef CUDA
        cuda::CudaSolver_t CudaSolver(TEST_CFG_FILE,Materials_Config,Int_system);
        switch(CudaSolver.get_Solver())
        {
        case cuda::Solvers::kMC:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing hysteresis test with kMC solver.");
            OUTPUT.open ("Tests/Hysteresis/Output/CUDA_kMC.dat", std::fstream::out);
            Steps_per_H = 10;
            break;
        case cuda::Solvers::LLG:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing hysteresis test with LLG solver.");
            OUTPUT.open ("Tests/Hysteresis/Output/CUDA_LLG.dat", std::fstream::out);
            break;
        case cuda::Solvers::LLB:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing hysteresis test with LLB solver.");
            OUTPUT.open ("Tests/Hysteresis/Output/CUDA_LLB.dat", std::fstream::out);
            break;
        }
    //#else
        Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,TEST_CFG_FILE,Materials_Config);
        Solver.disableInclusion();
        switch(Solver.get_Solver())
        {
        case Solvers::kMC:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing hysteresis test with kMC solver.");
            OUTPUTB.open ("Tests/Hysteresis/Output/kMC.dat", std::fstream::out);
            Steps_per_H = 10;
            break;
        case Solvers::LLG:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing hysteresis test with LLG solver.");
            OUTPUTB.open ("Tests/Hysteresis/Output/LLG.dat", std::fstream::out);
            break;
        case Solvers::LLB:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing hysteresis test with LLB solver.");
            OUTPUTB.open ("Tests/Hysteresis/Output/LLB.dat", std::fstream::out);
            break;
        }
    #endif

    // Write column headings
    OUTPUT << "#";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "mx" << layer+1 << " my" << layer+1 << " mz" << layer+1 << " ";
    }
    OUTPUT << "mx my mz Hx Hy Hz H_MAG ";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "Hk" << layer+1 << " ";
    }
    OUTPUT << "Hk ";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "ex" << layer+1 << " ey" << layer+1 << " ez" << layer+1 << " ";
    }
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "m.H" << layer+1 << " ";
    }
    OUTPUT << "m.H time\n";
    //~~~
    OUTPUTB << "#";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUTB << "mx" << layer+1 << " my" << layer+1 << " mz" << layer+1 << " ";
    }
    OUTPUTB << "mx my mz Hx Hy Hz H_MAG ";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUTB << "Hk" << layer+1 << " ";
    }
    OUTPUTB << "Hk ";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUTB << "ex" << layer+1 << " ey" << layer+1 << " ez" << layer+1 << " ";
    }
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUTB << "m.H" << layer+1 << " ";
    }
    OUTPUTB << "m.H time\n";


    Vec3 H_appl_unit;
    double Phi_H=0.0, time=0.0;
    std::vector<Vec3> Magnetisation(Voronoi_data.Num_Grains*Structure.Num_layers);
    // Create structures to store layer magnetisation components and set them to zero
    std::vector<Vec3> Magnetisation_layers(Structure.Num_layers);
    std::vector<double> Magnetisation_layers_l(Structure.Num_layers,0.0);
    std::vector<Vec3> Easy_axis_layers(Structure.Num_layers);

    // Loop over grains to find largest anisotropy
    double H_K_max = 2.0*Sim::Grains[0].K/Sim::Grains[0].Ms;
    double H_K_min = 2.0*Sim::Grains[0].K/Sim::Grains[0].Ms;
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        int Offset = Voronoi_data.Num_Grains*layer;
        for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
            const double H_K = 2.0*Sim::Grains[i+Offset].K/Sim::Grains[i+Offset].Ms;
            if(H_K > H_K_max){
                H_K_max = H_K;
            }
            else if(H_K < H_K_min){
                H_K_min = H_K;
            }
        }
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"\t***\tMax Hk = "+to_string_exact(H_K_max));
    Log.logandshow(Logger_t::LogLvl::INFO,"\t***\tMin Hk = "+to_string_exact(H_K_min));
    // Calculate max field applied based on maximum H_K
    double H_k_MAG = 1.5*H_K_max;
    double H_appl_MAG = H_k_MAG;
    // Set H_delta - We use smaller steps when we are close to the switching field
    double H_delta_coarse = (4.0*H_appl_MAG)/(H_steps*0.5);
    double H_delta_fine   = (4.0*H_appl_MAG)/(10*H_steps*0.5);

    bool Predict_time=true;
    double Theta_H = 90.0;
    double delta_Theta = Theta_H/(Angle_steps-1);
    while(Theta_H>-1.0){
        H_appl_MAG = 1.0*H_k_MAG;
        Log.logandshow(Logger_t::LogLvl::INFO,"\tSimulating at "+to_string_exact(Theta_H)+" Degrees");

        // Calculate switching field
        double tanH = pow(tan(Theta_H*M_PI_180),(1.0/3.0));
        double hs   = pow((1-pow(tanH,2.0)+pow(tanH,4.0)),0.5)/(1+pow(tanH,2.0));

        if(Theta_H == 0.0){Theta_H=0.01;}
        else if(Theta_H == 90.0){Theta_H=89.99;}

        unsigned int step_count=0;
        H_appl_unit.x = sin(Theta_H*M_PI_180)*cos(Phi_H*M_PI_180);
        H_appl_unit.y = sin(Theta_H*M_PI_180)*sin(Phi_H*M_PI_180);
        H_appl_unit.z = cos(Theta_H*M_PI_180);
        for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
            int Offset = Voronoi_data.Num_Grains*layer;
            for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                Sim::Grains[grain+Offset].H_appl=H_appl_unit*H_appl_MAG;
                Grains[grain+Offset].H_appl=H_appl_unit*H_appl_MAG;
            }
        }

        std::cout << std::setprecision(15);
        std::cout << fabs(Grains[0].m.x - Sim::Grains[0].m.x) << " " << fabs(Grains[0].m.y - Sim::Grains[0].m.y) << " " << fabs(Grains[0].m.z - Sim::Grains[0].m.z) << std::endl;
        std::cout << "CPU: " << Grains[0].m.x << " " << Grains[0].m.y << " " << Grains[0].m.z << std::endl;
        std::cout << "GPU: " << Sim::Grains[0].m.x << " " << Sim::Grains[0].m.y << " " << Sim::Grains[0].m.z << std::endl;
        std::cout << "CPU: " << Grains[0].H_appl.x << " " << Grains[0].H_appl.y << " " << Grains[0].H_appl.z << std::endl;
        std::cout << "GPU: " << Sim::Grains[0].H_appl.x << " " << Sim::Grains[0].H_appl.y << " " << Sim::Grains[0].H_appl.z << std::endl;
        std::cout << "####################" << std::endl;

        bool Decreasing_Field = true;
        while(step_count<=H_steps){

            // Integrate for desired number of steps
            auto TimerSTART = std::chrono::steady_clock::now();
            for(unsigned int step=0; step<Steps_per_H; ++step){
                #ifdef CUDA
                    CudaSolver.Integrate(Sim::Grains);
                    //time += CudaSolver.get_dt();
                //#else
                    Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                    Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_systemB,Grains);
                    time += Solver.get_dt();
#if 1
                    // Compare CPU and GPU result
                    CudaSolver.CopyToHost(Sim::Grains);
                    double err=1e-13;
                    if(fabs(Grains[0].m.x - Sim::Grains[0].m.x)>err || fabs(Grains[0].m.y - Sim::Grains[0].m.y)>err || fabs(Grains[0].m.z - Sim::Grains[0].m.z)>err){
                        std::cout << std::setprecision(15);
                        std::cout << "Error at step " << step_count*Steps_per_H+Steps_per_H << std::endl;
                        std::cout << fabs(Grains[0].m.x - Sim::Grains[0].m.x) << " " << fabs(Grains[0].m.y - Sim::Grains[0].m.y) << " " << fabs(Grains[0].m.z - Sim::Grains[0].m.z) << std::endl;
                        std::cout << "CPU: " << Grains[0].m.x << " " << Grains[0].m.y << " " << Grains[0].m.z << std::endl;
                        std::cout << "GPU: " << Sim::Grains[0].m.x << " " << Sim::Grains[0].m.y << " " << Sim::Grains[0].m.z << std::endl;
                        std::cout << "CPU: " << Grains[0].H_appl.x << " " << Grains[0].H_appl.y << " " << Grains[0].H_appl.z << std::endl;
                        std::cout << "GPU: " << Sim::Grains[0].H_appl.x << " " << Sim::Grains[0].H_appl.y << " " << Sim::Grains[0].H_appl.z << std::endl;
                        double stop;
                        std::cin >> stop;
                    }
#endif
                #endif
            }
            #ifdef CUDA
                CudaSolver.CopyToHost(Sim::Grains);
            #endif
            auto TimerEND = std::chrono::steady_clock::now();
            auto TimerDUR = std::chrono::duration_cast<std::chrono::milliseconds>(TimerEND-TimerSTART);

            // Compare CPU and GPU result
#if 1
            double err=1e-13;
            if(fabs(Grains[0].m.x - Sim::Grains[0].m.x)>err || fabs(Grains[0].m.y - Sim::Grains[0].m.y)>err || fabs(Grains[0].m.z - Sim::Grains[0].m.z)>err){
                std::cout << std::setprecision(15);
                std::cout << "Error at step " << step_count << std::endl;
                std::cout << fabs(Grains[0].m.x - Sim::Grains[0].m.x) << " " << fabs(Grains[0].m.y - Sim::Grains[0].m.y) << " " << fabs(Grains[0].m.z - Sim::Grains[0].m.z) << std::endl;
                std::cout << "CPU: " << Grains[0].m.x << " " << Grains[0].m.y << " " << Grains[0].m.z << std::endl;
                std::cout << "GPU: " << Sim::Grains[0].m.x << " " << Sim::Grains[0].m.y << " " << Sim::Grains[0].m.z << std::endl;
                std::cout << "CPU: " << Grains[0].H_appl.x << " " << Grains[0].H_appl.y << " " << Grains[0].H_appl.z << std::endl;
                std::cout << "GPU: " << Sim::Grains[0].H_appl.x << " " << Sim::Grains[0].H_appl.y << " " << Sim::Grains[0].H_appl.z << std::endl;
                double stop;
                std::cin >> stop;
            }
#endif
            if(Predict_time){
                Log.logandshow(Logger_t::LogLvl::INFO, "Time per field step "+to_string_exact(TimerDUR.count())+"ms");
                Log.logandshow(Logger_t::LogLvl::INFO, "Predicted simulation time "+to_string_exact(TimerDUR.count()*H_steps*Angle_steps*1e-3)+"s");
                Predict_time=false;
            }

            // Obtain desired data
            Vec3 Magnetisation_total;
            double Magnetisation_total_l = 0.0;
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                int Offset = Voronoi_data.Num_Grains*layer;
                // Dummy variables for calculating layer magnetisation
                Vec3 Magnetisation_layers_dummy;
                Vec3 Easy_axis_layers_dummy;
                double Magnetisation_layers_dummy_l = 0.0;

                for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
                    Magnetisation_total          += Sim::Grains[i+Offset].m;
                    Magnetisation_total_l        += sqrt(Sim::Grains[i+Offset].m*Sim::Grains[i+Offset].m);
                    Magnetisation_layers_dummy   += Sim::Grains[i+Offset].m;
                    Magnetisation_layers_dummy_l += sqrt(Sim::Grains[i+Offset].m*Sim::Grains[i+Offset].m);
                    Easy_axis_layers_dummy       += Sim::Grains[i+Offset].Easy_axis;
                }
                Magnetisation_layers[layer]   = Magnetisation_layers_dummy;
                Magnetisation_layers_l[layer] = Magnetisation_layers_dummy_l;
                Easy_axis_layers[layer]       = Easy_axis_layers_dummy;
            }
            // Normalise layers magnetisation
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                Magnetisation_layers[layer]   /= static_cast<double>(Voronoi_data.Num_Grains);
                Magnetisation_layers_l[layer] /= static_cast<double>(Voronoi_data.Num_Grains);
                Easy_axis_layers[layer]       /= static_cast<double>(Voronoi_data.Num_Grains);
            }
            Magnetisation_total   /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);
            Magnetisation_total_l /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);

            // Output data
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUT << Magnetisation_layers[layer] << " ";
            }
            OUTPUT << Magnetisation_total << " "
                    << H_appl_unit << " " << H_appl_MAG << " ";
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUT << 2.0*Sim::Grains[layer*Voronoi_data.Num_Grains].K/Sim::Grains[layer*Voronoi_data.Num_Grains].Ms << " ";
            }
            OUTPUT << H_K_max << " ";
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUT << Easy_axis_layers[layer] << " ";
            }
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUT << Magnetisation_layers[layer]*H_appl_unit << " ";
            }
            OUTPUT << Magnetisation_total*H_appl_unit << " "
                        << time << "\n";
            OUTPUT << std::flush;
            //~~~
            // Obtain desired data
            Magnetisation_total=Vec3(0.0,0.0,0.0);
            Magnetisation_total_l = 0.0;
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                int Offset = Voronoi_data.Num_Grains*layer;
                // Dummy variables for calculating layer magnetisation
                Vec3 Magnetisation_layers_dummy;
                Vec3 Easy_axis_layers_dummy;
                double Magnetisation_layers_dummy_l = 0.0;

                for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
                    Magnetisation_total          += Grains[i+Offset].m;
                    Magnetisation_total_l        += sqrt(Grains[i+Offset].m*Grains[i+Offset].m);
                    Magnetisation_layers_dummy   += Grains[i+Offset].m;
                    Magnetisation_layers_dummy_l += sqrt(Grains[i+Offset].m*Grains[i+Offset].m);
                    Easy_axis_layers_dummy       += Grains[i+Offset].Easy_axis;
                }
                Magnetisation_layers[layer]   = Magnetisation_layers_dummy;
                Magnetisation_layers_l[layer] = Magnetisation_layers_dummy_l;
                Easy_axis_layers[layer]       = Easy_axis_layers_dummy;
            }
            // Normalise layers magnetisation
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                Magnetisation_layers[layer]   /= static_cast<double>(Voronoi_data.Num_Grains);
                Magnetisation_layers_l[layer] /= static_cast<double>(Voronoi_data.Num_Grains);
                Easy_axis_layers[layer]       /= static_cast<double>(Voronoi_data.Num_Grains);
            }
            Magnetisation_total   /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);
            Magnetisation_total_l /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);

            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUTB << Magnetisation_layers[layer] << " ";
            }
            OUTPUTB << Magnetisation_total << " "
                    << H_appl_unit << " " << H_appl_MAG << " ";
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUTB << 2.0*Grains[layer*Voronoi_data.Num_Grains].K/Grains[layer*Voronoi_data.Num_Grains].Ms << " ";
            }
            OUTPUTB << H_K_max << " ";
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUTB << Easy_axis_layers[layer] << " ";
            }
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                OUTPUTB << Magnetisation_layers[layer]*H_appl_unit << " ";
            }
            OUTPUTB << Magnetisation_total*H_appl_unit << " "
                        << time << "\n";
            OUTPUTB << std::flush;

            // Update field
            if(Decreasing_Field){


                if(fabs(H_appl_MAG/H_K_max + hs)<0.1 || (H_appl_MAG/H_K_max>-hs && ((H_appl_MAG-H_delta_coarse)/H_K_max<-hs))){
                    H_appl_MAG -= H_delta_fine;
                }
                else{
                    H_appl_MAG -= H_delta_coarse;
                }

                if(step_count >= H_steps/2.0){
                    Decreasing_Field = false;
                }
            }
            else{
                if(fabs(H_appl_MAG/H_K_max - hs)<0.1 || (H_appl_MAG/H_K_max<hs && ((H_appl_MAG+H_delta_coarse)/H_K_max>hs))){
                    H_appl_MAG += H_delta_fine;
                }
                else{
                    H_appl_MAG += H_delta_coarse;
                }

            }

            ++step_count;
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                int Offset = Voronoi_data.Num_Grains*layer;
                for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                    Sim::Grains[grain+Offset].H_appl.x=H_appl_unit.x*H_appl_MAG;
                    Sim::Grains[grain+Offset].H_appl.y=H_appl_unit.y*H_appl_MAG;
                    Sim::Grains[grain+Offset].H_appl.z=H_appl_unit.z*H_appl_MAG;
                    Grains[grain+Offset].H_appl.x=H_appl_unit.x*H_appl_MAG;
                    Grains[grain+Offset].H_appl.y=H_appl_unit.y*H_appl_MAG;
                    Grains[grain+Offset].H_appl.z=H_appl_unit.z*H_appl_MAG;
                }
            }
        }
        if(Theta_H == 0.01){Theta_H=0.0;}
        else if(Theta_H == 89.99){Theta_H=90.0;}

        OUTPUT << "\n\n";
        OUTPUTB << "\n\n";
        Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Sim::Grains);
        Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Grains);
        #ifdef CUDA
            CudaSolver.Reinitialise();
        #endif
        Log.logandshow(Logger_t::LogLvl::INFO,to_string_exact(Theta_H)+" Degrees done.");
        Theta_H -= delta_Theta;
    }

    OUTPUT.close();

    return 0;
}
