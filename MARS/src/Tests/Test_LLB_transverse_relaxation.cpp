/* Test_LLB_transverse_relaxation.cpp
 *
 *  Created on: 11 Jun 2019
 *      Author: Ewan Rannala, Andrea Meo
 */

/** \file Test_LLB_transverse_relaxation.cpp
 * \brief Test simulation for LLB. Determines transverse relaxation for comparison with atomistic results. */

// System header files
#include <fstream>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>

// M.A.R.S. specific header files
#include "Classes/Solver.hpp"
#include "Structures.hpp"
#include "Importers/Structure_import.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Importers/Materials_import_Test_LLB.hpp"


int LLB_trans(){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running LLB transverse relaxation");

    Structure_t Structure;
    Material_t Material;
    Voronoi_t Voronoi_data;
    Interaction_t Int_Mat;
    std::vector<Grain_t> Grains;
    const ConfigFile CONFIG_FILE("Tests/LLB_Transverse/test_parameters.cfg");
    std::vector<ConfigFile> Materials_Config;

// Generate system parameters
    // Import structure parameters
    Structure_import(CONFIG_FILE, &Structure);
    // overwrite number of layers to be 1
    Structure.Num_layers = 1;
    // Import material parameters
    Materials_import_Test_LLB(CONFIG_FILE, Structure.Num_layers, &Materials_Config, &Material);
    // Import time step and damping
    Voronoi(Structure, &Voronoi_data);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,CONFIG_FILE,Materials_Config);
    Solver.disableInclusion();
    // This test is only designed for the LLB

    for(int Temps=1;Temps<=4;++Temps){
        double Temperature=1.0, time=0.0, Mx_AVG=0.0, My_AVG=0.0, Mz_AVG=0.0,Length_AVG=0.0;

        if(Temps==1){Temperature=1.0;}
        else if(Temps==2){Temperature=300.0;}
        else if(Temps==3){Temperature=500.0;}
        else if(Temps==4){Temperature=600.0;}

        Log.logandshow(Logger_t::LogLvl::INFO,"Running for "+to_string_exact(Temperature)+" K...");

        std::ofstream LLB_OUTPUT("Tests/LLB_Transverse/Output/LLB"+to_string_exact(Temperature)+"K.dat");

        Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temperature,Grains);
        Generate_interactions(CONFIG_FILE,Structure.Num_layers,Material,Voronoi_data,Grains,Int_Mat);

        // No applied field
        for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
            Grains[grain].H_appl.x=0.0;
            Grains[grain].H_appl.y=0.0;
            Grains[grain].H_appl.z=0.0;
        }
        // Reset time
        time=0.0;
        while(time<=25.0e-12){
            // Perform relaxation
            int num_grains=Voronoi_data.Num_Grains;
            Mx_AVG=My_AVG=Mz_AVG=Length_AVG=0.0;
            for(int grain=0;grain<num_grains;++grain){
                double Length = sqrt(Grains[grain].m.x*Grains[grain].m.x+Grains[grain].m.y*Grains[grain].m.y+Grains[grain].m.z*Grains[grain].m.z);
                Length_AVG += Length;
                Mx_AVG += Grains[grain].m.x/Length;
                My_AVG += Grains[grain].m.y/Length;
                Mz_AVG += Grains[grain].m.z/Length;
            }
            Mx_AVG /= num_grains;
            My_AVG /= num_grains;
            Mz_AVG /= num_grains;
            Length_AVG /= num_grains;

            LLB_OUTPUT << time << " " << Mx_AVG << " " << My_AVG << " " << Mz_AVG << " " << Length_AVG << std::endl;

            Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
            Solver.Integrate(Voronoi_data, Structure.Num_layers, Int_Mat, Grains);
            time += Solver.get_dt();
        }
    }
    return 0;
}


