/* LLB_analytical.cpp
 *  Created on: 22 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Test_analytical.cpp
 * \brief Test simulation for LLB and LLG solvers. Compares LLB/LLG output with an analytical solution. */

// System header files
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>

//M.A.R.S. specific header files
#include "Classes/Solver.hpp"
#include "Structures.hpp"
#include "Importers/Structure_import.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Importers/Materials_import_Test_LLB.hpp"

#include "Simulation_globals.hpp"

#include "cuda/Classes/CudaSolver.cuh"

/** This function performs an analytical test for the LLB/LLG solver. <BR>
 * <P>
 *  Test to verify the correct implementation of the Heun scheme for the LLB/LLG solver.
 *  The test simulates the damped precessional motion of grains whose
 *  magnetisation is initialised along a direction transverse to the
 *  easy-axis and subjected to an external field along the easy-axis.
 *  The test is performed at 0K and the components of the magnetisation
 *  are tested against the LLB analytical solution:
 *  \f[
 *     \begin{aligned}
 *       M_x(t) &= \text{sech}\bigg(\frac{\gamma \alpha_{\perp} Ht}{m^2}\bigg) \cos(\gamma \alpha_{\perp} Ht) \\
 *       M_y(t) &= -\text{sech}\bigg(\frac{\gamma \alpha_{\perp} Ht}{m^2}\bigg) \sin(\gamma \alpha_{\perp} Ht) \\
 *       M_z(t) &= \text{tanh}\bigg(\frac{\gamma \alpha_{\perp} Ht}{m^2}\bigg).
 *     \end{aligned}
 *  \f]
 *  Or the LLG analytical solution:
 *  \f[
 *     \begin{aligned}
 *       M_x(t) &= \frac{1.0}{\text{cosh}\bigg(\frac{\alpha \gamma H}{1+\alpha^2}t \bigg)} \cos(\frac{\gamma Ht}{1+\alpha^2}t) \\
 *       M_y(t) &= \frac{1.0}{\text{cosh}\bigg(\frac{\alpha \gamma H}{1+\alpha^2}t \bigg)} \sin(\frac{\gamma Ht}{1+\alpha^2}t) \\
 *       M_z(t) &= \text{cosh}\bigg(\frac{\alpha \gamma H}{1+\alpha^2}t \bigg).
 *     \end{aligned}
 *  \f]
 *  This test requires the Callen-Callen description of the anisotropy, not the version proposed by Garanin.
 *
 */
int analytic_test(){
    Log.logandshow(Logger_t::LogLvl::INFO,"Running analytical test");

    Structure_t Structure;
    Material_t Material;
    Voronoi_t Voronoi_data;
    Interaction_t Int_system,Int_systemB;
    std::vector<ConfigFile> Materials_Config;
    std::fstream Output,OutputB;
    std::vector<Grain_t> Grains;

    // Read configuration file and setup parameters
    const ConfigFile TEST_CFG_FILE("Tests/Analytical/test_parameters.cfg");
    Structure_import(TEST_CFG_FILE, &Structure);
    Materials_import_Test_LLB(TEST_CFG_FILE, Structure.Num_layers, &Materials_Config, &Material);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Sim::Grains);
    Generate_interactions(TEST_CFG_FILE,Structure.Num_layers,Material,Voronoi_data,Sim::Grains,Int_system);
    Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Grains);
    Generate_interactions(TEST_CFG_FILE,Structure.Num_layers,Material,Voronoi_data,Grains,Int_systemB);


    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,TEST_CFG_FILE,Materials_Config);
    Solver.disableInclusion();
    double Gamma = Solver.get_Gamma();
    double dt    = Solver.get_dt();
    double Alpha = Solver.get_Alpha()[0];
    // Generate output files
    switch(Solver.get_Solver())
    {
    case Solvers::kMC:
        Log.logandshow(Logger_t::LogLvl::WARN,"kMC solver selected. Skipping test.");
        return 0;
    case Solvers::LLB:
        Log.logandshow(Logger_t::LogLvl::INFO,"Performing analytical test using the LLB solver.");
        Output.open ("Tests/Analytical/Output/LLB.dat", std::fstream::out);
        break;
    case Solvers::LLG:
        Log.logandshow(Logger_t::LogLvl::INFO,"Performing analytical test using the LLG solver.");
        Output.open ("Tests/Analytical/Output/LLG.dat", std::fstream::out);
        break;
    }
    Output << "Ax Ay Az x y z Ex Ey Ez t" << std::endl;

    #ifdef CUDA
        cuda::CudaSolver_t CudaSolver(TEST_CFG_FILE,Materials_Config,Int_system);
        double Gamma_GPU = CudaSolver.get_Gamma()[0];
        double dt_GPU    = CudaSolver.get_dt();
        double Alpha_GPU = CudaSolver.get_Alpha()[0];
        // Generate output files
        switch(CudaSolver.get_Solver())
        {
        case cuda::Solvers::kMC:
            Log.logandshow(Logger_t::LogLvl::WARN,"kMC solver selected. Skipping test.");
            return 0;
        case cuda::Solvers::LLB:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing analytical test using the LLB solver.");
            OutputB.open ("Tests/Analytical/Output/LLB_CUDA.dat", std::fstream::out);
            break;
        case cuda::Solvers::LLG:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing analytical test using the LLG solver.");
            OutputB.open ("Tests/Analytical/Output/LLG_CUDA.dat", std::fstream::out);
            break;
        }
        OutputB << "Ax Ay Az x y z Ex Ey Ez t" << std::endl;

        Gamma=Gamma_GPU;
        dt=dt_GPU;
        Alpha=Alpha_GPU;
    #endif

    int steps = static_cast<int>(2e-9/dt);
    Log.logandshow(Logger_t::LogLvl::INFO,"Running for "+to_string_exact(steps)+" steps");

    // Set easy axis to zero. Set applied field.
    double Field_Mag=400.0*M_PI;
    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
        Sim::Grains[grain].Easy_axis=0.0;
        Sim::Grains[grain].H_appl.x=0.0;
        Sim::Grains[grain].H_appl.y=0.0;
        Sim::Grains[grain].H_appl.z=Field_Mag;
        #ifdef CUDA
            Grains[grain].Easy_axis=0.0;
            Grains[grain].H_appl.x=0.0;
            Grains[grain].H_appl.y=0.0;
            Grains[grain].H_appl.z=Field_Mag;
        #endif
    }

    Log.logandshow(Logger_t::LogLvl::INFO,"Performing integration steps...");
    double Ax=0.0, Ay=0.0, Az=0.0;
    for(int step=1; step<steps; ++step){
        std::cout << "Step: " << step << " of " << steps << "\n";

        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,Voronoi_data.Centre_X,Voronoi_data.Centre_Y);
        Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains);
        switch(Solver.get_Solver())
        {
        case Solvers::LLB:
            Ax = cos(Gamma*Field_Mag*step*dt) / cosh(Gamma*Alpha*Field_Mag*step*dt);
            Ay = sin(Gamma*Field_Mag*step*dt) / cosh(Gamma*Alpha*Field_Mag*step*dt);
            Az = tanh( (Gamma*Alpha*Field_Mag*step*dt));
            break;
        case Solvers::LLG:
            Ax = 1.0/(cosh((Alpha*Field_Mag*Gamma*step*dt)/(1+Alpha*Alpha)))*cos((Gamma*Field_Mag*step*dt)/(1+Alpha*Alpha));
            Ay = 1.0/(cosh((Alpha*Field_Mag*Gamma*step*dt)/(1+Alpha*Alpha)))*sin((Gamma*Field_Mag*step*dt)/(1+Alpha*Alpha));
            Az = tanh((Alpha*Gamma*Field_Mag*step*dt)/(1+Alpha*Alpha));
            break;
        default:
            break;
        }
        Output  << std::setprecision(15) << Ax << " " << Ay << " " << Az << " " << Sim::Grains[0].m << " "
                << fabs(Ax-Sim::Grains[0].m.x) << " " << fabs(Ay-Sim::Grains[0].m.y) << " " << fabs(Az-Sim::Grains[0].m.z) << " "
                << step*dt << "\n";

        #ifdef CUDA
            CudaSolver.Integrate(Grains);
            CudaSolver.CopyToHost(Grains);
            switch(CudaSolver.get_Solver())
            {
            case cuda::Solvers::LLB:
                Ax = cos(Gamma*Field_Mag*step*dt) / cosh(Gamma*Alpha*Field_Mag*step*dt);
                Ay = sin(Gamma*Field_Mag*step*dt) / cosh(Gamma*Alpha*Field_Mag*step*dt);
                Az = tanh( (Gamma*Alpha*Field_Mag*step*dt));
                break;
            case cuda::Solvers::LLG:
                Ax = 1.0/(cosh((Alpha*Field_Mag*Gamma*step*dt)/(1+Alpha*Alpha)))*cos((Gamma*Field_Mag*step*dt)/(1+Alpha*Alpha));
                Ay = 1.0/(cosh((Alpha*Field_Mag*Gamma*step*dt)/(1+Alpha*Alpha)))*sin((Gamma*Field_Mag*step*dt)/(1+Alpha*Alpha));
                Az = tanh((Alpha*Gamma*Field_Mag*step*dt)/(1+Alpha*Alpha));
                break;
            default:
                break;
            }
            OutputB << std::setprecision(15) << Ax << " " << Ay << " " << Az << " " << Sim::Grains[0].m << " "
                << fabs(Ax-Sim::Grains[0].m.x) << " " << fabs(Ay-Sim::Grains[0].m.y) << " " << fabs(Az-Sim::Grains[0].m.z) << " "
                << step*dt << "\n";
            // Compare CPU and GPU result
            double err=1e-15;
            if(fabs(Grains[0].m.x - Sim::Grains[0].m.x)>err || fabs(Grains[0].m.y - Sim::Grains[0].m.y)>err || fabs(Grains[0].m.z - Sim::Grains[0].m.z)>err){
                std::cout << std::setprecision(15);
                std::cout << "Delta: " << fabs(Grains[0].m.x - Sim::Grains[0].m.x) << " " << fabs(Grains[0].m.y - Sim::Grains[0].m.y) << " " << fabs(Grains[0].m.z - Sim::Grains[0].m.z) << std::endl;
                std::cout << "CPU: " << Grains[0].m.x << " " << Grains[0].m.y << " " << Grains[0].m.z << std::endl;
                std::cout << "GPU: " << Sim::Grains[0].m.x << " " << Sim::Grains[0].m.y << " " << Sim::Grains[0].m.z << std::endl;
                std::cout << "CPU: " << Grains[0].H_appl.x << " " << Grains[0].H_appl.y << " " << Grains[0].H_appl.z << std::endl;
                std::cout << "GPU: " << Sim::Grains[0].H_appl.x << " " << Sim::Grains[0].H_appl.y << " " << Sim::Grains[0].H_appl.z << std::endl;
                double stop;
                std::cin >> stop;
            }
        #endif
    }
    Output.flush();Output.close();
    #ifdef CUDA
        Output.flush();Output.close();
    #endif

    return 0;
}
