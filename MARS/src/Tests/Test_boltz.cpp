/* Test_boltz.cpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Test_boltz.cpp
 * \brief Test simulation for comparison of simulated Boltzmann distributions with analytical solutions. */

// System header files
#include <fstream>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <chrono>

// M.A.R.S. specific header files
#include "Globals.hpp"
#include "Classes/Solver.hpp"
#include "Structures.hpp"
#include "Importers/Structure_import.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Importers/Materials_import_Test_layers.hpp"

#include "Simulation_globals.hpp"

#include "cuda/Classes/CudaSolver.cuh"

int Test_boltz(){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running Boltzmann test");

    Structure_t Structure;
    Material_t Material;
    Voronoi_t Voronoi_data;
    Interaction_t Int_system;
    std::vector<Grain_t> Grains_BACKUP;
    std::vector<ConfigFile> Materials_Config;
    std::fstream OUTPUT;
    std::string OUTPUT_pre;
    double run_time=0.0;
    int Num_temps=0, NumBins=1;

    const ConfigFile TEST_CFG_FILE("Tests/Boltzmann/test_parameters.cfg");
    const std::string MAT_FILE_LOC("Tests/Boltzmann");
    Structure_import(TEST_CFG_FILE, &Structure);
    Structure.Num_layers=1;
    #ifdef CUDA
        Structure.Dim_x *= 10;
        Structure.Dim_y *= 10;
    #endif
    Materials_import_Test_layers(MAT_FILE_LOC,Structure.Num_layers,&Materials_Config,&Material);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Structure.Num_layers,Voronoi_data,Material,0.0,Sim::Grains);
    Generate_interactions(TEST_CFG_FILE,Structure.Num_layers,Material,Voronoi_data,Sim::Grains,Int_system);

    for(Grain_t & Grain : Sim::Grains){
        Grain.H_appl = 0.0;
    }

    double dt=0.0;
    #ifdef CUDA
        auto solvertype = TEST_CFG_FILE.getValueOfKey<std::string>("solver:solver_selection");
        if(solvertype == "llg"){
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing Boltzmann test using the LLG solver.");
            Material.Ani_method[0]=Material_t::AniMethods::Callen;
            OUTPUT_pre = "Tests/Boltzmann/Output/CUDA_LLG_";
            run_time=1.0e-8;//1.0e-6;
            Num_temps=4;
            NumBins=2000;
        }
        else if(solvertype == "llb"){
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing Boltzmann test using the LLB solver.");
            Material.Ani_method[0]=Material_t::AniMethods::Chi;
            OUTPUT_pre = "Tests/Boltzmann/Output/CUDA_LLB_";
            run_time = 1.0e-11;
            Num_temps=6;
            NumBins=50;
        }
        else{
            Log.logandshow(Logger_t::LogLvl::WARN,"kMC solver selected. Skipping test.");
            return 0;
        }
        cuda::CudaSolver_t CudaSolver(TEST_CFG_FILE,Materials_Config,Int_system);
        dt = CudaSolver.get_dt();
    #else
        Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,TEST_CFG_FILE,Materials_Config);
        Solver.disableInclusion();
        // Set up output file

        // TODO - setting anisotropy type in switch doesn't import correct settings
        /* Currently to correctly test the LLG solver you must ensure the material is set to use callen.
        * For the LLB the material must be set to use chi.
        */
        switch(Solver.get_Solver())
        {
        case Solvers::kMC:
            Log.logandshow(Logger_t::LogLvl::WARN,"kMC solver selected. Skipping test.");
            return 0;
        case Solvers::LLG:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing Boltzmann test using the LLG solver.");
            Material.Ani_method[0]=Material_t::AniMethods::Callen;
            OUTPUT_pre = "Tests/Boltzmann/Output/LLG_";
            run_time=1.0e-6;
            Num_temps=4;
            NumBins=2000;
            break;
        case Solvers::LLB:
            Log.logandshow(Logger_t::LogLvl::INFO,"Performing Boltzmann test using the LLB solver.");
            Material.Ani_method[0]=Material_t::AniMethods::Chi;
            OUTPUT_pre = "Tests/Boltzmann/Output/LLB_";
            run_time = 1.0e-9;
            Num_temps=6;
            NumBins=50;
            break;
        }
        dt = Solver.get_dt();
    #endif
    Grains_BACKUP = Sim::Grains;
    for(int Temps=1;Temps<=Num_temps;++Temps){
        double Temperature=600;
        std::vector<double> mx, my, mz;
        double time=0.0, m_theta=0.0, Mag=0.0;

        if(Temps==1){Temperature=600;}
        else if(Temps==2){Temperature=650;}
        else if(Temps==3){Temperature=680;}
        else if(Temps==4){Temperature=690;}
        else if(Temps==5){Temperature=700;}
        else if(Temps==6){Temperature=750;}

        Log.logandshow(Logger_t::LogLvl::INFO,"Running for "+to_string_exact(Temperature)+" K...\nUsing "+ Material.AniString(Material.Ani_method[0])+" method.");

        OUTPUT.open ((OUTPUT_pre+to_string_exact(Temperature)+"K.dat").c_str(), std::fstream::out);
//        OUTPUT << "theta_m frequency bin_width total_values Boltzmann Normaliser\n";

        int steps=0,rep=0;
        auto start = std::chrono::steady_clock::now();
        int N = NumBins;
        int Bin_pos;
        double dL = M_PI/N;
        double dLM = 1.05/N;
        std::vector<unsigned int> Bin(N,0);
        std::vector<unsigned int> BinMx(2*N,0),BinMy(2*N,0),BinMz(2*N,0);//,BinM(2*N,0);

        for(Grain_t & Grain : Sim::Grains){
            Grain.Temp = Temperature;
        }
        while(time<run_time){
#ifdef RealTimePrint
            std::cout << "Time: " << time << "\r" << std::flush;
#endif
            #ifdef CUDA
                CudaSolver.Integrate(Sim::Grains);
                CudaSolver.CopyToHost(Sim::Grains);
                int g=0;
                for(Grain_t & Grain : Sim::Grains){
                    switch(CudaSolver.get_Solver())
                    {
                    case cuda::Solvers::LLB:
                        // Components
                        Bin_pos = static_cast<int>(Grain.m.x/dLM)+N;
                        ++BinMx[Bin_pos];
                        Bin_pos = static_cast<int>(Grain.m.y/dLM)+N;
                        ++BinMy[Bin_pos];
                        Bin_pos = static_cast<int>(Grain.m.z/dLM)+N;
                        ++BinMz[Bin_pos];
                        break;
                    case cuda::Solvers::LLG:
                        // Angle
                        Mag = sqrt(Grain.m*Grain.m);
                        m_theta = acos(Grain.m.z/Mag);
/*
                        if(time>9.3e-11){
                            std::cout << g << ": " << Grain.m << "\n" << m_theta << std::endl;
                        }
*/
                        ++Bin[static_cast<int>(m_theta/dL)];
                        break;
                    default:
                        // Test applicable to LLG and LLB solvers only.
                        exit(EXIT_FAILURE);
                    }
                    ++g;
                }
/*
                std::cout << __LINE__ << std::endl;
                std::cout << time << " " << Sim::Grains[0].m << std::endl;
*/
                // Divide average by grains
                time += dt;
            #else
                Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                Solver.Integrate(Voronoi_data,Structure.Num_layers,Int_system,Sim::Grains);
                for(Grain_t & Grain : Sim::Grains){
                    switch(Solver.get_Solver())
                    {
                    case Solvers::LLB:
                        // Components
                        Bin_pos = static_cast<int>(Grain.m.x/dLM)+N;
                        ++BinMx[Bin_pos];
                        Bin_pos = static_cast<int>(Grain.m.y/dLM)+N;
                        ++BinMy[Bin_pos];
                        Bin_pos = static_cast<int>(Grain.m.z/dLM)+N;
                        ++BinMz[Bin_pos];
                        break;
                    case Solvers::LLG:
                        // Angle
                        Mag = sqrt(Grain.m*Grain.m);
                        m_theta = acos(Grain.m.z/Mag);
                        ++Bin[static_cast<int>(m_theta/dL)];
                        break;
                    default:
                        // Test applicable to LLG and LLB solvers only.
                        exit(EXIT_FAILURE);
                    }
                }
                // Divide average by grains
                time += Solver.get_dt();
            #endif

            ++steps; ++rep;
            if(rep==1e5){    // Check time taken for 1e6 steps in order to predict total time required per temperature.
                auto end = std::chrono::steady_clock::now();
                std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
                long int Iterations = run_time/dt;
                double predicted_Sim_time = Iterations*elapsed.count()*1e-5;
                Log.logandshow(Logger_t::LogLvl::INFO,"###############################################################################");
                Log.logandshow(Logger_t::LogLvl::INFO,"Predicted run time per temperature: "+to_string_exact(predicted_Sim_time/60)+" min + Binning time");
                Log.logandshow(Logger_t::LogLvl::INFO,"###############################################################################");
            }
            //std::cout << __LINE__ << std::endl;
        }

        // LLG values
        double K_as_T=0.0;
        std::vector<double> Boltz_data;
        double MAX_BOLTZ=0.0;
        unsigned int MAX_HIST=0;
        double Nomraliser=0.0;

        // LLB values
        unsigned int SUMx=0, SUMy=0, SUMz=0;
        int slices=20;
        double division = 0.2/slices;
        std::vector<std::vector<std::vector<double>>> BoltzF2DFULL (slices+1);
        std::vector<std::vector<double>> Normaliser2D(slices+1);
        std::vector<double> NM(slices+1,0);

        #ifdef CUDA
            std::vector<cuda::cu_real_t> ChiPara;
            std::vector<cuda::cu_real_t> ChiPerp;
            CudaSolver.CopyChiParaToHost(ChiPara);
            CudaSolver.CopyChiPerpToHost(ChiPerp);
            switch(CudaSolver.get_Solver())
            {
    //############################### LLG ###############################//
            case cuda::Solvers::LLG:
                // Determine Callen-Callen region for K(T) determination
                if (Temperature <= Sim::Grains[0].Callen_range_lowT){
                    K_as_T=Sim::Grains[0].K*Sim::Grains[0].Callen_factor_lowT * pow(Sim::Grains[0].mEQ(),Sim::Grains[0].Callen_power_lowT);
                }
                else if (Temperature > Sim::Grains[0].Callen_range_midT){
                    K_as_T=Sim::Grains[0].K*Sim::Grains[0].Callen_factor_highT * pow(Sim::Grains[0].mEQ(),Sim::Grains[0].Callen_power_highT);
                } else {
                    K_as_T=Sim::Grains[0].K*Sim::Grains[0].Callen_factor_midT * pow(Sim::Grains[0].mEQ(),Sim::Grains[0].Callen_power_midT);
                }
                // Generate Boltzmann and determine maximum for data and Boltzmann.
                for(int count=0;count<N;count++){
                    double Boltz=0.0;
                    Boltz = sin(count*dL)*exp(-(K_as_T*Sim::Grains[0].Vol*1.0e-21*sin(count*dL)*sin(count*dL))/(1.38e-16*Temperature));
                    Boltz_data.push_back(Boltz);
                    if(Boltz>MAX_BOLTZ){MAX_BOLTZ=Boltz;}

                    unsigned int Hist = Bin[count];
                    if(Hist>MAX_HIST){MAX_HIST=Hist;}
                }
                // Normalise to ensure MAX(Boltzmann)==Maximum probability density.
                Nomraliser = MAX_BOLTZ/(MAX_HIST/(steps*dL));
                for(int count=0;count<N;count++){
                    OUTPUT << count*dL*M_180_PI << " " << Bin[count] << " " << dL*M_180_PI << " " << steps*dL << " " << Boltz_data[count] << " " << Nomraliser << std::endl;
                }
                OUTPUT.close();
                break;
    //############################### LLB ###############################//
            case cuda::Solvers::LLB:
                for(int count=0;count<2*N;count++){
                    SUMx += BinMx[count];
                    SUMy += BinMy[count];
                    SUMz += BinMz[count];
                }
                // Calculate Boltzmann for a range of my slices
                for(int iter=0;iter<=slices;++iter){
                    double my = 0.1 + (iter*division);
                    BoltzF2DFULL[iter].resize(2*N);
                    Normaliser2D[iter].resize(2*N,0);
                    for(int count=-N;count<N;count++){
                        double mx = count*dLM;
                        BoltzF2DFULL[iter][count+N].resize(2*N,0);
                        for(int countZ=-N;countZ<N;countZ++){
                            double mz = countZ*dLM;
                            double m = sqrt(mx*mx+my*my+mz*mz);
                            double F;
                            if(Temperature<=Sim::Grains[0].Tc){
                                F = ( (mx*mx+my*my)/(2*ChiPerp[0]) + (pow(m*m-Sim::Grains[0].mEQ()*Sim::Grains[0].mEQ(),2.0))/(8*ChiPara[0]*Sim::Grains[0].mEQ()*Sim::Grains[0].mEQ()) );
                            } else {
                                F = ( (mx*mx+my*my)/(2*ChiPerp[0]) + ((3*Sim::Grains[0].Tc)/(20*ChiPara[0]*(Sim::Grains[0].Temp-Sim::Grains[0].Tc)))*pow(m*m+((5*(Sim::Grains[0].Temp-Sim::Grains[0].Tc))/(3*Sim::Grains[0].Tc)),2.0));
                            }
                            double MsVF = Sim::Grains[0].Ms*Sim::Grains[0].Vol*1e-21*F;
                            BoltzF2DFULL[iter][count+N][countZ+N]=(m*m)*exp((-1.0*MsVF)/(KB*Sim::Grains[0].Temp));
                            Normaliser2D[iter][count+N] += BoltzF2DFULL[iter][count+N][countZ+N];
                        }
                        NM[iter] += Normaliser2D[iter][count+N];
                    }
                }

                OUTPUT << "mx\tmz\tPmx\tPmz\tMax\t";
                for(int iter=0;iter<=slices;++iter){
                    OUTPUT << "Boltz_"+to_string_exact(division*iter)+"\tBoltzMax_"+to_string_exact(division*iter)+"\t";
                }
                // Loop over x
                for(int count=-N;count<N;count++){
                    // Loop over z
                    for(int countZ=-N;countZ<N;countZ++){
                        OUTPUT << count*dLM << "\t" << countZ*dLM << "\t"
                                << BinMx[count+N] << "\t" << BinMz[countZ+N] << "\t" << SUMx << "\t";
                        for(int iter=0;iter<=slices;++iter){
                            OUTPUT << BoltzF2DFULL[iter][count+N][countZ+N] << "\t" << NM[iter] << "\t";
                        }
                        OUTPUT << std::endl;
                    }
                    OUTPUT << std::endl;
                }
                OUTPUT.close();
                break;
    //############################### DEFAULT ###############################//
            default:
                exit(EXIT_FAILURE);
            }
        #else
            switch(Solver.get_Solver())
            {
    //############################### LLG ###############################//
            case Solvers::LLG:
                // Determine Callen-Callen region for K(T) determination
                if (Temperature <= Sim::Grains[0].Callen_range_lowT){
                    K_as_T=Sim::Grains[0].K*Sim::Grains[0].Callen_factor_lowT * pow(Solver.get_mEQ(0),Sim::Grains[0].Callen_power_lowT);
                }
                else if (Temperature > Sim::Grains[0].Callen_range_midT){
                    K_as_T=Sim::Grains[0].K*Sim::Grains[0].Callen_factor_highT * pow(Solver.get_mEQ(0),Sim::Grains[0].Callen_power_highT);
                } else {
                    K_as_T=Sim::Grains[0].K*Sim::Grains[0].Callen_factor_midT * pow(Solver.get_mEQ(0),Sim::Grains[0].Callen_power_midT);
                }
                // Generate Boltzmann and determine maximum for data and Boltzmann.
                for(int count=0;count<N;count++){
                    double Boltz=0.0;
                    Boltz = sin(count*dL)*exp(-(K_as_T*Sim::Grains[0].Vol*1.0e-21*sin(count*dL)*sin(count*dL))/(1.38e-16*Temperature));
                    Boltz_data.push_back(Boltz);
                    if(Boltz>MAX_BOLTZ){MAX_BOLTZ=Boltz;}

                    unsigned int Hist = Bin[count];
                    if(Hist>MAX_HIST){MAX_HIST=Hist;}
                }
                // Normalise to ensure MAX(Boltzmann)==Maximum probability density.
                Nomraliser = MAX_BOLTZ/(MAX_HIST/(steps*dL));
                for(int count=0;count<N;count++){
                    OUTPUT << count*dL*M_180_PI << " " << Bin[count] << " " << dL*M_180_PI << " " << steps*dL << " " << Boltz_data[count] << " " << Nomraliser << std::endl;
                }
                OUTPUT.close();
                break;
    //############################### LLB ###############################//
            case Solvers::LLB:
                for(int count=0;count<2*N;count++){
                    SUMx += BinMx[count];
                    SUMy += BinMy[count];
                    SUMz += BinMz[count];
                }
                // Calculate Boltzmann for a range of my slices
                for(int iter=0;iter<=slices;++iter){
                    double my = 0.1 + (iter*division);
                    BoltzF2DFULL[iter].resize(2*N);
                    Normaliser2D[iter].resize(2*N,0);
                    for(int count=-N;count<N;count++){
                        double mx = count*dLM;
                        BoltzF2DFULL[iter][count+N].resize(2*N,0);
                        for(int countZ=-N;countZ<N;countZ++){
                            double mz = countZ*dLM;
                            double m = sqrt(mx*mx+my*my+mz*mz);
                            double F;
                            if(Temperature<=Sim::Grains[0].Tc){
                                F = ( (mx*mx+my*my)/(2*Solver.get_ChiPerp(0)) + (pow(m*m-Solver.get_mEQ(0)*Solver.get_mEQ(0),2.0))/(8*Solver.get_ChiPara(0)*Solver.get_mEQ(0)*Solver.get_mEQ(0)) );
                            } else {
                                F = ( (mx*mx+my*my)/(2*Solver.get_ChiPerp(0)) + ((3*Sim::Grains[0].Tc)/(20*Solver.get_ChiPara(0)*(Sim::Grains[0].Temp-Sim::Grains[0].Tc)))*pow(m*m+((5*(Sim::Grains[0].Temp-Sim::Grains[0].Tc))/(3*Sim::Grains[0].Tc)),2.0));
                            }
                            double MsVF = Sim::Grains[0].Ms*Sim::Grains[0].Vol*1e-21*F;
                            BoltzF2DFULL[iter][count+N][countZ+N]=(m*m)*exp((-1.0*MsVF)/(KB*Sim::Grains[0].Temp));
                            Normaliser2D[iter][count+N] += BoltzF2DFULL[iter][count+N][countZ+N];
                        }
                        NM[iter] += Normaliser2D[iter][count+N];
                    }
                }

                OUTPUT << "mx\tmz\tPmx\tPmz\tMax\t";
                for(int iter=0;iter<=slices;++iter){
                    OUTPUT << "Boltz_"+to_string_exact(division*iter)+"\tBoltzMax_"+to_string_exact(division*iter)+"\t";
                }
                // Loop over x
                for(int count=-N;count<N;count++){
                    // Loop over z
                    for(int countZ=-N;countZ<N;countZ++){
                        OUTPUT << count*dLM << "\t" << countZ*dLM << "\t"
                                << BinMx[count+N] << "\t" << BinMz[countZ+N] << "\t" << SUMx << "\t";
                        for(int iter=0;iter<=slices;++iter){
                            OUTPUT << BoltzF2DFULL[iter][count+N][countZ+N] << "\t" << NM[iter] << "\t";
                        }
                        OUTPUT << std::endl;
                    }
                    OUTPUT << std::endl;
                }
                OUTPUT.close();
                break;
    //############################### DEFAULT ###############################//
            default:
                exit(EXIT_FAILURE);
            }
        #endif
        std::cout << "done." << std::endl;
        // Reset grains to initial state
        Sim::Grains = Grains_BACKUP;
        #ifdef CUDA
            CudaSolver.Reinitialise();
        #endif
    }
    return 0;
}


