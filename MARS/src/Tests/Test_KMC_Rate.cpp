/*
 * Test_KMC_Rate.cpp
 *
 *  Created on: 27 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file Test_KMC_Rate.cpp
 * \brief Test simulation for kMC solver. Determines coercivity as a function of sweep rate. */

#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "Classes/Solver.hpp"
#include "Structures.hpp"
#include "Voronoi.hpp"
#include "Importers/Grain_setup.hpp"
#include "Globals.hpp"
#include "Interactions/Generate_interactions.hpp"
#include "Importers/Structure_import.hpp"
#include "Importers/Materials_import_Test_layers.hpp"

#include "Simulation_globals.hpp"

#include "cuda/Classes/CudaSolver.cuh"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
/** Returns sign of argument. If argument is zero, then zero is returned. */

template <typename T>
int sign(T Val){
    if(Val < 0){return -1;}
    else if(Val > 0){return 1;}
    else{return 0;}
}
/** Returns boolean if sign of the arguments is different. */
template <typename Q>
bool Sign_change(Q Val1, Q Val2){
    if(sign(Val1)!=0 || sign(Val2)!=0){
        if(sign(Val1)*sign(Val2)==-1){return true;}
        else{return false;}
    }
    else{return false;}
}
#endif
/** This function tests the kMC solver. The coercivity is dependent on the sweep rate of the applied
 * field. This test obtains the coercivity as a function of sweep rate for comparison with the
 * theoretical relationship derived by Chantrell in 1988 under the assumption of constant attempt
 * frequency and an easy axis parallel to the applied field.
 * <BR>
 * (See R W Chantrell and K O’grady. Time dependence and rate dependence of the coercivity of particulate
 * recording media Related content Magnetic characterization of recording media.
 * J. Phys. D: Appl. Phys, 21:1469, 1988)
 */
int KMC_Rate_test(){

    Log.logandshow(Logger_t::LogLvl::INFO,"Running kMC hysteresis rate test");

    const ConfigFile TEST_CFG_FILE("Tests/KMC_Rate/test_parameters.cfg");

    Data_output_t DataOut(OutDir,TEST_CFG_FILE);
    Structure_t Structure;
    Material_t Material;
    Voronoi_t Voronoi_data;
    Interaction_t Int_system;
    std::vector<ConfigFile> Materials_Config;
    int Inter_steps = 100;
    double Phi_H=0.0, Theta_H=1e-7, time=0.0, H_delta = 5000, Avg_K=0.0, Avg_Ms=0.0, Avg_Vol=0.0, mDOTh_A=0.0, mDOTh_B=0.0;
    Vec3 H_appl_unit, Avg_m, Avg_e;
    #ifdef CUDA
        std::ofstream KMC_Output("Tests/KMC_Rate/Output/KMC_Hyst_CUDA.dat");
        std::ofstream KMC_Hc_Output("Tests/KMC_Rate/Output/KMC_Hc_CUDA.dat");
    #else
        std::ofstream KMC_Output("Tests/KMC_Rate/Output/KMC_Hyst.dat");
        std::ofstream KMC_Hc_Output("Tests/KMC_Rate/Output/KMC_Hc.dat");
    #endif

    KMC_Output << "mx my mz Hx Hy Hz Ex Ey Ez H_MAG Hk m.H Temp Rate time\n";
    KMC_Hc_Output << "Rate Temp Hc_left Hc_right error\n";

    const std::string MAT_FILE_LOC="Tests/KMC_Rate";
    Structure_import(TEST_CFG_FILE, &Structure);
    Materials_import_Test_layers(MAT_FILE_LOC,Structure.Num_layers, &Materials_Config, &Material);
    Voronoi(Structure, &Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,Sim::Grains);
    Generate_interactions(TEST_CFG_FILE,Structure.Num_layers,Material,Voronoi_data,Sim::Grains,Int_system);

    #ifdef CUDA
        cuda::CudaSolver_t CudaSolver(TEST_CFG_FILE,Materials_Config,Int_system);
        if(CudaSolver.get_Solver() != cuda::Solvers::kMC){
            Log.log(Logger_t::LogLvl::ERR,"This test is for the kMC only, please check specified solver in test config file.");
        }
    #else
        Solver_t Solver(TEST_CFG_FILE);
        // This test is only designed for the kMC
        Solver.Force_specific_solver_construction(Voronoi_data.Num_Grains,Structure.Num_layers, TEST_CFG_FILE, Materials_Config, Solvers::kMC);
        Solver.set_Solver(Solvers::kMC);
        Solver.disableInclusion();
    #endif

    // Save initial mag for backup
    std::vector<Vec3> MAG_BACK(Voronoi_data.Num_Grains);
    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
        MAG_BACK[grain].x = Sim::Grains[grain].m.x;
        MAG_BACK[grain].y = Sim::Grains[grain].m.y;
        MAG_BACK[grain].z = Sim::Grains[grain].m.z;
    }

    H_appl_unit.x = sin(Theta_H*M_PI_180)*cos(Phi_H*M_PI_180);
    H_appl_unit.y = sin(Theta_H*M_PI_180)*sin(Phi_H*M_PI_180);
    H_appl_unit.z = cos(Theta_H*M_PI_180);

    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
        Avg_K  += Sim::Grains[grain].K;
        Avg_Ms += Sim::Grains[grain].Ms;
        Avg_Vol += Sim::Grains[grain].Vol;  // Volume is in nm^3
    }
    Avg_K /= Voronoi_data.Num_Grains;
    Avg_Ms /= Voronoi_data.Num_Grains;
    Avg_Vol /= Voronoi_data.Num_Grains;

    double H_k_MAG = (2.0*Avg_K)/Avg_Ms;
    double H_appl_MAG = 1.5*H_k_MAG;
    Log.logandshow(Logger_t::LogLvl::INFO,"Vol: "+to_string_exact(Avg_Vol)+" nm^3\nK: "+to_string_exact(Avg_K)+" erg/cc\nMs: "+to_string_exact(Avg_Ms)+
                        " emu/cc\nHk: "+to_string_exact(H_k_MAG)+" Oe\nHappl: "+to_string_exact(H_appl_MAG)+" Oe");


    for(double Temp=100;Temp<800;Temp+=100){
        H_appl_MAG = 1.5*H_k_MAG;
        Log.logandshow(Logger_t::LogLvl::INFO,"Temperature = "+to_string_exact(Temp)+
                            "\tKV/kT = "+to_string_exact((Avg_K*Avg_Vol*1e-21)/(Temp*KB)));
            for(double Rate=1;Rate<1e+15;Rate*=10){
                Log.logandshow(Logger_t::LogLvl::INFO,"Rate = "+to_string_exact(Rate)+"Oe/s");
                #ifdef CUDA
                    CudaSolver.set_dt(H_delta/(Rate*Inter_steps));
                #else
                    Solver.set_dt(H_delta/(Rate*Inter_steps));
                #endif
                Log.logandshow(Logger_t::LogLvl::INFO,"dt = "+to_string_exact(H_delta/(Rate*Inter_steps)));
                for(Grain_t & Grain : Sim::Grains){
                    Grain.H_appl = H_appl_unit*H_appl_MAG;
                    Grain.Temp = Temp;
                }
                // INITIAL OUTPUT TO FILE
                Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
                for(const Grain_t& Grain : Sim::Grains){
                    Avg_m += Grain.m;
                    Avg_e += Grain.Easy_axis;
                }
                Avg_m /= Voronoi_data.Num_Grains;
                Avg_e /= Voronoi_data.Num_Grains;

                KMC_Output << Avg_m << " " << H_appl_unit << " " << Avg_e << " "
                        << H_appl_MAG << " " << H_k_MAG << " "
                        << Avg_m*H_appl_unit << " " << Temp << " "
                        << Rate << " " << time << "\n" << std::flush;

                // Calculate required integration steps
                unsigned int ExpectedSteps = (6.0*H_k_MAG)/(H_delta/Inter_steps);
                Log.logandshow(Logger_t::LogLvl::INFO,"Required integration steps = "+to_string_exact(ExpectedSteps));
                while(H_appl_MAG>=(-1.5*H_k_MAG)){    // First half of loop
                    //Log.logandshow(Logger_t::LogLvl::INFO,"H_appl = "+to_string_exact(H_appl_MAG));
                    H_appl_MAG-=(H_delta/Inter_steps);
                    for(Grain_t & Grain : Sim::Grains){
                        Grain.H_appl = H_appl_unit*H_appl_MAG;
                    }
                    #ifdef CUDA
                        CudaSolver.Integrate(Sim::Grains);
                        CudaSolver.CopyToHost(Sim::Grains);
                        time += CudaSolver.get_dt();
                    #else
                        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                        Solver.Integrate(Voronoi_data, Structure.Num_layers, Int_system, Sim::Grains);
                        time += Solver.get_dt();
                    #endif

                    // Average over the entire system then write out
                    Avg_m=Avg_e=0.0;
                    for(const Grain_t& Grain : Sim::Grains){
                        Avg_m += Grain.m;
                        Avg_e += Grain.Easy_axis;
                    }
                    Avg_m /= Voronoi_data.Num_Grains;
                    Avg_e /= Voronoi_data.Num_Grains;
                    mDOTh_A = Avg_m*H_appl_unit;
                    // OUTPUT TO FILE
                    KMC_Output << Avg_m << " " << H_appl_unit << " "<< Avg_e << " "
                            << H_appl_MAG << " " << H_k_MAG << " " << mDOTh_A << " "
                            << Temp << " " << Rate << " " << time << "\n" << std::flush;
                    if(Sign_change(mDOTh_A, mDOTh_B)){
                        KMC_Hc_Output << Rate << " " << Temp << " " << H_appl_MAG << " ";
                    }
                    mDOTh_B = mDOTh_A;
                }
                while(H_appl_MAG<=(1.5*H_k_MAG)){    // Second half of loop
                    //Log.logandshow(Logger_t::LogLvl::INFO,"H_appl = "+to_string_exact(H_appl_MAG));
                    H_appl_MAG+=(H_delta/Inter_steps);
                    for(Grain_t & Grain : Sim::Grains){
                        Grain.H_appl = H_appl_unit*H_appl_MAG;
                    }
                    #ifdef CUDA
                        CudaSolver.Integrate(Sim::Grains);
                        CudaSolver.CopyToHost(Sim::Grains);
                        time += CudaSolver.get_dt();
                    #else
                        Solver.Apply_Inclusion_zone(Voronoi_data,Structure.Num_layers,0.0,0.0);
                        Solver.Integrate(Voronoi_data, Structure.Num_layers, Int_system, Sim::Grains);
                        time += Solver.get_dt();
                    #endif
                    // Average over the entire system then write out
                    Avg_m=Avg_e=0.0;
                    for(const Grain_t& Grain : Sim::Grains){
                        Avg_m += Grain.m;
                        Avg_e += Grain.Easy_axis;
                    }
                    Avg_m /= Voronoi_data.Num_Grains;
                    Avg_e /= Voronoi_data.Num_Grains;
                    mDOTh_A = Avg_m*H_appl_unit;
                    // OUTPUT TO FILE
                    KMC_Output << Avg_m << " " << H_appl_unit << " " << Avg_e << " "
                                    << H_appl_MAG << " " << H_k_MAG << " "
                                    << Avg_m*H_appl_unit << " " << Temp << " "
                                    << Rate << " " << time << "\n" << std::flush;
                    if(Sign_change(mDOTh_A, mDOTh_B)){
                        KMC_Hc_Output << H_appl_MAG << " " << (H_delta/Inter_steps) << std::endl;
                    }
                    mDOTh_B = mDOTh_A;
                }
                time = 0.0;
                KMC_Output << "\n\n";
                Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temp,Sim::Grains);
                for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                    Sim::Grains[grain].m = MAG_BACK[grain];
                }
        }
        KMC_Hc_Output << "\n\n";
    }
    KMC_Output.close();
    KMC_Hc_Output.close();
    return 0;
}
