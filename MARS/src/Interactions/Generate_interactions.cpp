/*
 * Generate_interactions.cpp
 *
 *  Created on: 9 Feb 2019
 *      Author: Ewan Rannala
 */

/** \file Generate_interactions.cpp
 * \brief Function for generation of interaction data.
 *
 * All interaction data is handled by this function. This function only needs to be
 * performed once per simulation, after the system has been generated. There are two
 * forms of interactions which are accounted for magnetostatics and exchange.
 *
 * <P> Magnetostatic interactions: <BR>
 * The magnetostatics are determined by the dipole approximation.
 * \f[
 *         W = \frac{V}{4\pi r^3}
 *      \begin{bmatrix}
 *          \frac{3r_{x}^2}{r^2}-1 & \frac{3r_{x}r_{y}}{r^2} & \frac{3r_{x}r_{z}}{r^2} \\
 *          \frac{3r_{y}r_{x}}{r^2} & \frac{3r_{y}^2}{r^2}-1 & \frac{3r_{y}r_{z}}{r^2} \\
 *          \frac{3r_{z}r_{x}}{r^2} & \frac{3r_{z}r_{y}}{r^2} & \frac{3r_{z}^2}{r^2}-1
 *      \end{bmatrix}
 * \f]
 * \f[
 *         \vec{H_{dip}} = W\vec{m}
 * \f]
 * \f$V\f$ is the grain volume. <BR>
 * \f$r\f$ is the displacement between the target and source grains, with the subscript denoting the
 * component.<BR>
 * The geometrical centres for the grains are used to determine the grain positions, while the lattice
 * sites can be used, doing so produces significantly poorer results.<BR>
 * The computational efficiency of this method is improved by taking advantage of the symmetry in the
 * W-matrix (W=W<SUP>T</SUP>). Only six of the nine terms required calculation, specifically only
 * \f$W_{11}\f$, \f$W_{12}\f$, \f$W_{13}\f$, \f$W_{22}\f$, \f$W_{23}\f$, \f$W_{33}\f$ are
 * calculated.<BR>
 * While the dipole approximation provides a fast solution it is not the best solution available. For
 * grains with a large aspect ratio the accuracy is poor, this is due to the approximation that all
 * interactions are driven from the centre of the grain. Due to this MARS currently offers a method
 * to import externally an determined W-matrix. To use this method Structure_t.Magnetostatics_gen_type
 * must be set to <EM>import</EM>
 *
 * An issue with this method is its poor accuracy for grains with large aspect ratios. This poor
 * accuracy is due to the approximation that the interactions are driven from the centre of the
 * grain. <BR>
 * If a different method is desired then the w-matrix can be imported from an input file when
 * Struct_t.Magnetostatics_gen_type is set to <EM>import</EM> and a file named
 * <EM>W_matrix_MARS.dat</EM> must be placed in the Input directory.
 *

 * </P>
 * <P> Exchange interactions: <BR>
 * \f[
 *         H_{exch\,strength} = H_{sat}J_{ij}\frac{<A>}{A_i}\frac{L_{ij}}{<L>}
 * \f]
 * \f[
 *         \vec{H_{exch}} = H_{exch\,strength}\vec{m}
 * \f]
 * \f$< >\f$ denotes an average value.<BR>
 * \f$J_{ij}\f$ is the exchange constant between i<SUP>th</SUP> and j<SUP>th</SUP>.<BR>
 * \f$L_{ij}\f$ is the contact length between grain i<SUP>th</SUP> and j<SUP>th</SUP>.<BR>
 * \f$A_i\f$ is the area of the i<SUP>th</SUP> grain.<BR>
 * \f$H_{sat}\f$ is the exchange field strength at saturation.<BR>
 * \f$\vec{m}\f$ is the reduced magnetisation \f$(\frac{\Vec{M}}{M_s})\f$.<BR>
 * The exchange field strength has a maximum value of \f$H_{sat}\f$, to ensure this
 * \f$j_{ij}\f$ is always distributed about unity and
 * \f$<J_{ij}\frac{<A>}{A_i}\frac{L_{ij}}{<L>}>\f$ is set to equal unity via normalisation.<BR>
 *
 * When determining the exchange between separate layers a different much simpler
 * approach is applied. Here a new neighbour is added to the exchange array
 * accounting for the presence of a neighbour above and/or below the grain. The
 * exchange field strength is then assigned per material.
 * </P> */
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>

#include <forward_list>
#include <algorithm>
#include <iomanip>

#include "Globals.hpp"
#include "Io/Logger/Global_logger.hpp"
#include "Distributions_generator.hpp"
#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

int Generate_interactions(const ConfigFile& cfg, unsigned int Num_layers, const Material_t& MAT,
                          const Voronoi_t& VORO, std::vector<Grain_t>& Grains, Interaction_t& Int_system){


    Log.logandshow(Logger_t::LogLvl::INFO,"Generating interactions...");
    unsigned int Tot_grains = VORO.Num_Grains*Num_layers;
    // Set size of interaction vectors
    Int_system.Magneto_neigh_list_grains.resize(Tot_grains);
    Int_system.Magneto_neigh_list_subseg.resize(Tot_grains);
    Int_system.Magneto_neigh_list_seg.resize(Tot_grains);
    Int_system.Exchange_neigh_list.resize(Tot_grains);
    Int_system.Wxx.resize(Tot_grains); Int_system.Wxy.resize(Tot_grains);
    Int_system.Wxz.resize(Tot_grains);    Int_system.Wyy.resize(Tot_grains);
    Int_system.Wyz.resize(Tot_grains);    Int_system.Wzz.resize(Tot_grains);
    Int_system.Magneto_MeanField_grains.resize(Tot_grains);
    Int_system.MFWxx.resize(Tot_grains); Int_system.MFWxy.resize(Tot_grains);
    Int_system.MFWxz.resize(Tot_grains); Int_system.MFWyy.resize(Tot_grains);
    Int_system.MFWyz.resize(Tot_grains); Int_system.MFWzz.resize(Tot_grains);
    Int_system.H_exch_str.resize(Tot_grains);

//################### MAGNETOSTATIC ########################//

    Int_system.Method = cfg.getValueOfKey<std::string>("Interaction:Magneto_method");
    if(Int_system.Method != "segment" && Int_system.Method != "coarsesegment" &&
       Int_system.Method != "truncate" && Int_system.Method != "exact" && Int_system.Method != "none" && 
       Int_system.Method != "import"){
        Log.log(Logger_t::LogLvl::ERR,"CFG error: Unknown Magnetostatics method type."+Int_system.Method);
    }
    if(Int_system.Method == "none"){
        // Ensure sizes of all vectors are zero
        for(unsigned int g=0;g<Tot_grains;++g){
            Int_system.Magneto_neigh_list_grains[g].clear();
            Int_system.Magneto_neigh_list_subseg[g].clear();
            Int_system.Magneto_neigh_list_seg[g].clear();
            Int_system.Exchange_neigh_list[g].clear();
            Int_system.Wxx[g].clear();
            Int_system.Wxy[g].clear();
            Int_system.Wxz[g].clear();
            Int_system.Wyy[g].clear();
            Int_system.Wyz[g].clear();
            Int_system.Wzz[g].clear();
            Int_system.Magneto_MeanField_grains[g].clear();
            Int_system.MFWxx[g].clear();
            Int_system.MFWxy[g].clear();
            Int_system.MFWxz[g].clear();
            Int_system.MFWyy[g].clear();
            Int_system.MFWyz[g].clear();
            Int_system.MFWzz[g].clear();
        }
    }
    else if(Int_system.Method == "segment" || Int_system.Method == "coarsesegment"){
        //####################SEGMENT SYSTEM FOR MAGNETOSTATIC CALCULATIONS####################//

        /* For large systems this process will be expensive.
        * For each subsegment we need to check which grains are inside.
        * To reduce the cost a forward list is used to store the grains.
        * As a grain is placed it is deleted from the list. 
        */
        // Make a list of all grains - provides constant erase time
        std::forward_list<unsigned int>grains(VORO.Num_Grains);
        std::iota(grains.begin(), grains.end(),0);
        
        double SegSize=cfg.getValAndUnitOfKey<double>("Interaction:Segment_Size")*VORO.Real_grain_width;
        unsigned int SegmentsX=0;
        unsigned int SegmentsY=0;
        unsigned int Segments1D=0;
        unsigned int Subsegments1D=3;
        
        /* We need to determine the number of segments for each dimension.
        * As we assign the size based on the smallest dimension we will assign
        * the number of segments based on that result also.
        */
        if(VORO.Vy_MAX-VORO.Vy_MIN < VORO.Vx_MAX-VORO.Vx_MIN){ // Y smallest
            // If the specified range produces just over 1 box then we should adjust the size to create just 1 box
            double Boxes = (VORO.Vy_MAX-VORO.Vy_MIN)/SegSize;
            if(Boxes>1.0 && Boxes<1.5){SegSize=(VORO.Vy_MAX-VORO.Vy_MIN);}
            Segments1D = std::ceil((VORO.Vy_MAX-VORO.Vy_MIN)/SegSize);
            SegmentsY  = Segments1D;
            SegmentsX  = std::ceil((VORO.Vx_MAX-VORO.Vx_MIN)/SegSize);
        }
        else{ // X smallest
            double Boxes = (VORO.Vx_MAX-VORO.Vx_MIN)/SegSize;
            if(Boxes>1.0 && Boxes<1.5){SegSize=(VORO.Vx_MAX-VORO.Vx_MIN);}
            Segments1D = std::ceil((VORO.Vx_MAX-VORO.Vx_MIN)/SegSize);
            SegmentsX  = Segments1D;
            SegmentsY  = std::ceil((VORO.Vy_MAX-VORO.Vy_MIN)/SegSize);
        }
        double SubSegSize=SegSize/Subsegments1D;
        unsigned int Segments=SegmentsY*SegmentsX;
        unsigned int Subsegments=Subsegments1D*Subsegments1D;

        Int_system.segments.resize(Segments, std::vector<unsigned int>(Subsegments));
        Int_system.subsegments.resize(Subsegments*Segments);
        std::vector<std::pair<double,double>> SegCen(Segments);
        std::vector<std::pair<double,double>> SubSegCen(Subsegments*Segments);
        
        std::vector<std::vector<std::pair<int,int>>> segneighOffset(Segments);
        std::vector<std::vector<std::pair<int,int>>> segneighMidOffset(Segments);
        
        Int_system.segneigh.resize(Segments);
        Int_system.segneighMid.resize(Segments);
        Int_system.segneighFar.resize(Segments,std::vector<long int>(Segments));
        for(size_t idx=0;idx<Int_system.segneighFar.size();++idx){
            // Values of -1 indicates not a FAR neighbour
            std::iota(Int_system.segneighFar[idx].begin(),Int_system.segneighFar[idx].end(),0);
            // Remove this grain from the list
            Int_system.segneighFar[idx][idx]=-1;
        }

        std::vector<std::pair<double,double>> SegVert(5*Int_system.segments.size());
        std::vector<std::pair<double,double>> SubSegVert(5*Int_system.subsegments.size(),std::pair<double,double>(0,0));

        // TODO update logic determining Segment position to avoid casting from unsigned int to int (possible overflow with current implmentation).
        // Vertex array contains n+1 points -- removes sepcial case from winding algorithm
        // Order is BL -> BR -> TR -> TL -> BL (anti-clockwise)
        std::vector<double> VertX(5);
        std::vector<double> VertY(5); 
        for(unsigned int SegY=0;SegY<SegmentsY;++SegY){
            for(unsigned int SegX=0;SegX<SegmentsX;++SegX){
                unsigned int idx = SegX+(SegmentsX*SegY);
                SegCen[idx].first =VORO.Vx_MIN+(SegX*SegSize)+SegSize*0.5;
                SegCen[idx].second=(SegY*SegSize)+SegSize*0.5;

                //##############################################################################//
                // Nearest neighbours
                // Bottom neighbours
                int Yshift=0;
                int Xshift=0;
                int Desired_row = SegY-1;
                while(Desired_row<0){
                    Desired_row += SegmentsY;
                    --Yshift;
                }
                int Starting_col = SegX-1;
                while(Starting_col<0){
                    Starting_col += SegmentsX;
                    --Xshift;
                }
                for(unsigned int MN=0;MN<3;++MN){
                    unsigned int Xidx = Starting_col + MN;
                    int XshiftB=0;
                    while(Xidx>=SegmentsX){
                        Xidx -= SegmentsX;
                        ++XshiftB;
                    }
                    if(Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]!=-1){
                        // The Coarse segmentation method does NOT use mid neighbours only nearest and far
                        if(Int_system.Method == "segment"){
                            Int_system.segneigh[idx].emplace_back(Xidx+(SegmentsX*Desired_row));
                        }
                        else{
                            Int_system.segneighMid[idx].emplace_back(Xidx+(SegmentsX*Desired_row));
                        }
                        segneighOffset[idx].emplace_back(std::make_pair((Xshift+XshiftB),Yshift));
                        Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]=-1;
                    }
                }
                // Right neighbours
                Yshift=0, Xshift=0;
                int Desired_col = SegX+1;
                while(static_cast<unsigned int>(Desired_col)>=SegmentsX){
                    Desired_col -= SegmentsX;
                    ++Xshift;
                }
                if(Int_system.segneighFar[idx][Desired_col+(SegmentsX*SegY)]!=-1){
                    if(Int_system.Method == "segment"){
                        Int_system.segneigh[idx].emplace_back(Desired_col+(SegmentsX*SegY));
                    }
                    else{
                        Int_system.segneighMid[idx].emplace_back(Desired_col+(SegmentsX*SegY));
                    }
                    segneighOffset[idx].emplace_back(std::make_pair(Xshift,Yshift));
                    Int_system.segneighFar[idx][Desired_col+(SegmentsX*SegY)]=-1;
                }                                         
                // Top neighbours
                Yshift=0, Xshift=0;
                Desired_row = SegY+1;
                while(static_cast<unsigned int>(Desired_row)>=SegmentsY){
                    Desired_row -= SegmentsY;
                    ++Yshift;
                }
                Starting_col = SegX+1;
                while(static_cast<unsigned int>(Starting_col)>=SegmentsX){
                    Starting_col -= SegmentsX;
                    ++Xshift;
                }
                for(unsigned int MN=0;MN<3;++MN){
                    int Xidx = Starting_col - MN;
                    int XshiftB=0;
                    while(Xidx<0){
                        Xidx += SegmentsX;
                        --XshiftB;
                    }
                    if(Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]!=-1){
                        if(Int_system.Method == "segment"){
                            Int_system.segneigh[idx].emplace_back(Xidx+(SegmentsX*Desired_row));
                        }
                        else{
                            Int_system.segneighMid[idx].emplace_back(Xidx+(SegmentsX*Desired_row));
                        }
                        segneighOffset[idx].emplace_back(std::make_pair((Xshift+XshiftB),Yshift));
                        Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]=-1;
                    }
                }
                // Left neighbours
                Yshift=0, Xshift=0;
                Desired_col = SegX-1;
                while(Desired_col<0){
                    Desired_col += SegmentsX;
                    --Xshift;    
                }
                if(Int_system.segneighFar[idx][Desired_col+(SegmentsX*SegY)]!=-1){
                    if(Int_system.Method == "segment"){
                        Int_system.segneigh[idx].emplace_back(Desired_col+(SegmentsX*SegY));
                    }
                    else{
                        Int_system.segneighMid[idx].emplace_back(Desired_col+(SegmentsX*SegY));
                    }
                    segneighOffset[idx].emplace_back(std::make_pair(Xshift,Yshift));
                    Int_system.segneighFar[idx][Desired_col+(SegmentsX*SegY)]=-1;
                }
                // Mid range neighbours
                Yshift=0, Xshift=0; // Track the number of periodic boundaries
                // Bottom neighbours
                Desired_row = SegY-2;
                while(Desired_row<0){
                    Desired_row += SegmentsY;
                    --Yshift;
                }
                Starting_col = SegX-2;
                while(Starting_col<0){
                    Starting_col += SegmentsX;
                    --Xshift;
                }
                for(unsigned int MN=0;MN<5;++MN){
                    unsigned int Xidx = Starting_col + MN;
                    int XshiftB = 0;
                    while(Xidx>=SegmentsX){
                        Xidx -= SegmentsX;
                        ++XshiftB;
                    }
                    if(Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]!=-1){
                        Int_system.segneighMid[idx].emplace_back(Xidx+(SegmentsX*Desired_row));
                        segneighMidOffset[idx].emplace_back(std::make_pair((Xshift+XshiftB),Yshift));
                        Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]=-1;
                    }
                }
                // Right neighbours
                Yshift=0, Xshift=0;
                Desired_col = SegX+2;
                while(static_cast<unsigned int>(Desired_col)>=SegmentsX){
                    Desired_col -= SegmentsX;
                    ++Xshift;
                }
                int Starting_row = SegY-1; // We don't want to double count the bottom right neighbour
                while(Starting_row<0){
                    Starting_row += SegmentsY;
                    --Yshift;
                }
                for(unsigned int MN=0;MN<3;++MN){
                    unsigned int Yidx = Starting_row + MN;
                    int YshiftB=0;
                    while(Yidx>=SegmentsY){
                        Yidx -= SegmentsY;
                        ++YshiftB;
                    }
                    if(Int_system.segneighFar[idx][Desired_col+(SegmentsX*Yidx)]!=-1){
                        Int_system.segneighMid[idx].emplace_back(Desired_col+(SegmentsX*Yidx));
                        segneighMidOffset[idx].emplace_back(std::make_pair(Xshift,(Yshift+YshiftB)));
                        Int_system.segneighFar[idx][Desired_col+(SegmentsX*Yidx)]=-1;
                    }                            
                }
                // Top neighbours
                Yshift=0, Xshift=0;
                Desired_row = SegY+2;
                while(static_cast<unsigned int>(Desired_row)>=SegmentsY){
                    Desired_row -= SegmentsY;
                    ++Yshift;
                }
                Starting_col = SegX+2;
                while(static_cast<unsigned int>(Starting_col)>=SegmentsX){
                    Starting_col -= SegmentsX;
                    ++Xshift;
                }
                for(unsigned int MN=0;MN<5;++MN){
                    int Xidx = Starting_col - MN;
                    int XshiftB=0;
                    while(Xidx<0){
                        Xidx += SegmentsX;
                        --XshiftB;
                    }
                    if(Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]!=-1){
                        Int_system.segneighMid[idx].emplace_back(Xidx+(SegmentsX*Desired_row));
                        segneighMidOffset[idx].emplace_back(std::make_pair((Xshift+XshiftB),Yshift));
                        Int_system.segneighFar[idx][Xidx+(SegmentsX*Desired_row)]=-1;
                    }
                }
                // Left neighbours
                Yshift=0, Xshift=0;
                Desired_col = SegX-2;
                while(Desired_col<0){
                    Desired_col += SegmentsX;
                    --Xshift;
                }
                Starting_row = SegY+1; // We don't want to double count the top left neighbour
                while(static_cast<unsigned int>(Starting_row)>=SegmentsY){
                    Starting_row -= SegmentsY;
                    ++Yshift;
                }
                for(unsigned int MN=0;MN<3;++MN){
                    int Yidx = Starting_row - MN;
                    int YshiftB=0;
                    while(Yidx<0){
                        Yidx += SegmentsY;
                        --YshiftB;
                    }
                    if(Int_system.segneighFar[idx][Desired_col+(SegmentsX*Yidx)]!=-1){
                        Int_system.segneighMid[idx].emplace_back(Desired_col+(SegmentsX*Yidx));
                        segneighMidOffset[idx].emplace_back(std::make_pair(Xshift,(Yshift+YshiftB)));
                        Int_system.segneighFar[idx][Desired_col+(SegmentsX*Yidx)]=-1;
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
                // Used for outputting to file
                SegVert[5*idx].first    = VORO.Vx_MIN+(SegX*SegSize);
                SegVert[5*idx].second   = (SegY*SegSize);
                SegVert[5*idx+1].first  = VORO.Vx_MIN+((SegX+1)*SegSize);
                SegVert[5*idx+1].second = (SegY*SegSize);
                SegVert[5*idx+2].first  = VORO.Vx_MIN+((SegX+1)*SegSize);
                SegVert[5*idx+2].second = ((SegY+1)*SegSize);
                SegVert[5*idx+3].first  = VORO.Vx_MIN+(SegX*SegSize);
                SegVert[5*idx+3].second = ((SegY+1)*SegSize);
                SegVert[5*idx+4].first  = VORO.Vx_MIN+(SegX*SegSize);
                SegVert[5*idx+4].second = (SegY*SegSize);

                unsigned int Seg=SegX+(SegY*SegmentsX);
                unsigned int Subsegment_Offset=Subsegments*Seg;
                for(unsigned int row=0;row<Subsegments1D;++row){
                    // Set y bounding
                    VertY[4]=VertY[0]=VertY[1]=(row*SubSegSize)+(SegY*SegSize);
                    VertY[3]=VertY[2]=((row+1)*SubSegSize)+(SegY*SegSize);
                    for(unsigned int col=0;col<Subsegments1D;++col){
                        unsigned int SubSegIdx=(col+(row*Subsegments1D)+Subsegment_Offset);
                        SubSegCen[SubSegIdx].first =VORO.Vx_MIN+(col*SubSegSize)+(SegX*SegSize)+SubSegSize*0.5;
                        SubSegCen[SubSegIdx].second=(row*SubSegSize)+(SegY*SegSize)+SubSegSize*0.5;
                        // Set subsegment ID
                        Int_system.segments[Seg][col+(row*Subsegments1D)]=col+(row*Subsegments1D)+(Subsegment_Offset);
                        // Set x bounding
                        VertX[4]=VertX[3]=VertX[0]=VORO.Vx_MIN+(col*SubSegSize)+(SegX*SegSize);
                        VertX[2]=VertX[1]=VORO.Vx_MIN+((col+1)*SubSegSize)+(SegX*SegSize);

                        // Used for outputting to file
                        SubSegVert[5*SubSegIdx].first    = VertX[0];
                        SubSegVert[5*SubSegIdx].second   = VertY[0];
                        SubSegVert[5*SubSegIdx+1].first  = VertX[1];
                        SubSegVert[5*SubSegIdx+1].second = VertY[1];
                        SubSegVert[5*SubSegIdx+2].first  = VertX[2];
                        SubSegVert[5*SubSegIdx+2].second = VertY[2];
                        SubSegVert[5*SubSegIdx+3].first  = VertX[3];
                        SubSegVert[5*SubSegIdx+3].second = VertY[3];
                        SubSegVert[5*SubSegIdx+4].first  = VertX[4];
                        SubSegVert[5*SubSegIdx+4].second = VertY[4];
                        //################## Insert grains into segments ##################//
                        auto prev=grains.before_begin();
                        for(auto it=grains.begin();it!=grains.end();){
                            double Px=VORO.Geo_grain_centre_X[*it];
                            double Py=VORO.Geo_grain_centre_Y[*it];
                            int wn=0;
                            for(int edge=0;edge<4;++edge){
                                if(VertY[edge]<=Py){
                                    if(VertY[edge+1]>Py){
                                        if(((VertX[edge+1]-VertX[edge])*(Py-VertY[edge])-(Px-VertX[edge])*(VertY[edge+1]-VertY[edge]))>0){
                                            ++wn; // Upwards crossing
                                        } 
                                    }
                                }
                                else{
                                    if(VertY[edge+1]<=Py){
                                        if(((VertX[edge+1]-VertX[edge])*(Py-VertY[edge])-(Px-VertX[edge])*(VertY[edge+1]-VertY[edge]))<0){
                                            --wn; // Downwards crossing
                                        }
                                    }
                                }
                            }
                            if(wn!=0){ // Grain in segment
                                Grains[*it].SubSegment=SubSegIdx; // Store the subsegment this grain belongs to 
                                Grains[*it].Segment=idx; // Store the segment this grain belongs to
                                Int_system.subsegments[col+(row*Subsegments1D)+(Seg*Subsegments)].emplace_back(*it);
                                it = grains.erase_after(prev); // Delete grain from list
                            }
                            else{
                                prev=it;
                                ++it;
                            }
                        }
                    }
                }
            }
        }
        // Determine FAR neighbours
        // Loop through all FAR neighbours and remove any elements which appear as near/mid neighbour (=-1)
        for(std::vector<long> & Farneigh : Int_system.segneighFar){
            // The vectors will contain the values of all far neighbours. 
            // All min and nearest neighbouring grains are set to -1
            // Remove all values equal to -1
            Farneigh.erase(std::remove(Farneigh.begin(), Farneigh.end(), -1), Farneigh.end());
        }

        std::ofstream CentreSEG_FILE(OutDir+"/seg_centre.dat");

        for(size_t Seg=0;Seg<SegCen.size();++Seg){
            CentreSEG_FILE << Seg << " " << SegCen[Seg].first << " " << SegCen[Seg].second << std::endl;
        }
        CentreSEG_FILE.close();
        std::ofstream CentreSUBSEG_FILE(OutDir+"/subseg_centre.dat");

        for(size_t SubSeg=0;SubSeg<SubSegCen.size();++SubSeg){
            CentreSUBSEG_FILE << SubSeg << " " << SubSegCen[SubSeg].first << " " << SubSegCen[SubSeg].second << std::endl;
        }
        CentreSUBSEG_FILE.close();
        std::ofstream SEG_FILE(OutDir+"/seg_file.dat");
        for(size_t S=0;S<Int_system.segments.size();++S){
            SEG_FILE << "#" << S << std::endl;
            for(int i=0;i<5;++i){
                SEG_FILE << SegVert[5*S+i].first << " " << SegVert[5*S+i].second << std::endl;
            }
            SEG_FILE << std::endl;
        }
        SEG_FILE.close();

        std::ofstream SUBSEG_FILE(OutDir+"/subseg_file.dat");
        for(size_t SS=0;SS<Int_system.subsegments.size();++SS){
            SUBSEG_FILE << "#" << SS << std::endl;
            for(int i=0;i<5;++i){ 
                SUBSEG_FILE << SubSegVert[5*SS+i].first << " " << SubSegVert[5*SS+i].second << std::endl;
            }
            SUBSEG_FILE << std::endl;
        }
        SUBSEG_FILE.close();
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    
        // New Segmentation method
        // TODO implement import method
        // TODO expand to 3D
        // For each grain we will loop over all elements and construct the W-matrix.
        
        for(unsigned int CurGrain=0;CurGrain<Tot_grains;++CurGrain){
            unsigned int grain_in_layer_i = CurGrain % VORO.Num_Grains;
            unsigned int Z_layer_i = CurGrain/VORO.Num_Grains;
            double Grain_x=VORO.Geo_grain_centre_X[grain_in_layer_i];
            double Grain_y=VORO.Geo_grain_centre_Y[grain_in_layer_i];
            unsigned int CurSegment = Grains[CurGrain].Segment;
            unsigned int CurSubSeg  = Grains[CurGrain].SubSegment;
            for(unsigned int SubSegment : Int_system.segments[CurSegment]){
                for(unsigned int grain : Int_system.subsegments[SubSegment]){
                    unsigned int grain_in_layer_j = grain % VORO.Num_Grains;
                    unsigned int Z_layer_j = static_cast<int>(grain/VORO.Num_Grains);
                    if(SubSegment==CurSubSeg && grain==CurGrain){continue;}
                    Int_system.Magneto_neigh_list_grains[CurGrain].push_back(grain);
                    // Determine W-matrix
                    double Neigh_x = VORO.Geo_grain_centre_X[grain_in_layer_j];
                    double Neigh_y = VORO.Geo_grain_centre_Y[grain_in_layer_j];
                    // Account for the periodic boundaries of the system.
#if 0
                    if((Grain_x-Neigh_x) > (0.5*VORO.x_max)){Neigh_x += VORO.x_max;}                // LHS
                    else if((Grain_x-Neigh_x) < (-0.5*VORO.x_max)){Neigh_x -= VORO.x_max;}            // RHS
                    if((Grain_y-Neigh_y) < (-0.5*VORO.y_max)){Neigh_y -= VORO.y_max;}                // TOP
                    else if((Grain_y-Neigh_y) > (0.5*VORO.y_max)){Neigh_y += VORO.y_max;}            // BOT
#endif                    
                    double x_diff = Neigh_x-Grain_x;
                    double y_diff = Neigh_y-Grain_y;
                    double z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
                    double Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);
                    
                    double Wprefactor = (Grains[grain].Vol)/(4.0*M_PI*Separation*Separation*Separation);
                    // Symmetric matrix, thus only 6/9 elements are determined.
                    Int_system.Wxx[CurGrain].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
                    Int_system.Wxy[CurGrain].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
                    Int_system.Wxz[CurGrain].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
                    Int_system.Wyy[CurGrain].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
                    Int_system.Wyz[CurGrain].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
                    Int_system.Wzz[CurGrain].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
                }
            }
            // Loop over nearest neighbours
            for(unsigned int neigh : Int_system.segneigh[CurSegment]){
                for(unsigned int SubSegment : Int_system.segments[neigh]){
                    for(unsigned int grain : Int_system.subsegments[SubSegment]){
                        int Z_layer_j = static_cast<int>(grain/VORO.Num_Grains);
                        Int_system.Magneto_neigh_list_grains[CurGrain].push_back(grain);
                        // Determine W-matrix
                        double Neigh_x = VORO.Geo_grain_centre_X[grain];
                        double Neigh_y = VORO.Geo_grain_centre_Y[grain];
                        // Account for the periodic boundaries of the system.
#if 0                        
                        if((Grain_x-Neigh_x) > (0.5*VORO.x_max)){Neigh_x += VORO.x_max;}                // LHS
                        else if((Grain_x-Neigh_x) < (-0.5*VORO.x_max)){Neigh_x -= VORO.x_max;}            // RHS
                        if((Grain_y-Neigh_y) < (-0.5*VORO.y_max)){Neigh_y -= VORO.y_max;}                // TOP
                        else if((Grain_y-Neigh_y) > (0.5*VORO.y_max)){Neigh_y += VORO.y_max;}            // BOT
#endif                        
                        double x_diff = Neigh_x-Grain_x;
                        double y_diff = Neigh_y-Grain_y;
                        double z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
                        double Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);
                        double Wprefactor = (Grains[grain].Vol)/(4.0*M_PI*Separation*Separation*Separation);
                        // Symmetric matrix, thus only 6/9 elements are determined.
                        Int_system.Wxx[CurGrain].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
                        Int_system.Wxy[CurGrain].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
                        Int_system.Wxz[CurGrain].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
                        Int_system.Wyy[CurGrain].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
                        Int_system.Wyz[CurGrain].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
                        Int_system.Wzz[CurGrain].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
                    }
                }
            }
            // Loop over mid neighbours
            for(size_t MN=0;MN<Int_system.segneighMid[CurSegment].size();++MN){
                unsigned int midneigh = Int_system.segneighMid[CurSegment][MN];
                for(unsigned int SubSegment : Int_system.segments[midneigh]){
                    Int_system.Magneto_neigh_list_subseg[CurGrain].push_back(SubSegment);
                    double Wxx=0.0;
                    double Wxy=0.0;
                    double Wxz=0.0;
                    double Wyy=0.0;
                    double Wyz=0.0;
                    double Wzz=0.0;
                    unsigned int gcount=0;
                    for(unsigned int grain : Int_system.subsegments[SubSegment]){
                        int Z_layer_j = static_cast<int>(grain/VORO.Num_Grains);
                        // Determine W-matrix
                        double Neigh_x = VORO.Geo_grain_centre_X[grain];
                        double Neigh_y = VORO.Geo_grain_centre_Y[grain];
                        // Account for the periodic boundaries of the system.
#if 0                        
                        if((Grain_x-Neigh_x) > (0.5*VORO.x_max)){Neigh_x += VORO.x_max;}                // LHS
                        else if((Grain_x-Neigh_x) < (-0.5*VORO.x_max)){Neigh_x -= VORO.x_max;}            // RHS
                        if((Grain_y-Neigh_y) < (-0.5*VORO.y_max)){Neigh_y -= VORO.y_max;}                // TOP
                        else if((Grain_y-Neigh_y) > (0.5*VORO.y_max)){Neigh_y += VORO.y_max;}            // BOT
#endif                        
                        double x_diff = Neigh_x-Grain_x;
                        double y_diff = Neigh_y-Grain_y;
                        double z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
                        double Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);
                        double Wprefactor = (Grains[grain].Vol)/(4.0*M_PI*Separation*Separation*Separation);
                        // Symmetric matrix, thus only 6/9 elements are determined.
                        Wxx += Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1);
                        Wxy += Wprefactor*((3*x_diff*y_diff)/(Separation*Separation));
                        Wxz += Wprefactor*((3*x_diff*z_diff)/(Separation*Separation));
                        Wyy += Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1);
                        Wyz += Wprefactor*((3*y_diff*z_diff)/(Separation*Separation));
                        Wzz += Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1);
                        ++gcount;
                    }
                    if(gcount>0){
                        Int_system.Wxx[CurGrain].push_back(Wxx);
                        Int_system.Wxy[CurGrain].push_back(Wxy);
                        Int_system.Wxz[CurGrain].push_back(Wxz);
                        Int_system.Wyy[CurGrain].push_back(Wyy);
                        Int_system.Wyz[CurGrain].push_back(Wyz);
                        Int_system.Wzz[CurGrain].push_back(Wzz);
                    }
                    else{ // Remove empty subsegment from neighbour list
                        Int_system.Magneto_neigh_list_subseg[CurGrain].pop_back();
                    }
                }
            }
            // Loop over far neighbours    
            for(unsigned int Segment : Int_system.segneighFar[CurSegment]){
                Int_system.Magneto_neigh_list_seg[CurGrain].push_back(Segment);
                double Wxx=0.0;
                double Wxy=0.0;
                double Wxz=0.0;
                double Wyy=0.0;
                double Wyz=0.0;
                double Wzz=0.0;
                unsigned int gcount=0;                
                for(unsigned int SubSegment : Int_system.segments[Segment]){
                    for(unsigned int grain : Int_system.subsegments[SubSegment]){
                        // Determine W-matrix
                        double Neigh_x = VORO.Geo_grain_centre_X[grain];
                        double Neigh_y = VORO.Geo_grain_centre_Y[grain];
                        // Account for the periodic boundaries of the system.
#if 0                        
                        if((Grain_x-Neigh_x) > (0.5*VORO.x_max)){Neigh_x += VORO.x_max;}                    // LHS
                        else if((Grain_x-Neigh_x) < (-0.5*VORO.x_max)){Neigh_x -= VORO.x_max;}            // RHS
                        if((Grain_y-Neigh_y) < (-0.5*VORO.y_max)){Neigh_y -= VORO.y_max;}                // TOP
                        else if((Grain_y-Neigh_y) > (0.5*VORO.y_max)){Neigh_y += VORO.y_max;}            // BOT
#endif                        
                        double x_diff = Neigh_x-Grain_x;
                        double y_diff = Neigh_y-Grain_y;
                        double z_diff = 0.0; // Assuming the same layer -- This needs fixing
                        double Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);
                        double Wprefactor = (Grains[grain].Vol)/(4.0*M_PI*Separation*Separation*Separation);
                        // Symmetric matrix, thus only 6/9 elements are determined.
                        Wxx += Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1);
                        Wxy += Wprefactor*((3*x_diff*y_diff)/(Separation*Separation));
                        Wxz += Wprefactor*((3*x_diff*z_diff)/(Separation*Separation));
                        Wyy += Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1);
                        Wyz += Wprefactor*((3*y_diff*z_diff)/(Separation*Separation));
                        Wzz += Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1);
                        ++gcount;
                    }                
                }
                if(gcount>0){
                    Int_system.Wxx[CurGrain].push_back(Wxx);
                    Int_system.Wxy[CurGrain].push_back(Wxy);
                    Int_system.Wxz[CurGrain].push_back(Wxz);
                    Int_system.Wyy[CurGrain].push_back(Wyy);
                    Int_system.Wyz[CurGrain].push_back(Wyz);
                    Int_system.Wzz[CurGrain].push_back(Wzz);
                }
                else{ // Remove empty segment from neighbour list
                    Int_system.Magneto_neigh_list_seg[CurGrain].pop_back();
                }    
            }
        }
    }
    else if(Int_system.Method != "import"){
        //####################TRUNCATION OR ENTIRE SYSTEM####################//

        double IntRadius=1.0e20;
        if(Int_system.Method == "truncate"){
            IntRadius = cfg.getValAndUnitOfKey<double>("Interaction:Trunc_Radius")*VORO.Real_grain_width;
        }

        // look through all grains in the ENTIRE system
        for(unsigned int i=0;i<Tot_grains;++i){
            double WxxMF=0.0;
            double WxyMF=0.0;
            double WxzMF=0.0;
            double WyyMF=0.0;
            double WyzMF=0.0;
            double WzzMF=0.0;
            // No (sub)segments present make sure vectors are empty
            Int_system.Magneto_neigh_list_seg[i].clear();    
            Int_system.Magneto_neigh_list_subseg[i].clear();    
            unsigned int grain_in_layer_i = i % VORO.Num_Grains;
            unsigned int Z_layer_i = int(i/VORO.Num_Grains);
            double Grain_x=VORO.Geo_grain_centre_X[grain_in_layer_i];
            double Grain_y=VORO.Geo_grain_centre_Y[grain_in_layer_i];
            // Loop over grains is split into two to avoid i==j
            for(unsigned int j=0;j<i;++j){
                unsigned int grain_in_layer_j = j % VORO.Num_Grains;
                unsigned int Z_layer_j = j/VORO.Num_Grains;
                double Neigh_x=VORO.Geo_grain_centre_X[grain_in_layer_j];
                double Neigh_y=VORO.Geo_grain_centre_Y[grain_in_layer_j];
                double x_diff=0.0;
                double y_diff=0.0;
                double z_diff=0.0;
                double Separation=0.0;
                // Account for the periodic boundaries of the system.
#if 1                
                if((Grain_x-Neigh_x) > (VORO.Centre_X)){Neigh_x += VORO.x_max;}                    // LHS
                else if((Grain_x-Neigh_x) < (-VORO.Centre_X)){Neigh_x -= VORO.x_max;}            // RHS
                if((Grain_y-Neigh_y) < (-VORO.Centre_Y)){Neigh_y -= VORO.y_max;}                // TOP
                else if((Grain_y-Neigh_y) > (VORO.Centre_Y)){Neigh_y += VORO.y_max;}            // BOT
#endif                
                x_diff = (Neigh_x-Grain_x);
                y_diff = (Neigh_y-Grain_y);
                z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
                Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);

                double Wprefactor = (Grains[grain_in_layer_j].Vol)/(4*M_PI*Separation*Separation*Separation);
                if(Separation<=IntRadius){
                    Int_system.Magneto_neigh_list_grains[i].push_back(j);
                    // Symmetric matrix, thus only 6/9 elements are determined.
                    Int_system.Wxx[i].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
                    Int_system.Wxy[i].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
                    Int_system.Wxz[i].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
                    Int_system.Wyy[i].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
                    Int_system.Wyz[i].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
                    Int_system.Wzz[i].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
                }
                else{
                    Int_system.Magneto_MeanField_grains[i].push_back(j);
                    WxxMF += Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1);
                    WxyMF += Wprefactor*((3*x_diff*y_diff)/(Separation*Separation));
                    WxzMF += Wprefactor*((3*x_diff*z_diff)/(Separation*Separation));
                    WyyMF += Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1);
                    WyzMF += Wprefactor*((3*y_diff*z_diff)/(Separation*Separation));
                    WzzMF += Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1);
                }                
            }
            for(unsigned int j=i+1;j<Tot_grains;++j){
                unsigned int grain_in_layer_j = j % VORO.Num_Grains;
                unsigned int Z_layer_j = j/VORO.Num_Grains;
                double Neigh_x=VORO.Geo_grain_centre_X[grain_in_layer_j];
                double Neigh_y=VORO.Geo_grain_centre_Y[grain_in_layer_j];
                double x_diff=0.0;
                double y_diff=0.0;
                double z_diff=0.0;
                double Separation=0.0;
                // Account for the periodic boundaries of the system.
#if 1                
                if((Grain_x-Neigh_x) > (VORO.Centre_X)){Neigh_x += VORO.x_max;}                    // LHS
                else if((Grain_x-Neigh_x) < (-VORO.Centre_X)){Neigh_x -= VORO.x_max;}            // RHS
                if((Grain_y-Neigh_y) < (-VORO.Centre_Y)){Neigh_y -= VORO.y_max;}                // TOP
                else if((Grain_y-Neigh_y) > (VORO.Centre_Y)){Neigh_y += VORO.y_max;}            // BOT
#endif
                x_diff = (Neigh_x-Grain_x);
                y_diff = (Neigh_y-Grain_y);
                z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
                                Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);

                double Wprefactor = (Grains[grain_in_layer_j].Vol)/(4*M_PI*Separation*Separation*Separation);
                if(Separation<=IntRadius){
                    Int_system.Magneto_neigh_list_grains[i].push_back(j);
                    // Symmetric matrix, thus only 6/9 elements are determined.
                    Int_system.Wxx[i].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
                    Int_system.Wxy[i].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
                    Int_system.Wxz[i].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
                    Int_system.Wyy[i].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
                    Int_system.Wyz[i].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
                    Int_system.Wzz[i].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
                }
                else{
                    Int_system.Magneto_MeanField_grains[i].push_back(j);
                    WxxMF += Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1);
                    WxyMF += Wprefactor*((3*x_diff*y_diff)/(Separation*Separation));
                    WxzMF += Wprefactor*((3*x_diff*z_diff)/(Separation*Separation));
                    WyyMF += Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1);
                    WyzMF += Wprefactor*((3*y_diff*z_diff)/(Separation*Separation));
                    WzzMF += Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1);
                }
            }
            // Add mean field term.
            if(WxxMF!=0 || WxyMF!=0 || WxzMF!=0 || WyyMF!=0 || WyzMF!=0 || WzzMF!=0){
                Int_system.MFWxx[i].push_back(WxxMF);
                Int_system.MFWxy[i].push_back(WxyMF);
                Int_system.MFWxz[i].push_back(WxzMF);
                Int_system.MFWyy[i].push_back(WyyMF);
                Int_system.MFWyz[i].push_back(WyzMF);
                Int_system.MFWzz[i].push_back(WzzMF);
            }
        }
    }
    else{ // Import W-matrix

    // TODO: Update the import to account for segmentation changes

        Log.logandshow(Logger_t::LogLvl::INFO,"\tImporting...");
        double tempA=0.0;
        double tempB=0.0;
        double Wxx=0.0;
        double Wxy=0.0;
        double Wxz=0.0;
        double Wyy=0.0;
        double Wyz=0.0;
        double Wzz=0.0;
        unsigned int Import_grains=0;
        unsigned int grain_ID=0;
        unsigned int Neigh_size=0;
        std::ifstream INPUT_FILE("Input/W_matrix_MARS.dat");
        std::string line;
        std::getline(INPUT_FILE,line);
           std::stringstream ss(line);

        if(ss >> tempA >> Import_grains >> tempB){
            if(Import_grains!=Tot_grains){
                Log.log(Logger_t::LogLvl::ERR,"Mismatch between grains in magnetostatics input and total system.\nCheck grains per layer and total number of layers.\nHave you included interactions between layers?");
            }
            else{
                Int_system.Wxx.resize(Import_grains);
                Int_system.Wxy.resize(Import_grains);
                Int_system.Wxz.resize(Import_grains);
                Int_system.Wyy.resize(Import_grains);
                Int_system.Wyz.resize(Import_grains);
                Int_system.Wzz.resize(Import_grains);
            }
        }
        else{
            Log.log(Logger_t::LogLvl::ERR,"Error failed to read interactions file.");
        }
        // Read in rest of the data file
        for(unsigned int grain=0;grain<Import_grains;++grain){
            INPUT_FILE >> grain_ID >> Neigh_size;
            // Read in W-matrix for each neighbour.
            for(unsigned int neigh=0;neigh<Neigh_size;++neigh){
            INPUT_FILE >> tempA >> tempB >> Wxx >> Wxy >> Wxz >> Wyy >> Wyz >> Wzz;
                // Need to use grain_ID-1 as input file counts from 1.
                Int_system.Wxx[grain_ID-1].push_back(Wxx);
                Int_system.Wxy[grain_ID-1].push_back(Wxy);
                Int_system.Wxz[grain_ID-1].push_back(Wxz);
                Int_system.Wyy[grain_ID-1].push_back(Wyy);
                Int_system.Wyz[grain_ID-1].push_back(Wyz);
                Int_system.Wzz[grain_ID-1].push_back(Wzz);
            }
        }
    }

    std::ofstream WFILEavg (OutDir+"/Wdata.dat");

    for(unsigned int CurGrain=0;CurGrain<Tot_grains;++CurGrain){
        unsigned int grain_in_layer_i = CurGrain % VORO.Num_Grains;
        double Wxx=0.0;
        double Wxy=0.0;
        double Wxz=0.0;
        double Wyy=0.0;
        double Wyz=0.0;
        double Wzz=0.0;
        for(size_t w=0;w<Int_system.Wxx[CurGrain].size();++w){
    /*    
        WFILE << std::setprecision(15) 
                    << Int_system.Wxx[CurGrain][w] << " " << Int_system.Wxy[CurGrain][w] << " " << Int_system.Wxz[CurGrain][w] << " "
                    << Int_system.Wxy[CurGrain][w] << " " << Int_system.Wyy[CurGrain][w] << " " << Int_system.Wyz[CurGrain][w] << " "
                    << Int_system.Wxz[CurGrain][w] << " " << Int_system.Wyz[CurGrain][w] << " " << Int_system.Wzz[CurGrain][w] << std::endl;
    */                
            Wxx += Int_system.Wxx[CurGrain][w];
            Wxy += Int_system.Wxy[CurGrain][w];
            Wxz += Int_system.Wxz[CurGrain][w];
            Wyy += Int_system.Wyy[CurGrain][w];
            Wyz += Int_system.Wyz[CurGrain][w];
            Wzz += Int_system.Wzz[CurGrain][w];
        }
           WFILEavg << std::setprecision(15) << VORO.Geo_grain_centre_X[grain_in_layer_i] << " " 
                                          << VORO.Geo_grain_centre_Y[grain_in_layer_i] << " "
                                             << Wxx << " " << Wxy << " " << Wxz << " " 
                                             << Wxy << " " << Wyy << " " << Wyz << " " 
                                             << Wxz << " " << Wyz << " " << Wzz << std::endl;
    }





    

//################### EXCHANGE ########################//
    int counter=0;
    double Average=0.0;
    double Normaliser=0.0;
    std::vector<std::vector<double>> Product(Tot_grains);
    std::vector<std::vector<double>> exchange_constants(Tot_grains);
    // PERFORM NORMALISATION OF THE EXCHANGE DISTRIBUTION
    for(unsigned int i=0;i<Tot_grains;++i){
        unsigned int grain_in_layer_i = i % VORO.Num_Grains;
        unsigned int Z_layer_i = i/VORO.Num_Grains;
        // Generate random numbers for neighbours of grain i.
        Dist_gen(VORO.Neighbour_final[grain_in_layer_i].size(),MAT.J_Dist_Type[Z_layer_i],1.0,MAT.StdDev_J[Z_layer_i],&exchange_constants[i]);
        for(unsigned  j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
            double Avg_A_o_A = (VORO.Average_area/VORO.Grain_Area[grain_in_layer_i]);
            double L_o_Avg_L = (VORO.Contact_lengths[grain_in_layer_i][j]/VORO.Average_contact_length);
            Product[i].push_back(exchange_constants[i][j]*Avg_A_o_A*L_o_Avg_L);
    }    }

    for(unsigned int i=0;i<Tot_grains;++i){
        unsigned int grain_in_layer_i = i % VORO.Num_Grains;
        for(size_t j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
            counter++;
            Average += Product[i][j];
    }    }
    Average /= counter;
    Normaliser = 1.0/Average;

    // CREATE NEIGHBOUR LIST PLUS EXCHANGE VALUES IN-PLANE AND OUT-PLANE
    for(unsigned int i=0;i<Tot_grains;++i){
        unsigned int grain_in_layer_i = i % VORO.Num_Grains;
        unsigned int Z_layer_i = i/VORO.Num_Grains;
        unsigned int Layer_offset = Z_layer_i*VORO.Num_Grains; // Enables use of single layer vector for all layers
        for(size_t j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
            Int_system.Exchange_neigh_list[i].push_back(VORO.Neighbour_final[grain_in_layer_i][j]+Layer_offset);
            Int_system.H_exch_str[i].push_back(Product[i][j]*Normaliser*MAT.H_sat[Z_layer_i]);
        }
        // Conditions to check for layers above and below
        if(Z_layer_i>=1){ // Add exchange from layer below
            Int_system.Exchange_neigh_list[i].push_back(i-VORO.Num_Grains); // Add ith grain from upper layer
            double EXCH_FROM_DN = MAT.Hexch_str_out_plane_UP[Z_layer_i-1]*VORO.Grain_Area[grain_in_layer_i]/VORO.Average_area;
            Int_system.H_exch_str[i].push_back(EXCH_FROM_DN);
        }
        if(Z_layer_i+2<=Num_layers){ // Add exchange from layer above
            Int_system.Exchange_neigh_list[i].push_back(i+VORO.Num_Grains); // Add ith grain from lower layer
            double EXCH_FROM_UP = MAT.Hexch_str_out_plane_DOWN[Z_layer_i+1]*VORO.Grain_Area[grain_in_layer_i]/VORO.Average_area;
            Int_system.H_exch_str[i].push_back(EXCH_FROM_UP);
        }
    }
    Log.logandshow(Logger_t::LogLvl::INFO,"Complete.");
    return 0;
}



