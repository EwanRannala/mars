/*
 * HAMR_grain_output.cpp
 *
 *  Created on: 5 Oct 2018
 *      Author: ewan
 */

/** @file HAMR_grain_output.cpp
 * @brief Functions for output of grain individual data. */

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

#include "Structures.hpp"

/* Writes data for each grains vertices, magnetisation, temperature and applied field. */
// OUTPUT_PRE has a default case of no prefix for the output file. This is used in the case when a value isn't provided when calling the function.
int Idv_Grain_output(const unsigned int Num_Layers, const Voronoi_t& VORO, const std::vector<Grain_t>& Grains, const double Time, const std::string& OUTPUT,const std::string OUTPUT_PRE=""){

    std::string FILENAME_idv_START = std::string(OutDir+"/").append(OUTPUT_PRE).append("Time_");
    std::string FILENAME;
    std::ofstream OUTPUT_FILE;


    for(unsigned int layer=0;layer<Num_Layers;++layer){
        FILENAME.append(FILENAME_idv_START.append(to_string_exact(layer)).append("_").append(OUTPUT));
        OUTPUT_FILE.open(FILENAME);
        FILENAME.clear();
        if(!OUTPUT_FILE){throw std::runtime_error(std::string("Cannot create ").append(FILENAME_idv_START).append(to_string_exact(layer)).append("_").append(OUTPUT));}
        unsigned int offset = layer*VORO.Num_Grains;
        OUTPUT_FILE << "#Grain" << Delim << "Vx" << Delim << "Vy" << Delim << "Temp" << Delim 
                    << "Happl" << Delim << "m_x" << Delim << "m_y" << Delim << "m_z" << Delim 
                    << "Time" << Delim << "Tc\n";

        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+offset;

            for(size_t j=0;j<VORO.Vertex_X_final[grain_in_layer].size();++j){
                OUTPUT_FILE << std::setprecision(10) << grain_in_system << Delim <<  VORO.Vertex_X_final[grain_in_layer][j] << Delim 
                            << VORO.Vertex_Y_final[grain_in_layer][j] << Delim <<  Grains[grain_in_system].Temp << Delim 
                            << Grains[grain_in_system].H_appl.z << Delim <<  Grains[grain_in_system].m << Delim 
                            << Time << Delim <<  Grains[grain_in_system].Tc <<  std::endl;
            }
            OUTPUT_FILE << std::setprecision(10) << grain_in_system << Delim <<  VORO.Vertex_X_final[grain_in_layer][0] << Delim 
                        << VORO.Vertex_Y_final[grain_in_layer][0] << Delim <<  Grains[grain_in_system].Temp << Delim 
                        << Grains[grain_in_system].H_appl.z << Delim <<  Grains[grain_in_system].m << Delim 
                        << Time << Delim <<  Grains[grain_in_system].Tc << "\n\n" << std::endl;
        }
        OUTPUT_FILE.close();
    }
    return 0;
}