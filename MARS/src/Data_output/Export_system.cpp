/*
 * Export_system.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** @file Export_system.cpp
 * @brief Functions for exporting the entire simulated system to data files
 * later use in MARS. */

#include "Data_output/Export_system.hpp"

#include "Io/Logger/Global_logger.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

/** Exports all system data for future use. Multiple exports can be performed per simulation with the output_iter used to
 * differentiate different exports. Some files contain data which is constant throughout all simulations, these files are
 * overwritten on each call of this function.
 *
 * @param Num_layers Number of layers to export.
 * @param dz  Thickness of each layer.
 * @param VORO Voronoi data.
 * @param Int Interaction data.
 * @param Grain Grain data.
 * @param Solver Solver class.
 * @param Output_iter Export iteration number.
 */
int Export_system(const unsigned int Num_layers, const std::vector<double>& dz, const Voronoi_t& VORO,
                  const Interaction_t& Int, const std::vector<Grain_t>& Grains, Solver_t& Solver,
                  const std::string& Output_iter){

    /* We want to always output this data with spaces as the delimiter.
     * So we should save the set delimiter and restore it when we are done. */
    std::string DelimTMP = Delim;
    Delim=' ';

    Log.logandshow(Logger_t::LogLvl::INFO,"Exporting system...");

    const unsigned int total_grains = VORO.Num_Grains*Num_layers;

    std::ofstream OUTPUT_VORO_FILE     (OutDir+"/voro.dat");
    std::ofstream OUTPUT_ST_MAT_FILE   (OutDir+"/st_mat.dat");
    std::ofstream OUTPUT_GPV_FILE      ((OutDir+"/gpv_"+ Output_iter +".dat").c_str());
    std::ofstream OUTPUT_GPC_FILE      (OutDir+"/gpc.dat");
    std::ofstream OUTPUT_GPEQ_FILE     (OutDir+"/gpEQ.dat");
    std::ofstream OUTPUT_GP_FILE       ((OutDir+"/gp_"+ Output_iter + ".dat").c_str());
    std::ofstream OUTPUT_CHI_FILE      (OutDir+"/Chi.dat");
    std::ofstream OUTPUT_CHI_PERP_IDV_FILE  (OutDir+"/Chi_perp_Idv.dat");
    std::ofstream OUTPUT_CHI_PARA_IDV_FILE  (OutDir+"/Chi_para_Idv.dat");
    std::ofstream OUTPUT_ALPHA_FILE    (OutDir+"/alpha.dat");
    std::ofstream OUTPUT_INT_MN_FILE   (OutDir+"/int_mn.dat");
    std::ofstream OUTPUT_INT_EN_FILE   (OutDir+"/int_en.dat");
    std::ofstream OUTPUT_INT_W_FILE    (OutDir+"/int_w.dat");
    std::ofstream OUTPUT_INT_HEXC_FILE (OutDir+"/int_hexc.dat");

    Log.logandshow(Logger_t::LogLvl::INFO,"\tOutput files created");
    Log.logandshow(Logger_t::LogLvl::INFO,"\tTotal_grains = " + to_string_exact(total_grains));

//#####################################################################################//

    OUTPUT_VORO_FILE << VORO.Centre_X << " " << VORO.Centre_Y << " "
                     << VORO.Vx_MAX   << " " << VORO.Vy_MAX << " "
                     << VORO.Vx_MIN   << " " << VORO.Vy_MIN << " "
                     << VORO.Input_grain_width << " " << VORO.Real_grain_width << std::endl;
    OUTPUT_VORO_FILE.close();

    OUTPUT_ST_MAT_FILE << VORO.Num_Grains << " " << Num_layers << "\n";
    for(unsigned int LAYER=0;LAYER<Num_layers;++LAYER){
        OUTPUT_ST_MAT_FILE << dz[LAYER] << " ";
    }
    OUTPUT_ST_MAT_FILE << std::endl;
    OUTPUT_ST_MAT_FILE.close();
    Log.logandshow(Logger_t::LogLvl::DEBUG,"\tVoronoi written");

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################################INTERACTIONS#####################################//

    OUTPUT_INT_MN_FILE << Int.Method << "\n";
    if(Int.Method == "segment" || Int.Method == "coarsesegment"){

        for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){

            OUTPUT_INT_MN_FILE << grain_in_system << " " 
            << Int.Magneto_neigh_list_grains[grain_in_system].size() << " " << Int.Magneto_neigh_list_subseg[grain_in_system].size() << " "
            << Int.Magneto_neigh_list_seg[grain_in_system].size() << "\n";

            for(unsigned int Mgrain : Int.Magneto_neigh_list_grains[grain_in_system]){
                OUTPUT_INT_MN_FILE << Mgrain << " ";
            }
            OUTPUT_INT_MN_FILE << "\n";
            for(unsigned int Msseg : Int.Magneto_neigh_list_subseg[grain_in_system]){
                OUTPUT_INT_MN_FILE << Msseg << " ";
            }
            OUTPUT_INT_MN_FILE << "\n";
            for(unsigned int Mseg : Int.Magneto_neigh_list_seg[grain_in_system]){
                OUTPUT_INT_MN_FILE << Mseg << " ";
            }
            OUTPUT_INT_MN_FILE << "\n";
        }
        OUTPUT_INT_MN_FILE << Int.segments.size() << " " << Int.subsegments.size() << "\n";
        
        for(const std::vector<unsigned int>& seg : Int.segments){
            OUTPUT_INT_MN_FILE << seg.size() << "\n";
            for(unsigned int sseg : seg){
                OUTPUT_INT_MN_FILE << sseg << " ";
            }
        }
        OUTPUT_INT_MN_FILE << "\n";
        for(const std::vector<unsigned int>& sseg : Int.subsegments){
            OUTPUT_INT_MN_FILE << sseg.size() << "\n";
            for(unsigned int g : sseg){
                OUTPUT_INT_MN_FILE << g << " ";
            }
        }
    }
    else if(Int.Method == "truncate" || Int.Method == "exact"){
        for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
            OUTPUT_INT_MN_FILE << grain_in_system << " " << Int.Magneto_neigh_list_grains[grain_in_system].size() << "\n";
            for(unsigned int Mneigh : Int.Magneto_neigh_list_grains[grain_in_system]){
                OUTPUT_INT_MN_FILE << Mneigh << " ";
            }
            OUTPUT_INT_MN_FILE << "\n";
        }  
    }
    OUTPUT_INT_MN_FILE << std::flush;
    OUTPUT_INT_MN_FILE.close();

    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_EN_FILE << grain_in_system << " " << Int.Exchange_neigh_list[grain_in_system].size() << "\n";
        for(unsigned int Neigh : Int.Exchange_neigh_list[grain_in_system]){
            OUTPUT_INT_EN_FILE << Neigh << " ";
        }
        OUTPUT_INT_EN_FILE << "\n";
    }
    OUTPUT_INT_EN_FILE << std::flush;
    OUTPUT_INT_EN_FILE.close();

    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_W_FILE << grain_in_system << " " << Int.Wxx[grain_in_system].size() << "\n";
        for(size_t W=0;W<Int.Wxx[grain_in_system].size();++W){
            OUTPUT_INT_W_FILE << Int.Wxx[grain_in_system][W] << " " << Int.Wxy[grain_in_system][W] << " "
                              << Int.Wxz[grain_in_system][W] << " " << Int.Wyy[grain_in_system][W] << " "
                              << Int.Wyz[grain_in_system][W] << " " << Int.Wzz[grain_in_system][W] << "\n";
        }
        OUTPUT_INT_W_FILE << "\n";
    }
    OUTPUT_INT_W_FILE << std::flush;
    OUTPUT_INT_W_FILE.close();    

    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_HEXC_FILE << grain_in_system << " " << Int.H_exch_str[grain_in_system].size() << "\n";
        for(double H : Int.H_exch_str[grain_in_system]){
            OUTPUT_INT_HEXC_FILE << H << " ";
        }
        OUTPUT_INT_HEXC_FILE << "\n";
    }
    OUTPUT_INT_HEXC_FILE << std::flush;
    OUTPUT_INT_HEXC_FILE.close();    
    Log.logandshow(Logger_t::LogLvl::DEBUG,"\tInteractions written");

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################GRAIN PARAMETERS###################################//
    // What do these get used for?
    std::vector<double> K_grain;
    std::vector<unsigned> TmpGrains;
    for(unsigned int g=0;g<VORO.Num_Grains;++g){
        TmpGrains.push_back(g);
    }
    for(unsigned int L=1;L<Num_layers;++L){
        unsigned int offset=VORO.Num_Grains*L;
        for(unsigned int g=0;g<VORO.Num_Grains;++g){
            TmpGrains.push_back(g+offset);
        }
    }

    Solver.set_Chi_size(VORO.Num_Grains*Num_layers);
    Solver.Susceptibilities(VORO.Num_Grains, Num_layers, TmpGrains, Grains);
    for(unsigned int LAYER=0;LAYER<Num_layers;++LAYER){
        unsigned int Offset = VORO.Num_Grains*LAYER;
        OUTPUT_CHI_FILE << Grains[Offset].Ani_method << "\n";
        OUTPUT_CHI_FILE << Solver.get_SusType_int(LAYER) << "\n";
        OUTPUT_CHI_FILE << Solver.get_ChiScaling(LAYER) << "\n";
        OUTPUT_CHI_FILE << Solver.get_ChiParaFit(LAYER) << "\n";
        OUTPUT_CHI_FILE << Solver.get_ChiPerpFit(LAYER) << "\n";

        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
            unsigned grain_in_sys=grain_in_layer+Offset;
            double Temp=Grains[grain_in_sys].Temp;
            double Tc  = Grains[grain_in_sys].Tc;
            double m_EQ=0.0;
            if(Temp>=Tc){
                if(Grains[grain_in_sys].mEQ_Type==1){ // bulk
                    m_EQ=0.0;
                }
                else{
                    m_EQ = 1.0/(1.0/Grains[grain_in_sys].a0_mEQ + Grains[grain_in_sys].b1_mEQ*((Tc-Temp)/Tc) + Grains[grain_in_sys].b2_mEQ*pow((Tc-Temp)/Tc,2));
                }
            }
            else {
                if(Grains[grain_in_sys].mEQ_Type==1){ //bulk
                    m_EQ = pow(1.0-(Temp/Tc),Grains[grain_in_sys].Crit_exp);
                }
                else{
                    m_EQ = Grains[grain_in_sys].a0_mEQ + Grains[grain_in_sys].a1_2_mEQ*pow((Tc-Temp)/Tc,0.5) + Grains[grain_in_sys].a1_mEQ*pow((Tc-Temp)/Tc,1) +
                           Grains[grain_in_sys].a2_mEQ*pow((Tc-Temp)/Tc,2) + Grains[grain_in_sys].a3_mEQ*pow((Tc-Temp)/Tc,3) + Grains[grain_in_sys].a4_mEQ*pow((Tc-Temp)/Tc,4) +
                           Grains[grain_in_sys].a5_mEQ*pow((Tc-Temp)/Tc,5) + Grains[grain_in_sys].a6_mEQ*pow((Tc-Temp)/Tc,6) + Grains[grain_in_sys].a7_mEQ*pow((Tc-Temp)/Tc,7) +
                           Grains[grain_in_sys].a8_mEQ*pow((Tc-Temp)/Tc,8) + Grains[grain_in_sys].a9_mEQ*pow((Tc-Temp)/Tc,9);
                }
            }
           K_grain.push_back((Grains[grain_in_sys].Ms*m_EQ*m_EQ)/(2.0*Solver.get_ChiPerp(grain_in_sys)));
        }
    }
    OUTPUT_CHI_FILE << std::flush;
    OUTPUT_CHI_FILE.close();

    // Grain Magnetisation and Easy axis
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_GPV_FILE << Grains[grain_in_system].m         << " "
                        << Grains[grain_in_system].Easy_axis << " "
                        << Grains[grain_in_system].H_appl    << "\n";
    }
    OUTPUT_GPV_FILE << std::flush;
    OUTPUT_GPV_FILE.close();
    // Grain Temp, Vol, K, Tc, Ms
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_GP_FILE << Grains[grain_in_system].Temp << " " << Grains[grain_in_system].diameter << " "
                       << Grains[grain_in_system].Vol*1.0e-21 << " ";
        if(Grains[grain_in_system].Ani_method==2){ // Chi
            OUTPUT_GP_FILE << K_grain[grain_in_system] << " ";
        }
        else{
            OUTPUT_GP_FILE << Grains[grain_in_system].K << " ";
        }
        OUTPUT_GP_FILE << Grains[grain_in_system].Tc  << " " << Grains[grain_in_system].Ms   << "\n";
    }
    OUTPUT_GP_FILE << std::flush;
    OUTPUT_GP_FILE.close();    
    // Layer Damping
    for(unsigned int LAYER=0;LAYER<Num_layers;++LAYER){
        OUTPUT_ALPHA_FILE << Solver.get_Alpha()[LAYER] << "\n";
    }
    OUTPUT_ALPHA_FILE << std::flush;
    OUTPUT_ALPHA_FILE.close();
    Log.logandshow(Logger_t::LogLvl::DEBUG,"\tDamping written");
    // Here I need to make the choice of saving the correct m_eq data
    // If BULK - want crit_exp not polynomial
    // Else - want polynomial not crit_exp
    for(unsigned int LAYER=0;LAYER<Num_layers;++LAYER){
        unsigned int Offset = VORO.Num_Grains*LAYER;
        OUTPUT_GPEQ_FILE << Grains[Offset].mEQ_Type << "\n";
        if(Grains[Offset].mEQ_Type==1){ //bulk
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPEQ_FILE << Grains[grain_in_system].Crit_exp << "\n";
            }
        }
        else if(Grains[Offset].mEQ_Type==2){ // Polynomial
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPEQ_FILE << Grains[grain_in_system].a0_mEQ << " " << Grains[grain_in_system].a1_mEQ << " "
                                 << Grains[grain_in_system].a2_mEQ << " " << Grains[grain_in_system].a3_mEQ << " "
                                 << Grains[grain_in_system].a4_mEQ << " " << Grains[grain_in_system].a5_mEQ << " "
                                 << Grains[grain_in_system].a6_mEQ << " " << Grains[grain_in_system].a7_mEQ << " "
                                 << Grains[grain_in_system].a8_mEQ << " " << Grains[grain_in_system].a9_mEQ << " "
                                 << Grains[grain_in_system].a1_2_mEQ << " "
                                 << Grains[grain_in_system].b1_mEQ << " " << Grains[grain_in_system].b2_mEQ << "\n";
            }
        }
    }
    OUTPUT_GPEQ_FILE << std::flush;
    OUTPUT_GPEQ_FILE.close();
    Log.logandshow(Logger_t::LogLvl::DEBUG,"\tm_EQ written");
    for(unsigned int LAYER=0;LAYER<Num_layers;++LAYER){
        unsigned int Offset = VORO.Num_Grains*LAYER;
        OUTPUT_GPC_FILE << Grains[Offset].Callen_power_range << "\n";
        if(Grains[Offset].Callen_power_range==1){ // single
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPC_FILE << Grains[grain_in_system].Callen_power << "\n";
            }
        }
        else if(Grains[Offset].Callen_power_range==2){ // multiple
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                unsigned int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPC_FILE << Grains[grain_in_system].Callen_factor_lowT  << " " << Grains[grain_in_system].Callen_power_lowT  << " "
                                << Grains[grain_in_system].Callen_range_lowT   << " " << Grains[grain_in_system].Callen_factor_midT << " "
                                << Grains[grain_in_system].Callen_power_midT   << " " << Grains[grain_in_system].Callen_range_midT  << " "
                                << Grains[grain_in_system].Callen_factor_highT << " " << Grains[grain_in_system].Callen_power_highT << "\n";
            }
        }
    }
    OUTPUT_GPC_FILE << std::flush;
    OUTPUT_GPC_FILE.close();    
    Log.logandshow(Logger_t::LogLvl::DEBUG,"\tCallen written");
    for(unsigned int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        std::vector<double> PERP = Solver.get_ChiPerpFitIdv(grain_in_system);
        std::vector<double> PARA = Solver.get_ChiParaFitIdv(grain_in_system);

        for(double perp : PERP){
            OUTPUT_CHI_PERP_IDV_FILE << perp << " ";
        }
        OUTPUT_CHI_PERP_IDV_FILE << "\n";
        for(double para : PARA){
            OUTPUT_CHI_PARA_IDV_FILE << para << " ";
        }
        OUTPUT_CHI_PARA_IDV_FILE << "\n";
    }
    OUTPUT_CHI_PERP_IDV_FILE << std::flush;
    OUTPUT_CHI_PERP_IDV_FILE.close();
    OUTPUT_CHI_PARA_IDV_FILE << std::flush;
    OUTPUT_CHI_PARA_IDV_FILE.close();
    Log.logandshow(Logger_t::LogLvl::DEBUG,"\tSusceptibility written");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    Log.logandshow(Logger_t::LogLvl::INFO,"Complete.");
    
    // Ensure delimiter is reset before returning.
    Delim=DelimTMP;
    return 0;
}
