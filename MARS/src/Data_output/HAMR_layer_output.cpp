/*
 * HAMR_layer_output.cpp
 *
 *  Created on: 8 Oct 2018
 *      Author: ewan
 */

/** @file HAMR_layer_output.cpp
 * @brief Functions for output of the layer averaged data. */


#include <vector>
#include <cmath>
#include <fstream>
#include <iomanip>

#include "Structures.hpp"


// This code is now set up to work for a set range of grains - enabling entire system data or specific region system data output.
// Need to set up a way to organise the output of this data - separate files?
int HAMR_layer_averages(const unsigned int Num_layers, const unsigned int Num_Grains, const std::vector<Grain_t>& Grains, const double Field_MAG, const double Temperature,
                        const double Time, const std::vector<unsigned int>& Grain_list, const std::string& OUTPUT){

    std::fstream OUTPUT_FILE;
    std::vector<double> Normaliser (Num_layers);
    std::vector<double> Mx (Num_layers);
    std::vector<double> My (Num_layers);
    std::vector<double> Mz (Num_layers);
    std::vector<double> Ml (Num_layers);
    double MxT=0.0;
    double MyT=0.0;
    double MzT=0.0;
    double MlT=0.0;
    double Total_normaliser=0.0;
    unsigned int Number_of_Grains = Grain_list.size();

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ASSIGN TITLES
    OUTPUT_FILE.open(OUTPUT.c_str());
    if(OUTPUT_FILE.peek() == std::ifstream::traits_type::eof()){ // If file is empty.
        OUTPUT_FILE.close();
        OUTPUT_FILE.open(OUTPUT.c_str(), std::ofstream::out);
        OUTPUT_FILE << std::setprecision(10) << "#Time" << Delim << "Temp" << Delim << "Field";
        for(unsigned int Layer=0;Layer<Num_layers;++Layer){
            std::string Layer_string = to_string_exact(Layer+1);
            OUTPUT_FILE << std::string("m_x").append(Layer_string) << Delim <<
                           std::string("m_y").append(Layer_string) << Delim <<
                           std::string("m_z").append(Layer_string) << Delim <<
                           std::string("m_l").append(Layer_string) << Delim;
        }
        OUTPUT_FILE << "MxT" << Delim << "MyT" << Delim << "MzT" << Delim << "MlT" << std::endl;
    }
    else{
        OUTPUT_FILE.close();
        OUTPUT_FILE.open(OUTPUT.c_str(), std::ofstream::app);
    }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ WRITE DATA
    for(unsigned int Layer=0;Layer<Num_layers;++Layer){
        double Avg_vol=0.0;
        unsigned int Offset=(Num_Grains*Layer);
        Mx[Layer]=My[Layer]=Mz[Layer]=Ml[Layer]=0.0;
        for(unsigned int grain_in_layer=0;grain_in_layer<Number_of_Grains;++grain_in_layer){
            unsigned int data_grain = Grain_list[grain_in_layer];
            unsigned int data_grain_in_system = data_grain+Offset;
            Mx[Layer] += Grains[data_grain_in_system].m.x*Grains[data_grain_in_system].Vol;
            My[Layer] += Grains[data_grain_in_system].m.y*Grains[data_grain_in_system].Vol;
            Mz[Layer] += Grains[data_grain_in_system].m.z*Grains[data_grain_in_system].Vol;
            Ml[Layer] += Grains[data_grain_in_system].Vol*sqrt(Grains[data_grain_in_system].m.x*Grains[data_grain_in_system].m.x
                                                              +Grains[data_grain_in_system].m.y*Grains[data_grain_in_system].m.y
                                                              +Grains[data_grain_in_system].m.z*Grains[data_grain_in_system].m.z);
            Avg_vol += Grains[data_grain_in_system].Vol;
        }
        Normaliser[Layer] = Avg_vol;
        Total_normaliser += Normaliser[Layer];

        MxT += Mx[Layer];
        MyT += My[Layer];
        MzT += Mz[Layer];
        MlT += Ml[Layer];
        Mx[Layer] /= Normaliser[Layer];
        My[Layer] /= Normaliser[Layer];
        Mz[Layer] /= Normaliser[Layer];
        Ml[Layer] /= Normaliser[Layer];
    }
    // Determine average values for combined system
    Normaliser.push_back(Total_normaliser);
    Mx.push_back(MxT/Normaliser.back());
    My.push_back(MyT/Normaliser.back());
    Mz.push_back(MzT/Normaliser.back());
    Ml.push_back(MlT/Normaliser.back());

    // OUTPUT_FILE initial values to file
    OUTPUT_FILE << Time << Delim << Temperature << Delim << Field_MAG;
    for(size_t Layer=0;Layer<Mx.size();++Layer){
        OUTPUT_FILE << Delim;
        OUTPUT_FILE << Mx[Layer] << Delim << My[Layer] << Delim << Mz[Layer] << Delim << Ml[Layer];
    }
    OUTPUT_FILE << std::endl;

    OUTPUT_FILE.close();

    return 0;
}
