/*
 * HAMR_switching_output.cpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

/** @file HAMR_switching_output.cpp
 * @brief Functions for output of the initial and final states of the system. */

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "Structures.hpp"

// This function makes the buffer of the initial data.
int HAMR_Switching_BUFFER(const unsigned int Num_layers, const Voronoi_t& VORO, const std::vector<Grain_t>& Grains, const std::vector<double>& dz, std::vector<std::string>*BUFFER){

    BUFFER->resize(Num_layers*VORO.Num_Grains);

    for(unsigned int Layer=0;Layer<Num_layers;++Layer){
        unsigned int offset = Layer*VORO.Num_Grains;
        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+offset;
            BUFFER->at(grain_in_system) = to_string_exact(grain_in_layer) + Delim + to_string_exact(Layer) + Delim
                                        + to_string_exact(dz[Layer]) + Delim + to_string_exact(VORO.Grain_Area[grain_in_layer]) + Delim+ to_string_exact(Grains[grain_in_system].K) + Delim
                                        + to_string_exact(Grains[grain_in_system].m.x) + Delim + to_string_exact(Grains[grain_in_system].m.y) + Delim + to_string_exact(Grains[grain_in_system].m.z);
    }    }
    return 0;
}

// This function adds the final data to the input buffer to produce the desired output file.
int HAMR_Switching_OUTPUT(const unsigned int Num_layers, const unsigned int Num_Grains,const std::vector<Grain_t>& Grains, const std::vector<std::string>& BUFFER, const std::string& OUTPUT){

    std::ofstream OUTPUT_FILE;
    OUTPUT_FILE.open(OUTPUT.c_str());
    OUTPUT_FILE << "#Grain_index" << Delim << "Layer" << Delim << "thickness" << Delim << "grain_area" << Delim
                << "Anisotropy" << Delim << "mx_init" << Delim << "my_init" << Delim << "mz_init" << Delim
                << "mx_final" << Delim << "my_final" << Delim << "mz_final" << std::endl;

    for(unsigned int Layer=0;Layer<Num_layers;++Layer){
        unsigned int offset = Layer*Num_Grains;
        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+offset;
            OUTPUT_FILE << BUFFER[grain_in_system] << Delim << to_string_exact(Grains[grain_in_system].m.x)
                                                   << Delim << to_string_exact(Grains[grain_in_system].m.y)
                                                   << Delim << to_string_exact(Grains[grain_in_system].m.z) << std::endl;
    }    }
    OUTPUT_FILE.close();
    return 0;
}
