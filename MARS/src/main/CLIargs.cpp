/*
 * CLIargs.cpp
 *
 *  Created on: 22 Feb 2022
 *      Author: Ewan Rannala
 */

#include <vector>
#include <string>
#include <iostream>

#include "Globals.hpp"


void CLIargs(int argc, char* argv[], std::string &CFGfile){
    std::vector<std::string> args(argv, argv+argc);
    for(unsigned int arg_num=1;arg_num<args.size();++arg_num){
        std::string arg = args[arg_num];

        /*############ Help ############*/
        if(arg=="--help" || arg=="-h"){
            std::cout << "See Readme.md for configuration file details.\n";
            std::cout << "--f <input filename> To select a custom input configuration file.\n\n";
            std::cout << "Available simulations types are:\n";
            std::cout << "\tHAMR\n";
            std::cout << "\tHAMR_WRITE_LOCAL\n";
            std::cout << "\tHAMR_WRITE_DATA\n";
            std::cout << "\tHAMR_WRITE_DATA_CONT\n";
            std::cout << "\tSYSTEM_GEN\n";
            std::cout << "\tTHERMOREMANENCE\n";
            std::cout << "\tREAD_BACK\n";
            std::cout << "\tDATA_LONGEVITY\n";
            std::cout << "\tFMR\n";
            std::cout << "Available tests are:\n";
            std::cout << "\tANALYTICAL\n";
            std::cout << "\tHYSTERESIS\n";
            std::cout << "\tBOLTZMANN\n";
            std::cout << "\tLLB-MVT-MULTILAYER\n";
            std::cout << "\tLLB-HYSTERESIS-TEMPERATURE-MULTILAYER\n";
            std::cout << "\tLLB-TRANSVERSE\n";
            std::cout << "\tKMC-RATE\n";
            std::cout << "\t\n";
            std::cout << std::flush;
            exit (EXIT_SUCCESS);
        }
        else if(arg=="--file" || arg=="-f"){
            // Check number of provided arguments
            if(arg_num<args.size()-1){
                CFGfile=args[arg_num+1]; // Need to make this global!
                ++arg_num;
            }
            else{
                // Logger_t::LogLvl::ERROR HERE
            }
        }
        else{
            std::cout << "Unknown argument use -h or --help for help." << std::endl;
            std::cout << "Usage: ./MARS [--help] [--file <input filename>]" << std::endl;
            exit(EXIT_FAILURE);
        }
    }






}