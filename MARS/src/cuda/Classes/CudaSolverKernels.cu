#include "cuda/Classes/CudaSolverKernels.cuh"
#include <stdio.h>
namespace cuda
{
    namespace kernels
    {

        #ifdef CUDA

        __global__ void RNG_init(curandState *rand_states, const int seed){
            // Each thread is set a different sequence to ensure the random numbers between threads do not correlate
            size_t tid = blockIdx.x * blockDim.x + threadIdx.x;
            curand_init(seed, tid, 0, &rand_states[tid]);
        }

        //################################ FIELDS ################################//
        /* Thus function sets the field values and so it should be called first.*/
        __global__ void update_applied_field(const cu_real_t *H_appx, const cu_real_t *H_appy, const cu_real_t *H_appz,
                                cu_real_t *Fieldx, cu_real_t *Fieldy, cu_real_t *Fieldz, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                Fieldx[grain] = H_appx[grain];
                Fieldy[grain] = H_appy[grain];
                Fieldz[grain] = H_appz[grain];
#if 0
                if(grain==0){
                    printf("GPU Happ: %20.15e %20.15e %20.15e\n",H_appx[grain],H_appy[grain],H_appz[grain]);
                }
#endif
            }
        }

        __global__ void update_anisotropy_field(const int* Layer, const layer_params_t *LayerParams,
                                    const cu_real_t *m_x, const cu_real_t *m_y, const cu_real_t *m_z,
                                    const cu_real_t *EA_x, const cu_real_t *EA_y, const cu_real_t *EA_z,
                                    const cu_real_t *Ms, const cu_real_t *K, const cu_real_t *Temperature,
                                    const cu_real_t *Chi_perp, const cu_real_t *m_EQ,
                                    cu_real_t *Fieldx, cu_real_t *Fieldy, cu_real_t *Fieldz,
                                    const Solvers SolverType, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){

                size_t Lid = Layer[grain];
                int Ani_method = LayerParams[Lid].Anisotropy_method;

                cu_real_t H_ani = 0.0;
                cu_real_t MdotE = 0.0;
                cu_real_t Chi_perp_inv = 0.0;

                switch(Ani_method)
                {
                    case 1: // Callen-Callen
                        MdotE = (m_x[grain]*EA_x[grain]+m_y[grain]*EA_y[grain]+m_z[grain]*EA_z[grain]);

                        /* LLB does NOT store K(t) or Ms(t) so we calculate the temperature dependence here
                         * LLG stores K(t) and Ms(t) if T_dependent_variables is enabled, so we will
                         * always skip the temperature dependence here.
                         */
                        if(SolverType==cuda::Solvers::LLB){
                            if(Temperature[grain] <= LayerParams[Lid].Callen_range_lowT){
                                H_ani = 2.0*K[grain]/Ms[grain] * LayerParams[Lid].Callen_factor_lowT * pow(m_EQ[grain],LayerParams[Lid].Callen_power_lowT-1.0);
                            }
                            else if(Temperature[grain] > LayerParams[Lid].Callen_range_midT){
                                H_ani = 2.0*K[grain]/Ms[grain] * LayerParams[Lid].Callen_factor_highT * pow(m_EQ[grain],LayerParams[Lid].Callen_power_highT-1.0);
                            }
                            else{
                                H_ani = 2.0*K[grain]/Ms[grain] * LayerParams[Lid].Callen_factor_midT * pow(m_EQ[grain],LayerParams[Lid].Callen_power_midT-1.0);
                            }
                            #ifdef CUDA_DP
                                MdotE *= rnorm3d(m_x[grain],m_y[grain],m_z[grain]);
                            #else
                                MdotE *= rnorm3df(m_x[grain],m_y[grain],m_z[grain]);
                            #endif
                        }
                        else{
                            H_ani = 2.0*K[grain]/Ms[grain];
#if 0
                            if(grain==0){
                                printf("mEQ: %20.15e\n",m_EQ[grain]);
                                printf("K: %20.15e\n", K[grain]);
                                printf("Ms: %20.15e\n", Ms[grain]);
                                printf("Hani: %20.15e\n", H_ani);
                            }
#endif
                        }
                        Fieldx[grain] += H_ani*MdotE*EA_x[grain];
                        Fieldy[grain] += H_ani*MdotE*EA_y[grain];
                        Fieldz[grain] += H_ani*MdotE*EA_z[grain];
#if 0
                        if(grain==0){
                            printf("GPU Hani: %20.15e %20.15e %20.15e\n",H_ani*MdotE*EA_x[grain],H_ani*MdotE*EA_y[grain],H_ani*MdotE*EA_z[grain]);
                        }
#endif
                        break;
                    case 2: // Susceptibility
                        if(Chi_perp[grain]<1.0e-10){Chi_perp_inv=0.0;}
                        else{Chi_perp_inv = 1.0/Chi_perp[grain];}
                        Fieldx[grain] += -Chi_perp_inv*m_x[grain];
                        Fieldy[grain] += -Chi_perp_inv*m_y[grain];
                        Fieldz[grain] += 0.0;
                        break;
                }
            }
        }

        __global__ void update_internal_exchange_field(const cu_real_t *m_x, const cu_real_t *m_y, const cu_real_t *m_z,
                                                        const cu_real_t *m_EQ, const cu_real_t *Chi_para, const cu_real_t *Tc,
                                                        const cu_real_t *Temperature, cu_real_t *Fieldx,
                                                        cu_real_t *Fieldy, cu_real_t *Fieldz, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){

                cu_real_t m_MAG_SQ = m_x[grain]*m_x[grain] + m_y[grain]*m_y[grain] + m_z[grain]*m_z[grain];
                cu_real_t m_EQ_SQ  = m_EQ[grain]*m_EQ[grain];
                cu_real_t Internal_Exchange_field=0.0;
                if(Temperature[grain]<Tc[grain]){
                    Internal_Exchange_field = (1.0/(2.0*Chi_para[grain]))*(1.0-(m_MAG_SQ/(m_EQ_SQ)));
                }
                else{
                    Internal_Exchange_field = (-1.0/Chi_para[grain])*(1.0+((3.0*Tc[grain])/(5.0*(Temperature[grain]-Tc[grain])))*m_MAG_SQ);
                }
                Fieldx[grain] += Internal_Exchange_field*m_x[grain];
                Fieldy[grain] += Internal_Exchange_field*m_y[grain];
                Fieldz[grain] += Internal_Exchange_field*m_z[grain];
#if 0
                if(grain==0){
                    printf("GPU Hief: %20.15e\n",Internal_Exchange_field);
                }
#endif
            }
        }

        __global__ void update_exchange_field(const int *Num_Neighbours, const int *Neighbours,
                                const cu_real_t *H_exch_str, const cu_real_t *m_EQ,
                                const cu_real_t *m_x, const cu_real_t *m_y, const cu_real_t *m_z,
                                cu_real_t *Fieldx, cu_real_t *Fieldy, cu_real_t *Fieldz, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){

                /* I need to be able to determine the neighbours of each grain
                    * and then I need to access the magnetisation and equilibrium
                    * magnetisation of these neighbouring grains.
                    *
                    * The number of neighbours for each grain
                    * 0: 5
                    * A list of the neighbour IDs for each grain
                    * Use a zero padded list?
                    * 0: 1, 2, 3, 5 , 43, 99 ,-1 ...,-1 Length per grain = 20
                    * Loop until Num_neighbours
                    * Padding is used just to allow simple access to data
                    * A list of the Exch strengths between neighbour and grains
                    * I can then access the magnetisation values for the neighbouring grains.
                    */
                cu_real_t Exchange_x = 0.0;
                cu_real_t Exchange_y = 0.0;
                cu_real_t Exchange_z = 0.0;
                for(int neigh=0;neigh<Num_Neighbours[grain];neigh++){
                    int neigh_ID = Neighbours[grain*20+neigh];
                    Exchange_x += H_exch_str[grain*20+neigh]*m_x[neigh_ID]*m_EQ[neigh_ID]; // TODO: Check if I should be using m_x/m_l here
                    Exchange_y += H_exch_str[grain*20+neigh]*m_y[neigh_ID]*m_EQ[neigh_ID];
                    Exchange_z += H_exch_str[grain*20+neigh]*m_z[neigh_ID]*m_EQ[neigh_ID];
                }
                Fieldx[grain] += Exchange_x;
                Fieldy[grain] += Exchange_y;
                Fieldz[grain] += Exchange_z;
            }
        }

        __global__ void update_magnetostatic_field(){
            /* TODO: This*/
        }

        //################################ MISC ################################//
        __global__ void Calc_mEQ(const int *Layer, const layer_params_t *LayerParams, const cu_real_t *Temperature,
                                const cu_real_t *Tc, cu_real_t *m_EQ, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];
                double Tc_m_T_o_Tc=(Tc[grain]-Temperature[grain])/Tc[grain];
                switch(LayerParams[Lid].mEQtype)
                {
                case::Material_t::mEQTypes::Bulk:
                    if(Temperature[grain]<Tc[grain]){
                        m_EQ[grain] = pow(1.0-(Temperature[grain]/Tc[grain]),LayerParams[Lid].Crit_exp);
                    }
                    else{
                        m_EQ[grain] = 0.01;
                    }
                    break;
                case::Material_t::mEQTypes::Polynomial:
                    if(Temperature[grain]<Tc[grain]){
                        m_EQ[grain] = LayerParams[Lid].a0_mEQ
                                    + LayerParams[Lid].a1_2_mEQ * pow(Tc_m_T_o_Tc,0.5)
                                    + LayerParams[Lid].a1_mEQ * Tc_m_T_o_Tc
                                    + LayerParams[Lid].a2_mEQ * pow(Tc_m_T_o_Tc,2)
                                    + LayerParams[Lid].a3_mEQ * pow(Tc_m_T_o_Tc,3)
                                    + LayerParams[Lid].a4_mEQ * pow(Tc_m_T_o_Tc,4)
                                    + LayerParams[Lid].a5_mEQ * pow(Tc_m_T_o_Tc,5)
                                    + LayerParams[Lid].a6_mEQ * pow(Tc_m_T_o_Tc,6)
                                    + LayerParams[Lid].a7_mEQ * pow(Tc_m_T_o_Tc,7)
                                    + LayerParams[Lid].a8_mEQ * pow(Tc_m_T_o_Tc,8)
                                    + LayerParams[Lid].a9_mEQ * pow(Tc_m_T_o_Tc,9);
                    }
                    else{
                        m_EQ[grain] = 1.0/(1.0/LayerParams[Lid].a0_mEQ + LayerParams[Lid].b1_mEQ*((Temperature[grain]-Tc[grain])/Tc[grain]) + LayerParams[Lid].b2_mEQ*pow((Temperature[grain]-Tc[grain])/Tc[grain],2));
                    }
                    break;
                }
            }
        }

        __global__ void Calc_Damping(const int *Layer, const cu_real_t *Temperature, const cu_real_t *Tc,
                                    const layer_params_t *LayerParams, cu_real_t *Alpha_Para,
                                    cu_real_t *Alpha_Perp, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];

                if(Temperature[grain]<Tc[grain]){
                    Alpha_Para[grain] = LayerParams[Lid].BathCoupling*0.666666666666666*Temperature[grain]/Tc[grain];
                    Alpha_Perp[grain] = LayerParams[Lid].BathCoupling*(1.0-(Temperature[grain]/(3.0*Tc[grain])));
                }
                else{
                    Alpha_Para[grain] = LayerParams[Lid].BathCoupling*0.666666666666666*Temperature[grain]/Tc[grain];
                    Alpha_Perp[grain] = Alpha_Para[grain];
                }
            }
        }

        __global__ void Calc_Susceptibility(const cu_real_t *Temperature, const cu_real_t *Tc, const layer_params_t *LayerParams,
                                            const int *Layer, cu_real_t *Chi_para, cu_real_t *Chi_perp, const cu_real_t *m_EQ,
                                            const cu_real_t *chi_para_scaling, const cu_real_t *chi_perp_scaling, const size_t num_grains){

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];
                cu_real_t Tc1p68=0.0;
                switch (LayerParams[Lid].Susceptibility_Type)
                {
                case Material_t::SusTypes::Default:
                case Material_t::SusTypes::Kazantseva:
                    if(Temperature[grain]<Tc[grain]){
                        double Tc_m_T = Tc[grain]-Temperature[grain];
                        Chi_para[grain]  = LayerParams[Lid].t_a0_PARA * (Tc[grain]/(4.0*M_PI*Tc_m_T) )
                                                + LayerParams[Lid].t_a1_PARA * Tc_m_T
                                                + LayerParams[Lid].t_a2_PARA * pow(Tc_m_T,2)
                                                + LayerParams[Lid].t_a3_PARA * pow(Tc_m_T,3)
                                                + LayerParams[Lid].t_a4_PARA * pow(Tc_m_T,4)
                                                + LayerParams[Lid].t_a5_PARA * pow(Tc_m_T,5)
                                                + LayerParams[Lid].t_a6_PARA * pow(Tc_m_T,6)
                                                + LayerParams[Lid].t_a7_PARA * pow(Tc_m_T,7)
                                                + LayerParams[Lid].t_a8_PARA * pow(Tc_m_T,8)
                                                + LayerParams[Lid].t_a9_PARA * pow(Tc_m_T,9);
                    }
                    else{
                        Chi_para[grain] = LayerParams[Lid].t_b0_PARA * (Tc[grain]/(4.0*M_PI*(Temperature[grain]-Tc[grain])) );
                    }
                    // Chi_para must not be a negative value.
                    if(Chi_para[grain]<0){
                        Chi_para[grain] -= 1.1*Chi_para[grain];
                    }
                    Tc1p68=1.068*Tc[grain];
                    if(Temperature[grain]<Tc1p68){
                        double one_m_T_o_Tc1p68 = 1.0 - Temperature[grain]/Tc1p68;
                        Chi_perp[grain]  = LayerParams[Lid].t_a0_PERP
                                                + LayerParams[Lid].t_a1_PERP * one_m_T_o_Tc1p68
                                                + LayerParams[Lid].t_a2_PERP * pow(one_m_T_o_Tc1p68,2)
                                                + LayerParams[Lid].t_a3_PERP * pow(one_m_T_o_Tc1p68,3)
                                                + LayerParams[Lid].t_a4_PERP * pow(one_m_T_o_Tc1p68,4)
                                                + LayerParams[Lid].t_a5_PERP * pow(one_m_T_o_Tc1p68,5)
                                                + LayerParams[Lid].t_a6_PERP * pow(one_m_T_o_Tc1p68,6)
                                                + LayerParams[Lid].t_a7_PERP * pow(one_m_T_o_Tc1p68,7)
                                                + LayerParams[Lid].t_a8_PERP * pow(one_m_T_o_Tc1p68,8)
                                                + LayerParams[Lid].t_a9_PERP * pow(one_m_T_o_Tc1p68,9);
                    }
                    else{
                        Chi_perp[grain]= LayerParams[Lid].t_b0_PERP * (Tc[grain]/ (4.0*M_PI*(Temperature[grain]-Tc[grain])) );
                    }
                    Chi_para[grain] *= 1.000e-4*chi_para_scaling[grain];
                    Chi_perp[grain] *= 1.000e-4*chi_perp_scaling[grain];
                    break;
                case Material_t::SusTypes::Vogler:
                    if(Temperature[grain]<Tc[grain]){
                        Chi_para[grain]  = LayerParams[Lid].t_a0_PARA/(Tc[grain]-Temperature[grain]);
                        Chi_perp[grain]  = LayerParams[Lid].t_a0_PERP
                                                + LayerParams[Lid].t_a1_PERP*m_EQ[grain]
                                                + LayerParams[Lid].t_a2_PERP*pow(m_EQ[grain],2.0)
                                                + LayerParams[Lid].t_a3_PERP*pow(m_EQ[grain],3.0)
                                                + LayerParams[Lid].t_a4_PERP*pow(m_EQ[grain],4.0);
                    }
                    else{
                        Chi_para[grain] = LayerParams[Lid].t_b0_PARA/(Temperature[grain]-Tc[grain]);
                        Chi_perp[grain] = LayerParams[Lid].t_b0_PERP/(Temperature[grain]-Tc[grain]);
                    }
                    Chi_para[grain] *= 1e-4 * chi_para_scaling[grain];
                    Chi_perp[grain] *= 1e-4 * chi_perp_scaling[grain];
                    break;
                case Material_t::SusTypes::Inverse:
                    if(Temperature[grain]<Tc[grain]){
                        cu_real_t Tc_m_T_o_Tc = (Tc[grain]-Temperature[grain])/Tc[grain];
                        Chi_para[grain] = LayerParams[Lid].t_a0_PARA
                                        + LayerParams[Lid].t_a1_PARA * Tc_m_T_o_Tc
                                        + LayerParams[Lid].t_a2_PARA * pow(Tc_m_T_o_Tc,2.0)
                                        + LayerParams[Lid].t_a3_PARA * pow(Tc_m_T_o_Tc,3.0)
                                        + LayerParams[Lid].t_a4_PARA * pow(Tc_m_T_o_Tc,4.0)
                                        + LayerParams[Lid].t_a5_PARA * pow(Tc_m_T_o_Tc,5.0)
                                        + LayerParams[Lid].t_a6_PARA * pow(Tc_m_T_o_Tc,6.0)
                                        + LayerParams[Lid].t_a7_PARA * pow(Tc_m_T_o_Tc,7.0)
                                        + LayerParams[Lid].t_a8_PARA * pow(Tc_m_T_o_Tc,8.0)
                                        + LayerParams[Lid].t_a9_PARA * pow(Tc_m_T_o_Tc,9.0)
                                        + LayerParams[Lid].t_a1_2_PARA * pow(Tc_m_T_o_Tc,0.5);

                        Chi_perp[grain] = LayerParams[Lid].t_a0_PERP
                                        + LayerParams[Lid].t_a1_PERP * Tc_m_T_o_Tc
                                        + LayerParams[Lid].t_a2_PERP * pow(Tc_m_T_o_Tc,2.0)
                                        + LayerParams[Lid].t_a3_PERP * pow(Tc_m_T_o_Tc,3.0)
                                        + LayerParams[Lid].t_a4_PERP * pow(Tc_m_T_o_Tc,4.0)
                                        + LayerParams[Lid].t_a5_PERP * pow(Tc_m_T_o_Tc,5.0)
                                        + LayerParams[Lid].t_a6_PERP * pow(Tc_m_T_o_Tc,6.0)
                                        + LayerParams[Lid].t_a7_PERP * pow(Tc_m_T_o_Tc,7.0)
                                        + LayerParams[Lid].t_a8_PERP * pow(Tc_m_T_o_Tc,8.0)
                                        + LayerParams[Lid].t_a9_PERP * pow(Tc_m_T_o_Tc,9.0)
                                        + LayerParams[Lid].t_a1_2_PERP * pow(Tc_m_T_o_Tc,0.5);
                    }
                    else{
                        cu_real_t T_m_Tc_o_Tc = (Temperature[grain]-Tc[grain])/Tc[grain];
                        Chi_para[grain]  = LayerParams[Lid].t_b0_PARA
                                        + LayerParams[Lid].t_b1_PARA * T_m_Tc_o_Tc
                                        + LayerParams[Lid].t_b2_PARA * pow(T_m_Tc_o_Tc,2.0)
                                        + LayerParams[Lid].t_b3_PARA * pow(T_m_Tc_o_Tc,3.0)
                                        + LayerParams[Lid].t_b4_PARA * pow(T_m_Tc_o_Tc,4.0);

                        Chi_perp[grain]  = LayerParams[Lid].t_b0_PERP
                                        + LayerParams[Lid].t_b1_PERP * T_m_Tc_o_Tc
                                        + LayerParams[Lid].t_b2_PERP * pow(T_m_Tc_o_Tc,2.0)
                                        + LayerParams[Lid].t_b3_PERP * pow(T_m_Tc_o_Tc,3.0)
                                        + LayerParams[Lid].t_b4_PERP * pow(T_m_Tc_o_Tc,4.0);
                    }
                    Chi_para[grain] = 1.0/Chi_para[grain];
                    Chi_perp[grain] = 1.0/Chi_perp[grain];
                    Chi_para[grain] *= 1.000e-4 * chi_para_scaling[grain];
                    Chi_perp[grain] *= 1.000e-4 * chi_perp_scaling[grain];
                    if(Chi_perp[grain]<0.0){Chi_perp[grain]=0.0;}
                    break;
                }
            }
        }

        __global__ void Calc_Tdependence(const int *Layer, const layer_params_t *LayerParams, const cu_real_t *Temperature, const cu_real_t *m_EQ,
                                         const cu_real_t *K0K, const cu_real_t *Ms0K, cu_real_t *K, cu_real_t *Ms, const size_t num_grains){

            // Here we will calculate Ms(t) and K(t) to be stored on the device.

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];
                int Ani_method = LayerParams[Lid].Anisotropy_method;

                switch(Ani_method)
                {
                    case 1: // Callen-Callen
                        if(Temperature[grain] <= LayerParams[Lid].Callen_range_lowT){
                            K[grain] = K0K[grain] * LayerParams[Lid].Callen_factor_lowT  * pow(m_EQ[grain],LayerParams[Lid].Callen_power_lowT);
                        }
                        else if(Temperature[grain] > LayerParams[Lid].Callen_range_midT){
                            K[grain] = K0K[grain] * LayerParams[Lid].Callen_factor_highT * pow(m_EQ[grain],LayerParams[Lid].Callen_power_highT);
                        }
                        else{
                            K[grain] = K0K[grain] * LayerParams[Lid].Callen_factor_midT  * pow(m_EQ[grain],LayerParams[Lid].Callen_power_midT);
                        }
                        break;
                    case 2:
                        break;
                }

                Ms[grain] = Ms0K[grain] * m_EQ[grain];

            }
        }

        #endif
    } // namespace kernels
} // namespace cuda