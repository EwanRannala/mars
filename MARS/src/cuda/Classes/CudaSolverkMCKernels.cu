#include "cuda/Classes/CudaSolverKernels.cuh"
#include <stdio.h>
namespace cuda
{
    namespace kernels
    {
        #ifdef CUDA
        __device__ constexpr double PP = (1.0/3.0);
        __device__ constexpr double ANGTOL = 1.0e-4;
        __device__ constexpr double INFTY = 1.0e10;
        __device__ constexpr double FERRO_EBCUTOFF = 500.0;

        __global__ void kMC_core(const int* Layer, const layer_params_t *LayerParams,
                                 const cu_real_t *EA_x, const cu_real_t *EA_y, const cu_real_t *EA_z,
                                 const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                 const cu_real_t *K, const cu_real_t *Vol, const cu_real_t *Temperature, const cu_real_t *Ms,
                                 curandState *rand_states, cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                 const cu_real_t dt, const size_t num_grains){

            size_t tid = blockIdx.x * blockDim.x + threadIdx.x;
            for(size_t grain=tid; grain<num_grains; grain+= blockDim.x*gridDim.x){

                size_t Lid = Layer[grain];

                curandState local_state = rand_states[tid];

                cu_real_t KV_over_kBT = K[grain]*Vol[grain]*1e-21/(1.38064900000000e-16*Temperature[grain]);
                cu_real_t MsVHk_over_kBT = (Ms[grain]*Vol[grain]*1e-21*(2.0*K[grain]/Ms[grain]))/(1.38064900000000e-16*Temperature[grain]);

                cu_real_t eax = EA_x[grain];
                cu_real_t eay = EA_y[grain];
                cu_real_t eaz = EA_z[grain];

                cu_real_t mx = m_x[grain];
                cu_real_t my = m_y[grain];
                cu_real_t mz = m_z[grain];

                cu_real_t hx = Fieldx[grain]/(2.0*K[grain]/Ms[grain]);
                cu_real_t hy = Fieldy[grain]/(2.0*K[grain]/Ms[grain]);
                cu_real_t hz = Fieldz[grain]/(2.0*K[grain]/Ms[grain]);

                cu_real_t vecmag = eax*mx + eay*my + eaz*mz;
                int diro = -1;
                if(vecmag >= 0.0){diro=1;}

                cu_real_t ht=0.0;

                cu_real_t htotx=0.0;
                cu_real_t htoty=0.0;
                cu_real_t htotz=0.0;

                cu_real_t snewx=0.0;
                cu_real_t snewy=0.0;
                cu_real_t snewz=0.0;

                cu_real_t sdnx=0.0;
                cu_real_t sdny=0.0;
                cu_real_t sdnz=0.0;

                cu_real_t supx=0.0;
                cu_real_t supy=0.0;
                cu_real_t supz=0.0;

                //------------------------------The "up" state------------------------------//
                mx=eax;
                my=eay;
                mz=eaz;

                cu_real_t err_abs = INFTY;
                while(err_abs > ANGTOL){
                    vecmag = eax*mx + eay*my + eaz*mz;
                    htotx = vecmag*eax+hx;
                    htoty = vecmag*eay+hy;
                    htotz = vecmag*eaz+hz;
                    #ifdef CUDA_DP
                        ht =  norm3d(htotx,htoty,htotz);
                    #else
                        ht =  norm3df(htotx,htoty,htotz);
                    #endif
                    if(ht>0.0){
                        snewx=htotx/ht;
                        snewy=htoty/ht;
                        snewz=htotz/ht;
                    }
                    else{
                        snewx = mx;
                        snewy = my;
                        snewz = mz;
                    }
                    #ifdef CUDA_DP
                        err_abs = norm3d((snewx-mx),(snewy-my),(snewz-mz));
                    #else
                        err_abs = norm3df((snewx-mx),(snewy-my),(snewz-mz));
                    #endif
                    mx=snewx;
                    my=snewy;
                    mz=snewz;
                }
                supx=mx;
                supy=my;
                supz=mz;

                //-----------------------------The "down" state-----------------------------//
                mx=-eax;
                my=-eay;
                mz=-eaz;

                err_abs = INFTY;
                while(err_abs > ANGTOL){
                    vecmag = eax*mx + eay*my + eaz*mz;
                    htotx = vecmag*eax+hx;
                    htoty = vecmag*eay+hy;
                    htotz = vecmag*eaz+hz;
                    #ifdef CUDA_DP
                        ht = norm3d(htotx,htoty,htotz);
                    #else
                        ht = norm3df(htotx,htoty,htotz);
                    #endif
                    if(ht>0.0){
                        snewx=htotx/ht;
                        snewy=htoty/ht;
                        snewz=htotz/ht;
                    }
                    else{
                        snewx = mx;
                        snewy = my;
                        snewz = mz;
                    }
                    #ifdef CUDA_DP
                        err_abs = norm3d((snewx-mx),(snewy-my),(snewz-mz));
                    #else
                        err_abs = norm3df((snewx-mx),(snewy-my),(snewz-mz));
                    #endif
                    mx=snewx;
                    my=snewy;
                    mz=snewz;
                }
                sdnx=mx;
                sdny=my;
                sdnz=mz;
                //==========================================================================//

                // Calculate the energy barrier for reversal against the field.
                // First set the direction to the old value.
                #ifdef CUDA_DP
                    double hnorm=norm3d(hx,hy,hz);
                #else
                    float hnorm=norm3df(hx,hy,hz);
                #endif
                cu_real_t hux;
                cu_real_t huy;
                cu_real_t huz;
                if(hnorm != 0.0){
                    hux = hx/hnorm;
                    huy = hy/hnorm;
                    huz = hz/hnorm;
                }
                else{
                    hux = 0.0;
                    huy = 0.0;
                    huz = 0.0;
                }

                int dirn = diro;
                cu_real_t coshe = hux*eax+huy*eay+huz*eaz;
                cu_real_t pp1 = pow(coshe*coshe,PP);
                cu_real_t pp2 = pow(1.0-coshe*coshe,PP);
                cu_real_t hebsy = pow(pp1+pp2,-1.5);
                cu_real_t gebsy = 0.86+1.14*hebsy;
                cu_real_t hcon = hnorm/hebsy;
                cu_real_t de=0.0;
                cu_real_t eb=0.0;
                cu_real_t dele=0.0;
                cu_real_t ebdu=0.0;
                cu_real_t ebud=0.0;
                cu_real_t eplust=0.0;
                cu_real_t eminust=0.0;
                cu_real_t cosang=0.0;
                cu_real_t fielddirn=0.0;

                if(hcon >= 1.0){
                    de = 0.0;
                }
                else{
                    eb = pow(1.0-hcon, gebsy);
                    de = KV_over_kBT*eb;
                }

                // energy of up state
                eplust = -MsVHk_over_kBT*(supx*hx+supy*hy+supz*hz);
                cosang = eax*supx+eay*supy+eaz*supz;
                eplust = eplust+KV_over_kBT*(1.0-cosang*cosang);

                // energy of down state
                eminust = -MsVHk_over_kBT*(sdnx*hx+sdny*hy+sdnz*hz);
                cosang = eax*sdnx+eay*sdny+eaz*sdnz;
                eminust = eminust+KV_over_kBT*(1.0-cosang*cosang);

                dele = eminust-eplust;

                fielddirn = hx*eax+hy*eay+hz*eaz;
                if(fielddirn > 0.0){
                    ebdu = de;
                    ebud = de+dele;
                }
                else{
                    ebud = de;
                    ebdu = de-dele;
                }

                // If outside of the SW-asteroid...
                if(hcon >= 1.0){
                    if(fielddirn > 0.0){
                        dirn = 1;
                    }
                    else{
                        dirn = -1;
                    }

                    // Assign new moment direction
                    if(dirn < 0){
                        mx=sdnx;
                        my=sdny;
                        mz=sdnz;
                    }
                    else{
                        mx=supx;
                        my=supy;
                        mz=supz;
                    }
                }
                else{
                    // calculation of relaxation times
                    double towiv=0.0;
                    double towud=0.0;
                    double ttot=0.0;
                    double xvar=0.0;
                    double prot=0.0;
                    double test = 0.0;
                    if(fabs(ebud) > FERRO_EBCUTOFF){
                        towiv = 0.0;
                        if(ebud < 0.0){test=1.0;}
                    }
                    else{towiv = LayerParams[Lid].f0*exp(-ebud);}

                    if(fabs(ebdu) > FERRO_EBCUTOFF){
                        towud = 0.0;
                        if(ebdu < 0.0){test = 1.0;}
                    }
                    else{towud = LayerParams[Lid].f0*exp(-ebdu);}

                    ttot = towiv+towud;
                    prot = 1.0-exp(-dt*ttot);
                    if(test > 0.5){prot = 1.0;}
                    cu_real_t ran=0.0;
                    #ifdef CUDA_DP
                        ran = curand_uniform_double(&local_state);
                    #else
                        ran = curand_uniform(&local_state);
                    #endif
                    rand_states[tid] = local_state;
                    if(prot > ran){
                        dirn = 1;
                        #ifdef CUDA_DP
                            ran = curand_uniform_double(&local_state);
                        #else
                            ran = curand_uniform(&local_state);
                        #endif
                        rand_states[tid] = local_state;
                        if(dele > FERRO_EBCUTOFF){xvar = 0.0;}
                        else{
                            if(dele < -FERRO_EBCUTOFF){xvar = 1.0;}
                            else{
                                xvar = exp(-dele);
                                xvar = xvar/(xvar+1.0);
                            }
                        }
                        if(xvar > ran){dirn = -1;}
                    }
                    // Assign new moment direction
                    if(dirn > 0){
                        mx=supx;
                        my=supy;
                        mz=supz;
                    }
                    else{
                        mx=sdnx;
                        my=sdny;
                        mz=sdnz;
                    }
                }

                m_x[grain] = mx;
                m_y[grain] = my;
                m_z[grain] = mz;

            }
        }

        #endif
    } // namespace kernels
} // namespace cuda