#include <optional>
#include <random>

#include "cuda/Classes/CudaSolver.cuh"
#include "cuda/Classes/CudaSolverKernels.cuh"
#include "cuda/Error_check.cuh"

// TODO: Implement CUDA error checking

namespace cuda{

    #ifdef CUDA

    CudaSolver_t::CudaSolver_t(const ConfigFile& cfg, const std::vector<ConfigFile>& Materials_config, const Interaction_t& Int_system)
        : SolverType(Solvers::LLB)
        , T_dep(false)
        , block_size(256)
        , Initialised(false)
        , dt_kMC(1.0e-3)
        , dt_LLG(1.0e-13)
        , dt_LLB(1.0e-15)
    {
        Cuda_initialise();

        // Get desired solver type
        std::string _SolverType = cfg.getValueOfKey<std::string>("Solver:solver_selection");
        if(_SolverType != "kmc" && _SolverType != "llg" && _SolverType != "llb" /*&& _SolverType != "dynamic"*/){
            Log.log(Logger_t::LogLvl::ERR, "CFG error: Solver selection invalid.");
        }
        if(_SolverType == "kmc"){SolverType = Solvers::kMC;}
        else if(_SolverType == "llg"){SolverType = Solvers::LLG;}
        else if(_SolverType == "llb"){SolverType = Solvers::LLB;}

        // Check for specified solver SEED
        std::optional<int> _seed = cfg.getValueOfOptKey<int>("Solver:SEED");
        if(_seed.has_value()){SEED = _seed.value();}
        else{SEED = cfg.getValueOfKey<int>("Sim:SEED");}

        // Set the time step for the desired solver
        switch(SolverType)
        {
        case Solvers::kMC:
            dt_kMC = cfg.getValAndUnitOfKey<cu_real_t>("kMC:dt");
            break;
        case Solvers::LLG:
            dt_LLG = cfg.getValAndUnitOfKey<cu_real_t>("LLG:dt");
            break;
        case Solvers::LLB:
            dt_LLB = cfg.getValAndUnitOfKey<cu_real_t>("LLB:dt");
            break;
        }
        if(SolverType != Solvers::LLB){
            T_dep = cfg.getValueOfKey<bool>("Solver:t_dependent_variables");
        }

        hst_LayerParams.resize(Materials_config.size());
        for(size_t L=0; L<Materials_config.size(); ++L){
            std::optional<cu_real_t> _f0 = Materials_config[L].getValueOfOptKey<cu_real_t>("Mat:F0");
            if(_f0.has_value()){
                hst_LayerParams[L].f0 = _f0.value();
            }
            else{
                hst_LayerParams[L].f0 = 9.5e10;
            }
            hst_LayerParams[L].Gamma = 1.760859644e+7;
            hst_LayerParams[L].BathCoupling = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Alpha");

            std::string SusType = Materials_config[L].getValueOfKey<std::string>("Mat:Susceptibility_type");
            if (SusType != "default" && SusType != "kazantseva_susceptibility_fit" && SusType != "vogler_susceptibility_fit" && SusType != "inv_susceptibility_fit"){
                Log.log(Logger_t::LogLvl::ERR, "CFG error: Susceptibility type invalid.");
            }
            if(SusType == "default"){
                hst_LayerParams[L].Susceptibility_Type = Material_t::SusTypes::Default;
                hst_LayerParams[L].Chi_scaling_factor = 9.54393845712027;
                hst_LayerParams[L].t_a0_PARA = 1.21e-3;
                hst_LayerParams[L].t_a1_PARA = -2.2e-7;
                hst_LayerParams[L].t_a2_PARA = 0.000;
                hst_LayerParams[L].t_a3_PARA = 1.95e-13;
                hst_LayerParams[L].t_a4_PARA = -1.3e-17;
                hst_LayerParams[L].t_a5_PARA = 0.000;
                hst_LayerParams[L].t_a6_PARA = -4.00e-23;
                hst_LayerParams[L].t_a7_PARA = 0.000;
                hst_LayerParams[L].t_a8_PARA = 0.000;
                hst_LayerParams[L].t_a9_PARA = -6.51e-32;
                hst_LayerParams[L].t_a1_2_PARA = 0.000;
                hst_LayerParams[L].t_b0_PARA = 2.12e-3;
                hst_LayerParams[L].t_b1_PARA = 0.000;
                hst_LayerParams[L].t_b2_PARA = 0.000;
                hst_LayerParams[L].t_b3_PARA = 0.000;
                hst_LayerParams[L].t_b4_PARA = 0.000;

                hst_LayerParams[L].t_a0_PERP = 2.11e-3;
                hst_LayerParams[L].t_a1_PERP = 1.10e-1;
                hst_LayerParams[L].t_a2_PERP = -8.55e-1;
                hst_LayerParams[L].t_a3_PERP = 3.42;
                hst_LayerParams[L].t_a4_PERP = -7.85;
                hst_LayerParams[L].t_a5_PERP = 1.03e1;
                hst_LayerParams[L].t_a6_PERP = -6.86e-1;
                hst_LayerParams[L].t_a7_PERP = 7.97e-1;
                hst_LayerParams[L].t_a8_PERP = 1.54;
                hst_LayerParams[L].t_a9_PERP = -6.27e-1;
                hst_LayerParams[L].t_a1_2_PERP = 0.000;
                hst_LayerParams[L].t_b0_PERP = 4.85e-3;
                hst_LayerParams[L].t_b1_PERP = 0.000;
                hst_LayerParams[L].t_b2_PERP = 0.000;
                hst_LayerParams[L].t_b3_PERP = 0.000;
                hst_LayerParams[L].t_b4_PERP = 0.000;
            }
            else if (SusType == "kazantseva_susceptibility_fit"){
                hst_LayerParams[L].Susceptibility_Type = Material_t::SusTypes::Kazantseva;
                hst_LayerParams[L].Chi_scaling_factor = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Susceptibility_factor");
                hst_LayerParams[L].t_a0_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_PARA");
                hst_LayerParams[L].t_a1_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_PARA");
                hst_LayerParams[L].t_a2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_PARA");
                hst_LayerParams[L].t_a3_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_PARA");
                hst_LayerParams[L].t_a4_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_PARA");
                hst_LayerParams[L].t_a5_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_PARA");
                hst_LayerParams[L].t_a6_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_PARA");
                hst_LayerParams[L].t_a7_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_PARA");
                hst_LayerParams[L].t_a8_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_PARA");
                hst_LayerParams[L].t_a9_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_PARA");
                hst_LayerParams[L].t_a1_2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_PARA");
                hst_LayerParams[L].t_b0_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b0_PARA");
                hst_LayerParams[L].t_b1_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_PARA");
                hst_LayerParams[L].t_b2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_PARA");
                hst_LayerParams[L].t_b3_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b3_PARA");
                hst_LayerParams[L].t_b4_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b4_PARA");

                hst_LayerParams[L].t_a0_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_PERP");
                hst_LayerParams[L].t_a1_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_PERP");
                hst_LayerParams[L].t_a2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_PERP");
                hst_LayerParams[L].t_a3_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_PERP");
                hst_LayerParams[L].t_a4_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_PERP");
                hst_LayerParams[L].t_a5_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_PERP");
                hst_LayerParams[L].t_a6_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_PERP");
                hst_LayerParams[L].t_a7_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_PERP");
                hst_LayerParams[L].t_a8_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_PERP");
                hst_LayerParams[L].t_a9_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_PERP");
                hst_LayerParams[L].t_a1_2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_PERP");
                hst_LayerParams[L].t_b0_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b0_PERP");
                hst_LayerParams[L].t_b1_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_PERP");
                hst_LayerParams[L].t_b2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_PERP");
                hst_LayerParams[L].t_b3_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b3_PERP");
                hst_LayerParams[L].t_b4_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b4_PERP");
            }
            else if (SusType =="vogler_susceptibility_fit"){
                hst_LayerParams[L].Susceptibility_Type = Material_t::SusTypes::Vogler;
                hst_LayerParams[L].Chi_scaling_factor = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Susceptibility_factor");
                hst_LayerParams[L].t_a0_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_PARA");
                hst_LayerParams[L].t_a1_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_PARA");
                hst_LayerParams[L].t_a2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_PARA");
                hst_LayerParams[L].t_a3_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_PARA");
                hst_LayerParams[L].t_a4_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_PARA");
                hst_LayerParams[L].t_a5_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_PARA");
                hst_LayerParams[L].t_a6_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_PARA");
                hst_LayerParams[L].t_a7_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_PARA");
                hst_LayerParams[L].t_a8_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_PARA");
                hst_LayerParams[L].t_a9_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_PARA");
                hst_LayerParams[L].t_a1_2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_PARA");
                hst_LayerParams[L].t_b0_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b0_PARA");
                hst_LayerParams[L].t_b1_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_PARA");
                hst_LayerParams[L].t_b2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_PARA");
                hst_LayerParams[L].t_b3_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b3_PARA");
                hst_LayerParams[L].t_b4_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b4_PARA");

                hst_LayerParams[L].t_a0_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_PERP");
                hst_LayerParams[L].t_a1_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_PERP");
                hst_LayerParams[L].t_a2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_PERP");
                hst_LayerParams[L].t_a3_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_PERP");
                hst_LayerParams[L].t_a4_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_PERP");
                hst_LayerParams[L].t_a5_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_PERP");
                hst_LayerParams[L].t_a6_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_PERP");
                hst_LayerParams[L].t_a7_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_PERP");
                hst_LayerParams[L].t_a8_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_PERP");
                hst_LayerParams[L].t_a9_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_PERP");
                hst_LayerParams[L].t_a1_2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_PERP");
                hst_LayerParams[L].t_b0_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b0_PERP");
                hst_LayerParams[L].t_b1_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_PERP");
                hst_LayerParams[L].t_b2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_PERP");
                hst_LayerParams[L].t_b3_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b3_PERP");
                hst_LayerParams[L].t_b4_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b4_PERP");
            }
            else{
                hst_LayerParams[L].Susceptibility_Type = Material_t::SusTypes::Inverse;
                hst_LayerParams[L].Chi_scaling_factor = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Susceptibility_factor");
                hst_LayerParams[L].t_a0_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_PARA");
                hst_LayerParams[L].t_a1_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_PARA");
                hst_LayerParams[L].t_a2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_PARA");
                hst_LayerParams[L].t_a3_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_PARA");
                hst_LayerParams[L].t_a4_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_PARA");
                hst_LayerParams[L].t_a5_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_PARA");
                hst_LayerParams[L].t_a6_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_PARA");
                hst_LayerParams[L].t_a7_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_PARA");
                hst_LayerParams[L].t_a8_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_PARA");
                hst_LayerParams[L].t_a9_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_PARA");
                hst_LayerParams[L].t_a1_2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_PARA");
                hst_LayerParams[L].t_b0_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b0_PARA");
                hst_LayerParams[L].t_b1_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_PARA");
                hst_LayerParams[L].t_b2_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_PARA");
                hst_LayerParams[L].t_b3_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b3_PARA");
                hst_LayerParams[L].t_b4_PARA = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b4_PARA");

                hst_LayerParams[L].t_a0_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_PERP");
                hst_LayerParams[L].t_a1_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_PERP");
                hst_LayerParams[L].t_a2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_PERP");
                hst_LayerParams[L].t_a3_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_PERP");
                hst_LayerParams[L].t_a4_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_PERP");
                hst_LayerParams[L].t_a5_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_PERP");
                hst_LayerParams[L].t_a6_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_PERP");
                hst_LayerParams[L].t_a7_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_PERP");
                hst_LayerParams[L].t_a8_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_PERP");
                hst_LayerParams[L].t_a9_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_PERP");
                hst_LayerParams[L].t_a1_2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_PERP");
                hst_LayerParams[L].t_b0_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b0_PERP");
                hst_LayerParams[L].t_b1_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_PERP");
                hst_LayerParams[L].t_b2_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_PERP");
                hst_LayerParams[L].t_b3_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b3_PERP");
                hst_LayerParams[L].t_b4_PERP = Materials_config[L].getValueOfKey<cu_real_t>("Mat:b4_PERP");
            }

            std::string AniType =  Materials_config[L].getValueOfKey<std::string>("Mat:ani_method");
            if(AniType != "callen" && AniType != "chi"){
                Log.log(Logger_t::LogLvl::ERR, "CFG error: Anisotropy type invalid.");
            }
            if(AniType == "callen"){
                hst_LayerParams[L].Anisotropy_method = Material_t::AniMethods::Callen;
                std::string Calstr = Materials_config[L].getValueOfKey<std::string>("Mat:Callen_power_range");
                if(Calstr != "single" && Calstr != "multiple"){
                    Log.log(Logger_t::LogLvl::ERR, "CFG error: Callen range type invalid.");
                }
                else if(Calstr=="single"){
                    hst_LayerParams[L].Callen_factor_lowT = 1.000;
                    hst_LayerParams[L].Callen_factor_midT = 1.000;
                    hst_LayerParams[L].Callen_factor_highT = 1.000;

                    hst_LayerParams[L].Callen_power_lowT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_power");
                    hst_LayerParams[L].Callen_power_midT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_power");
                    hst_LayerParams[L].Callen_power_highT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_power");

                    hst_LayerParams[L].Callen_range_lowT = 1600.0;
                    hst_LayerParams[L].Callen_range_midT = 2000.0;
                }
                else{
                    hst_LayerParams[L].Callen_factor_lowT  = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_factor_lowT");
                    hst_LayerParams[L].Callen_factor_midT  = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_factor_midT");
                    hst_LayerParams[L].Callen_factor_highT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_factor_highT");

                    hst_LayerParams[L].Callen_power_lowT  = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_power_lowT");
                    hst_LayerParams[L].Callen_power_midT  = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_power_midT");
                    hst_LayerParams[L].Callen_power_highT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_power_highT");

                    hst_LayerParams[L].Callen_range_lowT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_range_lowT");
                    hst_LayerParams[L].Callen_range_midT = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Callen_range_midT");
                }
            }
            else{
                hst_LayerParams[L].Anisotropy_method = Material_t::AniMethods::Chi;
            }

            std::string mEQstr = Materials_config[L].getValueOfKey<std::string>("Mat:mEQ_type");
            if(mEQstr!="bulk" && mEQstr!="polynomial"){
                Log.log(Logger_t::LogLvl::ERR, "CFG error: mEQ type invalid.");
            }
            else if(mEQstr=="bulk"){
                hst_LayerParams[L].mEQtype = Material_t::mEQTypes::Bulk;
                hst_LayerParams[L].Crit_exp = Materials_config[L].getValueOfKey<cu_real_t>("Mat:Critical_Exponent");
                hst_LayerParams[L].a0_mEQ =   0.0;
                hst_LayerParams[L].a1_mEQ =   0.0;
                hst_LayerParams[L].a2_mEQ =   0.0;
                hst_LayerParams[L].a3_mEQ =   0.0;
                hst_LayerParams[L].a4_mEQ =   0.0;
                hst_LayerParams[L].a5_mEQ =   0.0;
                hst_LayerParams[L].a6_mEQ =   0.0;
                hst_LayerParams[L].a7_mEQ =   0.0;
                hst_LayerParams[L].a8_mEQ =   0.0;
                hst_LayerParams[L].a9_mEQ =   0.0;
                hst_LayerParams[L].a1_2_mEQ = 0.0;
                hst_LayerParams[L].b1_mEQ =   0.0;
                hst_LayerParams[L].b2_mEQ =   0.0;
            }
            else{ // Polynomial
                hst_LayerParams[L].mEQtype = Material_t::mEQTypes::Polynomial;
                hst_LayerParams[L].Crit_exp = 0.0;
                hst_LayerParams[L].a0_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a0_mEQ");
                hst_LayerParams[L].a1_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_mEQ");
                hst_LayerParams[L].a2_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a2_mEQ");
                hst_LayerParams[L].a3_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a3_mEQ");
                hst_LayerParams[L].a4_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a4_mEQ");
                hst_LayerParams[L].a5_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a5_mEQ");
                hst_LayerParams[L].a6_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a6_mEQ");
                hst_LayerParams[L].a7_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a7_mEQ");
                hst_LayerParams[L].a8_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a8_mEQ");
                hst_LayerParams[L].a9_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:a9_mEQ");
                hst_LayerParams[L].a1_2_mEQ = Materials_config[L].getValueOfKey<cu_real_t>("Mat:a1_2_mEQ");
                hst_LayerParams[L].b1_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:b1_mEQ");
                hst_LayerParams[L].b2_mEQ =   Materials_config[L].getValueOfKey<cu_real_t>("Mat:b2_mEQ");
            }
        }

        // This function allocates and transfers the data to the GPU
        Set_Susceptibility_params(Materials_config);

        // This function allocates and transfers the data to the GPU
        Set_Exchange_params(Int_system);

        Cuda_allocate();
        Log.logandshow(Logger_t::LogLvl::INFO, "CUDA: Initialisation done");
    }

    CudaSolver_t::~CudaSolver_t(){
        cudaFree(dev_LayerParams);
        cudaFree(dev_Num_Neighbours);
        cudaFree(dev_Neighbours);
        cudaFree(dev_H_exch_str);
        cudaFree(dev_H_ani);
        cudaFree(dev_EA_x);
        cudaFree(dev_EA_y);
        cudaFree(dev_EA_z);
        cudaFree(dev_mEQ);
        cudaFree(dev_m_x);
        cudaFree(dev_m_y);
        cudaFree(dev_m_z);
        cudaFree(dev_m_bufferx);
        cudaFree(dev_m_buffery);
        cudaFree(dev_m_bufferz);
        cudaFree(dev_dmx);
        cudaFree(dev_dmy);
        cudaFree(dev_dmz);
        cudaFree(dev_H_appx);
        cudaFree(dev_H_appy);
        cudaFree(dev_H_appz);
        cudaFree(dev_Fieldx);
        cudaFree(dev_Fieldy);
        cudaFree(dev_Fieldz);
        cudaFree(dev_Alpha_Para);
        cudaFree(dev_Alpha_Perp);
        cudaFree(dev_Chi_para);
        cudaFree(dev_Chi_perp);
        cudaFree(dev_K);
        cudaFree(dev_K_store);
        cudaFree(dev_Ms);
        cudaFree(dev_Ms_store);
        cudaFree(dev_Vol);
        cudaFree(dev_Temperature);
        cudaFree(dev_Tc);
        cudaFree(dev_RNG_PARAx);
        cudaFree(dev_RNG_PARAy);
        cudaFree(dev_RNG_PARAz);
        cudaFree(dev_RNG_PERPx);
        cudaFree(dev_RNG_PERPy);
        cudaFree(dev_RNG_PERPz);
        cudaFree(dev_chi_para_scaling_factor);
        cudaFree(dev_chi_perp_scaling_factor);
        cudaFree(dev_LayerID);
        cudaFree(dev_rand_states);
    }

    void CudaSolver_t::Cuda_initialise(){
        Log.logandshow(Logger_t::LogLvl::INFO, "CUDA: Initialising...");
        Num_Layers = Sim::NumLayers;
        Num_grains = Sim::NumLayers*Sim::GrainsperLayer;
        grid_size  = ( (Num_grains/2) / block_size) + 1;

        // Check if there is a compatible device
        int n_devices;
        CHECK_CUDA_ERROR(cudaGetDeviceCount(&n_devices));
        cudaError_t error = cudaGetLastError();

        if(error == cudaErrorNoDevice){
            Log.log(Logger_t::LogLvl::ERR, "CUDA error: No compatible devices are available.");
        }
        else if (error == cudaErrorInsufficientDriver){
            Log.log(Logger_t::LogLvl::ERR, "CUDA error: Incompatible drivers. Please update drivers.");
        }
        else if (error != cudaSuccess){
            Log.log(Logger_t::LogLvl::ERR, "CUDA error: Unable to query devices.");
        }
        Log.logandshow(Logger_t::LogLvl::INFO, "CUDA: "+to_string_exact(n_devices)+" devices found.");

        int device;
        cudaDeviceProp prop;
        CHECK_CUDA_ERROR(cudaGetDevice(&device));
        CHECK_CUDA_ERROR(cudaGetDeviceProperties(&prop, device));
        Log.logandshow(Logger_t::LogLvl::INFO, "CUDA: Using device " + std::string(prop.name) + " ID:" + to_string_exact(device));

    }

    void CudaSolver_t::Set_Susceptibility_params(const std::vector<ConfigFile>& Materials_config){

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_chi_para_scaling_factor, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_chi_perp_scaling_factor, Num_grains*sizeof(cu_real_t)));

        std::vector<cu_real_t> Chi_para_scaling(Num_grains);
        std::vector<cu_real_t> Chi_perp_scaling(Num_grains);

        // TODO: Implement Lognormal AND Normal distributions

        for(size_t L=0; L<Materials_config.size(); ++L){

            std::string Kstr = Materials_config[L].getValueOfKey<std::string>("Mat:K_Dist_type");
            if(Kstr!="normal" && Kstr!="log-normal"){
                Log.log(Logger_t::LogLvl::ERR, "CFG error: K distribution type invalid.");
            }
            cu_real_t SIG=Materials_config[L].getValueOfKey<cu_real_t>("Mat:StdDev_K");

            if(SIG==0.0){
                for(size_t g=0; g<Sim::GrainsperLayer; ++g){
                    Chi_para_scaling[L*Sim::GrainsperLayer+g] = hst_LayerParams[L].Chi_scaling_factor;
                    Chi_perp_scaling[L*Sim::GrainsperLayer+g] = hst_LayerParams[L].Chi_scaling_factor;
                }
            }
            else{
                std::lognormal_distribution<cu_real_t> dist(0.0,SIG);
                for(size_t g=0; g<Sim::GrainsperLayer; ++g){
                    cu_real_t RNG = dist(Gen);
                    Chi_para_scaling[L*Sim::GrainsperLayer+g] = hst_LayerParams[L].Chi_scaling_factor;
                    Chi_perp_scaling[L*Sim::GrainsperLayer+g] = hst_LayerParams[L].Chi_scaling_factor * RNG;
                }
            }
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_chi_para_scaling_factor, Chi_para_scaling.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_chi_perp_scaling_factor, Chi_perp_scaling.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
    }

    void CudaSolver_t::Set_Exchange_params(const Interaction_t& Int_system){

        // Num_neighbours should be the number of neighbours for each grain
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Num_Neighbours, Num_grains*sizeof(int)));
        std::vector<int> hst_data(Num_grains);
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Int_system.Exchange_neigh_list[g].size();
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Num_Neighbours, hst_data.data(), Num_grains*sizeof(int), cudaMemcpyHostToDevice));

        // Neighbour lists should be a list of neighbours for each grain
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Neighbours, 20*Num_grains*sizeof(int)));
        hst_data.resize(20*Num_grains);
        for(size_t g=0; g<Num_grains; ++g){
            for(size_t n=0; n<Int_system.Exchange_neigh_list[g].size(); ++n){
                hst_data[(g*20)+n] = Int_system.Exchange_neigh_list[g][n];
            }
            for(size_t n=Int_system.Exchange_neigh_list[g].size(); n<20; ++n){
                hst_data[(g*20)+n] = -1;
            }
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Neighbours, hst_data.data(), 20*Num_grains*sizeof(int), cudaMemcpyHostToDevice));

        // Exch strength list should be a list of Exch strength of each neighbour for each grain
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_H_exch_str, 20*Num_grains*sizeof(cu_real_t)));
        std::vector<cu_real_t> hst_data_B(20*Num_grains);
        for(size_t g=0; g<Num_grains; ++g){
            for(size_t n=0; n<Int_system.H_exch_str[g].size(); ++n){
                hst_data_B[(g*20)+n] = Int_system.H_exch_str[g][n];
            }
            for(size_t n=Int_system.H_exch_str[g].size(); n<20; ++n){
                hst_data_B[(g*20)+n] = 0.0;
            }
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_exch_str, hst_data_B.data(), 20*Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
    }

    void CudaSolver_t::Cuda_synchronise(){
        // TODO: Implement
    }

    void CudaSolver_t::CopyToHost(std::vector<Grain_t>& Grains){
        std::vector<cu_real_t> hst_data(Num_grains);
        std::vector<cu_real_t> hst_data_x(Num_grains);
        std::vector<cu_real_t> hst_data_y(Num_grains);
        std::vector<cu_real_t> hst_data_z(Num_grains);

        CHECK_CUDA_ERROR(cudaMemcpy(hst_data.data(), dev_Temperature, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToHost));
        CHECK_CUDA_ERROR(cudaMemcpy(hst_data_x.data(), dev_m_x, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToHost));
        CHECK_CUDA_ERROR(cudaMemcpy(hst_data_y.data(), dev_m_y, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToHost));
        CHECK_CUDA_ERROR(cudaMemcpy(hst_data_z.data(), dev_m_z, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToHost));

        for(size_t g=0; g<Num_grains; ++g){
            Grains[g].Temp = hst_data[g];
            Grains[g].m.x  = hst_data_x[g];
            Grains[g].m.y  = hst_data_y[g];
            Grains[g].m.z  = hst_data_z[g];
        }
    }

    void CudaSolver_t::CopyChiParaToHost(std::vector<cu_real_t>& hst_data){
        hst_data.resize(Num_grains);
        CHECK_CUDA_ERROR(cudaMemcpy(hst_data.data(), dev_Chi_para, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToHost));
    }

    void CudaSolver_t::CopyChiPerpToHost(std::vector<cu_real_t>& hst_data){
        hst_data.resize(Num_grains);
        CHECK_CUDA_ERROR(cudaMemcpy(hst_data.data(), dev_Chi_perp, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToHost));
    }

    void CudaSolver_t::Integrate(std::vector<Grain_t>& Grains){

        // TODO: Implement kMC and LLG
        switch (SolverType)
        {
            case Solvers::kMC:
                kMC(Grains);
                break;
            case Solvers::LLG:
                LLG(Grains);
                break;
            case Solvers::LLB:
                LLB(Grains);
                break;
        }
    }


    void CudaSolver_t::Integrate(std::vector<Grain_t>& Grains, Data_output_t& DataOut, bool ForceTransfer){

        // TODO: Implement kMC and LLG
        switch (SolverType)
        {
            case Solvers::kMC:
                kMC(Grains);
                break;
            case Solvers::LLG:
                LLG(Grains);
                break;
            case Solvers::LLB:
                LLB(Grains);
                break;
        }

        // TODO: Rework data output
        // TODO: Implement GPU based statistics
        // Check if we want to output data
        if(ForceTransfer){
            CopyToHost(Grains);
        }
        else if(DataOut.PrintNow(false)){
            // Copy data to host before printing data
            CopyToHost(Grains);
            DataOut.PrintData(Grains);
            // TODO: Look into implementing determining statisitcs on the GPU when outputting data
        }

    }

    // TODO: Implement direct access to the device for field and temperature classes
    void CudaSolver_t::Update_Field_and_Temp(std::vector<Grain_t>& Grains){
        std::vector<cu_real_t> hst_data_1(Num_grains);
        std::vector<cu_real_t> hst_data_2(Num_grains);
        std::vector<cu_real_t> hst_data_3(Num_grains);
        std::vector<cu_real_t> hst_data_4(Num_grains);

        for(size_t g=0; g<Num_grains; ++g){
            hst_data_1[g] = Grains[g].H_appl.x;
            hst_data_2[g] = Grains[g].H_appl.y;
            hst_data_3[g] = Grains[g].H_appl.z;
            hst_data_4[g] = Grains[g].Temp;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appx, hst_data_1.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appy, hst_data_2.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appz, hst_data_3.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Temperature, hst_data_4.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

    }

    void CudaSolver_t::Calc_mEQ(){
        kernels::Calc_mEQ<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams, dev_Temperature,
                                                    dev_Tc, dev_mEQ, Num_grains);
    }

    void CudaSolver_t::Calc_Susceptibility(){
            kernels::Calc_Susceptibility<<<grid_size, block_size>>>(dev_Temperature, dev_Tc, dev_LayerParams,
                                                            dev_LayerID, dev_Chi_para, dev_Chi_perp, dev_mEQ,
                                                            dev_chi_para_scaling_factor, dev_chi_perp_scaling_factor,
                                                            Num_grains);
    }

    void CudaSolver_t::Calc_Damping(){
        kernels::Calc_Damping<<<grid_size, block_size>>>(dev_LayerID, dev_Temperature, dev_Tc, dev_LayerParams, dev_Alpha_Para, dev_Alpha_Perp, Num_grains);
    }

    void CudaSolver_t::Calc_Tdependence(){
        kernels::Calc_Tdependence<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams, dev_Temperature, dev_mEQ,
                                                            dev_K_store, dev_Ms_store, dev_K, dev_Ms,Num_grains);
    }

    void CudaSolver_t::Cuda_allocate(){

        Log.logandshow(Logger_t::LogLvl::INFO,"CUDA: Allocating device memory");

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_rand_states, grid_size*block_size*sizeof(curandState)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_H_ani, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_EA_x, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_EA_y, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_EA_z, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_mEQ, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_m_x, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_m_y, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_m_z, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_m_bufferx, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_m_buffery, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_m_bufferz, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_dmx, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_dmy, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_dmz, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_H_appx, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_H_appy, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_H_appz, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Fieldx, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Fieldy, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Fieldz, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Alpha_Para, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Alpha_Perp, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Chi_para, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Chi_perp, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_K, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Ms, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Vol, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Temperature, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Tc, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_K_store, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_Ms_store, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_RNG_PARAx, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_RNG_PARAy, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_RNG_PARAz, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_RNG_PERPx, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_RNG_PERPy, Num_grains*sizeof(cu_real_t)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_RNG_PERPz, Num_grains*sizeof(cu_real_t)));

        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_LayerID, Num_grains*sizeof(int)));
        CHECK_CUDA_ERROR(cudaMalloc((void **)&dev_LayerParams, Num_Layers*sizeof(layer_params_t)));

    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Getters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    Solvers CudaSolver_t::get_Solver() const{
        return SolverType;
    }

    std::vector<cu_real_t> CudaSolver_t::get_Gamma() const{
        std::vector<cu_real_t> Gamma(hst_LayerParams.size(),0.0);
        for(size_t L=0; L<hst_LayerParams.size(); ++L){
            Gamma[L] = hst_LayerParams[L].Gamma;
        }
        return Gamma;
    }

    std::vector<cu_real_t> CudaSolver_t::get_Alpha() const{
        std::vector<cu_real_t> BathCoupling(hst_LayerParams.size(),0.0);
        for(size_t L=0; L<hst_LayerParams.size(); ++L){
            BathCoupling[L] = hst_LayerParams[L].BathCoupling;
        }
        return BathCoupling;
    }

    cu_real_t CudaSolver_t::get_dt() const{
        switch (SolverType)
        {
            case Solvers::kMC:
                return dt_kMC;
            case Solvers::LLG:
                return dt_LLG;
            case Solvers::LLB:
                return dt_LLB;
            default:
                Log.log(Logger_t::LogLvl::ERR, "Solver error: Unknown solver type encountered.");
                return 0;
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Setters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    void CudaSolver_t::Reinitialise(){
        Initialised=false;
    }

    void CudaSolver_t::set_dt(const cu_real_t dt){
        switch (SolverType)
        {
            case Solvers::kMC:
                dt_kMC = dt;
                break;
            case Solvers::LLG:
                dt_LLG = dt;
                break;
            case Solvers::LLB:
                dt_LLB = dt;
                break;
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Fields ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    // This function overwrites the previous field values so it shoudl go first
    void CudaSolver_t::update_applied_field(){
        kernels::update_applied_field<<<grid_size, block_size>>>(dev_H_appx, dev_H_appy, dev_H_appz, dev_Fieldx, dev_Fieldy, dev_Fieldz, Num_grains);
    }

    void CudaSolver_t::update_internal_exchange_field(){
        kernels::update_internal_exchange_field<<<grid_size, block_size>>>(dev_m_x, dev_m_y, dev_m_z,
                                                        dev_mEQ, dev_Chi_para, dev_Tc,
                                                        dev_Temperature, dev_Fieldx,
                                                        dev_Fieldy, dev_Fieldz, Num_grains);
    }

    void CudaSolver_t::update_anisotropy_field(){
        kernels::update_anisotropy_field<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams, dev_m_x, dev_m_y, dev_m_z,
                                                            dev_EA_x, dev_EA_y, dev_EA_z, dev_Ms, dev_K, dev_Temperature,
                                                            dev_Chi_perp, dev_mEQ, dev_Fieldx, dev_Fieldy, dev_Fieldz,
                                                            SolverType, Num_grains);
    }

    void CudaSolver_t::update_exchange_field(){
        kernels::update_exchange_field<<<grid_size, block_size>>>(dev_Num_Neighbours, dev_Neighbours, dev_H_exch_str, dev_mEQ,
                                                            dev_m_x, dev_m_y, dev_m_z, dev_Fieldx, dev_Fieldy, dev_Fieldz, Num_grains);
    }

    void CudaSolver_t::update_magnetostatic_field(){
        // TODO: Implement magnetostatics
    }

    //################################# kMC #################################//

    // TODO: Implement temperature dependent kMC
    /* For temperature dependence we need to be able to set
     * Ms and K for the appropriate temperature. */
    void CudaSolver_t::kMC(std::vector<Grain_t>& Grains){
        if(!Initialised){
            kMC_init(Grains);
        }
        else{
            Update_Field_and_Temp(Grains);
        }
        //Calc_Susceptibility();
        Calc_mEQ();
        update_applied_field();
        update_exchange_field();
        update_magnetostatic_field();
        kMC_core();
    }

    void CudaSolver_t::kMC_init(std::vector<Grain_t>& Grains){

        kernels::RNG_init<<<grid_size, block_size>>>(dev_rand_states, SEED);

        CHECK_CUDA_ERROR(cudaMemcpy(dev_LayerParams, hst_LayerParams.data(), Sim::NumLayers * sizeof(layer_params_t), cudaMemcpyHostToDevice));

        std::vector<cu_real_t> hst_data(Num_grains,1.0);

        CHECK_CUDA_ERROR(cudaMemcpy(dev_mEQ, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        std::vector<cu_real_t> hst_data_x(Num_grains);
        std::vector<cu_real_t> hst_data_y(Num_grains);
        std::vector<cu_real_t> hst_data_z(Num_grains);

        // Copy Applied field
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].H_appl.x;
            hst_data_y[g] = Grains[g].H_appl.y;
            hst_data_z[g] = Grains[g].H_appl.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appx, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appy, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appz, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy Easy axes
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].Easy_axis.x;
            hst_data_y[g] = Grains[g].Easy_axis.y;
            hst_data_z[g] = Grains[g].Easy_axis.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_x, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_y, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_z, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy initial magnetisation into buffer
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].m.x;
            hst_data_y[g] = Grains[g].m.y;
            hst_data_z[g] = Grains[g].m.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_bufferx, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_buffery, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_bufferz, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_x, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_y, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_z, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy K, Ms, Vol, Temperature
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Grains[g].K;
            hst_data_x[g] = Grains[g].Ms;
            hst_data_y[g] = Grains[g].Vol;
            hst_data_z[g] = Grains[g].Temp;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_K, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Ms, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Vol, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Temperature, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        CHECK_CUDA_ERROR(cudaMemcpy(dev_K_store, dev_K, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Ms_store, dev_Ms, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToDevice));

        // Copy Tc
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Grains[g].Tc;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Tc, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy LayerID
        std::vector<int> hst_data_INT(Num_grains);
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_INT[g] = Grains[g].Layer;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_LayerID, hst_data_INT.data(), Num_grains*sizeof(int), cudaMemcpyHostToDevice));

        // Only initialise once
        // TODO: Add option to set solver to uninitialised state
        Initialised = true;

        return ;
    }

    void CudaSolver_t::kMC_core(){
        kernels::kMC_core<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams,
                                                     dev_EA_x, dev_EA_y, dev_EA_z,
                                                     dev_Fieldx, dev_Fieldy, dev_Fieldz,
                                                     dev_K, dev_Vol, dev_Temperature, dev_Ms,
                                                     dev_rand_states, dev_m_x, dev_m_y, dev_m_z,
                                                     dt_kMC, Num_grains);
    }

    //################################# LLG #################################//

    // TODO: Implement temperature dependent LLG
    /* For temperature dependence we need to be able to set
     * Ms and K for the appropriate temperature. */
    void CudaSolver_t::LLG(std::vector<Grain_t>& Grains){
        if(!Initialised){
            LLG_init(Grains);
        }
        else{
            Update_Field_and_Temp(Grains);
        }

        if(T_dep){
            // Determine mEQ, K(t) and Ms(t)
            Calc_mEQ();
            Calc_Tdependence();
        }

        //Calc_Susceptibility();
        update_applied_field();
        update_anisotropy_field();
        update_exchange_field();
        update_magnetostatic_field();
        LLG_RNG();
        LLG_predictor();

        update_applied_field();
        update_anisotropy_field();
        update_exchange_field();
        update_magnetostatic_field();
        LLG_corrector();

        // TODO: Add option to output magnetisation * mEQ -> Makes transition between LLG and LLB smoother

    }

    void CudaSolver_t::LLG_init(std::vector<Grain_t>& Grains){

        kernels::RNG_init<<<grid_size, block_size>>>(dev_rand_states, SEED);

        CHECK_CUDA_ERROR(cudaMemcpy(dev_LayerParams, hst_LayerParams.data(), Sim::NumLayers * sizeof(layer_params_t), cudaMemcpyHostToDevice));

        std::vector<cu_real_t> hst_data(Num_grains,1.0);

        CHECK_CUDA_ERROR(cudaMemcpy(dev_mEQ, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        std::vector<cu_real_t> hst_data_x(Num_grains);
        std::vector<cu_real_t> hst_data_y(Num_grains);
        std::vector<cu_real_t> hst_data_z(Num_grains);

        // Copy Applied field
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].H_appl.x;
            hst_data_y[g] = Grains[g].H_appl.y;
            hst_data_z[g] = Grains[g].H_appl.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appx, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appy, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appz, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy Easy axes
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].Easy_axis.x;
            hst_data_y[g] = Grains[g].Easy_axis.y;
            hst_data_z[g] = Grains[g].Easy_axis.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_x, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_y, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_z, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy initial magnetisation into buffer
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].m.x;
            hst_data_y[g] = Grains[g].m.y;
            hst_data_z[g] = Grains[g].m.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_bufferx, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_buffery, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_bufferz, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_x, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_y, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_z, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy K, Ms, Vol, Temperature
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Grains[g].K;
            hst_data_x[g] = Grains[g].Ms;
            hst_data_y[g] = Grains[g].Vol;
            hst_data_z[g] = Grains[g].Temp;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_K, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Ms, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Vol, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Temperature, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        CHECK_CUDA_ERROR(cudaMemcpy(dev_K_store, dev_K, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Ms_store, dev_Ms, Num_grains*sizeof(cu_real_t), cudaMemcpyDeviceToDevice));

        // Copy Tc
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Grains[g].Tc;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Tc, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy LayerID
        std::vector<int> hst_data_INT(Num_grains);
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_INT[g] = Grains[g].Layer;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_LayerID, hst_data_INT.data(), Num_grains*sizeof(int), cudaMemcpyHostToDevice));

        // Only initialise once
        // TODO: Add option to set solver to uninitialised state
        Initialised = true;

        return ;
    }

    void CudaSolver_t::LLG_RNG(){
        kernels::LLG_RNG<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams, dev_Temperature,
                                                    dev_Ms, dev_Vol, dev_RNG_PERPx, dev_RNG_PERPy, dev_RNG_PERPz,
                                                    dev_rand_states, dt_LLG, Num_grains);
    }

    void CudaSolver_t::LLG_predictor(){
        kernels::LLG_predictor<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams,
                                                          dev_m_x, dev_m_y, dev_m_z,
                                                          dev_Fieldx, dev_Fieldy, dev_Fieldz,
                                                          dev_RNG_PERPx, dev_RNG_PERPy, dev_RNG_PERPz,
                                                          dev_dmx, dev_dmy, dev_dmz,
                                                          dt_LLG, Num_grains);
    }

    void CudaSolver_t::LLG_corrector(){
        kernels::LLG_corrector<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams,
                                                          dev_m_x, dev_m_y, dev_m_z,
                                                          dev_m_bufferx, dev_m_buffery, dev_m_bufferz,
                                                          dev_Fieldx, dev_Fieldy, dev_Fieldz,
                                                          dev_RNG_PERPx, dev_RNG_PERPy, dev_RNG_PERPz,
                                                          dev_dmx, dev_dmy, dev_dmz,
                                                          dt_LLG, Num_grains);
    }

    //################################# LLB #################################//

    // TODO: Clean function once testing is complete
    void CudaSolver_t::LLB(std::vector<Grain_t>& Grains){
        if(!Initialised){
            LLB_init(Grains);
        }
        else{
            Update_Field_and_Temp(Grains);
        }

        Calc_Susceptibility();
        Calc_mEQ();
        Calc_Damping();
        update_applied_field();
        update_anisotropy_field();
        update_internal_exchange_field();
        update_exchange_field();
        update_magnetostatic_field();
        LLB_RNG();
        LLB_predictor();

        update_applied_field();
        update_anisotropy_field();
        update_internal_exchange_field();
        update_exchange_field();
        update_magnetostatic_field();
        LLB_corrector();
    }

    void CudaSolver_t::LLB_init(std::vector<Grain_t>& Grains){
        std::cout << "Initialsing LLB" << std::endl;
        kernels::RNG_init<<<grid_size, block_size>>>(dev_rand_states, SEED);
        CHECK_CUDA_ERROR(cudaPeekAtLastError());
        #ifdef CUDA_DEBUG
            CHECK_CUDA_ERROR(cudaDeviceSynchronize());
        #endif

        CHECK_CUDA_ERROR(cudaMemcpy(dev_LayerParams, hst_LayerParams.data(), Sim::NumLayers * sizeof(layer_params_t), cudaMemcpyHostToDevice));

        std::vector<cu_real_t> hst_data(Num_grains);
        std::vector<cu_real_t> hst_data_x(Num_grains);
        std::vector<cu_real_t> hst_data_y(Num_grains);
        std::vector<cu_real_t> hst_data_z(Num_grains);

        // Copy Applied field
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].H_appl.x;
            hst_data_y[g] = Grains[g].H_appl.y;
            hst_data_z[g] = Grains[g].H_appl.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appx, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appy, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_H_appz, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy Easy axes
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].Easy_axis.x;
            hst_data_y[g] = Grains[g].Easy_axis.y;
            hst_data_z[g] = Grains[g].Easy_axis.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_x, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_y, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_EA_z, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy initial magnetisation into buffer
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_x[g] = Grains[g].m.x;
            hst_data_y[g] = Grains[g].m.y;
            hst_data_z[g] = Grains[g].m.z;
        }

        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_bufferx, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_buffery, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_bufferz, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_x, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_y, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_m_z, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy K, Ms, Vol, Temperature
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Grains[g].K;
            hst_data_x[g] = Grains[g].Ms;
            hst_data_y[g] = Grains[g].Vol;
            hst_data_z[g] = Grains[g].Temp;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_K, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Ms, hst_data_x.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Vol, hst_data_y.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Temperature, hst_data_z.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy Tc
        for(size_t g=0; g<Num_grains; ++g){
            hst_data[g] = Grains[g].Tc;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_Tc, hst_data.data(), Num_grains*sizeof(cu_real_t), cudaMemcpyHostToDevice));

        // Copy LayerID
        std::vector<int> hst_data_INT(Num_grains);
        for(size_t g=0; g<Num_grains; ++g){
            hst_data_INT[g] = Grains[g].Layer;
        }
        CHECK_CUDA_ERROR(cudaMemcpy(dev_LayerID, hst_data_INT.data(), Num_grains*sizeof(int), cudaMemcpyHostToDevice));

        // Only initialise once
        // TODO: Add option to set solver to uninitialised state
        Initialised = true;

        return ;
    }

    void CudaSolver_t::LLB_RNG(){
        kernels::LLB_RNG<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams, dev_Temperature,
                                            dev_Alpha_Para, dev_Alpha_Perp, dev_Ms,
                                            dev_Vol, dev_RNG_PARAx, dev_RNG_PARAy,
                                            dev_RNG_PARAz, dev_RNG_PERPx, dev_RNG_PERPy,
                                            dev_RNG_PERPz, dev_rand_states,
                                            dt_LLB, Num_grains);
        CHECK_CUDA_ERROR(cudaPeekAtLastError());
        #ifdef CUDA_DEBUG
            CHECK_CUDA_ERROR(cudaDeviceSynchronize());
        #endif
    }

    void CudaSolver_t::LLB_predictor(){
        kernels::LLB_predictor<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams,
                                                dev_m_x, dev_m_y, dev_m_z,
                                                dev_Alpha_Para, dev_Alpha_Perp,
                                                dev_Fieldx, dev_Fieldy, dev_Fieldz,
                                                dev_RNG_PARAx, dev_RNG_PARAy, dev_RNG_PARAz,
                                                dev_RNG_PERPx, dev_RNG_PERPy, dev_RNG_PERPz,
                                                dev_dmx, dev_dmy, dev_dmz,
                                                dt_LLB, Num_grains);
        CHECK_CUDA_ERROR(cudaPeekAtLastError());
        #ifdef CUDA_DEBUG
            CHECK_CUDA_ERROR(cudaDeviceSynchronize());
        #endif
    }

    void CudaSolver_t::LLB_corrector(){
        kernels::LLB_corrector<<<grid_size, block_size>>>(dev_LayerID, dev_LayerParams,
                                                dev_m_x, dev_m_y, dev_m_z,
                                                dev_m_bufferx, dev_m_buffery, dev_m_bufferz,
                                                dev_Alpha_Para, dev_Alpha_Perp,
                                                dev_Fieldx, dev_Fieldy, dev_Fieldz,
                                                dev_RNG_PARAx, dev_RNG_PARAy, dev_RNG_PARAz,
                                                dev_RNG_PERPx, dev_RNG_PERPy, dev_RNG_PERPz,
                                                dev_dmx, dev_dmy, dev_dmz,
                                                dt_LLB, Num_grains);
        CHECK_CUDA_ERROR(cudaPeekAtLastError());
        #ifdef CUDA_DEBUG
            CHECK_CUDA_ERROR(cudaDeviceSynchronize());
        #endif
    }

    #endif

} //namespace cuda