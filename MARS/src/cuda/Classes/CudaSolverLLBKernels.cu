#include "cuda/Classes/CudaSolverKernels.cuh"
#include <stdio.h>
namespace cuda
{
    namespace kernels
    {

        #ifdef CUDA

        __global__ void LLB_RNG(const int *layer_id, const layer_params_t *LayerParams, const cu_real_t *Temperature,
                                const cu_real_t *Alpha_Para, const cu_real_t *Alpha_Perp, const cu_real_t *Ms,
                                const cu_real_t *Vol, cu_real_t *RNG_PARAx, cu_real_t *RNG_PARAy,
                                cu_real_t *RNG_PARAz, cu_real_t *RNG_PERPx, cu_real_t *RNG_PERPy,
                                cu_real_t *RNG_PERPz, curandState *rand_states,
                                const cu_real_t dt, const size_t num_grains){
            size_t tid = blockIdx.x * blockDim.x + threadIdx.x;
            for(size_t grain=tid; grain<num_grains; grain+= blockDim.x*gridDim.x){
            //for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = layer_id[grain];
                curandState local_state = rand_states[tid];

                #ifdef CUDA_DP
                    double RNG_PARA_MAG =  __dsqrt_rn((LayerParams[Lid].Gamma*2.76129800000000e-16*Temperature[grain]*Alpha_Para[grain])/(dt*Ms[grain]*Vol[grain]*1e-21));
                    double RNG_PERP_MAG =  __dsqrt_rn((2.76129800000000e-16*Temperature[grain]*(Alpha_Perp[grain]-Alpha_Para[grain]))/(LayerParams[Lid].Gamma*Ms[grain]*Vol[grain]*1e-21*dt*(Alpha_Perp[grain]*Alpha_Perp[grain])));
                #else
                    float RNG_PARA_MAG =  __fsqrt_rn((LayerParams[Lid].Gamma*2.76129800000000e-16*Temperature[grain]*Alpha_Para[grain])/(dt*Ms[grain]*Vol[grain]*1e-21));
                    float RNG_PERP_MAG =  __fsqrt_rn((2.76129800000000e-16*Temperature[grain]*(Alpha_Perp[grain]-Alpha_Para[grain]))/(LayerParams[Lid].Gamma*Ms[grain]*Vol[grain]*1e-21*dt*(Alpha_Perp[grain]*Alpha_Perp[grain])));
                #endif

            #ifdef CUDA_DP
                RNG_PARAx[grain] = RNG_PARA_MAG * curand_normal_double(&local_state);
                RNG_PARAy[grain] = RNG_PARA_MAG * curand_normal_double(&local_state);
                RNG_PARAz[grain] = RNG_PARA_MAG * curand_normal_double(&local_state);

                RNG_PERPx[grain] = RNG_PERP_MAG * curand_normal_double(&local_state);
                RNG_PERPy[grain] = RNG_PERP_MAG * curand_normal_double(&local_state);
                RNG_PERPz[grain] = RNG_PERP_MAG * curand_normal_double(&local_state);
            #else
                RNG_PARAx[grain] = RNG_PARA_MAG * curand_normal(&local_state);
                RNG_PARAy[grain] = RNG_PARA_MAG * curand_normal(&local_state);
                RNG_PARAz[grain] = RNG_PARA_MAG * curand_normal(&local_state);

                RNG_PERPx[grain] = RNG_PERP_MAG * curand_normal(&local_state);
                RNG_PERPy[grain] = RNG_PERP_MAG * curand_normal(&local_state);
                RNG_PERPz[grain] = RNG_PERP_MAG * curand_normal(&local_state);
            #endif

                rand_states[tid] = local_state;

            }
        }

        __global__ void LLB_predictor(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        const cu_real_t *Alpha_Para, const cu_real_t *Alpha_Perp,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNG_PARAx, const cu_real_t *RNG_PARAy, const cu_real_t *RNG_PARAz,
                                        const cu_real_t *RNG_PERPx, const cu_real_t *RNG_PERPy, const cu_real_t *RNG_PERPz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains)
        {

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];

                // Load spin into registers
                cu_real_t mx = m_x[grain];
                cu_real_t my = m_y[grain];
                cu_real_t mz = m_z[grain];

                // Load damping into registers
                cu_real_t Apara = Alpha_Para[grain];
                cu_real_t Aperp = Alpha_Perp[grain];

                // Calculate total field
                cu_real_t Heffx = Fieldx[grain];
                cu_real_t Heffy = Fieldy[grain];
                cu_real_t Heffz = Fieldz[grain];

                cu_real_t Heff_p_PERPx = Heffx + RNG_PERPx[grain];
                cu_real_t Heff_p_PERPy = Heffy + RNG_PERPy[grain];
                cu_real_t Heff_p_PERPz = Heffz + RNG_PERPz[grain];

                cu_real_t one_o_mSQ = (1.0/(mx*mx + my*my + mz*mz));

                cu_real_t dmdtx =  LayerParams[Lid].Gamma*(-1.0*(my*Heffz - mz*Heffy)
                                        + (Apara*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mx
                                        - (Aperp*one_o_mSQ*(my*(mx*(Heff_p_PERPy) - my*(Heff_p_PERPx)) - mz*(mz*(Heff_p_PERPx) - mx*(Heff_p_PERPz)))))
                                        + RNG_PARAx[grain];

                cu_real_t dmdty = LayerParams[Lid].Gamma*(-1.0*(mz*Heffx - mx*Heffz)
                                        + (Apara*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*my
                                        - (Aperp*one_o_mSQ*(mz*(my*(Heff_p_PERPz) - mz*(Heff_p_PERPy)) - mx*(mx*(Heff_p_PERPy) - my*(Heff_p_PERPx)))))
                                        + RNG_PARAy[grain];

                cu_real_t dmdtz = LayerParams[Lid].Gamma*(-1.0*(mx*Heffy - my*Heffx)
                                        + (Apara*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mz
                                        - (Aperp*one_o_mSQ*(mx*(mz*(Heff_p_PERPx) - mx*(Heff_p_PERPz)) - my*(my*(Heff_p_PERPz) - mz*(Heff_p_PERPy)))))
                                        + RNG_PARAz[grain];
#if 0
                if(grain==0){
                    printf("GPU Gamma: %20.15e\n", LayerParams[Lid].Gamma);
                    printf("GPU Alpha: %20.15e %20.15e\n",Apara,Aperp);
                    printf("GPU m: %20.15e %20.15e %20.15e\n",mx,my,mz);
                    printf("GPU Heff: %20.15e %20.15e %20.15e\n",Heffx,Heffy,Heffz);
                    printf("GPU mSQ: %20.15e\n",one_o_mSQ);
                    printf("GPU Heff+: %20.15e %20.15e %20.15e\n",Heff_p_PERPx,Heff_p_PERPy,Heff_p_PERPz);
                    printf("GPU dmdt: %20.15e %20.15e %20.15e\n",dmdtx, dmdty, dmdtz);
                }
#endif
                // Store dmdt
                dmx[grain] = dmdtx;
                dmy[grain] = dmdty;
                dmz[grain] = dmdtz;

                // Calculate and store new magnetisation
                cu_real_t new_mx = mx + dmdtx*dt;
                cu_real_t new_my = my + dmdty*dt;
                cu_real_t new_mz = mz + dmdtz*dt;

                m_x[grain] = new_mx;
                m_y[grain] = new_my;
                m_z[grain] = new_mz;

            }
        }

        __global__ void LLB_corrector(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        cu_real_t *m_bufferx, cu_real_t *m_buffery, cu_real_t *m_bufferz,
                                        const cu_real_t *Alpha_Para, const cu_real_t *Alpha_Perp,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNG_PARAx, const cu_real_t *RNG_PARAy, const cu_real_t *RNG_PARAz,
                                        const cu_real_t *RNG_PERPx, const cu_real_t *RNG_PERPy, const cu_real_t *RNG_PERPz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains)
        {

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];

                // Load spin into registers
                cu_real_t mx = m_x[grain];
                cu_real_t my = m_y[grain];
                cu_real_t mz = m_z[grain];

                // Load damping into registers
                cu_real_t Apara = Alpha_Para[grain];
                cu_real_t Aperp = Alpha_Perp[grain];

                // Calculate total field
                cu_real_t Heffx = Fieldx[grain];
                cu_real_t Heffy = Fieldy[grain];
                cu_real_t Heffz = Fieldz[grain];

                cu_real_t Heff_p_PERPx = Heffx + RNG_PERPx[grain];
                cu_real_t Heff_p_PERPy = Heffy + RNG_PERPy[grain];
                cu_real_t Heff_p_PERPz = Heffz + RNG_PERPz[grain];

                cu_real_t one_o_mSQ = (1.0/(mx*mx + my*my + mz*mz));

                cu_real_t dmdtx =  LayerParams[Lid].Gamma*(-1.0*(my*Heffz - mz*Heffy)
                                        + (Apara*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mx
                                        - (Aperp*one_o_mSQ*(my*(mx*(Heff_p_PERPy) - my*(Heff_p_PERPx)) - mz*(mz*(Heff_p_PERPx) - mx*(Heff_p_PERPz)))))
                                        + RNG_PARAx[grain];

                cu_real_t dmdty = LayerParams[Lid].Gamma*(-1.0*(mz*Heffx - mx*Heffz)
                                        + (Apara*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*my
                                        - (Aperp*one_o_mSQ*(mz*(my*(Heff_p_PERPz) - mz*(Heff_p_PERPy)) - mx*(mx*(Heff_p_PERPy) - my*(Heff_p_PERPx)))))
                                        + RNG_PARAy[grain];

                cu_real_t dmdtz = LayerParams[Lid].Gamma*(-1.0*(mx*Heffy - my*Heffx)
                                        + (Apara*one_o_mSQ)*(mx*Heffx + my*Heffy + mz*Heffz)*mz
                                        - (Aperp*one_o_mSQ*(mx*(mz*(Heff_p_PERPx) - mx*(Heff_p_PERPz)) - my*(my*(Heff_p_PERPz) - mz*(Heff_p_PERPy)))))
                                        + RNG_PARAz[grain];
#if 0
                if(grain==0){
                    printf("GPU 2 Gamma: %20.15e\n", LayerParams[Lid].Gamma);
                    printf("GPU 2 Alpha: %20.15e %20.15e\n",Apara,Aperp);
                    printf("GPU 2 m: %20.15e %20.15e %20.15e\n",mx,my,mz);
                    printf("GPU 2 Heff: %20.15e %20.15e %20.15e\n",Heffx,Heffy,Heffz);
                    printf("GPU 2 mSQ: %20.15e\n",one_o_mSQ);
                    printf("GPU 2 Heff+: %20.15e %20.15e %20.15e\n",Heff_p_PERPx,Heff_p_PERPy,Heff_p_PERPz);
                    printf("GPU 2 dmdt: %20.15e %20.15e %20.15e\n",dmdtx, dmdty, dmdtz);
                }
#endif
                // Calculate and store new magnetisation
                cu_real_t new_mx = m_bufferx[grain] + 0.5 *(dmx[grain] + dmdtx) * dt;
                cu_real_t new_my = m_buffery[grain] + 0.5 *(dmy[grain] + dmdty) * dt;
                cu_real_t new_mz = m_bufferz[grain] + 0.5 *(dmz[grain] + dmdtz) * dt;

                m_x[grain] = new_mx;
                m_y[grain] = new_my;
                m_z[grain] = new_mz;

                // Update magnetisation buffer
                m_bufferx[grain] = new_mx;
                m_buffery[grain] = new_my;
                m_bufferz[grain] = new_mz;

            }
        }
        #endif
    } // namespace kernels
} // namespace cuda