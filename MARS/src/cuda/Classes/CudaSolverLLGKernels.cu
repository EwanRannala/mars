#include "cuda/Classes/CudaSolverKernels.cuh"
#include <stdio.h>
namespace cuda
{
    namespace kernels
    {

        #ifdef CUDA
        // TODO: Obtain the Gilbert damping from the thermal bath coupling
        /* This will make things much simpler when switching between LLG and LLB
         * Currently we will assume for the LLG than the input is equal to the Gilbert damping */
        __global__ void LLG_RNG(const int *layer_id, const layer_params_t *LayerParams, const cu_real_t *Temperature,
                                const cu_real_t *Ms, const cu_real_t *Vol,
                                cu_real_t *RNGx, cu_real_t *RNGy, cu_real_t *RNGz,
                                curandState *rand_states, const cu_real_t dt, const size_t num_grains){
            size_t tid = blockIdx.x * blockDim.x + threadIdx.x;
            for(size_t grain=tid; grain<num_grains; grain+= blockDim.x*gridDim.x){
                size_t Lid = layer_id[grain];
                curandState local_state = rand_states[tid];

                #ifdef CUDA_DP
                    double RNG_MAG = __dsqrt_rn( ((2.0*LayerParams[Lid].BathCoupling*1.38064900000000e-16*Temperature[grain])/(LayerParams[Lid].Gamma*Ms[grain]*(Vol[grain]*1e-21)))/dt );
                    RNGx[grain] = RNG_MAG * curand_normal_double(&local_state);
                    RNGy[grain] = RNG_MAG * curand_normal_double(&local_state);
                    RNGz[grain] = RNG_MAG * curand_normal_double(&local_state);
                #else
                    float RNG_MAG = __fsqrt_rn( ((2.0*LayerParams[Lid].BathCoupling*1.38064900000000e-16*Temperature[grain])/(LayerParams[Lid].Gamma*Ms[grain]*(Vol[grain]*1e-21)))/dt );
                    RNGx[grain] = RNG_MAG * curand_normal(&local_state);
                    RNGy[grain] = RNG_MAG * curand_normal(&local_state);
                    RNGz[grain] = RNG_MAG * curand_normal(&local_state);
                #endif

                rand_states[tid] = local_state;

            }
        }

        __global__ void LLG_predictor(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNGx, const cu_real_t *RNGy, const cu_real_t *RNGz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains)
        {

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];

                // Load spin into registers
                cu_real_t mx = m_x[grain];
                cu_real_t my = m_y[grain];
                cu_real_t mz = m_z[grain];

                cu_real_t Heffx = Fieldx[grain]+RNGx[grain];
                cu_real_t Heffy = Fieldy[grain]+RNGy[grain];
                cu_real_t Heffz = Fieldz[grain]+RNGz[grain];

                cu_real_t MxH_x = (my*Heffz - mz*Heffy);
                cu_real_t MxH_y = (mz*Heffx - mx*Heffz);
                cu_real_t MxH_z = (mx*Heffy - my*Heffx);

                cu_real_t MxMxH_x = (my*MxH_z - mz*MxH_y);
                cu_real_t MxMxH_y = (mz*MxH_x - mx*MxH_z);
                cu_real_t MxMxH_z = (mx*MxH_y - my*MxH_x);

                cu_real_t dmdtx = (-LayerParams[Lid].Gamma/(1.0+(LayerParams[Lid].BathCoupling*LayerParams[Lid].BathCoupling)))*(MxH_x+LayerParams[Lid].BathCoupling*MxMxH_x);
                cu_real_t dmdty = (-LayerParams[Lid].Gamma/(1.0+(LayerParams[Lid].BathCoupling*LayerParams[Lid].BathCoupling)))*(MxH_y+LayerParams[Lid].BathCoupling*MxMxH_y);
                cu_real_t dmdtz = (-LayerParams[Lid].Gamma/(1.0+(LayerParams[Lid].BathCoupling*LayerParams[Lid].BathCoupling)))*(MxH_z+LayerParams[Lid].BathCoupling*MxMxH_z);

                // Store dmdt
                dmx[grain] = dmdtx;
                dmy[grain] = dmdty;
                dmz[grain] = dmdtz;

                // Calculate and store new magnetisation
                cu_real_t new_mx = mx + dmdtx*dt;
                cu_real_t new_my = my + dmdty*dt;
                cu_real_t new_mz = mz + dmdtz*dt;

                #ifdef CUDA_DP
                    double norm = rnorm3d(new_mx,new_my,new_mz);
                #else
                    float norm = rnorm3df(new_mx,new_my,new_mz);
                #endif

                new_mx *= norm;
                new_my *= norm;
                new_mz *= norm;

                m_x[grain] = new_mx;
                m_y[grain] = new_my;
                m_z[grain] = new_mz;

            }
        }

        __global__ void LLG_corrector(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        cu_real_t *m_bufferx, cu_real_t *m_buffery, cu_real_t *m_bufferz,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNGx, const cu_real_t *RNGy, const cu_real_t *RNGz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains)
        {

            for(size_t grain = blockIdx.x * blockDim.x + threadIdx.x; grain < num_grains; grain+=blockDim.x * gridDim.x){
                size_t Lid = Layer[grain];

                // Load spin into registers
                cu_real_t mx = m_x[grain];
                cu_real_t my = m_y[grain];
                cu_real_t mz = m_z[grain];

                cu_real_t Heffx = Fieldx[grain]+RNGx[grain];
                cu_real_t Heffy = Fieldy[grain]+RNGy[grain];
                cu_real_t Heffz = Fieldz[grain]+RNGz[grain];

                cu_real_t MxH_x = (my*Heffz - mz*Heffy);
                cu_real_t MxH_y = (mz*Heffx - mx*Heffz);
                cu_real_t MxH_z = (mx*Heffy - my*Heffx);

                cu_real_t MxMxH_x = (my*MxH_z - mz*MxH_y);
                cu_real_t MxMxH_y = (mz*MxH_x - mx*MxH_z);
                cu_real_t MxMxH_z = (mx*MxH_y - my*MxH_x);

                cu_real_t dmdtx = (-LayerParams[Lid].Gamma/(1.0+(LayerParams[Lid].BathCoupling*LayerParams[Lid].BathCoupling)))*(MxH_x+LayerParams[Lid].BathCoupling*MxMxH_x);
                cu_real_t dmdty = (-LayerParams[Lid].Gamma/(1.0+(LayerParams[Lid].BathCoupling*LayerParams[Lid].BathCoupling)))*(MxH_y+LayerParams[Lid].BathCoupling*MxMxH_y);
                cu_real_t dmdtz = (-LayerParams[Lid].Gamma/(1.0+(LayerParams[Lid].BathCoupling*LayerParams[Lid].BathCoupling)))*(MxH_z+LayerParams[Lid].BathCoupling*MxMxH_z);

                // Calculate and store new magnetisation
                cu_real_t new_mx = m_bufferx[grain] + 0.5 * (dmx[grain] + dmdtx) * dt;
                cu_real_t new_my = m_buffery[grain] + 0.5 * (dmy[grain] + dmdty) * dt;
                cu_real_t new_mz = m_bufferz[grain] + 0.5 * (dmz[grain] + dmdtz) * dt;

                #ifdef CUDA_DP
                    double norm = rnorm3d(new_mx,new_my,new_mz);
                #else
                    float norm = rnorm3df(new_mx,new_my,new_mz);
                #endif

                new_mx *= norm;
                new_my *= norm;
                new_mz *= norm;

                m_x[grain] = new_mx;
                m_y[grain] = new_my;
                m_z[grain] = new_mz;

                // Update magnetisation buffer
                m_bufferx[grain] = new_mx;
                m_buffery[grain] = new_my;
                m_bufferz[grain] = new_mz;

            }
        }
        #endif
    } // namespace kernels
} // namespace cuda