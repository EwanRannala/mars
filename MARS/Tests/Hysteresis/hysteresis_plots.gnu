set term postscript eps color enhanced font "Times-Roman,25"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

LLB = "Output/LLB.dat"
LLG = "Output/LLG.dat"
KMC = "Output/kMC.dat"

set output "Hysteresis_test_LLB.eps"
p LLB i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "16 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg"
  
set output "Hysteresis_test_LLG.eps"
p LLG i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "16 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg"
  
set output "Hysteresis_test_kMC.eps"
p KMC i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "16 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg"
