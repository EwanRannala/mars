set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (degrees)"
set xrange[0:30]
set yrange[0:]
set style fill solid

#===================LLG====================#
#===================300K===================#
#===================10fs===================#
set output "LLG_300K_10fs.eps"
f0 = "Output/LLG_300K_10fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=10fs, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================50fs===================#
set output "LLG_300K_50fs.eps"
f0 = "Output/LLG_300K_50fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=50fs, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================100fs==================#
set output "LLG_300K_100fs.eps"
f0 = "Output/LLG_300K_100fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=100fs, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================500fs==================#
set output "LLG_300K_500fs.eps"
f0 = "Output/LLG_300K_500fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=500fs, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

#===================1ps====================#
set output "LLG_300K_1ps.eps"
f0 = "Output/LLG_300K_1000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=1ps, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

#===================5ps====================#
set output "LLG_300K_5ps.eps"
f0 = "Output/LLG_300K_5000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=15ps, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#===================10ps===================#
set output "LLG_300K_10ps.eps"
f0 = "Output/LLG_300K_10000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=10ps, T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
set xrange[0:180]  
#===================1000K==================#
#===================10fs===================#
set output "LLG_1000K_10fs.eps"
f0 = "Output/LLG_1000K_10fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=10fs, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================50fs===================#
set output "LLG_1000K_50fs.eps"
f0 = "Output/LLG_1000K_50fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=50fs, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================100fs==================#
set output "LLG_1000K_100fs.eps"
f0 = "Output/LLG_1000K_100fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=100fs, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================500fs==================#
set output "LLG_1000K_500fs.eps"
f0 = "Output/LLG_1000K_500fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=500fs, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

#===================1ps====================#
set output "LLG_1000K_1ps.eps"
f0 = "Output/LLG_1000K_1000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=1ps, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

#===================5ps====================#
set output "LLG_1000K_5ps.eps"
f0 = "Output/LLG_1000K_5000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=15ps, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#===================10ps===================#
set output "LLG_1000K_10ps.eps"
f0 = "Output/LLG_1000K_10000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data dt=10ps, T=1000K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'  
  
  
  
#===================LLB====================#
reset
unset key
set ticslevel 0
set view 44,203
set dgrid3d 50,50

set xlabel "m_z"
set ylabel "m_x"
set zlabel "P(m_x,m_z)" rotate by 90 offset 4,0
unset ztics 

set xrange [-1.1:1.1]
set yrange [-1.1:1.1]

#===================660K===================#
#===================0.5fs==================#
f0 = "Output/LLB_660K_0.5fs.dat"
set table "Anyl.tab.local"
sp f0 u ($2):($1):(column(10)/column(11)) w l
unset table
set output "LLB_660K_0.5fs.eps"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================1fs===================#
set output "LLB_660K_1fs.eps"
f0 = "Output/LLB_660K_1fs.dat"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================5fs===================#
set output "LLB_660K_5fs.eps"
f0 = "Output/LLB_660K_5fs.dat"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================10fs==================#
set output "LLB_660K_10fs.eps"
f0 = "Output/LLB_660K_10fs.dat"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================1000K==================#
#===================0.5fs==================#
f0 = "Output/LLB_1000K_0.5fs.dat"
set table "Anyl1000.tab.local"
sp f0 u ($2):($1):(column(10)/column(11)) w l
unset table
set output "LLB_1000K_0.5fs.eps"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl1000.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================1fs===================#
set output "LLB_1000K_1fs.eps"
f0 = "Output/LLB_1000K_1fs.dat"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl1000.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================5fs===================#
set output "LLB_1000K_5fs.eps"
f0 = "Output/LLB_1000K_5fs.dat"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl1000.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
#===================10fs==================#
set output "LLB_1000K_10fs.eps"
f0 = "Output/LLB_1000K_10fs.dat"
sp f0 u 2:1:( ($3/$5*$4/$5)) w l,\
   "Anyl1000.tab.local" u ($2==0.0160714?$1:0/0):(0):($3) w l lc -1 lw 2,\
   '' u (0):($1==0.0160714?$2:0/0):($3) w l lc -1 lw 2
