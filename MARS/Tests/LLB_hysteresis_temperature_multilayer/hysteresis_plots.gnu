# set term postscript eps color enhanced font "Times-Roman,25"
# set output "LLB_hysteresis_test.eps"
# unset key
# 
# set ylabel "M.H" offset 2,0
# set xlabel "H_{appl}/H_{k}"
# set xrange [-1.5:1.5]
# set yrange [-1.1:1.1]
# set grid xtics ytics mxtics mytics
# 
# f0 = "Output/LLB.dat"
# 
# 
# p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
#   '' i 1 u ($10/$11):($16) w l t "75 Deg",\
#   '' i 2 u ($10/$11):($16) w l t "60 Deg",\
#   '' i 3 u ($10/$11):($16) w l t "45 Deg",\
#   '' i 4 u ($10/$11):($16) w l t "30 Deg",\
#   '' i 5 u ($10/$11):($16) w l t "15 Deg",\
#   '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
###################################################################
reset
set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_hysteresis_test_0K.eps"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLB0K.dat"


p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "15 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
###################################################################
reset
set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_hysteresis_test_100K.eps"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLB100K.dat"


p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "15 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
###################################################################
reset
set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_hysteresis_test_300K.eps"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLB300K.dat"


p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "15 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
###################################################################
reset
set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_hysteresis_test_500K.eps"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLB500K.dat"


p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "15 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
###################################################################
reset
set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_hysteresis_test_600K.eps"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLB600K.dat"


p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "15 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
###################################################################
reset
set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_hysteresis_test_700K.eps"
unset key

set ylabel "M.H" offset 2,0
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLB700K.dat"


p f0 i 0 u ($10/$11):($16) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($16) w l t "75 Deg",\
  '' i 2 u ($10/$11):($16) w l t "60 Deg",\
  '' i 3 u ($10/$11):($16) w l t "45 Deg",\
  '' i 4 u ($10/$11):($16) w l t "30 Deg",\
  '' i 5 u ($10/$11):($16) w l t "15 Deg",\
  '' i 6 u ($10/$11):($16) w l t "0.01 Deg",\
