/*
 * Generate_interactions.hpp
 *
 *  Created on: 9 Feb 2019
 *      Author: Ewan Rannala
 */

/** \file Generate_interactions.hpp
 * \brief Header file for interactions generation function. */

#ifndef INTERACTIONS_AND_NEIGHBOURS_HPP_
#define INTERACTIONS_AND_NEIGHBOURS_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Generate_interactions(const ConfigFile& cfg, unsigned int Num_layers, const Material_t& MAT,
                                 const Voronoi_t& VORO, std::vector<Grain_t>& Grains, Interaction_t& Int_system);

#endif /* INTERACTIONS_AND_NEIGHBOURS_HPP_ */
