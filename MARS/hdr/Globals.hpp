/* Globals.hpp
 *  Created on: 3 Aug 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Globals.hpp
 * \brief Header file for global variables. */

#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_

#include <random>
#include <string>

#include "Io/Logger/Log.hpp"

extern const double M_2PI;    //!< 2*PI constant
extern const double M_PI_180; //!< PI/180 constant
extern const double M_180_PI; //!< 180/PI constant

extern const double KB;  //!< Boltzmann constant in CGS units
extern const double two_KB;  //!< Boltzmann constant multiplied by two in CGS units

extern std::mt19937_64 Gen; //!< Mersenne twister pseudorandom number generator
extern std::mt19937_64 VoroGen; //!< Mersenne twister pseudorandom number generator for Voronoi tesellation

extern std::string InputcfgFile; //!< String defining the input configuration file name

extern std::string Githash; //!< Githash to keep track of version

extern std::string Delim; //!< Delimiter to be used when data is output
extern std::string OutDir; //!< Directory used for output data

#endif /* GLOBALS_HPP_ */
