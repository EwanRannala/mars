/*
 * HAMR_switching_output.hpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

/** \file HAMR_switching_output.hpp
 * \brief Header file for HAMR output functions related to grain switching. */

#ifndef HAMR_SWITCHING_OUTPUT_HPP_
#define HAMR_SWITCHING_OUTPUT_HPP_

#include "Structures.hpp"

extern int HAMR_Switching_BUFFER(unsigned int Num_layers, const Voronoi_t& VORO, const std::vector<Grain_t>& Grains, const std::vector<double>& dz, std::vector<std::string>*BUFFER);

extern int HAMR_Switching_OUTPUT(unsigned int Num_layers, unsigned int Num_Grains, const std::vector<Grain_t>& Grains, const std::vector<std::string>& BUFFER,const std::string& OUTPUT);

#endif /* HAMR_SWITCHING_OUTPUT_HPP_ */
