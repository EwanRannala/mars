/*
 * HAMR_grain_output.hpp
 *
 *  Created on: 5 Oct 2018
 *      Author: ewan
 */

/** \file HAMR_grain_output.hpp
 * \brief Header file for HAMR output functions related to individual grain data. */

#ifndef HAMR_GRAIN_OUTPUT_HPP_
#define HAMR_GRAIN_OUTPUT_HPP_

#include <string>
#include "Structures.hpp"

extern int Idv_Grain_output(unsigned int Num_Layers, const Voronoi_t& VORO, const std::vector<Grain_t>& Grains, double Time, const std::string& OUTPUT,std::string OUTPUT_PRE="");

#endif /* HAMR_GRAIN_OUTPUT_HPP_ */
