/*
 * HAMR_layer_output.hpp
 *
 *  Created on: 8 Oct 2018
 *      Author: ewan
 */

/** \file HAMR_layer_output.hpp
 * \brief Header file for HAMR output functions related to entire layer averages. */

#ifndef HAMR_LAYER_OUTPUT_HPP_
#define HAMR_LAYER_OUTPUT_HPP_

#include "Structures.hpp"

extern int HAMR_layer_averages(unsigned int Num_layers, unsigned int Num_Grains, const std::vector<Grain_t>& Grains, double Field_MAG, double Temperature,
                        double Time, const std::vector<unsigned int>& Grain_list, const std::string& OUTPUT);

#endif /* HAMR_LAYER_OUTPUT_HPP_ */
