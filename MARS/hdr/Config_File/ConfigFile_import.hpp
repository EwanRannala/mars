/* ConfigFile_import.hpp
 *  Created on: 27 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file ConfigFile_import.hpp
 * \brief Header file for the ConfigFile class. */

#ifndef CONFIGFILE_IMPORT_HPP_
#define CONFIGFILE_IMPORT_HPP_

#include <string>
#include <map>
#include <optional>
#include <tuple>
#include <typeinfo>
#include <type_traits>


#include "Convert.hpp"
#include "Io/Logger/Global_logger.hpp"


/** \brief Class containing all parameter data from the configuration file.
 *
 * This class contains a hash table which stores all configuration parameters.
 * The configuration file is read and the hash table is generated on construction.
 */

class ConfigFile
{
private:

    typedef std::tuple<std::string, std::string, unsigned int> KeyValues_t;

    std::map<std::string, KeyValues_t> Contents; //!< Mapping of data and configuration file key.
    std::string FileName;                               //!< Configuration file name.

    static void removeComment(std::string& line);

    static void lowerCase(std::string & line);

    void KeepCase(std::string & line, size_t lineNum) const;

    static bool onlyWhitespace(const std::string& line);

    void FormatVector(std::string & line, size_t lineNum) const;

    bool validLine(const std::string &line, unsigned int lineNum);

    static void extractKey(std::string& key, size_t const& separator_pos, const std::string& line);

    static void extractValueAndUnit(std::string& value, std::string& unit, size_t const& separator_pos, const std::string& line);

    void extractContents(const std::string& line, unsigned int lineNum);

    void parseLine(std::string& line, unsigned int lineNum);

    bool keyExists(const std::string& key) const;

    void ExtractKeys();

    double ConvertUnit(const std::string& unit) const;

    template <typename T>
    struct is_vector : std::false_type {};
    template <typename T>
    struct is_vector<std::vector<T>> : std::true_type {};


public:
    ConfigFile(std::string InFileName);

    template <typename ValueType>
    ValueType getValueOfKey(const std::string&key) const;

    template <typename ValueType>
    std::optional<ValueType> getValueOfOptKey(const std::string&key) const;

    template <typename ValueType>
    std::optional<std::pair<ValueType,unsigned int>> getValueOfOptKeyWLineNum(const std::string&key) const;

    template<class ValueType>
    std::enable_if_t<!is_vector<ValueType>::value, ValueType> getValAndUnitOfKey(const std::string &key) const;
    template<class ValueType>
    std::enable_if_t< is_vector<ValueType>::value, ValueType> getValAndUnitOfKey(const std::string &key) const;
 
    template<class ValueType>
    std::optional<std::enable_if_t<!is_vector<ValueType>::value, ValueType>> getValAndUnitOfOptKey(const std::string &key) const;
    template<class ValueType>
    std::optional<std::enable_if_t< is_vector<ValueType>::value, ValueType>> getValAndUnitOfOptKey(const std::string &key) const;

};



#endif /* CONFIGFILE_IMPORT_HPP_ */
