/* Lognormal_distribution_generator.hpp
 *  Created on: 15 March 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Lognormal_distribution_generator.hpp
 * @brief Header file for the lognormal distribution generator function.
 */

#ifndef LOGNORMAL_DISTRIBUTION_GENERATOR_HPP_
#define LOGNORMAL_DISTRIBUTION_GENERATOR_HPP_

std::vector<double> Lognormal_generator(double mu, double sigma, unsigned int size=1);

#endif
