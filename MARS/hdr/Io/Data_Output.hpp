/*  Data_Output.hpp
 *  Created on: 29 Jun 2022
 *      Author: Samuel Ewan Rannala
 */

#ifndef DATA_OUTPUT_HPP_
#define DATA_OUTPUT_HPP_

#include <fstream>
#include <vector>
#include <string>

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"

class Data_output_t{

private:

    using OutFields =    enum {
        Magnetisation,
        Magnetisation_Normalised,
        Magnetisation_Length,
        Layer_Magnetisation,
        Layer_Magnetisation_Normalised,
        Layer_Magnetisation_Length,
        Easy_axis,
        Temperature,
        K,
        K_stddev,
        Kt,
        Kt_stddev,
        Ms,
        Ms_stddev,
        Mst,
        Mst_stddev,
        Hk,
        Hk_stddev,
        Hkt,
        Hkt_stddev,
        Tc,
        Tc_stddev,
        H_appl,
        H_ani,
        H_magneto,
        H_exchange,
        H_eff,
        Vol,
        Time
    };

    using Format = enum {
        Gnuplot,
        JMP
    };

    std::ofstream MainDataFile;           //!< Main data output file
    std::ofstream GrainDataFile;          //!< Individual grain data output file

    std::string MainFileNamePrefix;      //!< Filename prefix for Main data output file
    std::string GrainFileNamePrefix;     //!< Filename prefix for individual grain data output file
    std::string Delimiter;               //!< Delimiter of output data
    std::string FileExtension;           //!< Output File extension
    unsigned int GrainOutNum;                     //!< Counter for the number of previous individual grain data output files
    std::string MainFileName;            //!< Filename for Main data output file
    std::string GrainFileName;           //!< Filename for individual grain data output file

    unsigned int Width;                  //!< Width of output data for fixed width output
    unsigned int Precision;              //!< Precision of output data
    std::vector<OutFields> Main_output;  //!< List of desired output fields for main data output
    std::vector<OutFields> Grain_output; //!< List of desired output fields for individual grain output
    bool MainHeader;                     //!< Flag to control the output of column headers for the main output file
    bool GrainHeader;                    //!< Flag to control the output of column headers for individual grain files

    bool GrainOutput;                              //!< Flag to enable/disable individual grain output
    unsigned int MainOutCounter;                  //!< Counter to keep track of when to write Main data
    unsigned int GrainOutCounter;                 //!< Counter to keep track of when to write individual grain data
    unsigned int MainOutRate;                     //!< Output rate for main data output
    unsigned int GrainOutRate;                    //!< Output rate for individual Grain data output

    Format OutFmt;                       //!< Desired file format
    bool GrainwVert;                     //!< Flag to control the output of the individual grain data into per vertex (true) or per grain (false)

    std::string Outputdirectory;         //!< Output Directory name


    void DesiredInput(const ConfigFile& cfg);
    void ChangeGrainFile();

    void DesiredOutput(const std::vector<Grain_t>& Grains, std::stringstream& OutStr, bool MainOutput);
    template <typename T>
    void output_value(const std::string& ValName, const T Val, std::stringstream& stream, bool header);
    void FileOutput(const std::stringstream& OutStr, bool MainOut);

    static bool LineSort(const std::pair<OutFields,unsigned int> &i,
                const std::pair<OutFields,unsigned int> &j){ return (i.second<j.second); }
public:

    Data_output_t(const std::string& OutDir, const ConfigFile& cfg);
    ~Data_output_t();

    Data_output_t(const Data_output_t&);
    Data_output_t& operator=(const Data_output_t&);
    bool PrintNow(bool force);
    void PrintData(const std::vector<Grain_t>& Grains, bool force=false);
};



#endif /* DATA_OUTPUT_HPP_ */