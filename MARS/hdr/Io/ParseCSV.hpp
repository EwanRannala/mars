#ifndef PARSECSV_HPP_
#define PARSECSV_HPP_

#include <string>
#include <vector>

extern void ParseCSV(std::string FileName, std::vector<int> Cols, bool Headers, std::vector<std::vector<double>>& OutVec);

#endif /*PARSECSV_HPP_*/