/* Global_logger.hpp
 *  Created on: 24 Feb 2022
 *      Author: Samuel Ewan Rannala
 */

/** \file Global_logger.hpp
 * \brief Header file for global logger. */

#ifndef GLOBAL_LOGGER_HPP_
#define GLOBAL_LOGGER_HPP_

#include "Log.hpp"

extern Logger_t Log;             //!< Logging class

#endif /* GLOBAL_LOGGER_HPP_ */
