/*  Logger.hpp
 *  Created on: 23 Feb 2022
 *      Author: Samuel Ewan Rannala
 */

#ifndef LOG_HPP_
#define LOG_HPP_

#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <time.h>
#include <limits>
#include <iterator>

#ifdef WINDOWS
    #include <process.h>
    #include <windows.h>
    #include <direct.h>
#else
    #include <unistd.h>
#endif

#include "Globals.hpp"

#define BURed(text)      "\033[1;4;31m" text "\033[0m"
#define BUYellow(text)   "\033[1;4;33m" text "\033[0m"
#define BYellow(text)    "\033[1;33m" text "\033[0m"
#define BGreen(text)     "\033[1;32m"  text "\033[0m"
#define BCyan(text)      "\033[1;36m"  text "\033[0m"

class Logger_t{

public:

    using LogLvl = enum {
        SILENT,
        ERR,
        WARN,
        INFO,
        TIME,
        DEBUG
    };

    std::ofstream Logfile;
    const std::string RST="@\033[0m@";
    const std::string BUR="@\033[1;4;31m@";
    const std::string BUY="@\033[1;4;33m@";
    const std::string BG ="@\033[1;32m@";
    const std::string BC ="@\033[1;36m@";
    const std::string BY ="@\033[1;33m@";
    const std::string RSTraw="\033[0m";
    const std::string BURraw="\033[1;4;31m";
    const std::string BUYraw="\033[1;4;33m";
    const std::string BGraw ="\033[1;32m";
    const std::string BCraw ="\033[1;36m";
    const std::string BYraw ="\033[1;33m";

    Logger_t();
    ~Logger_t();

    // This class is not copyable
    Logger_t(const Logger_t&);
    Logger_t& operator=(const Logger_t&);

    void Initialise(std::string ExecCmd,bool main="false",LogLvl lvl=INFO);
    void SetLevel(LogLvl Lvl);
    void log(LogLvl MessageLvl,std::string Message);
    void logandshow(LogLvl MessageLvl,std::string Message);
    std::string Name() const;


private:
    LogLvl LogLevel;
    std::string Delim;
    std::string Exec;
    std::string Hostname;
    std::string Directory;
    std::string TimeStr;
    std::string LvlStr;
    std::string LogFileName;
    static const std::string SelfLvl;
    bool Initialised;

    void SetLevelStr(LogLvl Lvl);
    void Write(std::string output);

};

// Function to enable specify formatted output of numeric values via a string
template <typename T>
extern std::string to_string_exact(T x);

extern std::string to_string_exact(double x);


#endif /* LOG_HPP_ */