/* Materials_import_Test_LLB.hpp
 *  Created on: 29 Jul 2018
 *      Author: Andrea Meo, Samuel Ewan Rannala
 */

/** \file Materials_import_Test_LLB.hpp
 * \brief Header file for Materials import for LLB tests function. */

#ifndef MATERIALS_IMPORT_HPP_
#define MATERIALS_IMPORT_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Materials_import_Test_LLB(const ConfigFile& test_cfg, unsigned int Num_Layers, std::vector<ConfigFile>*Material_Config, Material_t*Material);

#endif /* MATERIALS_IMPORT_HPP_ */
