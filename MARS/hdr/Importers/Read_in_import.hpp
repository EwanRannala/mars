/*
 * Read_in_import.hpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Read_in_import.hpp
 * \brief Header file for read in import function. */

#ifndef READ_IN_IMPORT_HPP_
#define READ_IN_IMPORT_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Read_in_import(const ConfigFile& CFG, Data_input_t*Data_in);

#endif /* READ_IN_IMPORT_HPP_ */
