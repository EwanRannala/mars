/* Grain_setup.hpp
 *  Created on: 16 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Grain_setup.hpp
 * \brief Header file for grain construction function. */

#ifndef GRAIN_SETUP_HPP_
#define GRAIN_SETUP_HPP_

#include <string>
#include "Structures.hpp"

extern int Grain_setup(unsigned int Num_grains,unsigned int Num_Layers,
                        const Material_t& Material, const std::vector<double>& Grain_area,
                        const std::vector<double>& Grain_diameter, double Temperature, std::vector<Grain_t>& Grains);


extern int Grain_setup(const unsigned int Num_Layers, const Voronoi_t& Voro,
                       const Material_t& Material, const double Temperature, std::vector<Grain_t>& Grains);

#endif /* GRAIN_SETUP_HPP_ */
