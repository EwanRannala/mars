/* Materials_import.hpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Materials_import.hpp
 * \brief Header file for Materials import function. */

#ifndef MATERIALS_IMPORT_HPP_
#define MATERIALS_IMPORT_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Materials_import(const ConfigFile& cfg,unsigned int Num_Layers, std::vector<ConfigFile>*Material_Config, Material_t*Material);

#endif /* MATERIALS_IMPORT_HPP_ */
