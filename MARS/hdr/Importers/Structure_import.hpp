/* Structure_import.hpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Structure_import.hpp
 * \brief Header file for Structure import function. */

#ifndef PARAMETER_IMPORT_HPP_
#define PARAMETER_IMPORT_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Structure_import(const ConfigFile& cfg, Structure_t*Structure);

#endif /* PARAMETER_IMPORT_HPP_ */
