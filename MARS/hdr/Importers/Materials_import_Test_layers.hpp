/* Materials_import_test_layers.hpp
 *  Created on: 29 Jul 2018
 *      Author: Andrea Meo, Samuel Ewan Rannala
 */

/** \file Materials_import_Test_layers.hpp
 * \brief Header file for Material layers import for LLB tests function. */


#ifndef MATERIALS_IMPORT_TEST_LAYERS_HPP_
#define MATERIALS_IMPORT_TEST_LAYERS_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Materials_import_Test_layers(const std::string& FILE_LOC, unsigned int Num_Layers, std::vector<ConfigFile>*Material_Config, Material_t*Material);

#endif /* MATERIALS_IMPORT_TEST_LAYERS_HPP_ */
