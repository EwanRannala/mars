/*
 * Import_structure.hpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Import_structure.hpp
 * \brief Header file for granular generation from input files function. */

#ifndef IMPORT_STRUCTURE_HPP_
#define IMPORT_STRUCTURE_HPP_

#include "Structures.hpp"
#include "Classes/Solver.hpp"

extern int Read_in_structure(const Data_input_t IN_data, Structure_t& Structure, Voronoi_t& VORO);

#endif /* IMPORT_STRUCTURE_HPP_ */
