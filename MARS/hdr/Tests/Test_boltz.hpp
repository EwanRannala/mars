/* Test_boltz.hpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Test_boltz.hpp
 * \brief Header file for LLB Boltzmann distributions test. */

#ifndef TEST_BOLTZ_HPP_
#define TEST_BOLTZ_HPP_

extern int Test_boltz();

#endif /* TEST_BOLTZ_HPP_ */
