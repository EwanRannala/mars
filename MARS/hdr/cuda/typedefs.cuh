#ifndef TYPEDEFS_CUH_
#define TYPEDEFS_CUH_

#ifdef CUDA
    #include <curand.h>
    #include <curand_kernel.h>
    #include <cuda_runtime.h>
#endif

namespace cuda{

    #ifdef CUDA
        #ifdef CUDA_DP
            typedef double cu_real_t;
        #else
            typedef float cu_real_t;
        #endif
    #else
        typedef double cu_real_t;
    #endif

}   // namespace cuda

#endif /* TYPEDEFS_CUH_ */