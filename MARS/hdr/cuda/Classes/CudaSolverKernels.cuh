#ifndef CUDASOLVERKERNELS_CUH_
#define CUDASOLVERKERNELS_CUH_

#include "Globals.hpp"

#include "cuda/Classes/CudaSolver.cuh"

namespace cuda
{
    namespace kernels
    {

        __global__ void RNG_init(curandState *rand_states, int seed);

        //########################## FIELDS ##########################//
        /* Thus function sets the field values and so it should be called first.*/
        __global__ void update_applied_field(const cu_real_t *H_appx, const cu_real_t *H_appy, const cu_real_t *H_appz,
                                cu_real_t *Fieldx, cu_real_t *Fieldy, cu_real_t *Fieldz, const size_t num_grains);

        __global__ void update_anisotropy_field(const int* Layer, const layer_params_t *LayerParams,
                                    const cu_real_t *m_x, const cu_real_t *m_y, const cu_real_t *m_z,
                                    const cu_real_t *EA_x, const cu_real_t *EA_y, const cu_real_t *EA_z,
                                    const cu_real_t *Ms, const cu_real_t *K, const cu_real_t *Temperature,
                                    const cu_real_t *Chi_perp, const cu_real_t *m_EQ,
                                    cu_real_t *Fieldx, cu_real_t *Fieldy, cu_real_t *Fieldz,
                                    const Solvers SolverType, const size_t num_grains);

        __global__ void update_internal_exchange_field(const cu_real_t *m_x, const cu_real_t *m_y, const cu_real_t *m_z,
                                                        const cu_real_t *m_EQ, const cu_real_t *Chi_para, const cu_real_t *Tc,
                                                        const cu_real_t *Temperature, cu_real_t *Fieldx,
                                                        cu_real_t *Fieldy, cu_real_t *Fieldz, const size_t num_grains);

        __global__ void update_exchange_field(const int *Num_Neighbours, const int *Neighbours,
                                const cu_real_t *H_exch_str, const cu_real_t *m_EQ,
                                const cu_real_t *m_x, const cu_real_t *m_y, const cu_real_t *m_z,
                                cu_real_t *Fieldx, cu_real_t *Fieldy, cu_real_t *Fieldz, const size_t num_grains);

        __global__ void update_magnetostatic_field();

        //########################## MISC ##########################//
        __global__ void Calc_mEQ(const int *Layer, const layer_params_t *LayerParams, const cu_real_t *Temperature,
                                const cu_real_t *Tc, cu_real_t *m_EQ, const size_t num_grains);

        __global__ void Calc_Damping(const int *Layer, const cu_real_t *Temperature, const cu_real_t *Tc,
                                    const layer_params_t *LayerParams, cu_real_t *Alpha_Para,
                                    cu_real_t *Alpha_Perp, const size_t num_grains);

        __global__ void Calc_Susceptibility(const cu_real_t *Temperature, const cu_real_t *Tc, const layer_params_t *LayerParams,
                                            const int *Layer, cu_real_t *Chi_para, cu_real_t *Chi_perp, const cu_real_t *m_EQ,
                                            const cu_real_t *chi_para_scaling, const cu_real_t *chi_perp_scaling, const size_t num_grains);

        __global__ void Calc_Tdependence(const int *Layer, const layer_params_t *LayerParams, const cu_real_t *Temperature, const cu_real_t *m_EQ,
                                         const cu_real_t *K0K, const cu_real_t *Ms0K, cu_real_t *K, cu_real_t *Ms, const size_t num_grains);

        //########################## kMC ##########################//
        __global__ void kMC_core(const int* Layer, const layer_params_t *LayerParams,
                            const cu_real_t *EA_x, const cu_real_t *EA_y, const cu_real_t *EA_z,
                            const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                            const cu_real_t *K, const cu_real_t *Vol, const cu_real_t *Temperature, const cu_real_t *Ms,
                            curandState *rand_states, cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                            const cu_real_t dt, const size_t num_grains);

        //########################## LLG ##########################//
        __global__ void LLG_RNG(const int *layer_id, const layer_params_t *LayerParams, const cu_real_t *Temperature,
                                const cu_real_t *Ms, const cu_real_t *Vol,
                                cu_real_t *RNGx, cu_real_t *RNGy, cu_real_t *RNGz,
                                curandState *rand_states, const cu_real_t dt, const size_t num_grains);

        __global__ void LLG_predictor(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNGx, const cu_real_t *RNGy, const cu_real_t *RNGz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains);

        __global__ void LLG_corrector(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        cu_real_t *m_bufferx, cu_real_t *m_buffery, cu_real_t *m_bufferz,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNGx, const cu_real_t *RNGy, const cu_real_t *RNGz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains);

        //########################## LLB ##########################//
        __global__ void LLB_RNG(const int *layer_id, const layer_params_t *LayerParams, const cu_real_t *Temperature,
                                const cu_real_t *Alpha_Para, const cu_real_t *Alpha_Perp, const cu_real_t *Ms,
                                const cu_real_t *Vol, cu_real_t *RNG_PARAx, cu_real_t *RNG_PARAy,
                                cu_real_t *RNG_PARAz, cu_real_t *RNG_PERPx, cu_real_t *RNG_PERPy,
                                cu_real_t *RNG_PERPz, curandState *rand_states,
                                const cu_real_t dt, const size_t num_grains);

        __global__ void LLB_predictor(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        const cu_real_t *Alpha_Para, const cu_real_t *Alpha_Perp,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNG_PARAx, const cu_real_t *RNG_PARAy, const cu_real_t *RNG_PARAz,
                                        const cu_real_t *RNG_PERPx, const cu_real_t *RNG_PERPy, const cu_real_t *RNG_PERPz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains);

        __global__ void LLB_corrector(const int* Layer, const layer_params_t *LayerParams,
                                        cu_real_t *m_x, cu_real_t *m_y, cu_real_t *m_z,
                                        cu_real_t *m_bufferx, cu_real_t *m_buffery, cu_real_t *m_bufferz,
                                        const cu_real_t *Alpha_Para, const cu_real_t *Alpha_Perp,
                                        const cu_real_t *Fieldx, const cu_real_t *Fieldy, const cu_real_t *Fieldz,
                                        const cu_real_t *RNG_PARAx, const cu_real_t *RNG_PARAy, const cu_real_t *RNG_PARAz,
                                        const cu_real_t *RNG_PERPx, const cu_real_t *RNG_PERPy, const cu_real_t *RNG_PERPz,
                                        cu_real_t *dmx, cu_real_t *dmy, cu_real_t *dmz,
                                        const cu_real_t dt, const size_t num_grains);

    } // namespace kernels
} // namespace cuda

#endif /* CUDASOLVERKERNELS_CUH_ */