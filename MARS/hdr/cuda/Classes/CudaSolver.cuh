#ifndef CUDASOLVER_CUH_
#define CUDASOLVER_CUH_

#include <vector>

#include "Simulation_globals.hpp"
#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Io/Data_Output.hpp"

#include "cuda/typedefs.cuh"

namespace cuda{

    #ifdef CUDA
        struct layer_params_t {
            cu_real_t f0;
            cu_real_t Gamma;
            cu_real_t BathCoupling;

            Material_t::AniMethods Anisotropy_method;
            Material_t::CallenMethods Callen_method;
            cu_real_t Callen_range_lowT;
            cu_real_t Callen_range_midT;
            cu_real_t Callen_factor_lowT;
            cu_real_t Callen_factor_midT;
            cu_real_t Callen_factor_highT;
            cu_real_t Callen_power_lowT;
            cu_real_t Callen_power_midT;
            cu_real_t Callen_power_highT;

            Material_t::SusTypes Susceptibility_Type;
            cu_real_t Chi_scaling_factor;
            cu_real_t t_a0_PARA;
            cu_real_t t_a1_PARA;
            cu_real_t t_a2_PARA;
            cu_real_t t_a3_PARA;
            cu_real_t t_a4_PARA;
            cu_real_t t_a5_PARA;
            cu_real_t t_a6_PARA;
            cu_real_t t_a7_PARA;
            cu_real_t t_a8_PARA;
            cu_real_t t_a9_PARA;
            cu_real_t t_a1_2_PARA;
            cu_real_t t_b0_PARA;
            cu_real_t t_b1_PARA;
            cu_real_t t_b2_PARA;
            cu_real_t t_b3_PARA;
            cu_real_t t_b4_PARA;

            cu_real_t t_a0_PERP;
            cu_real_t t_a1_PERP;
            cu_real_t t_a2_PERP;
            cu_real_t t_a3_PERP;
            cu_real_t t_a4_PERP;
            cu_real_t t_a5_PERP;
            cu_real_t t_a6_PERP;
            cu_real_t t_a7_PERP;
            cu_real_t t_a8_PERP;
            cu_real_t t_a9_PERP;
            cu_real_t t_a1_2_PERP;
            cu_real_t t_b0_PERP;
            cu_real_t t_b1_PERP;
            cu_real_t t_b2_PERP;
            cu_real_t t_b3_PERP;
            cu_real_t t_b4_PERP;

            Material_t::mEQTypes mEQtype;
            cu_real_t Crit_exp;
            cu_real_t a0_mEQ;
            cu_real_t a1_mEQ;
            cu_real_t a2_mEQ;
            cu_real_t a3_mEQ;
            cu_real_t a4_mEQ;
            cu_real_t a5_mEQ;
            cu_real_t a6_mEQ;
            cu_real_t a7_mEQ;
            cu_real_t a8_mEQ;
            cu_real_t a9_mEQ;
            cu_real_t a1_2_mEQ;
            cu_real_t b1_mEQ;
            cu_real_t b2_mEQ;
        };

        enum struct Solvers {
            kMC,    //!< Kinetic Monte Carlo solver
            LLB,    //!< Landau-Lifshitz-Bloch solver
            LLG     //!< Landau-Lifshitz-Gilbert solver
        };

        class CudaSolver_t {

            public:

            CudaSolver_t(const ConfigFile& cfg, const std::vector<ConfigFile>& Materials_config, const Interaction_t& Int_system);
            ~CudaSolver_t();

            void Integrate(std::vector<Grain_t>& Grains);
            void Integrate(std::vector<Grain_t>& Grains, Data_output_t& DataOut, bool ForceTransfer=false);

            void CopyToHost(std::vector<Grain_t>& Grains);
            void CopyChiParaToHost(std::vector<cu_real_t>& hst_data);
            void CopyChiPerpToHost(std::vector<cu_real_t>& hst_data);

            //~~~~~~~~~~~~~~~ Getters ~~~~~~~~~~~~~~~//
            Solvers get_Solver() const;
            std::vector<cu_real_t> get_Gamma() const;
            std::vector<cu_real_t> get_Alpha() const;
            cu_real_t get_dt() const;

            //~~~~~~~~~~~~~~~ Setters ~~~~~~~~~~~~~~~//
            void Reinitialise();
            void set_dt(const cu_real_t dt);

            private:

            void Cuda_initialise();
            void Set_Susceptibility_params(const std::vector<ConfigFile>& Materials_config);
            void Set_Exchange_params(const Interaction_t& Int_system);
            void Cuda_allocate();
            void Cuda_synchronise();

            //~~~~~~~~~~~~~~ Data transfer ~~~~~~~~~~~~~//
            void Update_Field_and_Temp(std::vector<Grain_t>& Grains);

            //~~~~~~~~~~ Integrator functions ~~~~~~~~~~//
            void kMC_init(std::vector<Grain_t>& Grains);
            void kMC(std::vector<Grain_t>& Grains);
            void kMC_core();

            void LLG_init(std::vector<Grain_t>& Grains);
            void LLG(std::vector<Grain_t>& Grains);
            void LLG_predictor();
            void LLG_corrector();
            void LLG_RNG();

            void LLB_init(std::vector<Grain_t>& Grains);
            void LLB(std::vector<Grain_t>& Grains);
            void LLB_predictor();
            void LLB_corrector();
            void LLB_RNG();
            //==========================================//

            void Calc_mEQ();
            void Calc_Susceptibility();
            void Calc_Damping();
            void Calc_Tdependence();

            //~~~~~~~~~~~~ Field functions ~~~~~~~~~~~~~//
            void update_internal_exchange_field();
            void update_anisotropy_field();
            void update_exchange_field();
            void update_magnetostatic_field();
            void update_applied_field();
            //==========================================//


            size_t grid_size;
            size_t block_size;
            size_t Num_grains;
            size_t Num_Layers;

            Solvers SolverType;

            bool T_dep;
            int SEED;
            curandState *dev_rand_states;

            std::vector<layer_params_t> hst_LayerParams;
            layer_params_t *dev_LayerParams;

            int *dev_Num_Neighbours;
            int *dev_Neighbours;

            cu_real_t *dev_H_exch_str;

            cu_real_t *dev_H_ani;

            cu_real_t *dev_EA_x;
            cu_real_t *dev_EA_y;
            cu_real_t *dev_EA_z;

            // Does not need data from cpu
            cu_real_t *dev_mEQ;

            // Does not need data from cpu
            cu_real_t *dev_m_x;
            cu_real_t *dev_m_y;
            cu_real_t *dev_m_z;

            cu_real_t *dev_m_bufferx;
            cu_real_t *dev_m_buffery;
            cu_real_t *dev_m_bufferz;

            // Does not need data from cpu
            cu_real_t *dev_dmx;
            cu_real_t *dev_dmy;
            cu_real_t *dev_dmz;

            // TODO - method to update applied field
            cu_real_t *dev_H_appx;
            cu_real_t *dev_H_appy;
            cu_real_t *dev_H_appz;

            // Does not need data from cpu
            cu_real_t *dev_Fieldx;
            cu_real_t *dev_Fieldy;
            cu_real_t *dev_Fieldz;

            // Does not need data from cpu
            cu_real_t *dev_Alpha_Para;
            cu_real_t *dev_Alpha_Perp;

            // Does not need data from cpu
            cu_real_t *dev_Chi_para;
            cu_real_t *dev_Chi_perp;

            cu_real_t *dev_K;
            cu_real_t *dev_Ms;
            cu_real_t *dev_Vol;
            cu_real_t *dev_Temperature;
            cu_real_t *dev_Tc;

            // These store K(0K) and Ms(0K) for T dependent LLG/kMC
            cu_real_t *dev_K_store;
            cu_real_t *dev_Ms_store;

            // Does not need data from cpu
            cu_real_t *dev_RNG_PARAx;
            cu_real_t *dev_RNG_PARAy;
            cu_real_t *dev_RNG_PARAz;
            cu_real_t *dev_RNG_PERPx;
            cu_real_t *dev_RNG_PERPy;
            cu_real_t *dev_RNG_PERPz;

            cu_real_t *dev_chi_para_scaling_factor;
            cu_real_t *dev_chi_perp_scaling_factor;

            int *dev_LayerID;

            bool Initialised;

            cu_real_t dt_kMC;
            cu_real_t dt_LLG;
            cu_real_t dt_LLB;
        };
    #endif
} //namespace

#endif /* CUDASOLVER_CUH_ */