#ifndef ERROR_CHECK_CUH_
#define ERROR_CHECK_CUH_

#include <string>

#include "Io/Logger/Global_logger.hpp"

#include <cuda_runtime.h>

#ifdef CUDA
    #define CHECK_CUDA_ERROR(val) cuda::ErrCheck((val), #val, __FILE__, __LINE__)
    #define CHECK_CUDA_ERROR_NOABORT(val) cuda::ErrCheck((val), #val, __FILE__, __LINE__,false)
    #define CHECK_LAST_CUDA_ERROR() cuda::checkLast(__FILE__, __LINE__)
#endif

namespace cuda
{
    #ifdef CUDA
        inline void ErrCheck(cudaError_t error, const char* const function, const char* const file, const int line, const bool abort=true){

            if(abort){
                if(error != cudaSuccess){
                    if(function!=NULL){
                        Log.log(Logger_t::LogLvl::ERR,"CUDA Runtime Error at: "+std::string(file)+" : "+to_string_exact(line)+"\n\t"
                                                        +cudaGetErrorString(error)+" "+std::string(function));
                    }
                    else{
                        Log.log(Logger_t::LogLvl::ERR,"CUDA Runtime Error at: NULL : "+to_string_exact(line)+"\n\t"
                                                        +cudaGetErrorString(error)+" NULL");
                    }
                }
            }
            else{
                if(error != cudaSuccess){
                    if(function!=NULL){
                        Log.logandshow(Logger_t::LogLvl::INFO,"CUDA Runtime Error at: "+std::string(file)+" : "+to_string_exact(line)+"\n\t"
                                                        +cudaGetErrorString(error)+" "+std::string(function));
                    }
                    else{
                        Log.logandshow(Logger_t::LogLvl::INFO,"CUDA Runtime Error at: NULL : "+to_string_exact(line)+"\n\t"
                                                        +cudaGetErrorString(error)+" NULL");
                    }
                }
            }
        }

        inline void checkLast(const char* const file, const int line){

            cudaError_t error{cudaGetLastError()};
            if (error != cudaSuccess)
            {
                Log.log(Logger_t::LogLvl::ERR,"CUDA Runtime Error at: "+std::string(file)+" : "+to_string_exact(line)+"\n\t"
                                                +cudaGetErrorString(error));
            }

        }
    #endif
}

#endif /* ERROR_CHECK_CUH_ */