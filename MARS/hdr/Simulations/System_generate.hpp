/*
 * System_generate.hpp
 *
 *  Created on: 5 Sep 2018
 *      Author: ewan
 */

/** \file System_generate.hpp
 * \brief Header file for system generation function. */

#ifndef SYSTEM_GENERATE_HPP_
#define SYSTEM_GENERATE_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Gen_sys(ConfigFile& cfg);

#endif /* SYSTEM_GENERATE_HPP_ */
