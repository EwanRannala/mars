/*
 * Data_longevity.hpp
 *
 *  Created on: 14 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Data_longevity.hpp
 * \brief Header file for data longevity simulation. */

#ifndef DATA_LONGEVITY_HPP_
#define DATA_LONGEVITY_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Data_longevity(const ConfigFile& CFG);

#endif /* DATA_LONGEVITY_HPP_ */
