/*
 * Thermal_Decay.hpp
 *
 *  Created on: 16 Nov 2022
 *      Author: Ewan Rannala
 */

/** \file Thermal_Decay.hpp
 * \brief Header file for thermal decay simulation. */

#ifndef THERMAL_DECAY_HPP_
#define THERMAL_DECAY_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Thermal_Decay(const ConfigFile& CFG);

#endif /* DATA_LONGEVITY_HPP_ */
