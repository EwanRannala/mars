/*
 * HAMR_Write_Data.hpp
 *
 *  Created on: 2 Oct 2018
 *      Author: ewan
 */

/** \file HAMR_Write_Data.hpp
 * \brief  Header file for simulation of HAMR data write process with an instantaneously moving write head. */

#ifndef HAMR_WRITE_DATA_HPP_
#define HAMR_WRITE_DATA_HPP_

#include "Config_File/ConfigFile_import.hpp"

extern int HAMR_write_data(ConfigFile& cfg);

#endif /* HAMR_WRITE_DATA_HPP_ */
