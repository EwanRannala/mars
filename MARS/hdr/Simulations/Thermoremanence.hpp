/*
 * Thermoremanence.hpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

/** \file Thermoremanence.hpp
 * \brief Header file for thermoremanence simulation. */

#ifndef SIGMA_TC_HPP_
#define SIGMA_TC_HPP_

#include "Config_File/ConfigFile_import.hpp"

extern int Thermoremanence(const ConfigFile& cfg);

#endif /* SIGMA_TC_HPP_ */
