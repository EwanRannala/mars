/*
 * Hysteresis.hpp
 *
 *  Created on: 5 April 2023
 *      Author: Ewan Rannala
 */

/** \file Hysteresis.hpp
 * \brief Header file for Hysteresis simulation. */

#ifndef HYSTERESIS_HPP_
#define HYSTERESIS_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

extern int Hysteresis(const ConfigFile& CFG);

#endif /* HYSTERESIS_HPP__ */
