/*
 * FMR.hpp
 *
 *  Created on: 20 May 2020
 *      Author: Ewan Rannala
 */

/** \file FMR.hpp
 * \brief Header file for ferromagnetic resonance simulation. */

#ifndef FMR_HPP_
#define FMR_HPP_

#include "Config_File/ConfigFile_import.hpp"

extern int FMR(const ConfigFile& cfg);

#endif /* FMR_HPP_ */
