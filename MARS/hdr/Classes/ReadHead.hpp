/*
 * ReadHead.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadHead.hpp
 * @brief Header file for ReadHead class */

#ifndef READHEAD_HPP_
#define READHEAD_HPP_

#include <utility>

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "RecLayer.hpp"

/** @brief Class for a read head.
 *
 * This class provides basic functionality of a read head. */
class ReadHead{

public:

    using SensType = enum {Box=1,Gauss=2,Custom=3};

    ReadHead(const RecLayer& RecordingLayer, const ConfigFile&cfg);
    ReadHead(const RecLayer& RecordingLayer);
    ReadHead(const ConfigFile&cfg);

    void Create_S_function(SensType type, double Res);

    double getSizeX() const;
    double getSizeY() const;
    double getPosX() const;
    double getPosY() const;
    double getRes() const;
    SensType getSensitivityFunction() const;

    bool Norm() const;
    double ApplySfunc(double x, double y) const;

    void setPosX(double x);
    void setPosY(double y);

protected:

    double PosX;        //!< Position of read head in x-direction.
    double PosY;        //!< Position of read head in y-direction.

    SensType Sensitvity_type;   //!< Desired sensitivity function
    // For box shaped sensitivity function
    double SizeX;       //!< Width of read head
    double SizeY;       //!< Length of read head
    // For Gaussian shaped sensitivity function
    double sX;          //!< Sigma X
    double sY;          //!< Sigma Y
    bool Normalise;     //!< Flag to determine if the output signal should be normalised w.r.t. the number of pixels - Only used when sensitivity type is Box.

    double Resolution;  //!< Size of each pixel in sensitivity function
    std::vector<std::vector<double>> Sfunc; // !< Sensitivity function

};

#endif /* READHEAD_HPP_ */
