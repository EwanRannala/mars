/*
 * HAMR_laser.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file HAMRLaser.hpp
 * @brief Header file for HAMR_Laser class */

#ifndef HAMRLASER_HPP_
#define HAMRLASER_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
/** @brief Class for a HAMR like laser profile.
 *
 * Base class for a laser like those found in the recording head of HAMR systems. */
class HAMR_Laser {

private:

    using Laser_t = enum {Linear=1, gaussiandouble=2, gaussian=3};
    const char* LaserTypeString(Laser_t e){
        switch (e)
        {
            case Laser_t::Linear: return "linear";
            case Laser_t::gaussiandouble: return "double-gaussian";
            case Laser_t::gaussian: return "gaussian";
            default: return "UNKNOWN";
        }
    }

    double Temp_Max;        //!< Maximum temprature of the laser.
    double Temp_Min;        //!< Temperature when the laser is off.
    double Cooling_time;    //!< A sixth of the time taken to perform a laser pulse.
    double Temp_Cur;     //!< Current temperature of the grain.
    Laser_t Type;           //!< Type of laser.



public:

    HAMR_Laser(const ConfigFile&cfg);

    void ApplyT(double Time, std::vector<Grain_t>& Grains);

    void setCoolingTime(double value);

    double getCurTemp() const;
    double getPeakTemp() const;
    double getCoolingTime() const;
    double getProfileTime() const;

};

#endif /* HAMRLASER_HPP_ */
