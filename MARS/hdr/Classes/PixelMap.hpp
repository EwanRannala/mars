/*
 * PixelMap.hpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file PixelMap.hpp
 * @brief Header file for PixelMap class */

#ifndef M_A_R_S__SRC_PIXELMAP_HPP_
#define M_A_R_S__SRC_PIXELMAP_HPP_

#include <unordered_set>
#include "Pixel.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Io/Logger/Global_logger.hpp"
/// List of available output data.
enum struct DataType {M,    //!< Magnetisation
                      EA,   //!< Easy axis
                      H,    //!< Field
                      T,    //!< Temperature
                      ALL   //!< All data
};
/** @brief Class for the pixelmap.
 *
 * This class contains the functionality to create a pixelmap to discretise the granular
 * system into pixels using the pixel class. */
class PixelMap {

private:
    std::vector<std::vector<Pixel>> Cells;    //!< 2D list of pixels within the pixel map
    double cellsize;                          //!< Size of each pixel
public:
    PixelMap(double Xmin, double Ymin, double Xmax, double Ymax, double cellsize);
    PixelMap(double Xmin, double Ymin, double Xmax, double Ymax, double cellsize, const ConfigFile&cfg, bool isImport=false);

    void Discretise(const Voronoi_t &VORO, const std::vector<Grain_t>& Grains);
    void MapVal(const std::vector<Grain_t>& Grains, DataType type);
    void Print(std::string filename);
    void PrintwUpdate(std::string filename, const std::vector<Grain_t>& Grains, DataType type);
    void StoreinGrain(std::vector<Grain_t>& Grains);
    void ClearH();
    void ClearT();


    void InRect(double xMin, double yMin, double xMax, double yMax,
                std::vector<unsigned int>&PixelList,std::unordered_set<unsigned int>&ImpactedGrains) const;
    Pixel Access(unsigned int X, unsigned int Y) const;
    Pixel Access(unsigned int ID) const;
    unsigned int getRows() const;
    unsigned int getCols() const;
    double getCellsize() const;
};

#endif /* M_A_R_S__SRC_PIXELMAP_HPP_ */
