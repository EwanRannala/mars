/*
 * Hdc.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Hdc.hpp
 * @brief Header file for Hdc class */

#ifndef HDC_HPP_
#define HDC_HPP_

#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"

/// Possible states of the field.
enum struct HdcConditions{min,           //!< Field is at minimum strength.
                          increasing,    //!< Field is ramping up to maximum strength.
                          max,           //!< Field is at maximum strength.
                          decreasing,    //!< Field is ramping down to minimum strength.
                          overshootDec   //!< Field is ramping down from overshoot to maximum strength.
};
/** @brief Base class for a constant strength field.
 *
 * This class provides the functionality for an constant strength applied field.
 * The field can be set with two strengths a minimum and a maximum and switches between
 * the two via a linear increase based on the set ramp time depending on the value of the
 * On parameter.
 */
class Hdc {

private:

    // Instantaneous values
    Vec3 field;             //!< Field vector
    HdcConditions state;    //!< Current state of the field
    bool On;                //!< Sets if the field should be at maximum or minimum.
    double CurTime;         //!< Internal timer till switch
    double mag;             //!< Field magnitude

    // Descriptive values
    Vec3 dirn;           //!< Direction vector
    double min;          //!< Minimum strength (Oe)
    double max;          //!< Maximum strength (Oe)
    double overshootAmp; //!< Magnitude of overshoot when turning the field on (Oe)
    double maxTotal;     //!< Maximum strength plus overshoot (Oe)
    double rise_time;    //!< Time required to switch between min/maxTotal (s)
    double overshootDur; //!< Total time of the overshoot (s)
    double RTrate;       //!< Rate of change of field during change between min/maxTotal (K/s)
    double OSrate;       //!< Rate of change of field during change between maxTotal/max (K/s)
    double SwitchTimer;  //!< Length of time field will be on (s)

protected:

 // TODO add off Timer?
    void setTimer(double Timer);
    void Switch();
    void TimedSwitch();

public:

    Hdc(const ConfigFile&cfg);

    // Setters
    void updateField(double dt);
    void turnOn();
    void turnOff();
    void turnOnfor(double Timer);
    void turnOfffor(double Timer);
    void updateTime(double dt);
    void updateState();
    void updateMag(double dt);
    void setDirn(Vec3 Direction);
    void setMax(double Maximum);
    void setMin(double Minimum);
    void setOvershoot(double Overshoot);

    // Getters
    Vec3 getField() const;
    double getMag() const;
    HdcConditions getState() const;
    Vec3 getDirn() const;
    double getMin() const;
    double getMax() const;
    double getOvershoot() const;
    double getRampTime() const;
    double getRate() const;
    bool isOn() const;

};

#endif /* HDC_HPP_ */
