/*
 * Writer.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Writer.hpp
 * @brief Header file for Writer class */

#ifndef WRITER_HPP_
#define WRITER_HPP_

#define _USE_MATH_DEFINES
#include <cmath>
#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Hdc.hpp"
#include "PixelMap.hpp"
/** @brief Sub-class for a write head.
 *
 *  Sub-Class of the Hdc class. This class provides the functionality of write head,
 * specifically spatial information on the write head's field profile and also
 * speed of movement.
 */
class Writer : public Hdc {

private:
    double H_width_X;     //!< Width of applied field in x-axis [nm]
    double H_width_Y;     //!< Width of applied field in y-axis [nm]
    double Pos_X;         //!< x-coordinate for head centre [nm]
    double Pos_Y;         //!< y-coordinate for head centre [nm]
    double speed;         //!< Speed of head in x-direction [nm/s]
    double Skew;          //!< Rotation angle (skew) of write head [radian]
    double CosSkew;       //!< Cos(Skew)
    double SinSkew;       //!< Sin(Skew)

public:

    Writer(const ConfigFile&cfg);

    void setX(double x);
    void setY(double y);
    void moveX(double dx);
    void moveY(double dy);

    void ApplyHspatial(const Voronoi_t&VORO, unsigned int Num_Layers,
                       double dt, const std::vector<unsigned int>&Included_grains_in_layer,
                       std::vector<Grain_t>& Grains);
    void ApplyHspatial(const Voronoi_t&VORO, unsigned int Num_Layers,
                       double dt, std::vector<Grain_t>& Grains);
    
    void ApplyHspatialPM(const Voronoi_t&VORO, unsigned int Num_Layers, double dt,
                         std::vector<Grain_t>& Grains, PixelMap& PixMap);

    double getX() const;
    double getY() const;
    double getHwidthX() const;
    double getHwidthY() const;
    double getSpeed() const;
    double getSkew() const;
    double getCosSkew() const;
    double getSinSkew() const;
    double getSkewDEG() const;


};

#endif /* WRITER_HPP_ */
