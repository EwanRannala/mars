/*
 * RecHead.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file RecHead.hpp
 * @brief Header file for RecHead class */

#define _USE_MATH_DEFINES
#include <cmath>
#include "Structures.hpp"
#include "Config_File/ConfigFile_import.hpp"
#include "Writer.hpp"
#include "HAMRLaser.hpp"
#include "PixelMap.hpp"

#ifndef RECHEAD_HPP_
#define RECHEAD_HPP_
/** @brief Sub-class for a write head with a near field transducer (NFT).
 *
 * Sub-class of Writer and HAMR_laser. This class provides the functionality of a write head
 * for HAMR systems. It adds spatial information to the HAMR_laser.
 */
class RecHead : public Writer, public HAMR_Laser {

private:

    bool Pulsing;         //!< Sets if laser should turn pulse during writing
    double NPS;           //!< Distance between field profile and laser profile in x-direction
    double FWHM_X;        //!< Full width at half maximum of the laser profile in the x-dimension
    double FWHM_Y;        //!< Full width at half maximum of the laser profile in the y-dimension
    double two_SigmaX_SQ; //!< Denominator for X term of Gaussian profile
    double two_SigmaY_SQ; //!< Denominator for Y term of Gaussian profile

public:


    RecHead(const ConfigFile&cfg);

    void ApplyTspatialCont(unsigned int Num_Layers, const Voronoi_t&VORO, double Environ_Temp,
                           double Time, double&Temperature, const std::vector<unsigned int>&Included_grains_in_layer,
                           std::vector<Grain_t>& Grains);

    void ApplyTspatialCont(unsigned int Num_Layers, const Voronoi_t&VORO, double Environ_Temp,
                           double Time, double&Temperature,std::vector<Grain_t>& Grains);

    void ApplyTspatialContPM(unsigned int Num_Layers, const Voronoi_t&VORO, double Environ_Temp,
                             double Time, double&Temperature, std::vector<Grain_t>& Grains, PixelMap&PixMap);

    void ApplyTspatial(const Voronoi_t&VORO, unsigned int Num_Layers,
                       double Environ_Temp, double Time,
                       double&Spatial_Temp, std::vector<Grain_t>& Grains);

    void ApplyTspatialPM(const Voronoi_t&VORO, unsigned int Num_Layers, double Environ_Temp,
                         double Time, double&Spatial_Temp, std::vector<Grain_t>& Grains, PixelMap&PixMap);


    bool getPulsing()const;
    double getNPS()const;
    double getFWHMx()const;
    double getFWHMy()const;

};
#endif /* RECHEAD_HPP_ */
