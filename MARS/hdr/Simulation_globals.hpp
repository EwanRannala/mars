/* Simulation_globals.hpp
 *  Created on: 05/06/2022
 *      Author: Samuel Ewan Rannala
 */

/** \file Simulation_globals.hpp
 * \brief Header file global simulations variables. */

#ifndef SIMULATION_GLOBALS_HPP_
#define SIMULATION_GLOBALS_HPP_

#include "Structures.hpp"

namespace Sim{

    extern double Time;
    extern unsigned int NumLayers;
    extern unsigned int GrainsperLayer;
    extern std::vector<Grain_t> Grains;

}


#endif /* SIMULATION_GLOBALS_HPP_ */