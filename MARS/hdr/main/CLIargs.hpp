/* CLIargs.hpp
 *  Created on: 22 Feb 2022
 *      Author: Samuel Ewan Rannala
 */

#ifndef CLIARGS_HPP_
#define CLIARGS_HPP_

void CLIargs(int argc, char* argv[], std::string &CFGfile);

#endif /* CLIARGS_HPP_ */