unset key

set view equal xy

f0 = "../Output/pos_file.dat"
f1 = "../Output/gnuplot_vert_file.dat"
f2 = "../Output/geo_file.dat"

set xrange [Xmin:Xmax]
set yrange [Ymin:Ymax]

p f0 u ($1):($2):0 w labels, f1 u ($1):($2) w l, f2 u ($1):($2) w p
#p f1 u ($1):($2) w l

pause -1
