## 	This script requires input for:
###		Start  -- file number to start from.
###		Files  -- file number to end at.		
###		Layer  -- The layer you want to output
### 	Grains -- Number of grains PER LAYER
## To input arguments to gnuplot you need to use "-e"
## (e.g. gnuplot -e Start=0 -e Files=10 ... )

# Graph configurations
Resolution_x = 3840
Resolution_y = 2160
Measurement_time = 1e-11
Field_Mag = 10000

set term pngcairo size Resolution_x,Resolution_y enhanced font "Times-Roman,26"
set key font "Times-Roman,16"
set key center tm

do for [i=Start:Files]{

	Time=(i*Measurement_time)
	print i, " Out of ", Files
	
	set output sprintf('Plot_Layer_%03.0f_part_%010.0f.png', Layer, i)
	set multiplot layout 2,1 title sprintf('Time %4.2e s',Time)

	set cbrange [300:750]
	set palette rgb 34,35,36
	set title "Temperature"
	p "../Time_".Layer."_".i.".dat" u 1:2:3 w filledcurve palette t '',\
	 '' u 1:(($4*$4)>=(Field_Mag*Field_Mag)?$2:0/0) w l lt rgb "#F0F8FF" t ''

	set cbrange [-1:1]
	set palette defined (-1 "#00539B", 0.0 "#A5ACAF", 1 "#FFB612")
	set title "M_z"
	p "../Time_".Layer."_".i.".dat" u 1:2:7 w filledcurve palette t '',\
	  '' u 1:2 w l lt rgb "#000000" t '',\
	  '' u 1:(($4*$4)>=(Field_Mag*Field_Mag)?$2:0/0) w l lt rgb "#F0F8FF" t ''

	unset multiplot
}

### This command can be used to generate a video from the output images 
### (Change input filename to match desired layer data)
#ENCODER = system('which ffmpeg');
#if (strlen(ENCODER)==0) print '=== ffmpeg not found ==='; exit
#CMD = "ffmpeg -y -framerate 30 -pattern_type glob -i 'Plot_part_*.png' -s:v 3840x2160 -c:v libx264 -preset slow -crf 17 -pix_fmt yuv444p -b:v 2000k -v verbose Output.mkv"
#system(CMD)
